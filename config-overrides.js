const { override, disableEsLint, addLessLoader, fixBabelImports } = require('customize-cra');

module.exports = override(
  disableEsLint(),
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true, // change importing css to less
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { '@primary-color': '#EA725B' },
  }),
  (config) => {
    let loaders = config.module.rules[1].oneOf;
    loaders.splice(loaders.length - 1, 0, {
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
    });
    return config;
  }
);
