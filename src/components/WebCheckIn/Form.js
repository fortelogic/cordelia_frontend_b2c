import { StylesProvider } from '@material-ui/core';
import React,{useState,useRef} from 'react';
import styled from 'styled-components';
import Guests from './Guests';
import styles from "./webcheckin.module.css"
import cx from "classnames";
import { Tabs} from 'antd';
const { TabPane } = Tabs;


const pushToArray =(array,guest) =>{
    let obj = array.find(o => o.id === guest.id);
    if(obj===undefined){
        array.push({
            "id":guest.id,
            "country": guest.country,
            "state": guest.state,
            "meal":guest.meal,
            "comments": guest.comments,
            "web_checkin_doc_type": guest.web_checkin_doc_type,
            "web_checkin_doc_number": guest.web_checkin_doc_number,
            "web_checkin_doc_place": guest.web_checkin_doc_place,
            "web_checkin_doc_date": guest.web_checkin_doc_date,
            // "rtpcr_doc": null
        })
    }
}
const pushToHealthArray =(array,guest)=>{
    let obj = array.find(o => o.id === guest.id);
    if(obj===undefined){
        array.push({
            "id":guest.id,
            "declaration":guest.declaration_accepted?guest.declaration_accepted:false,
            "checkbox":false
        })
    }
}
const renderFormDesktop = (guest,bookingId,guestArray,isInternational,healthArray,setGuestState,setHealth) =>{
    // pushToArray(guestArray,guest);
    // pushToHealthArray(healthArray,guest);
    // console.log( guestArray,"nik-log")
    return (<div className="col-3">
                <h4 className={styles.cardHeading}>Guest - {guest.name}</h4>
                <Guests guestData={guest} bookingId={bookingId} guestArray={guestArray} healthArray={healthArray} key={guest.id} isInternational={isInternational} setGuestState={setGuestState} setHealth={setHealth}/>
            </div>);
}

const renderFormMobile = (guest,bookingId,guestArray,isInternational,healthArray,setGuestState,setHealth) =>{
    // pushToArray(guestArray,guest);
    // pushToHealthArray(healthArray,guest);
    return (<TabPane tab={`Guest - ${guest.name}`} key={guest.id}>
                <div>
                    <Guests guestData={guest} bookingId={bookingId} guestArray={guestArray} healthArray={healthArray} key={guest.id} isInternational={isInternational} setGuestState={setGuestState}  setHealth={setHealth}/>
                </div>
            </TabPane>);
}


const Form = (props)=> {
    const [formData, setFormData] = useState()
    const form = useRef(null)
    return (
        <Container>
        {/* For cabin separation
            {props.roomData.map(room=>{
            return (
                <>
                    <h4 className={styles.cardHeading}>Cabin- {room.category}</h4>
                    <div className="row mb-5 d-none d-md-flex">
                        {room.guests.map(guest=>renderFormDesktop(guest))}
                    </div>
                </>
                )
        })} */}
        <div className="row mb-5 d-none d-md-flex">
        {props.roomData.map(room=>{
            return (
                    room.guests.map(guest=>renderFormDesktop(guest,props.bookingId,props.guestArray,props.isInternational,props.healthArray,props.setGuestState,props.setHealth))
                )
        })}
        </div>
        <Tabs className={cx("tabHeadingPurple","d-md-none")} defaultActiveKey="1" >
                    {props.roomData.map((room)=>{
                        return (
                                room.guests.map(guest=>renderFormMobile(guest,props.bookingId,props.guestArray,props.isInternational,props.healthArray,props.setGuestState,props.setHealth))
                            )
                    })}
                {/* <TabPane tab="Guest 1" key="1">
                    <div>
                        <Guests/>
                    </div>
                </TabPane>
                <TabPane tab="Guest 2" key="2">
                    <div>
                        <Guests/>
                    </div>
                </TabPane>
                <TabPane tab="Guest 3" key="3">
                    <div>
                        <Guests/>
                    </div>
                </TabPane>
                <TabPane tab="Guest 4" key="4">
                    <div>
                        <Guests/>
                    </div>
                </TabPane> */}
            </Tabs>
        </Container>
    //  <Container>
    //         <CardBody>
    //             <section>
    //                 <h4>Guest1 - Andy</h4>
    //                 <Guests/>
    //             </section>
    //             <section>
    //                 <h4>Guest1 - Andy</h4>
    //                 <Guests/>
    //             </section>
    //             <section>
    //                 <h4>Guest1 - Andy</h4>
    //                 <Guests/>
    //             </section>
    //             <section>
    //                 <h4>Guest1 - Andy</h4>
    //                 <Guests/>
    //             </section>

    //         </CardBody>
    //     </Container>
    )
}

export default Form;

const Container = styled.div`
@media screen and (max-width:450px){
    padding: 0px 20px;
}
`;


const CardBody = styled.div`
display: flex;
flex-direction:row;
padding:5px;
justify-content:center;
width:100%;
height:auto;
@media screen and (max-width:1200px){
    width:40%%;

}
@media screen and (max-width:600px){
    width:20%%;
    margin-left:5px;
    flex-direction:column;
}
>section{
>h4{
    color: #93329E;
    font-size:800; 
    font-family: 'Poppins', sans-serif;
    margin-bottom:10px;
    margin-left:15px;
}
}


`;
