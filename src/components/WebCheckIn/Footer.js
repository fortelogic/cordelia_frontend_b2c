import React from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import {
    Button
  } from "antd";

function Footer(openModal) {
    let history = useHistory();
    return (
        <FooterBtn>
            <Button 
            type="primary" 
            className="bg-purple-button" 
            // onClick={()=>{ history.push("/boarding-pass")}}
            onClick={openModal}
            >Proceed</Button>
        </FooterBtn>
    )
}

export default Footer;

const FooterBtn = styled.div`
    display: flex;
    justify-content:center;
    margin-bottom:3rem;
`;
