import React, { useState } from 'react';


function GuestDetails(props) {
    const [state, setState] = useState([]);
    var checkBoxes = []
    const checkIfChecked = (id) => {
        let obj = state.find(o => o.id === id);
        if (obj !== undefined) {
            return obj.active
        } else {
            return false
        }
    }
    const toggleCheck = (id, value) => {
        setState(checkBoxes)
        if (value === false) {
            checkBoxes.map(obj => obj.active = false)
            let index = checkBoxes.findIndex(obj => obj.id === id);
            checkBoxes[index].active = true
        } else {
            checkBoxes.map(obj => obj.active = false)
        }

    }
    return (
        <section>
            <div className="row">
                <div className="col">
                    {props.room.guests.map((guest, index) => {
                        return (
                            <div key={index}>
                                <span>Guest {index + 1} - {guest.type}</span>
                                <h2> {guest.name}</h2>
                            </div>
                        )
                    })}
                </div>
                <div className="col text-right">
                    {props.room.guests.map((guest, index) => {
                        checkBoxes.push({ id: `checkbox${index}`, active: index === 0 ? true : false })
                        return (
                            <div key={index}>
                                <label for="Guest">Primary Guest</label><br />
                                <input type="checkbox" name={`checkbox${index}`} id={`checkbox${index}`} checked={checkIfChecked(`checkbox${index}`)} onClick={() => toggleCheck(`checkbox${index}`, checkIfChecked(`checkbox${index}`))} />
                            </div>
                        )
                    })}
                </div>
            </div>
        </section>
    )
}

export default GuestDetails;

