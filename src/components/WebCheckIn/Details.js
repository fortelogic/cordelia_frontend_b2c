import React, { useState } from 'react'
import styled from 'styled-components';
import GuestDetails from './GuestDetails';
import styles from "./webcheckin.module.css"
import { Divider, Collapse } from 'antd';
import moment from 'moment'
import TwinWhite from '../../assets/icons/twin-beds-white.png'
import TwinPurple from '../../assets/icons/twin-beds-purple.png'
import DoubleWhite from '../../assets/icons/bed-white.png'
import DoublePurple from '../../assets/icons/bed-purple.png'

const { Panel } = Collapse;

const RenderCabinDeatail = ({ room, index, handleBedPreference, roomBedSelectionSelected }) => {

    console.log(roomBedSelectionSelected)

    // const [roomBedSlectionInner, setRoomBedSelectionInner] = useState(roomBedSelection[index])
    const [test, setTest] = useState(true)
    // console.log(roomBedSelection)

    const handleInnerBedPreference = (x, v) => {
        // setTest(false)
        // innerSelectionArray[x] = v
        // setRoomBedSelectionInner(innerSelectionArray)
        // console.log(roomBedSelection)
        handleBedPreference(x, v)
        // setTest(true)
    }

    return (
        <div className="col-6 mb-4">
            <h6 className="mb-2">Cabin {index + 1}</h6>
            <p className="mb-0">{room.category}</p>
            <p className="mb-0">Deck No: {room.deck_no}</p>
            <p className="mb-0">Cabin No: {room.number}</p>
            <p className="mb-0">Guest: {room.guests.length}</p>
            <h6 className={styles.preffereText}>Prefered Bed Orientation<span>*</span></h6>
            {test && <div className="row">
                <div className="col">
                    <button className={roomBedSelectionSelected.is_selected == 0 ? styles.bedbutton : styles.unselectedButton} onClick={() => handleInnerBedPreference(index, 0)}>{roomBedSelectionSelected.is_selected == 0 ? <img src={TwinWhite} className={styles.imageStyle} /> : <img src={TwinPurple} className={styles.imageStyle} />}</button>

                </div>
                <div className="col">
                    <button className={roomBedSelectionSelected.is_selected == 1 ? styles.bedbutton : styles.unselectedButton} onClick={() => handleInnerBedPreference(index, 1)}>{roomBedSelectionSelected.is_selected == 1 ? <img src={DoubleWhite} className={styles.imageStyle} /> : <img src={DoublePurple} className={styles.imageStyle} />}</button>
                </div>

            </div>}
        </div>
    )
}
const renderPricingDetailMobile = (data, count) => {
    let netAmount = parseFloat(data.cabin_fare) - parseFloat(data.discount + data.gratuity) + parseFloat(data.gratuity) + parseFloat(data.port_charges)
    return (
        <>
            <p className="mb-0">Cabin {count + 1}</p>
            <div className="row detailsCard">
                <div className="col">
                    <p className="details" >Cabin Fare</p>
                    <p className="details" >Discount ( - )</p>

                    <p className="details" >Gratuity</p>
                    <p className="details" >Port Charges</p>
                    <p className="detailsNet">Net Payable Amount</p>
                </div>
                <div className="col text-right">
                    <p className="details"> ₹ {Math.round(data.cabin_fare)}</p>
                    <p className="details"> ₹ {Math.round(data.discount)}</p>
                    <p className="details"> ₹ {Math.round(data.gratuity)}</p>
                    <p className="details"> ₹ {Math.round(data.port_charges)}</p>
                    <p className="detailsNet"> ₹ {Math.round(netAmount)}</p>
                </div>
            </div>
            <p style={{ "borderTop": "1px solid #D8E3E7", "padding": "0px", "marginBottom": "10px" }}></p>
        </>
    )
}
const renderPricingDetail = (data, count) => {
    let netAmount = parseFloat(data.cabin_fare) - parseFloat(data.discount + data.gratuity) + parseFloat(data.gratuity) + parseFloat(data.port_charges)
    return (
        <Panel header={<p className="mb-0 title">Cabin {count + 1}</p>} key={count + 1}>
            <div className="row detailsCard">
                <div className="col">
                    <p className="details" >Cabin Fare</p>
                    <p className="details" >Discount ( - )</p>

                    <p className="details" >Gratuity</p>
                    <p className="details" >Port Charges</p>
                    <p className="detailsNet">Net Payable Amount</p>
                </div>
                <div className="col text-right">
                    <p className="details"> ₹ {Math.round(data.cabin_fare)}</p>
                    <p className="details"> ₹ {Math.round(data.discount)}</p>
                    <p className="details"> ₹ {Math.round(data.gratuity)}</p>
                    <p className="details"> ₹ {Math.round(data.port_charges)}</p>
                    <p className="detailsNet"> ₹ {Math.round(netAmount)}</p>
                </div>
            </div>
            <p style={{ "borderTop": "1px solid #D8E3E7", "padding": "0px", "marginBottom": "10px" }}></p>
        </Panel>
    )
}
// const renderGuestDeatail=(room)=>{
//     return(
//             <GuestDetails room={room}/>
//     )
// }
export default function Details({
    roomData,
    itineraryData,
    pricingData,
    total,
    tax,
    booking,
    handleBedPreference,
    roomBedSelection
}) {
    console.log(roomBedSelection)
    return (
        <Container>
            <CardBody>
                <h4>Booking Details</h4>
                <CardContent>
                    <h6 className="mb-2" onClick={() => handleBedPreference()}>Ship Name</h6>
                    <p>Empress</p>
                    <div className="mb-5" />
                    <div className="row mb-4">
                        <div className="col">
                            <h6 className="mb-2">Itinerary</h6>
                            <p className="mb-0">{itineraryData.name}</p>
                        </div>
                    </div>
                    <div className="row mb-4">
                        <div className="col">
                            <h6 className="mb-2">Departure</h6>
                            <p className="mb-0">{itineraryData.ports[0].city}</p>
                            <p className="mb-0">{moment(itineraryData.start_time).format("ddd, D-MMM-YYYY")}</p>
                            <p className="mb-0">{moment(itineraryData.start_time).format("hh:mm A")}</p>
                        </div>
                        <div className="col">
                            <h6 className="mb-2">Arrival</h6>
                            <p className="mb-0">{itineraryData.ports[itineraryData.ports.length - 1].city}</p>
                            <p className="mb-0">{moment(itineraryData.end_time).format("ddd, D-MMM-YYYY")}</p>
                            <p className="mb-0">{moment(itineraryData.end_time).format("hh:mm A")}</p>
                        </div>
                    </div>
                    <div className="row">
                        {roomData.map((room, index) => {
                            return (
                                <>
                                    <RenderCabinDeatail room={room} index={index} handleBedPreference={handleBedPreference} roomBedSelectionSelected={roomBedSelection[index]} />
                                </>
                            )
                        })}
                    </div>
                </CardContent>
            </CardBody>
            <CardBody>
                <h4>Guest Details</h4>
                <GuestCardContent>
                    {roomData.map((room, index) => {
                        return (
                            <>
                                <p style={{ "borderBottom": "1px solid #D8E3E7" }} key={index}>Cabin {index + 1}</p>
                                <GuestDetails room={room} />
                            </>
                        )
                    })}
                </GuestCardContent>
            </CardBody>
            {/* <CardBody>
                <h4>Pricing Details</h4>
                <CardPricingDetails>
                    {pricingData&&pricingData.map((data,index)=>{
                                return (
                                        <>
                                            <Collapse defaultActiveKey={['1']} expandIconPosition={'right'} ghost>
                                                {renderPricingDetail(data,index)}
                                            </Collapse>
                                        </>
                                    )
                            })}
                    <div className="row detailsCard pb-0">
                        <div className="col-7 pr-1">
                            <p className="details">Tax</p>
                        </div>
                        <div className="col-5 pl-1 text-right">
                            <p className="details"> ₹ {Math.round(tax)}</p>
                        </div>
                    </div>
                    {(booking.special_request_total!==0)&&<div className="row detailsCard pb-0">
                        <div className="col-7 pr-1">
                            <p className="details">Special Request</p>
                        </div>
                        <div className="col-5 pl-1 text-right">
                            <p className="details"> ₹ {Math.round(parseFloat(booking.special_request_total))}</p>
                        </div>
                    </div>}
                    {(booking.promo_amount&&booking.promo_amount!=="0")&&<div className="row detailsCard pb-0">
                        <div className="col-7 pr-1">
                            <p className="details">Promo Amount ( - )</p>
                        </div>
                        <div className="col-5 pl-1 text-right">
                            <p className="details"> ₹ {Math.round(parseFloat(booking.promo_amount))}</p>
                        </div>
                    </div>}
                    <div className="row detailsCard">
                        <div className="col-7 pr-1">
                            <p className="detailsTotal">Total Paid Amount</p>
                        </div>
                        <div className="col-5 pl-1 text-right">
                            <p className="detailsTotal"> ₹ {(booking.commission_deducted?Math.round(total)+booking.commission_deducted:Math.round(total))}</p>
                        </div>
                    </div>
                    
                </CardPricingDetails>
            </CardBody> */}
            <div className="d-md-none">
                <Collapse defaultActiveKey={['1']} expandIconPosition={'right'} ghost>
                    <Divider style={{ borderColor: '#616161', margin: '5px 0px' }} />
                    <Panel header={<AccordionHeading><h4 className="mb-0">Booking Details</h4></AccordionHeading>} key="1">
                        <CardContent>
                            <h6 className="mb-2">Ship Name</h6>
                            <p>Empress</p>
                            <div className="mb-5" />
                            <div className="row mb-4">
                                <div className="col">
                                    <h6 className="mb-2">Itinerary</h6>
                                    <p className="mb-0">{itineraryData.name}</p>
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col">
                                    <h6 className="mb-2">Departure</h6>
                                    <p className="mb-0">{itineraryData.ports[0].city}</p>
                                    <p className="mb-0">{moment(itineraryData.start_time).format("ddd, D-MMM-YYYY")}</p>
                                    <p className="mb-0">{moment(itineraryData.start_time).format("hh:mm A")}</p>
                                </div>
                                <div className="col">
                                    <h6 className="mb-2">Arrival</h6>
                                    <p className="mb-0">{itineraryData.ports[itineraryData.ports.length - 1].city}</p>
                                    <p className="mb-0">{moment(itineraryData.end_time).format("ddd, D-MMM-YYYY")}</p>
                                    <p className="mb-0">{moment(itineraryData.end_time).format("hh:mm A")}</p>
                                </div>
                            </div>
                            <div className="row">
                                {roomData.map((room, index) => {
                                    return (
                                        <>
                                            <RenderCabinDeatail room={room} index={index} handleBedPreference={handleBedPreference} roomBedSelectionSelected={roomBedSelection[index]} />
                                        </>
                                    )
                                })}
                            </div>
                        </CardContent>
                    </Panel>
                    <Divider style={{ borderColor: '#616161', backgroundColor: '#616161', margin: '5px 0px' }} />
                    <Panel header={<AccordionHeading><h4 className="mb-0">Guest Details</h4></AccordionHeading>} key="2">
                        <GuestCardContent>
                            {roomData.map((room, index) => {
                                return (
                                    <>
                                        <p style={{ "borderBottom": "1px solid #D8E3E7" }} key={index}>Cabin {index + 1}</p>
                                        <GuestDetails room={room} />
                                    </>
                                )
                            })}
                        </GuestCardContent>
                    </Panel>
                    {/* <Divider style={{ borderColor: '#616161', backgroundColor: '#616161', margin: '5px 0px' }} />
                  <Panel header={<AccordionHeading><h4 className="mb-0">Pricing Details</h4></AccordionHeading>} key="3">
                  <CardPricingDetails>
                        {pricingData&&pricingData.map((data,index)=>{
                                        return (
                                                <>
                                                    {renderPricingDetailMobile(data,index)}
                                                </>
                                            )
                                    })}
                    <div className="row detailsCard">
                        <div className="col-7 pr-1">
                            <p className="details">Tax</p>
                        </div>
                        <div className="col-5 pl-1 text-right">
                            <p className="details"> ₹ {Math.round(tax)}</p>
                        </div>
                    </div>
                    {(booking.special_request_total!==0)&&<div className="row detailsCard pb-0">
                        <div className="col-7 pr-1">
                            <p className="details">Special Request</p>
                        </div>
                        <div className="col-5 pl-1 text-right">
                            <p className="details"> ₹ {Math.round(parseFloat(booking.special_request_total))}</p>
                        </div>
                    </div>}
                    {(booking.promo_amount&&booking.promo_amount!=="0")&&<div className="row detailsCard pb-0">
                        <div className="col-7 pr-1">
                            <p className="details">Promo Amount ( - )</p>
                        </div>
                        <div className="col-5 pl-1 text-right">
                            <p className="details"> ₹ {Math.round(parseFloat(booking.promo_amount))}</p>
                        </div>
                    </div>}
                    <div className="row detailsCard">
                        <div className="col-7 pr-1">
                            <p className="detailsTotal">Total Paid Amount</p>
                        </div>
                        <div className="col-5 pl-1 text-right">
                            <p className="detailsTotal"> ₹ {(booking.commission_deducted?Math.round(total)+booking.commission_deducted:Math.round(total))}</p>
                        </div>
                    </div>
                    
                </CardPricingDetails>
                  </Panel> */}
                    <Divider style={{ borderColor: '#616161', backgroundColor: '#616161', margin: '0px' }} />
                </Collapse>
            </div>

        </Container>
    )
}

const Container = styled.div`
display: flex;
flex-flow:wrap;
height: auto;
justify-content:center;
flex-direction: row;
padding: 25px 0px;
margin: 25px 0px;
@media screen and (max-width:600px){
    justify-content:left;
}
@media screen and (max-width:450px){
    padding: 0px 20px;
    display:block;
}
`;

const CardBody = styled.div`

flex:0.25;
padding:20px;
width:100%;
height:auto;
@media screen and (max-width:1200px){
    width:40%%;
}
@media screen and (max-width:600px){
    width:20%%;
    margin-left:5px;
    flex-direction:column;
}
@media screen and (max-width:600px){
    display:none;
}
>h4{
    font-family:"Roboto-Medium";
    color: #500E4B;
    font-size:500;
    font-size:18px;
    margin-bottom:10px;
}

`;

const CardContent = styled.div`
width:350px;
min-height:500px;
background-color:white;
padding:30px;
box-sizing:border-box;
border-radius:10px;
border: 0.3px solid #500e4b78;
box-shadow: 0px 6px 13px -5px rgba(147, 50, 158, 0.5);
h6{
    font-size:14px;
    color:gray;
}
>p{
    font-family:"Roboto-Medium";
    font-weight:500;
    color: #500E4B;
    font-size:18px;
}
.col>p{
    font-family:"Roboto-Medium";
    font-weight:500;
    font-size:16px;
    color: #500E4B;
}
>section{
    margin-top:3px;
    display:flex;
    flex-direction: row;
    justify-content:space-around;
    text-align:left;
    >ul{
         list-style-type: none;
         padding-left:0;
         >li{
        font-size: 14px;
         color: #500E4B;
         font-weight:700;
         margin:5px;
    }
    }   
}
@media screen and (max-width:600px){
    width:300px;
    margin-left:5px;
    flex-direction:column;
}
@media screen and (max-width:450px){
    border:none;
    padding:0px;
    box-shadow:none;
    margin-left:0px;
    height:auto;
    min-height: auto;
    width: 100%;
}
`;

const GuestCardContent = styled.div`
width:350px;
min-height:500px;
background-color:white;
box-sizing:border-box;
border-radius:10px;
border: 0.3px solid #500e4b78;
box-shadow: 0px 6px 13px -5px rgba(147, 50, 158, 0.5);
h6{
    font-size:14px;
    color:gray;
}
>p{
    padding:10px 30px;
    font-family:"Roboto-Medium";
    font-weight:500;
    color: #500E4B;
    font-size:16px;
}
.col>p{
    font-family:"Roboto-Medium";
    font-weight:500;
    font-size:16px;
    color: #500E4B;
}
>section{
    margin-top:3px;
    padding:8px 30px;
    text-align:left;
    >ul{
         list-style-type: none;
         padding-left:0;
         >li{
        font-size: 14px;
         color: #500E4B;
         font-weight:700;
         margin:5px;
    }
    }   
}
span{
    font-size: 14px;
    font-weight:400;
    color:#500E4B;
}
h2{
    color: #838383;
    font-size: 14px;

}
label{
    font-size: 14px;
    font-weight:400;
    color:#500E4B;
    margin-bottom:0px;
}
@media screen and (max-width:600px){
    width:300px;
    margin-left:5px;
    flex-direction:column;
}
@media screen and (max-width:450px){
    border:none;
    padding:0px;
    box-shadow:none;
    margin-left:0px;
    height:auto;
    min-height: auto;
    width: 100%;
    >p{
        padding:10px 0px;
    }
    >section{
        padding:8px 0px;
    }
}

`;
const CardPricingDetails = styled.div`
width:350px;
min-height:500px;
background-color:white;
box-sizing:border-box;
border-radius:10px;
border: 0.3px solid #500e4b78;
box-shadow: 0px 6px 13px -5px rgba(147, 50, 158, 0.5);
p.title{
    padding:10px 30px;
    font-family:"Roboto-Medium";
    font-weight:500;
    color: #500E4B;
    font-size:16px;
}
.ant-collapse-header{
    padding:0px !important;
}
.ant-collapse-content-box{
    padding:0px !important;
}
>section{
>span{
    font-size:14px;
    color:gray;
    margin-left:10px;
    font-weight:400;
}
}
.detailsCard{
    padding:0px 30px 10px;
}
.detailsCard .details{
    color: #535353;
    font-size: 14px;
    margin-bottom: 10px;
}
.detailsCard .detailsNet{
    font-family:"Roboto-Medium";
    font-weight:500;
    color: #500E4B;
    font-size: 14px;
    margin-bottom: 10px;
}
.detailsCard .detailsTotal{
    font-family:"Roboto-Medium";
    font-weight:500;
    color: #500E4B;
    font-size: 18px;
    padding-top:10px;
    margin-bottom: 10px;
}
@media screen and (max-width:600px){
    width:300px;
    margin-left:5px;
    flex-direction:column;
}
@media screen and (max-width:450px){
    border:none;
    padding:0px;
    box-shadow:none;
    margin-left:0px;
    height:auto;
    min-height: auto;
    width: 100%;
    >p{
        padding:10px 0px;
    }
    .detailsCard{
        padding:0px 0px 10px;
    }
}
`;

const AccordionHeading = styled.div`
>h4{
    font-family:"Roboto-Medium";
    color: #313131;
    font-size:500;
    font-size:16px;
}
`;

