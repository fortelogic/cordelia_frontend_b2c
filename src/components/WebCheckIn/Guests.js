import React,{useEffect, useState, shouldComponentUpdate} from 'react';
import styled from 'styled-components';
import CountryStates from "./country-states.json";
import { Select, DatePicker, Button, Upload} from "antd";
import styles from "./webcheckin.module.css"
import cx from "classnames";
import Modal from '../Modal';
import { CloseOutlined, CalendarOutlined, DownloadOutlined, PrinterOutlined } from '@ant-design/icons';
import { saveGuestDeclaration as saveGuestDeclarationAPI } from '../../services/webcheckin';
import moment from 'moment'
import {ValidatePan,ValidateVoterID,ValidateAadhar } from "../../utils/validations"

const Guests=(props)=> {
    const { Option } = Select;
    const [country, setCountry] = useState(props.guestData?props.guestData.country:"India");
    const [state, setState] = useState(props.guestData?(props.guestData.state!=="Choose a State"?props.guestData.state:null):null);
    const [countryStatesArray, setCountryState] = useState(["Maharashtra", "Tamil Nadu", "Karnataka", "Kerala", "Andaman and Nicobar Islands", "Andhra Pradesh", "Telangana", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Lakshadweep", "Madhya Pradesh", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Pondicherry", "Punjab", "Rajasthan", "Sikkim", "Tripura", "Uttaranchal", "Uttar Pradesh", "West Bengal"]);
    const [mealType, setMealType]=useState(props.guestData?props.guestData.meal:null);
    const [idType,setIdType] = useState(props.guestData?(props.guestData.web_checkin_doc_type?props.guestData.web_checkin_doc_type:null):null);
    const [error,setError]= useState(false)
    const [comments,setComments] = useState();
    const [isModalShow, setIsModalShow] = useState(false);
    const [isUploadSucess, setisUploadSucess] = useState(false);
    const [isSuccessModalShow, setIsSuccessModalShow] = useState(false);
    const [question1, setQuestion1]  = useState()
    const [question2, setQuestion2]  = useState()
    const [question3, setQuestion3]  = useState()
    const [question4, setQuestion4]  = useState()
    const [question5, setQuestion5]  = useState()
    const [question6, setQuestion6]  = useState()
    const [question7, setQuestion7]  = useState()
    const [question8, setQuestion8]  = useState()
    const [question9, setQuestion9]  = useState()
    const [question10, setQuestion10]  = useState()
    const [question11, setQuestion11]  = useState()
    const [question12, setQuestion12]  = useState()
    const [question13, setQuestion13]  = useState()
    const [question14, setQuestion14]  = useState()
    const [question15, setQuestion15]  = useState()
    const [question16, setQuestion16]  = useState()
    const [question17, setQuestion17]  = useState()
    const [place, setPlace]  = useState()
    const [arival, setArival]  = useState()
    const [departure, setDeparture]  = useState()
    const [temperature, setTemperature]  = useState()
    const [vital, setVital]  = useState()
    const [declarationAccepted, setDeclarationAccepted]= useState(null)
    const [declarationDoc,setDeclarationDoc] = useState()
    const [loading, setLoading] = useState(false)



    const uploadProps = {
        // name: 'file',
        // action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        // headers: {
        //   authorization: 'authorization-text',
        // },
        previewFile:(file)=> {
            console.log('nik-log preview', file);
        },
        accept:"image/png",
        beforeUpload: file => {
            console.log(file,"nik-log file");
            updateArray(props.guestArray,props.guestData.id,"rtpcr_doc",file);
            return false
        },
        onChange: info=> {
          if (info.file.status !== 'uploading') {
            console.log(info, info.file,"nik-log");
            setisUploadSucess(true);
          }
          if (info.file.status === 'done') {
            // message.success(`${info.file.name} file uploaded successfully`);
            console.log(info.file, info.fileList,"nik-log success");
            setisUploadSucess(true);
          } else if (info.file.status === 'error') {
            console.log(info.file, info.fileList,"nik-log error");
            // message.error(`${info.file.name} file upload failed.`);
          }
        },
        maxCount:1
      };
      
    // useEffect(()=>{
    //     console.log(props,"nik-log useEffect")
    //     if(props.guestData){
    //         let defaultCountry=props.guestData.country
    //         setCountry("Afghanistan")
    //         setCountryState(CountryStates.countries.filter((country) => country.country === defaultCountry)[0].states);
    //     }
    // },[console.log(country,"nik-log country")])

    const openHealthDeclartaion = () =>{
        if(declarationAccepted===null){
            setIsModalShow(true)
        }else{
            setIsSuccessModalShow(true);
        }
        
    }
    const updateArray =(array,id,key,value)=>{
        console.log(array,id,key,value,"nik-log array update")
        let index = array.findIndex((obj => obj.id == id));
        array[index][key]=value;
        props.setGuestState(array)
    }
    const setDeclaration=(array,id)=>{

        let index = array.findIndex((obj => obj.id == id));
        array[index]["declaration"]=true;
        props.setHealth(array)
    }
    const setCheckbox=(array,id)=>{
        console.log(array,id,"nik-log array update")
        let index = array.findIndex((obj => obj.id == id));
        array[index]["checkbox"]=!array[index]["checkbox"];
        props.setHealth(array)
    }
    // const setVaccinatedCheckbox=(array,id)=>{
    //     console.log(array,id,"nik-log array update")
    //     let index = array.findIndex((obj => obj.id == id));
    //     array[index]["is_vaccinated"]=!array[index]["is_vaccinated"];
    //     props.setHealth(array)
    // }
    // const validateFields =(array)=>{
    //     if(guestArray){
    //         for (let i = 0; i < guestArray.length; i++) {
    //         if (
    //             guestArray[i]["state"]===null||guestArray[i]["state"]==="Choose a State"||
    //             guestArray[i]["web_checkin_doc_number"]===null||guestArray[i]["web_checkin_doc_number"]===""||
    //             guestArray[i]["web_checkin_doc_type"]===null||guestArray[i]["web_checkin_doc_type"]==="") {
    //                 console.log(guestArray[i],"nik-log")
    //                 valid3 = false;
    //                 break;
    //         }
    //     }}else valid3=false
    // }
    const saveGuestDeclaration=async()=>{
        setLoading(true)
        const data={
            "booking_id": props.bookingId,
            "guest_id": props.guestData.id,
            "declaration_accepted": true,
            "question_responses": [
                {
                    "question_no": 1,
                    "question": "Have you recently developed cough (dry or productive)",
                    "question_type": "boolean",
                    "answer": question1
                },
                {
                    "question_no": 2,
                    "question": "Fever (or feeling feverish)",
                    "question_type": "boolean",
                    "answer": question2
                },
                {
                    "question_no": 3,
                    "question": "General weakness",
                    "question_type": "boolean",
                    "answer": question3
                },
                {
                    "question_no": 4,
                    "question": "Generalized muscle ache",
                    "question_type": "boolean",
                    "answer": question4
                },
                {
                    "question_no": 5,
                    "question": "Sudden loss of smell and/or taste",
                    "question_type": "boolean",
                    "answer": question5
                },
                {
                    "question_no": 6,
                    "question": "Any respiratory distress",
                    "question_type": "boolean",
                    "answer": question6
                },
                {
                    "question_no": 7,
                    "question": "In the last 14 days before your journey, were you in contact with anyone diagnosed with COVID-19 inspection?",
                    "question_type": "boolean",
                    "answer": question7
                },
                {
                    "question_no": 8,
                    "question": "In the last four hours before temperature check, have you consumed antipyretics or other analgesics?",
                    "question_type": "boolean",
                    "answer": question8
                },
                {
                    "question_no": 9,
                    "question": "In the last 14 days before your journey, list the cities and countries you have visited and indicate the duration of your stay in each one;",
                    "question_type": "2d_array",
                    "answer": [
                        {
                            "row": 1,
                            "columns": [
                                {
                                    "name": "Place",
                                    "value": place
                                },
                                {
                                    "name": "Arrival",
                                    "value": arival
                                },
                                {
                                    "name": "Departure",
                                    "value": departure
                                }
                            ]
                        }
                    ]
                },
                {
                    "question_no": 10,
                    "question": "I/we am/are not residing in any containment zone.",
                    "question_type": "boolean",
                    "answer": question10
                },
                {
                    "question_no": 11,
                    "question": "I/we am are not under quarantine.",
                    "question_type": "boolean",
                    "answer": question11
                },
                {
                    "question_no": 12,
                    "question": "I/we develop any of the above-mentioned symptoms I shall contact the concerned health authorities immediately.",
                    "question_type": "boolean",
                    "answer": question12
                },
                {
                    "question_no": 13,
                    "question": "I/we have not tested COVID-19 positive in last two months.",
                    "question_type": "boolean",
                    "answer": question13
                },
                {
                    "question_no": 14,
                    "question": "I /we are eligible to travel as per the extant norms.",
                    "question_type": "boolean",
                    "answer": question14
                },
                {
                    "question_no": 15,
                    "question": "I/we make my mobile number/contact details available to the cruise lines whenever required by them",
                    "question_type": "boolean",
                    "answer": question15
                },
                {
                    "question_no": 16,
                    "question": "I/we undertake to adhere to the health protocol prescribed by the destination State/UT.",
                    "question_type": "boolean",
                    "answer": question16
                },
                {
                    "question_no": 17,
                    "question": "Assessment Section",
                    "question_type": "2d_array",
                    "answer": [
                        {
                            "row": 1,
                            "columns": [
                                {
                                    "name": "Temperature Check",
                                    "value": temperature
                                },
                                {
                                    "name": "Other Vital Signs",
                                    "value": vital
                                }
                            ]
                        }
                    ]
                }
            ]
        }
        console.log(data,"nik-log")
        const response = await saveGuestDeclarationAPI(data)
        console.log(response,"nik-log")
        if(response.data.status==="success"){
            console.log(response,"nik-log")
            setDeclarationDoc(response.data.health_declaration_doc_url)
            setDeclarationAccepted(response.data.data.declaration_accepted)
            setDeclaration(props.healthArray,props.guestData.id)
            setIsModalShow(false);
            setIsSuccessModalShow(true);
            setLoading(false)
        }
        
    }
    return (
            <CardContent className={styles.Form}>
                    <section>
                            <label for="type">Country</label>
                            {/* <input type="text" placeholder="India" />    */}
                            <Select 
                            placeholder="Country" 
                            defaultValue={country}
                            className={styles.webcheckinDropdown}
                             onChange={(event) =>  {
                                 console.log(event,"nik-log event")
                                let x = event;
                                setCountry(x);
                                updateArray(props.guestArray,props.guestData.id,"country",x);
                                setCountryState(CountryStates.countries.filter((country) => country.country === x)[0].states);
                            }}>
                                {
                                    CountryStates.countries.map((country) => {
                                    return  <Option value={country.country}>{country.country}</Option>
                                    })
                                }
                            </Select>
                    </section>
                    <section>
                        <label for="type">State</label>
                        {/* <input type="text" placeholder="Maharastra" /> */}
                        <Select 
                            placeholder="State" 
                            defaultValue={state}
                            className={styles.webcheckinDropdown}
                            onChange={(event) =>  {
                                let x = event;
                                setState(x);
                                updateArray(props.guestArray,props.guestData.id,"state",x);
                            }}
                            >
                            {
                            countryStatesArray.map((states) => {
                            return  <Option value={states}>{states}</Option>
                            })
                            }
                        </Select>
                    </section>
                    <section>
                            <label for="type">Meal Type</label>
                                <Select 
                                    placeholder="Meal Type" 
                                    className={styles.webcheckinDropdown}
                                    defaultValue={mealType}
                                    onChange={(event) =>  {
                                        let x = event;
                                        setMealType(x);
                                        updateArray(props.guestArray,props.guestData.id,"meal",x);
                                    }}
                                >
                                    <Option value="Veg">Veg</Option>
                                    <Option value="Non. Veg">Non Veg</Option>
                                    <Option value="Jain">Jain</Option>
                               
                            </Select>
                            <textarea 
                                placeholder="Dietary requirements (if any)"
                                defaultValue={comments}
                                onChange={(e)=>{
                                    let x = e.target.value;
                                    setComments(x);
                                    updateArray(props.guestArray,props.guestData.id,"comments",x);
                                }}
                                defaultValue={props.guestData?(props.guestData.comments?props.guestData.comments:null):null}
                            />
                    </section>
                    <section>
                            <label for="type">Identification Type</label>
                            {/* select can be used but I dont think aadhar cards have an option to select from so I am using an input */}
                            {/* <select name="Aadhar Card" id="card" placeholder="Aadhar Card"></select> */}
                            <Select 
                                    placeholder="Identification Type" 
                                    className={styles.webcheckinDropdown}
                                    onChange={(event) =>  {
                                        let x = event;
                                        setIdType(x);
                                        updateArray(props.guestArray,props.guestData.id,"web_checkin_doc_type",x);
                                        // setError(false)
                                    }}
                                    defaultValue={idType}
                                >
                                   
                                    {(props.isInternational)?
                                     <Option value="passport">Passport</Option>
                                    :<>
                                    <Option value="aadhar_card">Aadhar</Option>
                                    <Option value="passport">Passport</Option>
                                    <Option value="pan_card">Pan Card</Option>
                                    <Option value="driving_license">Driving License</Option>
                                    <Option value="birth_certificate">Birth certificate</Option>
                                    </>}
                                    
                                    
                               
                            </Select>
                            <input 
                                type="text" 
                                placeholder="Enter ID Number"
                                onChange={(e)=>{
                                    let x = e.target.value;
                                    updateArray(props.guestArray,props.guestData.id,"web_checkin_doc_number",x);
                                    if(!props.isInternational){
                                        if(idType==="aadhar_card"){
                                            setError(!ValidateAadhar(x))

                                        }else if(idType==="pan_card"){
                                            setError(!ValidatePan(x))
                                        }
                                        else if(idType==="election_id"){
                                            setError(!ValidateVoterID(x))
                                        }
                                        
                                    }
                                }}
                                defaultValue={props.guestData?(props.guestData.web_checkin_doc_number?props.guestData.web_checkin_doc_number:null):null}
                            />
                           {error&&
                                <div class="alert alert-danger" role="alert">
                                    Enter Valid ID
                                </div>}
                            {(props.isInternational||idType==="passport")?<div className="row">
                                <div className="col-6">
                                    <input type="text" 
                                    placeholder="Place of issue" 
                                    className={styles.webcheckinInput} 
                                    onChange={(e)=>{
                                        let x = e.target.value;
                                        updateArray(props.guestArray,props.guestData.id,"web_checkin_doc_place",x);
                                    }}
                                    defaultValue={props.guestData?(props.guestData.web_checkin_doc_place?props.guestData.web_checkin_doc_place:null):null}
                                    />
                                </div>
                                <div className="col-6">
                                    <DatePicker 
                                    className={styles.dateInput} 
                                    placeholder="Date of Exp." 
                                    onChange={(date, dateString)=>{
                                        let x = dateString
                                        updateArray(props.guestArray,props.guestData.id,"web_checkin_doc_date",x);
                                    }}
                                    defaultValue={props.guestData?(props.guestData.web_checkin_doc_date?moment(props.guestData.web_checkin_doc_date):null):null}
                                    // picker="month" 
                                    />
                                    {/* <CalendarOutlined className={cx("inputIcon",styles.dateIcon)}/> */}
                                    {/* <input 
                                        type="text" 
                                        placeholder="Date of Exp." 
                                        className={styles.webcheckinInput}
                                        onChange={(e)=>{
                                            let x = e.target.value;
                                            updateArray(props.guestArray,props.guestData.id,"web_checkin_doc_date",x);
                                        }}
                                    /> */}
                                </div>
                            </div>:null}
                    </section>
                    {/* <section>
                        <label for="type">Negative RTPCR Report</label>
                        <div className="row align-items-baseline">
                            <div className="col-6">
                            <Upload className={styles.webcheckinUpload} {...uploadProps}>
                                <Button className={styles.webcheckinUploadbtn}>Upload file</Button>
                            </Upload>
                            {isUploadSucess?<p className="font-italic text-success">File Uploaded Successfully</p>:<span/>}
                            </div>
                            <div className="col-6">.pdf, .png, .jpeg</div>
                        </div>
                    </section> */}
                    <section>
                        <button className={styles.Formdeclaration} onClick={()=>openHealthDeclartaion()}>Health Declaration Form* &nbsp;&nbsp; {declarationAccepted&&<span className="fst-normal fw-bold text-white bg-purple bg-purple-hover p-1 px-2 rounded cursor-pointer" onClick={()=>setIsModalShow(true)}>Edit</span>}</button>
                       {declarationAccepted&&
                        <p className="font-italic text-success">Successfully submitted</p>}
                        <div className={cx("font-italic",styles.terms)}>
                            <input type="checkbox" name="guest" id="guest" 
                                    onChange={(e)=>{
                                        setCheckbox(props.healthArray,props.guestData.id)
                                    }}
                                    />&nbsp; &nbsp; I Agree that I have not been tested COVID positive</div>
                        {/* <div className={cx("font-italic",styles.terms)}>
                            <input type="checkbox" name="guest" id="guest" 
                                    onChange={(e)=>{
                                        setVaccinatedCheckbox(props.healthArray,props.guestData.id)
                                        updateArray(props.guestArray,props.guestData.id,"web_checkin_doc_date",x);
                                    }}
                                    />&nbsp; &nbsp; I Agree that I have not been Vaccinated</div> */}
                    </section>
                    <Modal show={isModalShow} onClose={()=>setIsModalShow(false)}>
                        <div className="card-body p-0 position-relative">
                        <div className="modal-close modalmap-close mt-2">
                            <CloseOutlined
                            style={{ color: "#EA725B",fontSize: "18pt" }}
                            onClick={()=>setIsModalShow(false)}
                            />
                        </div>
                        <div className={cx("modal-content p-0 m-0 pt-4",styles.healthDeclaration)}>
                            <div class="p-3">
                            <h6>Health Declaration Form</h6>
                            <p>In the last 8 days before your journey, have you had any of the symptoms? (Please mark yes or no against each symptom)</p>
                            <div className="row mb-3">
                                <div className="col-9 col-md-10"></div>
                                <div className="col-1"><h5>Yes</h5></div>
                                <div className="col-1"><h5>No</h5></div>
                            </div>
                            <ol>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>Have you recently developed cough (dry or productive)</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion1("Yes")}} checked={question1==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion1("No")}} checked={question1==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li> Fever (or feeling feverish)</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion2("Yes")}} checked={question2==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion2("No")}} checked={question2==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li> General weakness</li>
                            </div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion3("Yes")}} checked={question3==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion3("No")}} checked={question3==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>Generalized muscle ache</li>
                            </div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion4("Yes")}} checked={question4==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion4("No")}} checked={question4==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>Sudden loss of smell and/or taste</li>
                            </div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion5("Yes")}} checked={question5==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion5("No")}} checked={question5==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>Any respiratory distress</li>
                            </div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion6("Yes")}} checked={question6==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion6("No")}} checked={question6==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>In the last 14 days before your journey, were you in contact with anyone diagnosed with COVID-19 inspection?</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion7("Yes")}} checked={question7==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion7("No")}} checked={question7==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>In the last four hours before temperature check, have you consumed antipyretics or other analgesics?</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion8("Yes")}} checked={question8==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion8("No")}} checked={question8==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-11"> <li>In the last 14 days before your journey, list the cities and countries you have visited and indicate the duration of your stay in each one;</li></div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-md-3">
                                    <input type="text" name="aadhar" id="place" placeholder="Enter Place" className={styles.otherInput} onChange={(e)=>{setPlace(e.target.value)}} defaultValue={place}/>
                                </div>
                                <div className="col-6 col-md-3">
                                    <DatePicker bordered={false} className={cx("ShowIcon Pad-right-placeholder",styles.dateInput)} placeholder="Date of Arrival" onChange={(date, dateString)=>{setArival(dateString)}} defaultValue={arival?moment(arival):null}/>
                                </div>
                                <div className="col-6 col-md-3 d-none d-md-block">
                                    <DatePicker bordered={false} className={cx("ShowIcon Pad-right-placeholder",styles.dateInput)} placeholder="Date of Departure" onChange={(date, dateString)=>{setDeparture(dateString)}} defaultValue={departure?moment(departure):null}/>
                                </div>
                                <div className="col-6 col-md-3 d-md-none">
                                    <DatePicker bordered={false} className={cx("ShowIcon Pad-right-placeholder",styles.dateInput)} placeholder="Date of Dept" onChange={(date, dateString)=>{setDeparture(dateString)}} defaultValue={departure?moment(departure):null}/>
                                </div>
                                <div className="col-3">

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we am/are not residing in any containment zone.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion10("Yes")}} checked={question10==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion10("No")}} checked={question10==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we am are not under quarantine.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion11("Yes")}} checked={question11==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion11("No")}} checked={question11==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we develop any of the above-mentioned symptoms I shall contact the concerned health authorities, immediately.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion12("Yes")}} checked={question12==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion12("No")}} checked={question12==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we have not tested COVID-19 positive in last two months.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion13("Yes")}} checked={question13==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion13("No")}} checked={question13==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I /we are eligible to travel as per the extant norms.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion14("Yes")}} checked={question14==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion14("No")}} checked={question14==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we make my mobile number/contact details available to the cruise lines whenever required by them</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion15("Yes")}} checked={question15==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion15("No")}} checked={question15==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we undertake to adhere to the health protocol prescribed by the destination State/UT.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion16("Yes")}} checked={question16==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion16("No")}} checked={question16==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-12"> <li>Assessment Section</li></div>
                                <div className="col-12 col-md-4"><label>Temperature Check</label> 
                                <input type="text" name="temparature" id="temparature" className={styles.otherInput} onChange={(e)=>setTemperature(e.target.value)} defaultValue={temperature}/></div>
                                <div className="col-12 col-md-7 d-none d-md-block"><label>Other Vital Signs</label>
                                <input type="text" name="vital" id="vital" className={styles.otherInput} onChange={(e)=>setVital(e.target.value)} defaultValue={vital}/>
                                </div>
                                <div className="col-12 col-md-7 d-md-none"><label>Other Vital Signs</label>
                                <textarea type="text" name="vital" id="vitalMobile" className={styles.otherInput} onChange={(e)=>setVital(e.target.value)} defaultValue={vital}/>
                                </div>
                            </div>
                            </ol>
                            <div className="px-3">
                            <h4>Assessment Decision</h4>
                            <div className={cx("font-italic",styles.terms)}><input type="checkbox" name="assessment" id="assessment" onChange={(e)=>{e.target.checked?setQuestion17("Yes"):setQuestion17("No")}} checked={question17==="Yes"}/>&nbsp; &nbsp; I Accept the <a href="/tnc" target="_blank">terms & conditions</a></div>
                            </div>
                            <div className="row my-5 mt-md-2 mb-md-5">
                            <div className="col-6 text-md-right"><Button type="primary" className="outline-purple-button w-xs-100">CLEAR ALL</Button></div>
                            <div className="col-6"><Button type="primary" className="bg-purple-button w-xs-100" onClick={()=>saveGuestDeclaration()} disabled={question17==="Yes"?false:true}>{loading?<div class="spinner-border spinner-border-sm text-light" role="status"><span class="sr-only">Loading...</span></div>:"SUBMIT"}</Button></div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </Modal>
                    <Modal show={isSuccessModalShow} onClose={()=>setIsSuccessModalShow(false)}>
                        <div className="card-body p-0 position-relative">
                        <div className="modal-close modalmap-close mt-2">
                            <CloseOutlined
                            style={{ color: "#EA725B",fontSize: "18pt" }}
                            onClick={()=>setIsSuccessModalShow(false)}
                            />
                        </div>
                        <div className={cx("modal-content p-0 m-0 pt-4",styles.successfulModal)}>
                            <div class="p-3">
                                <h6>Successful</h6>
                                <p>You have succesfully submitted <em className="text-primary font-normal">Health Declaration</em> Form, Please keep copy of the same at time of sailing</p>
                                <div className="row my-3 mt-md-2 mb-md-3">
                                <div className="col-12 col-md-6 mb-3 md-md-0 text-right"><Button type="primary" className="bg-purple-button" onClick={()=>{window.open(declarationDoc)}} icon={<DownloadOutlined className="d-none d-md-inline-block"/>}>Download Form</Button></div>
                                <div className="col-12 col-md-6"><Button type="primary" className="bg-purple-button" onClick={()=>{window.open(declarationDoc)}} icon={<PrinterOutlined className="d-none d-md-inline-block" />}>Print Form</Button></div>
                                </div>
                            </div>
                        </div>
                        </div>
                </Modal>
            </CardContent>
    )
}

export default Guests;


const CardContent = styled.div`

display: flex;
flex-direction:column;
height:auto;
padding:10px;

>section{
    display: flex;
    flex-direction:column;
>label{
    padding:2px;
    font-size:14px;
    font-weight: 500;
    color: #500E4B;
}
>input{
    padding:9px;
    margin-bottom:10px;
   border:1px solid #DDDD;
    border-radius:10px;
}
>textarea{
    padding:9px;
    margin-bottom:10px;
    border:1px solid #DDDD;
    border-radius:10px;
}
>select{
    padding:9px;
    margin-bottom:10px;
   border:1px solid #DDDD;
    border-radius:10px;
}
}
@media screen and (max-width:600px){
    width:300px;
    margin-left:5px;
    
}
@media screen and (max-width:450px){
    width:100%;
    
}

`;
