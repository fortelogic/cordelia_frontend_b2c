import React from "react";
import BannerStyles from "./Banner.module.css";
import cx from "classnames";
import Slider from "react-slick";

const Banner = ({
  title,
  subheading,
  image,
  logo = false,
  isThankPage = false,
  withMenu = false,
  banners,
  mobileBanners,
  scroll
}) => {
  const [isMobile, setMobile] = React.useState(false);

  React.useEffect(() => {
    _isMobileScreen();
    // window.addEventListener("resize", _isMobileScreen);
    // () => {
    //   window.removeEventListener("resize", _isMobileScreen);
    // };
  }, []);

  const _isMobileScreen = () => {
    if (window.innerWidth <= 768) {
      setMobile(true);
      return;
    }
    setMobile(false);
  };

  let style = isThankPage
    ? { background: "#500E4B", position: "relative" }
    : { backgroundImage: `url(${image})` };
    const sliderSettings = {
      infinite: true,
      slidesToShow: 1,
      speed: 300,
      cssEase: "ease-out",
      dots: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 2,
            infinite: false,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            initialSlide: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
  return (
    <div
      className={cx(
        BannerStyles["banner"],
        "flex flex-col content-center justify-center"
      )}
      style={style}
    >
      <Slider autoplay={true} autoplaySpeed={2000} {...sliderSettings} className={
        "bannerCarouselSlider packagesCardSlider"
      }>
        
        {isMobile ? mobileBanners.map(data=>{
          return <img className={cx(
            BannerStyles["bannerImage"]
          )} src={data.image}></img>
        }) : banners.map(data=>{
          return <img style={{}} src={data.image}></img>
        })}
      </Slider>
    </div>
  );
};

export default Banner;
