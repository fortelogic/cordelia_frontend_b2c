import React, { useState, useEffect } from "react";

const rechedulePolicyComp = ({reschedulePolicy}) => {
    return (
        <>
            {reschedulePolicy ? (
                <div>
                    {reschedulePolicy.map((ele) => {
                    return (
                        <>
                        <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                            <u>
                            {ele.title}
                            </u>
                        </h3>
                        <br></br>
                        <table style={{ color: "black", borderCollapse: "collapse", width: "100%" }}>
                            <tr>
                            {Object.keys(ele.fees[0]).map((heading) => {
                                heading = heading.replaceAll("_", " ")
                                return (
                                <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", textTransform: "capitalize" }}>{heading}</th>
                                )
                            })}
                            </tr>
                            {ele.fees.map((rows) => {
                            return (
                                <tr>
                                <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>{rows["days_to_depature"]}</td>
                                <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>{rows["fee"]}</td>
                                </tr>
                            );
                            })}
                        </table>
                        <br></br>
                        <ul style={{ color: "black", marginLeft: "1rem", }}>
                            {ele.terms.map((term) => {
                            return(
                                <li style={{display: "revert"}}>{term}</li>
                            )
                            })}
                        </ul>
                        <br></br>
                        </>
                    );
                    })}
                </div>
                ) : (<></>)
        }
        </>
    )
}

export default rechedulePolicyComp;