import React, { useState, useEffect } from "react";

const cancellationPolicyComp = ({ cancellationPolicy }) => {
    return (
        <>
            {cancellationPolicy ? (
                <div>
                    {cancellationPolicy.map((ele, index) => {
                        return (
                            <div key={index}>
                                <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                                    <u>
                                        {ele.title}
                                    </u>
                                </h3>
                                <br></br>
                                <table style={{ color: "black", borderCollapse: "collapse", width: "100%" }}>
                                    <tr>
                                        {Object.keys(ele.fees[0]).map((heading, index) => {
                                            heading = heading.replaceAll("_", " ")
                                            return (
                                                <th key={index} style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", textTransform: "capitalize" }}>{heading}</th>
                                            )
                                        })}
                                    </tr>
                                    {ele.fees.map((rows, index) => {
                                        return (
                                            <tr key={index}>
                                                <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>{rows["days_to_depature"]}</td>
                                                <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>{rows["fee"]}</td>
                                            </tr>
                                        );
                                    })}
                                </table>
                                <br></br>
                                <ul style={{ color: "black", marginLeft: "1rem", }}>
                                    {ele.terms.map((term, index) => {
                                        return (
                                            <li key={index} style={{ display: "revert" }}>{term}</li>
                                        );
                                    })}
                                </ul>
                                <br></br>
                            </div>
                        );
                    })}
                </div>
            ) : (<></>)
            }
        </>
    )
}

export default cancellationPolicyComp;