import styles from './Footer.module.scss';
import { Row, Col } from 'antd';
import WhiteLogo from '../../assets/img/footer-logo.png';
import {
  FacebookFilled,
  InstagramFilled,
  LinkedinFilled,
  TwitterCircleFilled,
  MailFilled,
  PhoneFilled,
} from '@ant-design/icons';
import Container from '../Container';
import { Divider, Collapse } from 'antd';
import cx from 'classnames';
import FacebookIcon from '../../assets/img/mobile-menu/facebook.svg';
import InstagramIcon from '../../assets/img/mobile-menu/instagram.svg';
import LinkedInIcon from '../../assets/img/mobile-menu/linkedin.svg';
import TwitterIcon from '../../assets/img/mobile-menu/twitter.svg';
import PhoneIcon from '../../assets/img/mobile-menu/phone.svg';
import EmailIcon from '../../assets/img/mobile-menu/email.svg';
import './footer.scss';
import { Link } from 'react-router-dom';

function Footer() {
  const { Panel } = Collapse;

  const queryParams = new URLSearchParams(window.location.search)


  return (
    <div className="Footer">
      <footer className={styles.siteFooter}>
        <Container strict={true}>
          <Row>
            {/* <Col span={1}></Col> */}
            <Col className="col-12 col-md-6 col-lg-4 p-0 px-md-4">
              <div className={styles.siteFooterIdentity}>
                <img src={WhiteLogo} alt="Site Name" />
                <div>
                  <h5>Cordelia Cruises</h5>
                  <h6 className={styles.header6}>A city on the sea</h6>
                </div>
              </div>
              <p className={styles.textJustify}>
                Cordelia Cruises by Waterways Leisure Tourism Pvt Ltd is India’s premium cruise liner. True to its name,
                Cordelia aspires to promote and drive the cruise culture in India through experiences that are stylish,
                luxurious and most importantly, inherently Indian.
              </p>
              <div className={cx(styles.mobileNavLinks, 'd-md-none')}>
                <Collapse defaultActiveKey={['1']} expandIconPosition={'right'} ghost>
                  <Divider style={{ borderColor: '#ffffff80', margin: '5px 0px' }} />
                  <Panel header={<h6 className="mb-0">Links</h6>} key="1">
                    <div>
                      <a href="https://cordelia.fortelogic.in/cruise-destinations">Destination</a>
                    </div>
                    <div>
                      <a href="/contact">Group booking form</a>
                    </div>
                    <div>
                      <a href="/contact">Lost and Found</a>
                    </div>
                    <div>
                      <a href="https://agent.cordeliacruises.com/login">Agent Login</a>
                    </div>
                    <div>
                      <a href="/about-us">About Us</a>
                    </div>
                    <div>
                      <Link to={"/cruise-deals"}>Promotions</Link>
                    </div>
                  </Panel>
                  <Divider style={{ borderColor: '#ffffff80', backgroundColor: '#ffffff80', margin: '5px 0px' }} />
                  <Panel header={<h6 className="mb-0">Privacy & Policy</h6>} key="2">
                    <div>
                      <a href={`/onboard-policy`}>Onboard Policy</a>
                    </div>
                    <div>
                      <a href={`/healthy-waves`}>Healthy Waves Policy</a>
                    </div>
                    <div>
                      <a href="/tnc">Clean Waves Policy</a>
                    </div>
                    <div>
                      <a href="/tnc">Terms and Conditions</a>
                    </div>
                    <div>
                      <a href="https://images.cordeliacruises.com/static/passenger-cruise-ticket-contract.pdf">Passenger Cruise Ticket Contract</a>
                    </div>
                  </Panel>
                  <Divider style={{ borderColor: '#ffffff80', backgroundColor: '#ffffff80', margin: '5px 0px' }} />
                  <Panel header={<h6 className="mb-0">Contact Us</h6>} key="3">
                    <div>
                      <a href="">
                        <img src={EmailIcon} />
                        <span style={{ marginLeft: '8px' }}>info@cordeliacruises.com</span>
                      </a>
                    </div>
                    <div>
                      <a href="">
                        <img src={PhoneIcon} />
                        <span style={{ marginLeft: '8px' }}>022688111111</span>
                      </a>
                    </div>
                  </Panel>
                  <Divider style={{ borderColor: '#ffffff80', backgroundColor: '#ffffff80', margin: '0px' }} />
                </Collapse>
              </div>
              <ul className={cx(styles.socialIcons)}>
                <b className="mr-2">Follow Us:</b>
                <li>
                  <a className={styles.facebook} target="_blank" href="https://www.facebook.com/cordeliacruises">
                    <img src={FacebookIcon} />
                  </a>
                </li>
                <li>
                  <a className={styles.instagram} target="_blank" href="https://www.instagram.com/cordeliacruises/?hl=en">
                    <img src={InstagramIcon} />
                  </a>
                </li>
                <li>
                  <a className={styles.linkedin} target="_blank" href="https://www.linkedin.com/company/cordelia-cruises">
                    <img src={LinkedInIcon} />
                  </a>
                </li>
                <li>
                  <a className={styles.twitter} target="_blank" href="https://twitter.com/CordeliaCruises">
                    <img src={TwitterIcon} />
                  </a>
                </li>
                <li>
                  <a className={styles.youtube} target="_blank" href="https://www.youtube.com/channel/UCIGZzyqWsbCH1-VNFsXrY9g">
                    <i class="fab fa-youtube"></i>
                  </a>
                </li>
              </ul>
            </Col>
            <Col className="col-6 col-md-6 col-lg-2 my-3 d-none d-md-block">
              <h6>Quick Links</h6>
              <ul className={styles.footerLinks}>
                <li>
                  <a href="/cruise-destinations">Destination</a>
                </li>
                <li>
                  <a href="/contact">Group Booking Form</a>
                </li>
                <li>
                  <a href="/contact">Lost and Found</a>
                </li>
                <li>
                  <a href="https://agent.cordeliacruises.com/login">Agent Login</a>
                </li>
                <li>
                  <Link to={"/about-us"}>About Us</Link>
                </li>
                <li>
                  <Link to={"/cruise-deals"}>Promotions</Link>
                </li>
                <li>
                  <Link to={queryParams.get("platform") !== null && queryParams.get("platform") !== undefined && queryParams.get("platform") === "true" ? "/faq?platform=true" : "/faq"}>FAQ</Link>
                </li>
              </ul>
            </Col>
            <Col className="col-6 col-md-6 col-lg-3 my-3  d-none d-md-block">
              <h6>Privacy & Policy</h6>
              <ul className={styles.footerLinks}>
                <li>
                  <a href="/privacy-policy">Privacy Policy</a>
                </li>
                <li>
                  <a href={`/onboard-policy`}>Onboard Policy</a>
                </li>
                <li>
                  <a href={`/healthy-waves`}>Healthy Waves Policy</a>
                </li>
                <li>
                  <a href="/tnc">Clean Waves Policy</a>
                </li>
                <li>
                  <a href="/tnc">Terms and Conditions</a>
                </li>
                <li>
                  <a href="https://images.cordeliacruises.com/static/passenger-cruise-ticket-contract.pdf">Passenger Cruise Ticket Contract</a>
                </li>
              </ul>
            </Col>
            <Col className="col-12 col-md-6 col-lg-3 my-3  d-none d-md-block">
              <h6>Contact Us</h6>
              <ul className={styles.footerLinks}>
                <li>
                  <a href="">
                    <MailFilled />
                    <span style={{ marginLeft: '8px' }}>info@cordeliacruises.com</span>
                  </a>
                </li>
                <li>
                  <a href="">
                    <PhoneFilled />
                    <span style={{ marginLeft: '8px' }}>022688111111</span>
                  </a>
                </li>
              </ul>
            </Col>
          </Row>
          <Row>
            <Col span={1}></Col>
            <Divider
              style={{ borderColor: '#ffffff80', backgroundColor: '#ffffff80', margin: '5px 0px' }}
              className="d-none d-md-block"
            />
            <Col span={22}>
              <p className={styles.copyrightText}>&copy; 2021 Cordelia Cruises All Right Reserved</p>
            </Col>
            <Col span={1}></Col>
          </Row>
        </Container>
      </footer>
    </div>
  );
}

export default Footer;
