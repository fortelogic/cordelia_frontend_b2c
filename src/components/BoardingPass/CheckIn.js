import React,{useState} from 'react'
import styled from 'styled-components';
import ShareIcon from '@material-ui/icons/Share';
import PrintIcon from '@material-ui/icons/Print';
import GetAppIcon from '@material-ui/icons/GetApp';
import TripInfo from './TripInfo';
import styles from "./boardingpass.module.css"
import Image from "../../assets/img/boarding-pass/qrcode.png";
import moment from 'moment'
import cx from "classnames"
import Modal from '../Modal';
import { CloseOutlined } from '@ant-design/icons';
import {Button,DatePicker} from "antd";
import {ValidateEmail} from "../../utils/validations"
import {shareBoardingPass,saveGuestDeclaration as saveGuestDeclarationAPI }  from '../../services/webcheckin';
import { ShareAltOutlined, DownloadOutlined, PrinterOutlined} from '@ant-design/icons';

const Details=(props)=> {
    const [isModalShow, setIsModalShow] = useState(false);
    const [isHealthModalShow, setIsHealthModalShow] = useState(false);
    const [isSuccessModalShow, setIsSuccessModalShow] = useState(false);
    const [email, setEmail] = useState(props.guestData.email);
    const [loading, setLoading] = useState(false);
    const [valid,setValid]= useState(true);
    const [error,setError]= useState();
    const [question1, setQuestion1]  = useState()
    const [question2, setQuestion2]  = useState()
    const [question3, setQuestion3]  = useState()
    const [question4, setQuestion4]  = useState()
    const [question5, setQuestion5]  = useState()
    const [question6, setQuestion6]  = useState()
    const [question7, setQuestion7]  = useState()
    const [question8, setQuestion8]  = useState()
    const [question9, setQuestion9]  = useState()
    const [question10, setQuestion10]  = useState()
    const [question11, setQuestion11]  = useState()
    const [question12, setQuestion12]  = useState()
    const [question13, setQuestion13]  = useState()
    const [question14, setQuestion14]  = useState()
    const [question15, setQuestion15]  = useState()
    const [question16, setQuestion16]  = useState()
    const [question17, setQuestion17]  = useState()
    const [place, setPlace]  = useState()
    const [arival, setArival]  = useState()
    const [departure, setDeparture]  = useState()
    const [temperature, setTemperature]  = useState()
    const [vital, setVital]  = useState()
    const [declarationAccepted, setDeclarationAccepted]= useState(null)
    const [declarationDoc,setDeclarationDoc] = useState()
    console.log(props,"nik-log props")

    const share=async()=>{
        setError()
        let isValid=ValidateEmail(email)
        console.log(isValid,email,"nik-log valid")
        setValid(isValid)
        if(isValid){
            setLoading(true);
            const data ={
                "guest_id":props.guestData.id,
                "email":email
            }
            const response = await shareBoardingPass(data)
            console.log(response,"nik-log")
            if(response.status===200&&response.data.status==="success"){
                setError(response.data.message)
                setLoading(false);
            }else{
                setError("Failed")
                setLoading(false);
            }
        }

        
    }
    const resetForm = ()=>{
        setQuestion1()
        setQuestion2()
        setQuestion3()
        setQuestion4()
        setQuestion5()
        setQuestion6()
        setQuestion7()
        setQuestion8()
        setQuestion9()
        setQuestion10()
        setQuestion11()
        setQuestion12()
        setQuestion13()
        setQuestion14()
        setQuestion15()
        setQuestion16()
        setQuestion17()
        setPlace()
        setArival()
        setDeparture()
        setTemperature()
        setVital()
    }
    const saveGuestDeclaration=async()=>{
        setLoading(true)
        const data={
            "booking_id": props.bookingId,
            "guest_id": props.guestData.id,
            "declaration_accepted": true,
            "question_responses": [
                {
                    "question_no": 1,
                    "question": "Have you recently developed cough (dry or productive)",
                    "question_type": "boolean",
                    "answer": question1
                },
                {
                    "question_no": 2,
                    "question": "Fever (or feeling feverish)",
                    "question_type": "boolean",
                    "answer": question2
                },
                {
                    "question_no": 3,
                    "question": "General weakness",
                    "question_type": "boolean",
                    "answer": question3
                },
                {
                    "question_no": 4,
                    "question": "Generalized muscle ache",
                    "question_type": "boolean",
                    "answer": question4
                },
                {
                    "question_no": 5,
                    "question": "Sudden loss of smell and/or taste",
                    "question_type": "boolean",
                    "answer": question5
                },
                {
                    "question_no": 6,
                    "question": "Any respiratory distress",
                    "question_type": "boolean",
                    "answer": question6
                },
                {
                    "question_no": 7,
                    "question": "In the last 14 days before your journey, were you in contact with anyone diagnosed with COVID-19 inspection?",
                    "question_type": "boolean",
                    "answer": question7
                },
                {
                    "question_no": 8,
                    "question": "In the last four hours before temperature check, have you consumed antipyretics or other analgesics?",
                    "question_type": "boolean",
                    "answer": question8
                },
                {
                    "question_no": 9,
                    "question": "In the last 14 days before your journey, list the cities and countries you have visited and indicate the duration of your stay in each one;",
                    "question_type": "2d_array",
                    "answer": [
                        {
                            "row": 1,
                            "columns": [
                                {
                                    "name": "Place",
                                    "value": place
                                },
                                {
                                    "name": "Arrival",
                                    "value": arival
                                },
                                {
                                    "name": "Departure",
                                    "value": departure
                                }
                            ]
                        }
                    ]
                },
                {
                    "question_no": 10,
                    "question": "I/we am/are not residing in any containment zone.",
                    "question_type": "boolean",
                    "answer": question10
                },
                {
                    "question_no": 11,
                    "question": "I/we am are not under quarantine.",
                    "question_type": "boolean",
                    "answer": question11
                },
                {
                    "question_no": 12,
                    "question": "I/we develop any of the above-mentioned symptoms I shall contact the concerned health authorities immediately.",
                    "question_type": "boolean",
                    "answer": question12
                },
                {
                    "question_no": 13,
                    "question": "I/we have not tested COVID-19 positive in last two months.",
                    "question_type": "boolean",
                    "answer": question13
                },
                {
                    "question_no": 14,
                    "question": "I /we are eligible to travel as per the extant norms.",
                    "question_type": "boolean",
                    "answer": question14
                },
                {
                    "question_no": 15,
                    "question": "I/we make my mobile number/contact details available to the cruise lines whenever required by them",
                    "question_type": "boolean",
                    "answer": question15
                },
                {
                    "question_no": 16,
                    "question": "I/we undertake to adhere to the health protocol prescribed by the destination State/UT.",
                    "question_type": "boolean",
                    "answer": question16
                },
                {
                    "question_no": 17,
                    "question": "Assessment Section",
                    "question_type": "2d_array",
                    "answer": [
                        {
                            "row": 1,
                            "columns": [
                                {
                                    "name": "Temperature Check",
                                    "value": temperature
                                },
                                {
                                    "name": "Other Vital Signs",
                                    "value": vital
                                }
                            ]
                        }
                    ]
                }
            ]
        }
        const response = await saveGuestDeclarationAPI(data)
        setLoading(false)
        console.log(response,"nik-log")
        if(response.data.status==="success"){
            console.log(response,"nik-log")
            setDeclarationDoc(response.data.health_declaration_doc_url)
            setDeclarationAccepted(response.data.data.declaration_accepted)
            setIsHealthModalShow(false);
            setIsSuccessModalShow(true);
        }
        
    }
    return (
        <>
        <Container>
            <Heading>
                <div className="row">
                    <div className="col-8"> 
                        <div className={styles.webCheckIN}>
                            <h5>Boarding Pass (Web Check-In)</h5>
                        </div>
                    </div>
                    <div className="col-4">
                    <div className="row ml-4 align-items-center justify-content-around">
                        <p className="cursor-pointer" onClick={()=>setIsModalShow(true)}>Share <ShareAltOutlined /></p>
                        <p className="cursor-pointer" onClick={()=>window.open(props.passData.boarding_pass_doc_url)}>Print <PrinterOutlined /></p>
                        <p className="cursor-pointer" onClick={()=>window.open(props.passData.boarding_pass_doc_url)}>Download < DownloadOutlined/></p></div>
                    </div>
                </div>
            </Heading>
                <HeadingBanner>
                    <h5>Trip Information</h5>
                </HeadingBanner>
                <Content>
                    <div className="row mb-4">
                        <div className="col-2">
                            <h6>Ship Name</h6>
                            <p>Empress</p>
                            </div>
                        <div className="col-2">
                            <h6>Guest Name</h6>
                            <p>{props.guestData.name}</p>
                        </div>
                        <div className="col"></div>
                        <div className="col"></div>
                        <div className={cx(styles.bookingId,"col-4")}><h5>Booking ID: {props.refId}</h5></div>
                        {/* <div className="col text-right"><h6>&nbsp;</h6><p className="pr-2">Scan QR Code</p></div> */}
                    </div>
                    <div className="row col-12">
                        <div className={cx(styles.colcontainer,"col-9")}>
                            <div className="row mb-4">
                                <div className="col-5">
                                    <h6>Itinerary</h6>
                                    <p>{props.itineraryData.nights} Nights</p>
                                    <p>{props.itineraryData.name}</p>
                                </div>
                                <div className="col-4 pl-5">
                                    <p className="font-big">Group {props.guestData.group}</p>
                                    <p><span className="font-big">{props.guestData.embarkation_slot}</span></p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-4">
                                    <p className="font-big">Departure</p>
                                    <p>{props.itineraryData.ports[0].city}</p>
                                    <p>{ moment(props.itineraryData.start_time).format("ddd, D-MMM-YYYY")}</p>
                                    <p>{ moment(props.itineraryData.start_time).format("hh:mm A")}</p>
                                </div>
                                <div className="col-4">
                                    <p className="font-big">Arrival</p>
                                    <p>{props.itineraryData.ports[props.itineraryData.ports.length - 1].city}</p>
                                    <p>{ moment(props.itineraryData.end_time).format("ddd, D-MMM-YYYY")}</p>
                                    <p>{ moment(props.itineraryData.end_time).format("hh:mm A")}</p>
                                </div>
                                <div className="col-4">
                                    <div className="row align-items-baseline px-3"><h6 className="mb-0">Cabin Category:&nbsp;&nbsp;</h6><p>{props.roomData.category}</p></div>
                                    <div className="row align-items-baseline px-3"><h6 className="mb-0">Deck No:&nbsp;&nbsp;</h6><p>{props.roomData.deck_no}</p></div>
                                    <div className="row align-items-baseline px-3"><h6 className="mb-0">Cabin No:&nbsp;&nbsp;</h6><p>{props.roomData.number}</p></div>
                                </div>
                               
                            </div>
                        </div>
                        <div className={cx(styles.colcontainer,"col-3")}>
                            <div className="col-12 text-right">
                                    <p className={styles.qrCode}>Scan QR Code</p>
                                    <span dangerouslySetInnerHTML={{__html: props.passData.boarding_pass_qr_svg}} />
                                    {/* <img src={`data:image/svg+xml;utf8,${props.passData.boarding_pass_qr_svg}`} alt="" /> */}
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <Button type="primary" className="bg-purple-button w-auto" onClick={()=>{resetForm(); setIsHealthModalShow(true)}}>Re-Submit Health Declaration</Button>
                    </div>
                </Content>

            {/* <FirstDiv>
                <h5>Boarding Pass (Web Check-In)</h5>
                <nav>
                    <li>Search<span><ShareIcon style={{"color":"#500E4B","fontSize":"large","marginLeft":"2px"}}/></span> </li>
                    <li>Print<span><PrintIcon style={{"color":"#500E4B","fontSize":"large","marginLeft":"2px"}}/></span></li>
                    <li>Download <span><GetAppIcon style={{"color":"#500E4B","fontSize":"large","marginLeft":"2px"}}/></span></li>
                </nav>
                
            </FirstDiv>
            <HeadingBanner>
                <h5>Trip Information</h5>
            </HeadingBanner>
            <ThirdDiv>
                <TripInfo/>
            </ThirdDiv> */}
            <HeadingBanner>
                <h5>COVID 19 related Travel Advisory:</h5>
            </HeadingBanner>
            <FifthDiv>
                <ul className={styles.list}>
                    {/* <li><strong>Kindly carry QR Code Negative RT-PCR test report not earlier than 48 hours of Departure</strong></li>
                    <li> Cordelia Cruises may for any reason at any time and without prior notice, cancel, advance, postpone or deviate from any scheduled sailing, port of call, destination, lodging or any activity on or off the Vessel, or change port of call, destination, lodging or activity in case of any actual or suspected COVID-19 infection, and except where a refund is required by law as a result of a declaration of a public health emergency or government order cancelling the Cruise or delaying boarding of the Vessel by Passengers by 24 hours or more. Cordelia Cruises shall not be liable for any claim whatsoever by Passenger, including but not limited to loss, compensation or refund, by reason of such cancellation, advancement, postponement, substitution or deviation.</li>
                    <li> Cordelia Cruises shall have the right to comply with any orders, recommendations, or directions whatsoever given by any State/Central governmental entity or by persons purporting to act with such authority and such compliance shall not be deemed a breach of the Passenger Cruise Ticket Contract entitling the Passenger to assert any claim for liability, compensation or refund except as provided and required by law as a result of a declaration of a public health emergency or government order cancelling the Cruise or delaying boarding of the Vessel by Passengers by 24 hours or more. Cordelia Cruises shall not be liable for any costs in respect of impromptu medical tests or quarantine as prescribed by the state/ central administration during embarkation and disembarkation a port calls.</li>
                    <li> Guests are highly recommended to read and understand all travel advisories published by state/ central administration prior to embarkation and disembarkation to avoid any disappointments"</li> */}
                    <li>All guests above 18 years need to have a mandatory double vaccination certificate & a negative RT-PCR report, 48 hours prior to sailing and should be ICMR approved with the QR code.</li>
                    <li>All guests below18 years need to have a mandatory negative RT-PCR report, 48 hours prior to sailing and should be ICMR approved with the QR code.</li>
                    <li>All guests below 5 years will be exempted from the RT-PCR reports. </li>
                    <li>Pregnant women are only allowed to sail if pregnant for 24 completed weeks or less at timeof cruise disembarkation.</li>
                    <li>Infant must have an ID with photo, also authorization of either parent if travelling with only one parent</li>
                    <li>Please fill in the attached Health Declaration form and keep it ready for verification in the terminal.</li>
                </ul>
            </FifthDiv>   
        </Container>
        <MobileContainer>
            <div className="container">
                <div className="row align-items-end">
                    <div className="col"><h4>Boarding Pass</h4></div>
                    <div className="col"><h6 className="font-weight-bold pl-4">Booking ID: <br/>{props.refId}</h6></div>
                </div>
                
            </div>
            <MobileCard>

            <HeadingBanner className={styles.BorderRadius}>
                <div class="container">
                 <div className="row">
                    <div className="col-6"><h5>Trip Information</h5></div>
                    <div className="col">
                        <div className="row ml-4 align-items-center justify-content-around">
                            <ShareAltOutlined onClick={()=>setIsModalShow(true)}/>
                            <PrinterOutlined onClick={()=>window.open(props.passData.boarding_pass_doc_url)}/>
                            <DownloadOutlined onClick={()=>window.open(props.passData.boarding_pass_doc_url)}/>
                        </div>
                    </div>
                </div>
                </div>
                </HeadingBanner>
                <Content>
                    <div class="container">
                        <div className="row mb-4">
                            <div className="col-6">
                                <p className="font-big">Group {props.guestData.group}</p>
                                <p className="mb-4"><span className="font-big">{props.guestData.embarkation_slot}</span></p>
                                <h6 >Guest Name</h6>
                                <p>{props.guestData.name}</p>
                                <h6 className="mt-4">Ship Name</h6>
                                <p>Empress</p>
                            </div>
                            <div className="col-6">
                                <p className="qrCode">Scan QR Code</p>
                                <div className={styles.qrMobileTranform} dangerouslySetInnerHTML={{__html: props.passData.boarding_pass_qr_svg}} />
                                {/* <img src={props.passData.boarding_pass_qr_svg} alt="" /> */}
                            </div>
                        </div>

                        <div className="row mb-4">
                            <div className="col-12">
                                <h6>Itinerary</h6>
                                <p>{props.itineraryData.nights} Nights</p>
                                <p>{props.itineraryData.name}</p>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col-6">
                                <h6>Departure</h6>
                                <p>{props.itineraryData.ports[0].city}</p>
                                <p>{ moment(props.itineraryData.start_time).format("ddd, D-MMM-YYYY")}</p>
                                <p>{ moment(props.itineraryData.start_time).format("hh:mm A")}</p>
                            </div>
                            <div className="col-6 pl-2">
                                <h6>Arrival</h6>
                                <p>{props.itineraryData.ports[props.itineraryData.ports.length - 1].city}</p>
                                <p>{ moment(props.itineraryData.end_time).format("ddd, D-MMM-YYYY")}</p>
                                <p>{ moment(props.itineraryData.end_time).format("hh:mm A")}</p>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col-12">
                                <div className="row align-items-baseline px-3"><h6 className="mb-0">Cabin Category:&nbsp;&nbsp;</h6><p>{props.roomData.category}</p></div>
                                <div className="row align-items-baseline px-3"><h6 className="mb-0">Deck No:&nbsp;&nbsp;</h6><p>{props.roomData.deck_no}</p></div>
                                <div className="row align-items-baseline px-3"><h6 className="mb-0">Cabin No:&nbsp;&nbsp;</h6><p>{props.roomData.number}</p></div>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <Button type="primary" className="bg-purple-button w-auto" onClick={()=>{resetForm(); setIsHealthModalShow(true)}}>Re-Submit Health Declaration</Button>
                        </div>
                    </div>
                </Content>
            <HeadingBanner>
                <div class="container">
                    <h5>COVID 19 related Travel Advisory:</h5>
                </div>
            </HeadingBanner>
            <FifthDiv>
                <ul className={styles.list}>
                    <li><strong>Kindly carry QR Code Negative RT-PCR test report not earlier than 48 hours of Departure</strong></li>
                    <li> Cordelia Cruises may for any reason at any time and without prior notice, cancel, advance, postpone or deviate from any scheduled sailing, port of call, destination, lodging or any activity on or off the Vessel, or change port of call, destination, lodging or activity in case of any actual or suspected COVID-19 infection, and except where a refund is required by law as a result of a declaration of a public health emergency or government order cancelling the Cruise or delaying boarding of the Vessel by Passengers by 24 hours or more. Cordelia Cruises shall not be liable for any claim whatsoever by Passenger, including but not limited to loss, compensation or refund, by reason of such cancellation, advancement, postponement, substitution or deviation.</li>
                    <li> Cordelia Cruises shall have the right to comply with any orders, recommendations, or directions whatsoever given by any State/Central governmental entity or by persons purporting to act with such authority and such compliance shall not be deemed a breach of the Passenger Cruise Ticket Contract entitling the Passenger to assert any claim for liability, compensation or refund except as provided and required by law as a result of a declaration of a public health emergency or government order cancelling the Cruise or delaying boarding of the Vessel by Passengers by 24 hours or more. Cordelia Cruises shall not be liable for any costs in respect of impromptu medical tests or quarantine as prescribed by the state/ central administration during embarkation and disembarkation a port calls.</li>
                    <li> Guests are highly recommended to read and understand all travel advisories published by state/ central administration prior to embarkation and disembarkation to avoid any disappointments"</li>
                </ul>
            </FifthDiv>  
            </MobileCard>
        </MobileContainer>
        <Modal show={isModalShow} onClose={()=>setIsModalShow(false)}>
                        <div className="card-body p-0 position-relative">
                        <div className="modal-close modalmap-close mt-2">
                            <CloseOutlined
                            style={{ color: "#EA725B",fontSize: "18pt" }}
                            onClick={()=>setIsModalShow(false)}
                            />
                        </div>
                        <div className={cx("modal-content p-0 px-md-5 m-0 pt-4",styles.shareModal)}>
                            <div class="p-3  text-center">
                                <h6>Share</h6>
                                <p className="mb-0">Enter the email ID you would like to share the boarding pass</p>
                                <input type="email" name="email" id="email" placeholder="Enter the email ID" className={styles.otherInput} onChange={(e)=>setEmail(e.target.value)} defaultValue={email}/>
                                {!valid&&<p className="mb-0 text-danger">Enter a valid Email ID</p>}
                                {error&&<p className="mb-0">{error}</p>}
                                <div className="row my-3 mt-md-2 mb-md-3">
                                <div className="mx-auto">
                                    <Button type="primary" className="bg-purple-button" 
                                        onClick={()=>share()} 
                                        // icon={<ShareIcon className="d-none d-md-inline-block"/>}
                                        >
                                            {loading?<div class="spinner-border spinner-border-sm text-light" role="status"><span class="sr-only">Loading...</span></div>:"Share"}
                                    </Button></div>
                                </div>
                            </div>
                        </div>
                        </div>
                </Modal>
                <Modal show={isHealthModalShow} onClose={()=>setIsHealthModalShow(false)}>
                        <div className="card-body p-0 position-relative">
                        <div className="modal-close modalmap-close mt-2">
                            <CloseOutlined
                            style={{ color: "#EA725B",fontSize: "18pt" }}
                            onClick={()=>setIsHealthModalShow(false)}
                            />
                        </div>
                        <div className={cx("modal-content p-0 m-0 pt-4",styles.healthDeclaration)}>
                            <div class="p-3">
                            <h6>Health Declaration Form</h6>
                            <p>In the last 8 days before your journey, have you had any of the symptoms? (Please mark yes or no against each symptom)</p>
                            <div className="row mb-3">
                                <div className="col-9 col-md-10"></div>
                                <div className="col-1"><h5>Yes</h5></div>
                                <div className="col-1"><h5>No</h5></div>
                            </div>
                            <ol>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>Have you recently developed cough (dry or productive)</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion1("Yes")}} checked={question1==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion1("No")}} checked={question1==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li> Fever (or feeling feverish)</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion2("Yes")}} checked={question2==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion2("No")}} checked={question2==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li> General weakness</li>
                            </div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion3("Yes")}} checked={question3==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion3("No")}} checked={question3==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>Generalized muscle ache</li>
                            </div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion4("Yes")}} checked={question4==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion4("No")}} checked={question4==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>Sudden loss of smell and/or taste</li>
                            </div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion5("Yes")}} checked={question5==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion5("No")}} checked={question5==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>Any respiratory distress</li>
                            </div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion6("Yes")}} checked={question6==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion6("No")}} checked={question6==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>In the last 14 days before your journey, were you in contact with anyone diagnosed with COVID-19 inspection?</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion7("Yes")}} checked={question7==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion7("No")}} checked={question7==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>In the last four hours before temperature check, have you consumed antipyretics or other analgesics?</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion8("Yes")}} checked={question8==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion8("No")}} checked={question8==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-11"> <li>In the last 14 days before your journey, list the cities and countries you have visited and indicate the duration of your stay in each one;</li></div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-md-3">
                                    <input type="text" name="aadhar" id="place" placeholder="Enter Place" className={styles.otherInput} onChange={(e)=>{setPlace(e.target.value)}} defaultValue={place}/>
                                </div>
                                <div className="col-6 col-md-3">
                                    <DatePicker bordered={false} className={cx("ShowIcon Pad-right-placeholder",styles.dateInput)} placeholder="Date of Arrival" onChange={(date, dateString)=>{setArival(dateString)}} defaultValue={arival?moment(arival):null}/>
                                </div>
                                <div className="col-6 col-md-3 d-none d-md-block">
                                    <DatePicker bordered={false} className={cx("ShowIcon Pad-right-placeholder",styles.dateInput)} placeholder="Date of Departure" onChange={(date, dateString)=>{setDeparture(dateString)}} defaultValue={departure?moment(departure):null}/>
                                </div>
                                <div className="col-6 col-md-3 d-md-none">
                                    <DatePicker bordered={false} className={cx("ShowIcon Pad-right-placeholder",styles.dateInput)} placeholder="Date of Dept" onChange={(date, dateString)=>{setDeparture(dateString)}} defaultValue={departure?moment(departure):null}/>
                                </div>
                                <div className="col-3">

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we am/are not residing in any containment zone.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion10("Yes")}} checked={question10==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion10("No")}} checked={question10==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we am are not under quarantine.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion11("Yes")}} checked={question11==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion11("No")}} checked={question11==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we develop any of the above-mentioned symptoms I shall contact the concerned health authorities, immediately.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion12("Yes")}} checked={question12==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion12("No")}} checked={question12==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we have not tested COVID-19 positive in last two months.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion13("Yes")}} checked={question13==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion13("No")}} checked={question13==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I /we are eligible to travel as per the extant norms.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion14("Yes")}} checked={question14==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion14("No")}} checked={question14==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we make my mobile number/contact details available to the cruise lines whenever required by them</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion15("Yes")}} checked={question15==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion15("No")}} checked={question15==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-9 col-md-10"> <li>I/we undertake to adhere to the health protocol prescribed by the destination State/UT.</li></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion16("Yes")}} checked={question16==="Yes"}/></div>
                                <div className="col-1"><input type="checkbox" name="guest" id="guest" onClick={()=>{setQuestion16("No")}} checked={question16==="No"}/></div>
                            </div>
                            <div className="row">
                                <div className="col-12"> <li>Assessment Section</li></div>
                                <div className="col-12 col-md-4"><label>Temperature Check</label> 
                                <input type="text" name="temparature" id="temparature" className={styles.otherInput} onChange={(e)=>setTemperature(e.target.value)} defaultValue={temperature}/></div>
                                <div className="col-12 col-md-7 d-none d-md-block"><label>Other Vital Signs</label>
                                <input type="text" name="vital" id="vital" className={styles.otherInput} onChange={(e)=>setVital(e.target.value)} defaultValue={vital}/>
                                </div>
                                <div className="col-12 col-md-7 d-md-none"><label>Other Vital Signs</label>
                                <textarea type="text" name="vital" id="vitalMobile" className={styles.otherInput} onChange={(e)=>setVital(e.target.value)} defaultValue={vital}/>
                                </div>
                            </div>
                            </ol>
                            <div className="px-3">
                            <h4>Assessment Decision</h4>
                            <div className={cx("font-italic",styles.terms)}><input type="checkbox" name="assessment" id="assessment" onChange={(e)=>{e.target.checked?setQuestion17("Yes"):setQuestion17("No")}} checked={question17==="Yes"}/>&nbsp; &nbsp; I Accept the <a href="/tnc" target="_blank">terms & conditions</a></div>
                            </div>
                            <div className="row my-5 mt-md-2 mb-md-5">
                            <div className="col-6 text-md-right"><Button type="primary" 
                            className="outline-purple-button w-xs-100" 
                            // onClick={()=>{resetForm()}}
                            >CLEAR ALL</Button></div>
                            <div className="col-6"><Button type="primary" className="bg-purple-button w-xs-100" onClick={()=>saveGuestDeclaration()} disabled={question17==="Yes"?false:true}>{loading?<div class="spinner-border spinner-border-sm text-light" role="status"><span class="sr-only">Loading...</span></div>:"SUBMIT"}</Button></div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </Modal>
                    <Modal show={isSuccessModalShow} onClose={()=>setIsSuccessModalShow(false)}>
                        <div className="card-body p-0 position-relative">
                        <div className="modal-close modalmap-close mt-2">
                            <CloseOutlined
                            style={{ color: "#EA725B",fontSize: "18pt" }}
                            onClick={()=>setIsSuccessModalShow(false)}
                            />
                        </div>
                        <div className={cx("modal-content p-0 m-0 pt-4",styles.successfulModal)}>
                            <div class="p-3">
                                <h6>Successful</h6>
                                <p>You have succesfully submitted <em className="text-primary font-normal">Health Declaration</em> Form, Please keep copy of the same at time of sailing</p>
                                <div className="row my-3 mt-md-2 mb-md-3">
                                <div className="col-12 col-md-6 mb-3 md-md-0 text-right"><Button type="primary" className="bg-purple-button" onClick={()=>{window.open(declarationDoc)}} icon={<DownloadOutlined className="d-none d-md-inline-block"/>}>Download Form</Button></div>
                                <div className="col-12 col-md-6"><Button type="primary" className="bg-purple-button" onClick={()=>{window.open(declarationDoc)}} icon={<PrinterOutlined className="d-none d-md-inline-block" />}>Print Form</Button></div>
                                </div>
                            </div>
                        </div>
                        </div>
                </Modal>
        </>
    )
}

export default Details;

const Container = styled.div`
height: auto;
padding:30px;
width:95%;
margin:auto;
margin-top:50px;
margin-bottom:100px;
border: 0.3px solid rgba(80, 14, 75, 0.5);
box-sizing: border-box;
border-radius: 10px;
background:white;
@media screen and (max-width:1200px){
     justify-content: center;
   margin-bottom:20px;
   

}
@media screen and (max-width:600px){
    
    justify-content: center;
   margin-bottom:20px;
}

@media screen and (max-width:450px){
    display:none;
}

`;

const MobileContainer = styled.div`
h4{
    font-family: "Roboto-Medium", sans-serif;
    color: #500E4B;
    font-weight: 500;
    font-size: 18px;
}
@media screen and (min-width:450px){
    display:none;
}
`;

const MobileCard = styled.div`
    margin: 20px;
    background: #FFFFFF;
    box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.05);
    border-radius: 5px;
`;


const HeadingBanner = styled.div`
padding: 8px;
background: rgba(80, 14, 75, 0.6);
width:100%;
h5{
font-weight: 500;
font-size: 16px;
color:white;
margin-bottom:0px;
}
@media screen and (max-width:450px){
    padding: 10px 0px;
    span{
        color:white;
        font-size:18px;
        padding-left:5px;
        padding-right:5px
    }
    
}   

`;

const Heading = styled.div`
    width:100%;
    h5{
        font-family: "Roboto-Medium";
        font-weight: 500;
        font-size: 18px;
        color: #500E4B;
        margin-bottom:0px;
    }
    p{
        font-size: 16px;
        color: #500E4B;
        padding-right:10px;
        align-items: center;
        display: flex;
    }
    span{
        color: #500E4B;
        font-size: 15px;
        padding-left:10px;
        padding-right:10px
    }
`;
const Content = styled.div`
    padding: 30px;
    h6{
        font-size: 14px;
        color: #535353;
    }
    p{
        font-family: "Roboto-Medium";
        font-weight: 500;
        font-size: 18px;
        color: #500E4B;
        margin-bottom:0px;
    span{
        font-size: 18px;
        color: #535353;
    }
    }
    p.font-big,span.font-big{
        font-size: 20px;
    }
    @media screen and (max-width:450px){
        padding: 20px 0px 10px;
        p.qrCode{
            font-size: 14px;
            margin-bottom: 5px;
            text-align: center;
        }
    }
`;

const FifthDiv = styled.div`
    padding: 40px 10px 10px;
   li{
    font-size: 14px;
    font-style: italic;
    color: #535353;
   }
   li::marker{
    padding-bottom:10px
   }
   @media screen and (max-width:450px){
    padding: 20px 0px;
}

`;


