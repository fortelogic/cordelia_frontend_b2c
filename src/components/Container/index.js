import React from 'react';

const Container = (props) => {

    const [isMobile, setMobile] = React.useState(false);
    const { strict = false } = props;

    React.useEffect(() => {
        _isMobileScreen()
        window.addEventListener("resize", _isMobileScreen)
        return () => {
            window.removeEventListener("resize", _isMobileScreen)
        }
    }, [])

    const _isMobileScreen = () => {
        if (window.innerWidth <= 768) {
            setMobile(true);
            return;
        }
        setMobile(false);
    };

    return (
        <div className={`${props.wrapperClass ? props.wrapperClass : ""}`}>
            <div className={`${!isMobile || strict ? "container mx-auto" : ""} ${props.innerClass ? props.innerClass : ""}`}>
                {props.children}
            </div>
        </div>
    );
}

export default Container;