import { useState, useEffect } from "react"
import { useHistory, Link, Router } from "react-router-dom"
import Selection from "./pagecontent/Selection"
import Card from "./pagecontent/Card"
import "./room-selection.css"
import { createbooking } from "../../services/createbooking"
import { useDispatch, useSelector } from "react-redux"
import { bookingsummary } from "../../redux/actions/roomAction"
import LoadingIcon from "../Loading/LoadingIcon"
import localStore from "../../utils/localStore";

const Pagecontent = (props) => {
    const { selectedCabin, queryParams } = props
    const [loading, setLoading] = useState(false)
    const history = useHistory()
    const allcabins = useSelector(state => state.allcabins)
    const dispatch = useDispatch()

    const roomssummary = async () => {

        setLoading(true)
        let rooms = []
        let itineraryID = ''

        allcabins.map((itemup, indexup) => {
            rooms.push({ priceKey: itemup.detail.priceKey, roomId: "" })
            itemup.room && itemup.room.map((itemdown, indexdown) => {
                itineraryID = itemdown ? itemdown.itineraryID : ''
                rooms[indexup]["roomId"] = itemdown.itinerary_room_id
            })
        })

        if (queryParams.get("from")) {
            if (queryParams.get("from") === "booking-success") {
                const booking = localStore.get("bookingSummary")
                itineraryID = booking.itinerary.id
            }
        }

        if (itineraryID) {
            if (queryParams.get("from")) {
                if (queryParams.get("from") === "booking-success") {
                    const response = await createbooking(rooms, itineraryID, "staging")
                    dispatch(bookingsummary(response.data))
                    setLoading(false)
                }
            } else {
                const response = await createbooking(rooms, itineraryID)
                dispatch(bookingsummary(response.data))
                setLoading(false)
            }
            // props.platform === "true" ? history.push('/booking-summary?platform=true') : history.push('/booking-summary')


            let rem = ''
            for (let i = 0; i < allcabins.length; i++) {
                rem += allcabins[i].name.split(" ").join("_") + '_'
                rem += 'A_' + allcabins[i].guest.adults + '_'
                rem += 'C_' + allcabins[i].guest.children + '_'
                rem += 'I_' + allcabins[i].guest.infants + '_'
                rem += 'deck_' + allcabins[i].deck.id + "_"
                rem += 'room_' + allcabins[i].room[0].roomno
            }


            if (queryParams.get("from")) {
                if (queryParams.get("from") === "booking-success") {

                    const booking = localStore.get("bookingSummary")
                    itineraryID = booking.booking.itinerary_id

                    queryParams.get("platform") === "true" ?
                        history.push(`/guest-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&from=booking-success&platform=true`) :
                        history.push(`/guest-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&from=booking-success`)

                    // queryParams.get("platform") === "true" ?
                    //     history.push(`/booking-summary?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&from=booking-success&platform=true`) :
                    //     history.push(`/booking-summary?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&from=booking-success`)
                }
            } else {
                queryParams.get("platform") === "true" ?
                    history.push(`/booking-summary?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&platform=true`) :
                    history.push(`/booking-summary?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}`)
            }
        }
        else {
            setLoading(false)
        }
    }

    const checkCabins = () => {
        let res = false;
        allcabins.map((item) => {
            if (!item.hasOwnProperty('room')) {
                res = true;
            }
        })
        return res;
    }

    return (
        <div className="container">

            {!loading ? (
                <div className="row">
                    <div className="col-lg-6 col-12">
                        <Selection selectedCabin={selectedCabin} />
                    </div>

                    <div className="col-lg-6 col-sm-12 col-12 card-box">
                        <Card selectedCabin={selectedCabin} />
                    </div>
                </div>

            ) : (
                <LoadingIcon />
            )}

            <div className="d-flex justify-content-center my-4">
                <button className="btn view-summary" disabled={checkCabins()} onClick={roomssummary}>
                    View Summary
                </button>
            </div>
        </div>
    )
}

export default Pagecontent
