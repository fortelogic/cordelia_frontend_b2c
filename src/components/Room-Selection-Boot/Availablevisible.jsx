import "./room-selection.css"

const Availablevisible = () => {
    return (
        
        <div className="d-flex flex-row justify-content-center align-items-baseline w-100 col-lg-7 col-md-12 mb-3">

            <div className="box-show border-magenta"></div>
            <p className="ml-2 mr-4">Available</p>

            <div className="box-show border-1 box-not-available"></div>
            <p className="ml-2 mr-4">Not Available</p>

            <div className="box-show border-1 box-selected"></div>
            <p className="ml-2 mr-4">Selected</p>

        </div>
    )
}

export default Availablevisible