import { useState, useEffect, Fragment } from "react"

import { getRooms } from "../../../redux/actions/roomAction"

import { useDispatch, useSelector } from "react-redux"
import { addRoom, removeRoom } from "../../../redux/actions/cabinAction"
import localStore from "../../../utils/localStore";

const Selection = (props) => {
    const { selectedCabin } = props
    const [selection, setSelection] = useState(99999)
    const [current, setCurrentCabin] = useState({})
    const dispatch = useDispatch()
    let cabins = localStore.get("cabins")
    let itineraryID = localStore.get("itineraryID")

    const queryParams = new URLSearchParams(window.location.search)

    const toggleButton = (i, roomno, itinerary_room_id, status, current = null) => {
        if (selection != i) {
            setSelection(i)
            setCurrentCabin({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id, selectedCabin: selectedCabin.id, deckId: selectedCabin.deck.id })
        } else {
            setCurrentCabin({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id, selectedCabin: selectedCabin.id, deckId: selectedCabin.deck.id })
            setSelection(99999)
        }

        // status === false ? dispatch(selectRooms({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id })) : dispatch(removeRooms({ roomno: roomno, itinerary_room_id: itinerary_room_id }))
        selection != i ? dispatch(addRoom({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id, selectedCabin: selectedCabin.id, deckId: selectedCabin.deck.id })) : dispatch(removeRoom({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id, selectedCabin: selectedCabin.id, deckId: selectedCabin.deck.id }))
    }

    let z = 0;
    const [rows, setRows] = useState(0)
    const [rooms, setRooms] = useState([])
    const [roomsData, setRoomsData] = useState([])


    useEffect(async () => {

        if (queryParams.get("from")) {
            if (queryParams.get("from") === "booking-success") {
                const booking = localStore.get("bookingSummary")

                // const response = await dispatch(getRooms({ deckId: selectedCabin.deck.id, itineraryID: booking.itinerary.id, beds: selectedCabin.guest.total, category_id: selectedCabin.detail.category_id }, "staging"))
                const response = await dispatch(getRooms({ deckId: selectedCabin.deck.id, itineraryID: booking.itinerary.id, beds: selectedCabin.guest.total, category_id: selectedCabin.detail.category_id }, "staging"))
                setRoomsData(response.payload)
            }
        } else {
            const response = await dispatch(getRooms({ deckId: selectedCabin.deck.id, itineraryID: itineraryID, beds: selectedCabin.guest.total, category_id: selectedCabin.detail.category_id }))
            setRoomsData(response.payload)
        }

        for (let i = 0; i < cabins.length; i++) {
            if (cabins[i].id === selectedCabin.id) {
                cabins[i].room ? setCurrentCabin(cabins[i].room[0]) : setCurrentCabin({})
            }
        }
    }, [selectedCabin])

    useEffect(async () => {

        if (roomsData) {
            let rowLength = roomsData.reduce((acc, room) => acc = acc > room.y ? acc : room.y, 0);

            setRows(rowLength)

            setRooms(roomsData)
        }

    }, [roomsData])

    const checkIfSelected = () => {
        return false;
    };

    return (
        <>

            <div className="col-md-11 pl-sm-5 ml-sm-5 rooms-container">

                <div className="px-lg-5 position-relative grid-parent col-12">
                    <div className="curve"></div>
                    {
                        z = 0,
                        Array(rows).fill(0).map((items, y) => {
                            return (
                                <div key={y} className="row justify-content-center mb-1">
                                    {
                                        Array(5).fill(0).map((item, x) => {
                                            let obj = rooms.find(o => o.x === x && o.y === y);
                                            let temp = 0;
                                            temp = z;
                                            obj && z++;
                                            return (
                                                obj ? <button
                                                    key={item.itinerary_room_id}
                                                    onClick={() => toggleButton(temp, obj.number, obj.itinerary_room_id, selection, current)}
                                                    disabled={!obj.available}
                                                    className={`border-magenta ${obj.available ? current && (current.deckId == selectedCabin.deck.id && obj.itinerary_room_id === current.itinerary_room_id) ? `box-selected text-white` : `bg-white text-magenta` : `box-not-available text-white`} selection-button col-2 pl-1 pr-1 text-sm`}>
                                                    {obj.number}
                                                </button>
                                                    :
                                                    <div className="selection-button col-2  pl-1 pr-1"></div>)
                                        })
                                    }
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
}

export default Selection
