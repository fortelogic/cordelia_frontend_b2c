import User from "../../../assets/img/room-selection/user.png"
import Dinner from "../../../assets/img/room-selection/dinner.png"
import Ruler from "../../../assets/img/room-selection/ruler.png"
import Ellipse from "../../../assets/icons/ellipse.svg"
import Confetti from "../../../assets/img/room-selection/confetti.png"

const Card = (props) => {
    const { selectedCabin } = props
    console.log("Selected Ccabin", selectedCabin)
    return (
        <div className="interior-card shadow-lg border p-2 mt-lg-0 mt-5">
            <h5 className="mb-4 mt-3"><b>{selectedCabin.name}</b></h5>
            <div className="d-flex row justify-content-start col-lg-12 p-0 ml-1">
                <div className="col-md-6 p-0 pr-lg-1">
                    <img className="interior-img" src={selectedCabin.detail.hasOwnProperty("imageList") ? selectedCabin.detail.imageList[0] ? selectedCabin.detail.imageList[0] : '' : selectedCabin.detail.images[0] ? selectedCabin.detail.images[0] : ''} />
                </div>
                <div className="col-md-6 p-0 pl-lg-1 pt-3 pt-lg-0 pt-sm-0">
                    <img className="interior-img" src={selectedCabin.detail.hasOwnProperty("imageList") ? selectedCabin.detail.imageList[1] ? selectedCabin.detail.imageList[1] : '' : selectedCabin.detail.images[1] ? selectedCabin.detail.images[1] : ''} />
                </div>
            </div>
            <p className="mt-4">{selectedCabin.detail ? selectedCabin.detail.description : null}</p>

            <div className="d-flex row justify-content-start col-lg-12 p-0 ml-1 mt-4">
                <div className="col-md-6 p-0">
                    <p><b>ROOM FEATURE</b></p>
                    {selectedCabin.detail ?
                        selectedCabin.detail.feature2 ?
                            <div className="col-md-12 flex row p-0">
                                <img className="icons-img ml-3 mr-2" src={User} />
                                <span>{selectedCabin.detail.feature2}</span>
                            </div> : null

                        : null}
                    {selectedCabin.detail ?
                        selectedCabin.detail.feature1 ?
                            <div className="col-md-12 flex row p-0">
                                <img className="icons-img ml-3 mr-2" src={Ruler} />
                                <span>{selectedCabin.detail.feature1}</span>
                            </div> : null
                        : null}
                </div>

                <div className="col-md-6 p-0 mt-md-0 mt-lg-0 mt-4">

                    <p><b>INCLUSIONS</b></p>
                    {selectedCabin.detail && selectedCabin.detail.inclusions.map((val, i) =>{
                        return(
                            <div className="col-md-12 flex row">
                            <img className="icons-img mr-3" style = {{width:5}} src={Ellipse} />
                            <p className="inc-detail">{val}</p>
                        </div>
                        )
                    })}
                </div>
            </div>

           
        </div>
    )
}

export default Card
