import { useState, useEffect, Fragment } from "react"

import { getRooms } from "../../../redux/actions/roomAction"

import { useDispatch, useSelector } from "react-redux"
import { addRoom, removeRoom } from "../../../redux/actions/cabinAction"
import localStore from "../../../utils/localStore";
import useScrollPercentage from "../../../utils/useScrollPercentage";

function buildLayoutGrid(rooms) {
    let gridmap = {};
    let maxX = 0;
    let maxY = 0;
    rooms.forEach(function (room) {
        if (!gridmap[room.y]) {
            gridmap[room.y] = {};
        }
        gridmap[room.y][room.x] = room;

        if (room.y > maxX) maxX = room.y;
        if (room.x > maxY) maxY = room.x;
    });
    let gridString = [new Array(maxY + 1).fill("tri").join(" "),
    new Array(maxY + 1).fill("dck").join(" "),
    new Array(maxY + 1).fill("pre").join(" "),
    new Array(maxY + 1).fill("exp").join(" ")];
    let roomNo = 1;
    for (let x = 0; x <= maxX; x++) {
        let gridLine = "";
        for (let y = 0; y <= maxY; y++) {
            if (gridLine != "") {
                gridLine += " "
            }
            if (gridmap[x] && gridmap[x][y]) {
                if (gridmap[x][y].number.indexOf('lift') != -1) {
                    gridLine += ("lift");
                } else {
                    gridLine += ("r" + x + y);
                }
            } else {
                gridLine += ".";
            }
        }
        gridString.push(gridLine);
    }
    return gridString;
}

const Selection = (props) => {
    const { selectedCabin } = props
    const [selection, setSelection] = useState(99999)
    const [current, setCurrentCabin] = useState({})
    const [scrollRef, scrollPercentage] = useScrollPercentage()

    const [selectedRooms, setSelectedRooms] = useState([])

    const allcabins = useSelector((state) => state.allcabins)
    const dispatch = useDispatch()
    let cabins = localStore.get("cabins")
    let itineraryID = localStore.get("itineraryID")

    const queryParams = new URLSearchParams(window.location.search)

    const toggleButton = (i, roomno, itinerary_room_id) => {
        if (selection != i) {
            setSelection(i)
            setCurrentCabin({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id, selectedCabin: selectedCabin.id, deckId: selectedCabin.deck.id })
        } else {
            setCurrentCabin({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id, selectedCabin: selectedCabin.id, deckId: selectedCabin.deck.id })
            setSelection(99999)
        }

        // status === false ? dispatch(selectRooms({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id })) : dispatch(removeRooms({ roomno: roomno, itinerary_room_id: itinerary_room_id }))
        selection != i ? dispatch(addRoom({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id, selectedCabin: selectedCabin.id, deckId: selectedCabin.deck.id })) : dispatch(removeRoom({ itineraryID: itineraryID, roomno: roomno, itinerary_room_id: itinerary_room_id, selectedCabin: selectedCabin.id, deckId: selectedCabin.deck.id }))
    }

    let z = 0;
    const [rows, setRows] = useState(0)
    const [rooms, setRooms] = useState([])
    const [roomsData, setRoomsData] = useState([])


    useEffect(async () => {

        if (queryParams.get("from")) {
            if (queryParams.get("from") === "booking-success") {
                const booking = localStore.get("bookingSummary")
                // const response = await dispatch(getRooms({ deckId: selectedCabin.deck.id, itineraryID: booking.itinerary.id, beds: selectedCabin.guest.total, category_id: selectedCabin.detail.category_id }, "staging"))
                const response = await dispatch(getRooms({ deckId: selectedCabin.deck.id, itineraryID: booking.booking.itinerary_id, beds: selectedCabin.guest.total, category_id: selectedCabin.detail.category_id }, "staging"))
                setRoomsData(response.payload)
            }
        } else {
            const response = await dispatch(getRooms({ deckId: selectedCabin.deck.id, itineraryID: itineraryID, beds: selectedCabin.guest.total, category_id: selectedCabin.detail.category_id }))
            setRoomsData(response.payload)
        }

        for (let i = 0; i < cabins.length; i++) {
            if (cabins[i].id === selectedCabin.id) {
                cabins[i].room ? setCurrentCabin(cabins[i].room[0]) : setCurrentCabin({})
            }
        }
    }, [selectedCabin])


    // useEffect(() => {
    //     for (let i = 0; i < allcabins.length; i++) {
    //         if (allcabins[i].hasOwnProperty("room")) {
    //         }
    //     }
    // }, [selectedCabin])

    const checkIfAvailable = (roomno) => {
        let res = []
        allcabins.map(data => {
            if (data.id != selectedCabin.id && data.detail.category_id === selectedCabin.detail.category_id && data.deck.description === selectedCabin.deck.description) {
                console.log('hre 1')
                if (data.hasOwnProperty('room')) {
                    console.log('here 2')
                    if (data.room[0].roomno == roomno) {
                        console.log('here 3')
                        res.push(true)
                    }
                }
            }
        })
        return res.includes(true)
    }


    useEffect(async () => {
        // checkIfAvailable(3509)
        if (roomsData) {
            let rowLength = roomsData.reduce((acc, room) => acc = acc > room.y ? acc : room.y, 0);

            setRows(rowLength)

            setRooms(roomsData)
        }

    }, [roomsData])

    const checkIfSelected = () => {
        return false;
    };

    return (
        <>
            <div className="col-md-11 pl-sm-5 ml-sm-5 rooms-container position-relative" ref={scrollRef}>
                <div className="px-lg-5 position-relative grid-parent col-12">
                    <div className="curve"></div>
                    {/* <> */}
                    <div className="container-room" style={{ gridTemplateAreas: "'" + buildLayoutGrid(roomsData).join("' '") + "'" }}>
                        {roomsData.map((room, index) => (
                            // <div
                            //     // className={`border-magenta ${room.available ? current && (current.deckId == selectedCabin.deck.id && room.itinerary_room_id === current.itinerary_room_id) ? `box-selected text-white` : `bg-white text-magenta` : `box-not-available text-white`} selection-button col-2 pl-1 pr-1 text-sm`}
                            //     style={{ gridArea: `r${room.y}${room.x}` }}
                            // // {/* onClick={() => room.selection && onClick()} */}
                            // >
                            //     {room.number}
                            // </div >

                            /*** Sample ***/
                            // <button
                            //     className={`border-magenta ${room.available ? current && (current.deckId == selectedCabin.deck.id && room.itinerary_room_id === current.itinerary_room_id) ? `box-selected text-white` : `bg-white text-magenta` : `box-not-available text-white`} selection-button text-sm`}
                            //     style={{ gridArea: `r${room.y}${room.x}` }}
                            //     onClick={() => { toggleButton(index, room.number, room.itinerary_room_id) }}
                            // // {/* onClick={() => room.selection && onClick()} */}
                            // >
                            //     {room.number}
                            // </button>

                            <button
                                className={`border-magenta selection-button text-sm ${checkIfAvailable(room.number) ? 'not-available-for-other-cabin' : room.available ? current && (current.deckId == selectedCabin.deck.id && room.itinerary_room_id === current.itinerary_room_id) ? `box-selected text-white` : `bg-white text-magenta` : `box-not-available text-white`} `}
                                style={{ gridArea: `r${room.y}${room.x}` }}
                                onClick={() => { toggleButton(index, room.number, room.itinerary_room_id) }}
                            >
                                {room.number}
                            </button>
                        ))}

                        {console.log("allcabins", allcabins)}
                    </div>
                    {/* {
                        z = 0,
                        Array(rows).fill(0).map((items, y) => {
                            return (
                                <div key={y} className="row justify-content-center mb-1">
                                    {/* {
                                        Array(5).fill(0).map((item, x) => {
                                            let obj = rooms.find(o => o.x === x && o.y === y);
                                            let temp = 0;
                                            temp = z;
                                            obj && z++;
                                            return (
                                                obj ? <button
                                                    key={item.itinerary_room_id}
                                                    onClick={() => toggleButton(temp, obj.number, obj.itinerary_room_id, selection, current)}
                                                    disabled={!obj.available}
                                                    className={`border-magenta ${obj.available ? current && (current.deckId == selectedCabin.deck.id && obj.itinerary_room_id === current.itinerary_room_id) ? `box-selected text-white` : `bg-white text-magenta` : `box-not-available text-white`} selection-button col-2 pl-1 pr-1 text-sm`}>
                                                    {obj.number}
                                                </button>
                                                    :
                                                    <div className="selection-button col-2  pl-1 pr-1"></div>)
                                        })
                                    } */}


                </div>

            </div>
            <p className="position-front" style={{ fontWeight: scrollPercentage >= 0 && scrollPercentage <= 33 ? "bold" : "normal", color: scrollPercentage >= 0 && scrollPercentage <= 33 ? "#450F3B" : "black" }}>FRONT</p>
            <p className="position-middle" style={{ fontWeight: scrollPercentage > 33 && scrollPercentage <= 66 ? "bold" : "normal", color: scrollPercentage > 33 && scrollPercentage <= 66 ? "#450F3B" : "black" }}>MIDDLE</p>
            <p className="position-back" style={{ fontWeight: scrollPercentage > 66 && scrollPercentage <= 100 ? "bold" : "normal", color: scrollPercentage > 66 && scrollPercentage <= 100 ? "#450F3B" : "black" }}>BACK</p>
        </>
    )
}

export default Selection
