import { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import selectedCabin from "../../views/cabin-selection/SelectedCabin"

const Heading = (props) => {

    const rooms = useSelector(state => state.allcabins)

    const element = rooms.find(item => item.id === props.selectedCabin.id)

    return (
        <div className="d-flex flex-column p-3">
            <p className="h4 font-weight-bold">Choose Your Perfect Room</p>
            <p className="select-room-desk">Please Select A Room From Below Available Rooms</p>
            {element != undefined && (element.hasOwnProperty('room') && element.room.length != 0) && <div className="room-details-mobile bg-white shadow row col-12 mx-2 rounded mb-3 align-self-center">
                <div className="col-5 justify-center">
                    <p className="room-card-title text-muted text-center">Room Type</p>
                    <p className="room-card-value text-center mt-3">{element.detail.name}</p>
                </div>

                <div className="col-7 justify-center">
                    <p className="room-card-title text-muted text-center">Room no</p>
                    <p className="room-card-value text-center mt-3">{element.room[0].roomno}</p>
                </div>
            </div>}
        </div>
    )
}

export default Heading
