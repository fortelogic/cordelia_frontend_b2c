import React, { useState, useEffect } from 'react';
import { DatePicker, Divider } from 'antd';
import styles from './SearchComponent.module.scss';
import Autocomplete from '@material-ui/lab/Autocomplete';
import cx from 'classnames';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import arrowDownIcon from '../../assets/icons/arrowDown-icon.svg';
import calanderIcon from '../../assets/icons/calander-Icon.svg';
import DepartureIcon from "../../assets/img/departure-icon.svg";
import ArrivalIcon from "../../assets/img/arrival-icon.svg";
import moment from 'moment'
import { SearchRounded } from '@material-ui/icons';
import ReactGA from "react-ga"
import { useHistory } from 'react-router-dom';


const SearchComponent = (props) => {
  const portsOrdered = ["Mumbai", "Goa", "Kochi", "Lakshadweep", "Diu", "Chennai", "Galle", "Jaffna", "Male", "Trincomalee", "Colombo"]
  // const { ports = [], onSearch } = props;
  const { onSearch } = props;

  const history = useHistory()

  const allports = props.ports.sort((a, b) => {
    return (
      portsOrdered.indexOf(a.port.name) - portsOrdered.indexOf(b.port.name)
    )
  })

  const ports = allports.slice(1)

  // if (props.preFilledData.sailingFrom) {
  const sailingFromPort = props.preFilledData ? ports.filter((item) => (props.preFilledData.sailingFrom === item.port.name && item)) : ''
  const sailingToPort = props.preFilledData ? ports.filter((item) => (props.preFilledData.sailingTo === item.port.name && item)) : ''
  const monthOfSailing = props.preFilledData ? props.preFilledData.month : ''

  const [formData, setFormData] = useState({
    sailingFrom: '',
    sailingTo: '',
    month: `${new Date().getFullYear()}/${new Date().getMonth()}`
  });

  useEffect(() => {
    if (formData.sailingFrom == "")
      setFormData({ sailingFrom: sailingFromPort[0] ? sailingFromPort[0] : '', sailingTo: sailingToPort[0] ? sailingToPort[0] : '', month: props.preFilledData ? props.preFilledData.month : "" })
  }, [props.preFilledData])

  const settings = (props.preFilledData && props.preFilledData.month != null && props.preFilledData.month != '') ? {
    defaultValue: moment(props.preFilledData.month),
  } : {};

  const monthFormat = 'MMMM-YYYY';
  const onChangeHandler = (e, item) => {
    const { id } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [id.split('-')[0]]: item,
    }));
  };

  const onDateChange = (dateFormat, dateString) => {
    setFormData((prevState) => ({
      ...prevState,
      month: dateString,
    }));
  };

  function disabledDate(current) {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  const searchCruiseHandler = (e) => {
    e.preventDefault();

    ReactGA.event({
      category: 'Searching Itineraries',
      action: 'User has searched for itineraries'
    });


    onSearch(formData)
    props.platform === "true" ?
      history.push(
        `/cruise-routes?sailingFrom=${formData.sailingFrom ? formData.sailingFrom.port.name : ''}&sailingTo=${formData.sailingTo ? formData.sailingTo.port.name : ''
        }&month=${formData.month}&platform=true`
      )
      : history.push(
        `/cruise-routes?sailingFrom=${formData.sailingFrom ? formData.sailingFrom.port.name : ''}&sailingTo=${formData.sailingTo ? formData.sailingTo.port.name : ''
        }&month=${formData.month}`
      )
  };
  return (
    <>
      <div className={cx(styles.searchComponentBar, "rounded p-3 p-md-0 position-relative")} style={props.style}>
        {/* {props.titleText && <p className="find-cruise-title-mb">Find Your Cruise</p>} */}
        <form
          onSubmit={searchCruiseHandler}
          className={cx("form-inline", styles.inlineSearchForm)}
        >
          <div className="row ml-lg-0 p-3 align-items-center rounded search--fill-component">
            <div className="col-12 col-md-6 col-lg p-0">
              <div className={styles.inputGroup}>
                <label htmlFor="sailingFrom" className="labelSearch-txt">Sailing From</label>
                <div className={styles.inputContainer}>
                  <Autocomplete
                    id="sailingFrom"
                    className={cx("form-control inputtxt--control", styles.autoComplete)}
                    openOnFocus={true}
                    options={ports}
                    getOptionLabel={(option) => option && option.port.name}
                    value={formData.sailingFrom}
                    onChange={onChangeHandler}
                    renderInput={(params) => (
                      <div ref={params.InputProps.ref} className="d-flex align-items-center border-input-srch">
                        <img className={styles.imgIcon} src={ArrivalIcon} />
                        <input
                          type="text"
                          {...params.inputProps}
                          className="form-control inputtxt--control"
                          placeholder="Any Destination"
                        />
                        <img src={arrowDownIcon} />
                      </div>
                    )}
                  />
                </div>
              </div>
            </div>

            <div className="col-12 col-md-5 col-lg p-0">
              <div className={styles.inputGroup}>
                <label htmlFor="sailingTo" className="labelSearch-txt">Sailing To</label>
                <div className={styles.inputContainer}>
                  <Autocomplete
                    id="sailingTo"
                    openOnFocus={true}
                    className={cx("form-control inputtxt--control", styles.autoComplete)}
                    options={ports}
                    getOptionLabel={(option) => option && option.port.name}
                    value={formData.sailingTo}
                    onChange={onChangeHandler}
                    renderInput={(params) => (
                      <div ref={params.InputProps.ref} className="d-flex align-items-center border-input-srch">
                        <img className={styles.imgIcon} src={DepartureIcon} />
                        <input
                          type="text"
                          {...params.inputProps}
                          className="form-control inputtxt--control"
                          placeholder="Any destination"
                        />
                        <img src={arrowDownIcon} />
                      </div>
                    )}
                  />

                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg p-0">
              <div className={styles.inputGroup}>
                <label htmlFor="inlineFormInputName2" className="labelSearch-txt">Month of Sail</label>
                <div className={styles.inputContainer}>
                  <div className="d-flex align-items-center inputtxt--control border-input-srch">
                    {/* <DatePicker
                      suffixIcon={() => <img onClick={() => setCalenderOpenClose(!isCalenderOpen)} style={{ zIndex: '0', position: 'absolute', right: '20px' }} src={calanderIcon} />}
                      open={isCalenderOpen}
                      openOnFocus={isCalenderOpen}
                      {...settings}
                      className={styles.datePicker}
                      format={monthFormat}
                      picker="month"
                      disabledDate={disabledDate}
                      onChange={onDateChange}
                      id={"datee"}
                      name={"datee"}>

                    </DatePicker> */}

                    <DatePicker
                      {...settings}
                      className={styles.datePicker}
                      format={monthFormat}
                      picker="month"
                      disabledDate={disabledDate}
                      onChange={onDateChange}
                    />
                    <img style={{ pointerEvents: 'none', zIndex: '0', position: 'absolute', right: '20px' }} src={calanderIcon} />

                    <label htmlFor={"datee"}></label>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg pr-lg-0">
              <div className="Search-cruse-btn pt-4 pr-4">
                <button type="submit" className="btn btn-primary">
                  <span><SearchRounded style={{ fontSize: "18pt", marginRight: "5px", marginTop: "-5px" }} /></span>Search
                </button>
              </div>
            </div>
          </div>
        </form>
      </div >
    </>
  );
};

export default SearchComponent;
