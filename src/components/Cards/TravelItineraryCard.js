import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom"
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedDate, getBookingStatus, setBookingStatus } from '../../redux/actions/bookingAction';
import styles from './TravelItineraryCard.module.scss';
import { CloseOutlined } from '@ant-design/icons';
import { Row, Col, Card, Button } from 'antd';
import moment from 'moment';
import { ReactComponent as DiscountIcon } from '../../assets/icons/discount.svg';
import mapiImage from '../../assets/img/map-image.jpg';
import routeImg from '../../assets/img/cruise-routes/card.png';
import { getRoutes } from '../../redux/actions/routeAction';
import { EnvironmentFilled, InfoCircleOutlined } from '@ant-design/icons';
import localStore from '../../utils/localStore';
import Carousel from '../UI/Carousel';
import cx from 'classnames';
import Modal from '../Modal';
import { useHistory } from 'react-router-dom';
import ReactGA from "react-ga"
const { Meta } = Card;


function TravelItineraryCard(props) {
  let history = useHistory();
  const dispatch = useDispatch();
  const { routes } = props
  const { queryParams } = props
  const {
    data: { ports, itineraries, starting_fare, title, id },
  } = props

  const { firstFiltered } = props
  const [isModalShow, setIsModalShow] = useState(false)
  const [isAllDateModalShow, setisAllDateModalShow] = useState(false)

  const [offerModal, setModalOffers] = useState(false)

  const [isInclusionModal, setIsInclusionModal] = useState(false)
  const [isDateSelected, setIsDateSelected] = useState(false)
  const [selectedItineary, setItineary] = useState("")
  // const title = 'NIGHT CRUISE';
  const cruise_name = itineraries[0].ship.name;
  const starting_price = '₹27,000';
  const offer_description = '2 offers available on this itinerary';
  const { bookingStatus } = useSelector((state) => ({
    bookingStatus: state.booking,
  }))

  const [cardPorts, setCardPorts] = useState([])

  const preBooking = localStore.get("booking")

  useEffect(async () => {
    await dispatch(getBookingStatus())
    // await dispatch(setBookingStatus({ itineraryID: preBooking[0] ? preBooking[0].id : firstFiltered.itineraries[0].id, booking: [firstFiltered.itineraries[0]] }))
    // await dispatch(setBookingStatus({ itineraryID: preBooking ? preBooking[0].id : firstFiltered.itineraries[0].id, booking: preBooking ? preBooking[0] : [firstFiltered.itineraries[0]] }))
  }, [])

  const handleExplore = (id) => {

    const iti = routes.filter((item) => item.id == id)

    const allports = []

    for (let i = 0; i < iti[0].ports.length; i++) {
      allports.push(iti[0].ports[i].port.name)
    }

    ReactGA.event({
      category: 'Explore Itinerary',
      action: 'User Explored Itinerary',
      value: allports.join("-"),
      label: allports.join("-")
    });

    history.push("/itinerary")
  }

  const onBook = async (itineary, id) => {
    const response = props.routes.filter((route, i) => route.id === id)
    let completeItineary
    if (selectedItineary !== "" && selectedItineary !== undefined && selectedItineary) {
      completeItineary = response[0].itineraries.filter((item) => item.id === selectedItineary)
    } else {
      completeItineary = response[0].itineraries.filter((item) => item.id === response[0].itineraries[0].id)
    }
    let filteredBooking = itineary.filter((val, i) => val.id == bookingStatus.itineraryID)
    await dispatch(setBookingStatus({ itineraryID: bookingStatus.itineraryID !== undefined ? bookingStatus.itineraryID : (selectedItineary !== "" && selectedItineary !== undefined && selectedItineary ? selectedItineary : response[0].itineraries[0].id), booking: filteredBooking.length > 0 ? filteredBooking : completeItineary }))
    // await dispatch(setBookingStatus({ itineraryID: bookingStatus.itineraryID, booking: filteredBooking }))
    // history.push('/cabin-selection?' + props.platform === "true" ? "platform=" + props.platform : "")

    const allports = []

    for (let i = 0; i < completeItineary[0].ports.length; i++) {
      allports.push(completeItineary[0].ports[i].port.name)
    }

    ReactGA.event({
      category: 'Book Now',
      action: 'User booked the itinerary',
      value: 'book now-' + completeItineary[0].nightCount + 'N-' + allports.join("-").toLowerCase(),
      label: 'book now-' + completeItineary[0].nightCount + 'N-' + allports.join("-").toLowerCase()
    });

    // props.platform === "true" ? history.push('/cabin-selection?platform=true') : history.push('/cabin-selection')
    queryParams.get("platform") === "true" ?
      history.push(
        `/cabin-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''
        }&month=${queryParams.get("month")}&platform=true`
      )
      : history.push(
        `/cabin-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''
        }&month=${queryParams.get("month")}`
      )
  }

  const onDateSelectHandler = async (itineraryID) => {
    setItineary(itineraryID)
    await dispatch(setSelectedDate({ itineraryID }))
  }

  const modalCloseHandler = async () => {
    await setIsModalShow(false)
  }

  const modalCloseOfferHandler = async () => {
    await setModalOffers(false)
  }

  const showRoute = (e) => {
    e.preventDefault();
    setIsModalShow(true);
  }

  const modalCloseInclusion = () => {
    setIsInclusionModal(false)
  }

  const showInclusion = (e) => {
    e.preventDefault()
    setIsInclusionModal(true)
  }


  const groupBy = (array, key) => {

    return array.reduce((result, currentValue) => {

      (result[moment(currentValue[key]).format("MMM' YYYY")] = result[moment(currentValue[key]).format("MMM' YYYY")] || []).push(
        currentValue
      )

      return result
    }, {})
  }


  return (
    <div className="container  px-0">
      <Modal
        show={isAllDateModalShow}
        onClose={() => setisAllDateModalShow(false)}
      >
        <div className="card-body p-2 position-relative mt-2">
          <div className="modal-close modalmap-close">
            <CloseOutlined
              style={{ color: "#EA725B", fontSize: "18pt", margin: '35px' }}
              onClick={() => setisAllDateModalShow(false)}
            />
          </div>
          <div className={cx("mt-3 mt-md-4", styles.dateSection)}>
            <h3 className="font-weight-bold text-capitalize pl-4 ml-1 mb-2">Cruise Dates</h3>
            <div className={cx("col-12 ml-3")}>
              {/* <ul className="dateCruise--btnmore mr-5" style={{ width: '500px', maxWidth: '500px' }}> */}
              <ul className="dateCruise--btnmore mr-5">
                {
                  groupBy(itineraries, 'startTime') != undefined && Object.entries(groupBy(itineraries, 'startTime')).map((groupedMonths, index) => {
                    return (<div key={index}>
                      <h5>{groupedMonths[0]}</h5>
                      {
                        groupedMonths[1].map((itineary, i) => {
                          return (
                            <li
                              className="btnmore--Itemdate"
                              key={itineary.id}
                              onClick={(e) => {
                                e.preventDefault()
                                onDateSelectHandler(itineary.id)
                              }}
                            >
                              <Button
                                className={`${bookingStatus && bookingStatus.itineraryID === itineary.id
                                  ? styles.selected
                                  : ""
                                  }`}
                              >
                                {moment(itineary.startTime).format("ddd, D MMM YYYY")}
                              </Button>
                            </li>
                          );

                        }
                        )}</div>)
                  }
                  )}
              </ul>
            </div>
            {/* <div className="row mx-4 pr-4 mb-4 justify-items-right">
              <Button
                onClick={() => onBook(itineraries)}
                type="primary"
                className="ml-auto"
                disabled={
                  !itineraries.some(
                    (itineary) =>
                      itineary.id === bookingStatus.itineraryID
                  )
                }
              >
                Book Now
              </Button>
            </div> */}

            <div className="row mx-4 pr-4 mb-4 justify-items-right">
              {
                bookingStatus.itineraryID ?
                  <Button
                    onClick={() => onBook(itineraries, id)}
                    type="primary"
                    disabled={
                      !itineraries.some(
                        (itineary) =>
                          itineary.id === bookingStatus.itineraryID
                      )
                    }
                  >
                    Book Now
                  </Button>
                  :
                  <Button onClick={() => onBook(itineraries, id)} type="primary">
                    Book Now
                  </Button>
              }
            </div>
          </div>
        </div>
      </Modal>
      <Modal show={isModalShow} onClose={modalCloseHandler}>
        <div className="card-body p-0 position-relative">
          <div className="modal-close modalmap-close">
            <CloseOutlined
              style={{ color: "#EA725B", fontSize: "18pt" }}
              onClick={modalCloseHandler}
            />
          </div>
          <div className="modal-content p-0 m-0">
            <div className="maprow-image">
              <img src={mapiImage} />
            </div>
          </div>
        </div>
      </Modal>

      <Modal show={offerModal} onClose={modalCloseOfferHandler}>
        <div className="card-body p-0 position-relative">
          <div className="modal-close modalmap-close">
            <CloseOutlined
              style={{ color: "#EA725B", fontSize: "18pt" }}
              onClick={modalCloseOfferHandler}
            />
          </div>
          <div className="modal-content p-5 m-0">
            <ul>
              <li>Free sailing for kids upto 12 years of age, across stateroom categories </li>
              <li>Free upgrade from Interior to Ocean View stateroom on full payment at the time of booking for 5 Night Sailings</li>
              <li>Free Rescheduling*</li>
            </ul>
          </div>
        </div>
      </Modal>

      <Modal show={isInclusionModal} onClose={modalCloseInclusion}>
        <div className="card-body">
          <div className="modal-close">
            <CloseOutlined
              style={{ color: "#EA725B", fontSize: "18pt" }}
              onClick={modalCloseInclusion}
            />
          </div>
          <div className="modal-content mt-0">
            <div className="inclusive--modal-wraper">
              <h3 className="main-inclus-title">Inclusions</h3>
              <div className="inclusive-colum-Items">
                <h4 className="subTitle-inclus">
                  Interior / Ocean View / Balcony
                </h4>
                <ul className="inclusive-listItems">
                  <li>Accommodation</li>
                  <li>
                    All meals are included at the All-day dining & Starlight
                    restaurant
                  </li>
                  <li>Access to the swimming pool</li>
                  <li>Access to our fitness center</li>
                  <li>Access to all public areas and lounges*</li>
                  <li>Entertainment shows as per the itinerary*</li>
                  <li>Entry to Casino</li>
                  <li>Rock climbing wall experience</li>
                  <li>Access to the Cordelia Academy for kids</li>
                </ul>
              </div>
              <div className="inclusive-colum-Items">
                <h4 className="subTitle-inclus">Suite / Chairman’s Suite</h4>
                <ul className="inclusive-listItems">
                  <li>Accommodation</li>
                  <li>
                    All meals are included at the All-day dining & Starlight
                    restaurant
                  </li>
                  <li>Access to the swimming pool</li>
                  <li>Access to our fitness cente</li>
                  <li>Access to all public areas and lounges*</li>
                  <li>Entertainment shows as per the itinerary*</li>
                  <li>Entry to Casiono</li>
                  <li>Rock climbing wall experience</li>
                  <li>Access to the Cordelia Academy for kids</li>

                  <li>
                    A bottle of sparkling wine on the first evening of holiday
                  </li>
                  <li>
                    Priority embarkation and disembarkation at the beginning and
                    at the end of cruise
                  </li>
                  <li>Proter service during check-in and check-out</li>
                  <li>Complimentary WIFI*</li>
                  <li>Complimentary laundary services*</li>
                  <li>Reserved section at the Royal Theatre</li>
                  <li>Complimentary Bridge tour*</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <div id={`${id}`}>
        <div className="TravelItineraryCard TravelCard--listgrid">
          <Card
            className={styles.travelItineraryCard}
            style={{
              borderRadius: "10px",
              WebkitBoxShadow: "0px 5px 15px rgba(0, 0, 0, 0.08)",
              MozBoxShadow: "0px 5px 15px rgba(0, 0, 0, 0.08)",
              boxShadow: "0px 5px 15px rgba(0, 0, 0, 0.08)",
              padding: "0px",
            }}
            bodyStyle={{ padding: "0" }}
          >
            <div className={styles.container}>
              <div className={styles.courselContainer}>
                <Carousel imgs={[routeImg]} className="carousel" />
              </div>
              <div className="rightSide--RouteBody">
                <div className="card-body">
                  <div className="row">
                    <div className={cx("col-md-8", styles.description)}>
                      <Meta
                        className="find-cruise-title"
                        title={`${itineraries.length ? itineraries[0].nightCount : ""
                          } ${`NIGHTS CRUISE`}`}
                        description={title}
                        style={{ color: "#ffffff" }}
                      />
                      <div className="portrow-grid mt-4">
                        <div className={cx("portflex-port", styles.portName)}>
                          {" "}
                          Ports: <br></br>
                        </div>
                        <div className="portgrid--arealist">
                          <div
                            id="port"
                            className={cx(
                              "portlocation-main",
                              styles.travelPortsList
                            )}
                          >
                            {ports.map((port, i) => (
                              <div
                                key={i}
                                className={cx(
                                  styles.travelPortsListItem,
                                  "listrout--port"
                                )}
                              >
                                <EnvironmentFilled />
                                <span className="ml-1">{port.port.name}</span>
                              </div>
                            ))}
                            <a
                              className="font-italic route-map"
                              onClick={showRoute}
                              href="#"
                            >
                              Route Map
                            </a>
                          </div>
                        </div>
                      </div>
                      <div className={cx("row mt-3 mt-md-4", styles.dateSection)}>
                        <div
                          className={cx("col-12 mb-2", styles.cruseDateHeading)}
                        >
                          <div className="cruse-dateheading">
                            <h3>Cruise Dates</h3>
                            <div className="view--all--datebtn d-block d-md-none">
                              <a
                                className="route-btn-btn"
                                onClick={() => setisAllDateModalShow(true)}
                              >
                                View all dates
                              </a>
                            </div>
                          </div>
                        </div>
                        <div className={cx("col-12")}>
                          <ul className="dateCruise--btnmore">
                            {itineraries.map((itineary, i) => {
                              if (i < 4) {
                                return (
                                  <li
                                    className="btnmore--Itemdate"
                                    key={itineary.id}
                                    onClick={(e) => {
                                      e.preventDefault();
                                      onDateSelectHandler(itineary.id);
                                      setIsDateSelected(true);
                                    }}
                                  >
                                    {
                                      !isDateSelected && i == 0 && bookingStatus.itineraryID === undefined ?
                                        (<Button
                                          className={styles.selected}>  {moment(itineary.startTime).format("ddd, D MMM YYYY")}
                                        </Button>)
                                        : bookingStatus.itineraryID !== undefined && bookingStatus.itineraryID === itineary.id ? <Button
                                          className={styles.selected}>  {moment(itineary.startTime).format("ddd, D MMM YYYY")}
                                        </Button>
                                          : <Button
                                            className="">  {moment(itineary.startTime).format("ddd, D MMM YYYY")}
                                          </Button>
                                    }
                                  </li>
                                )
                              }
                              else if (bookingStatus.itineraryID !== undefined && bookingStatus.itineraryID === itineary.id) {
                                return <li
                                  className="btnmore--Itemdate"
                                  key={itineary.id}
                                  onClick={(e) => {
                                    e.preventDefault();
                                    onDateSelectHandler(itineary.id);
                                    setIsDateSelected(true);
                                  }}
                                >

                                  <Button
                                    className={styles.selected}>  {moment(itineary.startTime).format("ddd, D MMM YYYY")}
                                  </Button>

                                </li>
                              }

                              // if (isDateSelected && bookingStatus.itineraryID !== undefined && i > 4) {
                              //   return (
                              //     <Button className="">  {moment(itineary.startTime).format("ddd, D MMM YYYY")}</Button>
                              //   )
                              // }

                            })}
                          </ul>

                        </div>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="row px-2 px-md-0">
                        <div className="col-6 col-md-12 p-0 pl-15 offerpercent-icon">
                          <div className={styles.offerSection} onClick={() => setModalOffers(true)}>
                            <DiscountIcon className="mr-2 ml-2" /> <span className={styles.offerTwoText}>View 2 offers</span>
                          </div>
                          <div className={styles.documentSection}>
                            {/* <div className="font-weight-bold text-capitalize mr-2 medtony--font">
                            Mandatory Documents
                          </div> */}
                            <div className="inf-dropdown">
                              {/* <InfoCircleOutlined /> */}
                              <div className="toltips--mandtory dropdown--mwndtory">
                                <p>
                                  Following Documnets are mandatory on
                                  International Sailings
                                </p>
                                <ul className="infomation-card">
                                  <li>Passport</li>
                                  <li>Visa</li>
                                  <li>PAN Card</li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-6 col-md-12 p-0 pr-15 pricedtl-pt">
                          <div className={styles.priceDetailWrapper}>
                            <div className="text-uppercase startfrom--view">
                              Starting From
                            </div>
                            <div className={styles.priceDetail}>
                              <h1 className={cx(styles.price)}>
                                {" "}
                                {`₹ ` + starting_fare}{" "}
                              </h1>
                              <span>/&nbsp;person</span>
                            </div>
                            <p className="insurefont">+ Taxes & Fees</p>
                            <a
                              href="#"
                              onClick={showInclusion}
                              style={{ textDecoration: "underline" }}
                            >
                              View inclusions
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <div className="view--all--datebtn d-none d-md-block">
                        <a className="route-btn-btn" onClick={() => setisAllDateModalShow(true)}>
                          View all dates
                        </a>
                      </div>
                    </div>
                    <div className="col-12 col-md-6 bookingnow---routin--save">
                      <div className={styles.bookItineraryWrapper}>
                        <button className="d-inline mr-5 mr-md-3 explore--intery">
                          <a className="text-nowrap" onClick={e => handleExplore(props.data.id)}>Explore Itinerary</a>
                        </button>
                        {/* <Button
                        onClick={() => onBook(itineraries)}
                        type="primary"
                        disabled={
                          !itineraries.some(
                            (itineary) =>
                              itineary.id === (bookingStatus.itineraryID === undefined ? firstFiltered.itineraries[0].id : bookingStatus.itineraryID)
                          )
                        }
                      >
                        Book Now
                      </Button> */}
                        {
                          bookingStatus.itineraryID ?
                            <Button
                              onClick={() => onBook(itineraries, id)}
                              type="primary"
                              disabled={
                                !itineraries.some(
                                  (itineary) =>
                                    itineary.id === bookingStatus.itineraryID
                                )
                              }
                            >
                              Book Now
                            </Button>
                            :
                            <Button className={styles.bookItineraryBookNow} onClick={() => onBook(itineraries, id)} type="primary">
                              Book Now
                            </Button>
                        }
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </div>
  );
}

export default TravelItineraryCard;
