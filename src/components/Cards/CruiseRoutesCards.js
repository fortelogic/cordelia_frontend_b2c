import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getRoutes, filterRoutes } from "../../redux/actions/routeAction";
import { getFilter, setFilter } from "../../redux/actions/filterAction";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import cx from "classnames";
import { Col, Dropdown, Menu, Row, Tag } from "antd";
import {
  FunnelPlotFilled,
  CloseOutlined,
  SortAscendingOutlined,
  SortDescendingOutlined,
} from "@ant-design/icons";
import styles from "./CommonCards.module.scss";
import TravelItinerary from "./TravelItineraryCard";
import FilterIcon from "../../assets/img/filter-short-icon.svg";
import ShortIcon from "../../assets/img/shortby-icon.svg";
import arrowDownIcon from "../../assets/icons/arrow-down-Icon.svg";
const { CheckableTag } = Tag;

export const CruiseRoutesCards = (props) => {
  const { filterBy, sortBy, defaultSort, selectedTags, onTagSelect, isLoading, filteredData, disabledTags, setDisabledTags, queryParams } = props;
  // const [selectedTags, setselectedTags] = useState([]);
  const dispatch = useDispatch();

  const { filter } = useSelector((state) => ({
    filter: state.filterReducer.filter,
  }));

  const { searchResult } = useSelector((state) => ({
    searchResult: state.filterReducer.searchResult,
  }));

  const tagsData = [
    "2 - Nights",
    "3 - Nights",
    "4 - Nights",
    "5 - Nights",
    "7 - Nights",
  ];

  const sortFilter = {
    1: { value: "price", type: "asc" },
    2: { value: "price", type: "desc" },
    3: { value: "nightCount", type: "asc" },
    4: { value: "nightCount", type: "desc" },
  };

  useEffect(async () => {
    let data = await dispatch(getFilter());
  }, []);

  useEffect(() => {
    if (Array.isArray(filter)) {
      onTagSelect(filter);
    }
  }, [filter]);

  useEffect(() => {
    if (Array.isArray(filter) && !isLoading) {
      filterBy(filter.map((tag) => tag.split("-")[0].trim()));
    }
  }, [isLoading]);

  useEffect(() => {
    filterBy(selectedTags.map((tag) => tag.split("-")[0].trim()));
  }, [selectedTags]);

  const handleChange = (tag, checked) => {
    onTagSelect((prevState) =>
      checked ? [...prevState, tag] : prevState.filter((t) => t !== tag)
    );
    dispatch(setFilter(checked ? [...selectedTags, tag] : selectedTags.filter((t) => t !== tag)));
  };

  useEffect(() => {
    onTagSelect(tagsData)
    dispatch(setFilter(tagsData))
  }, [])

  useEffect(() => {
    // console.log("Lets", filteredData)
    if (selectedTags.length == 0) {
      let nc = []
      for (let i = 0; i < filteredData.length; i++) {
        if (filteredData[i].itineraries) {
          for (let j = 0; j < filteredData[i].itineraries.length; j++) {
            nc.push(filteredData[i].itineraries[j].nightCount)
          }
        }
      }

      let nightcounts = [...new Set(nc)]
      let preSelectedTags = []
      nightcounts.map(data => {
        preSelectedTags.push(data + ' - Nights');
      })
      let tobeDisabled = [...tagsData]
      preSelectedTags.map(item => {
        tobeDisabled.map((data, index) => {
          if (data == item) {
            tobeDisabled[index] = ''
          }
        }
        )
      })
      onTagSelect(preSelectedTags);
      if (filteredData != "NO DATA")
        setDisabledTags(tobeDisabled)
    }
  }, [filteredData])

  const onSortFilterClickHandler = (e) => {
    sortBy(sortFilter[e.key]);
  };

  const menu = (
    <Menu onClick={onSortFilterClickHandler}>
      <Menu.Item key={1}>
        Price: Low to High
      </Menu.Item>
      <Menu.Item key={2}>
        Price: High to Low
      </Menu.Item>
      <Menu.Item key={3}>
        Number of Nights: Low to High
      </Menu.Item>
      <Menu.Item key={4}>
        Number of Nights: High to Low
      </Menu.Item>
    </Menu>
  );
  return (
    <>
      <div className="filter-itemSelect--box">
        <div className="container px-0">
          <Row className="my-4">
            <Col span={24}>
              <div className={styles.FilterNightTags}>
                <div className="filter---flex-view">

                  <div className="filterSelect--listCheck">
                    <div>
                    <span className="filterBy">
                      {" "}
                      <img className="mr-2" src={FilterIcon} alt="" /> Filter By{" "}
                    </span>
                    <div className={styles.isMobileF}>
                    <Dropdown className="float-right" overlay={menu}>
                      <a
                        // className="ant-dropdown-link"
                        onClick={(e) => e.preventDefault()}
                      >
                        {defaultSort.type === "asc" ? (
                          <img className="mr-2" src={ShortIcon} alt="" />
                        ) : (
                          <img className="mr-2" src={ShortIcon} alt="" />
                        )}
                        Sort By <img className="ml-2 arrow-IconImg" src={arrowDownIcon} />{" "}

                      </a>
                    </Dropdown>
                    </div>
                    </div>
                    {tagsData.map((tag, i) => (
                      <CheckableTag
                        key={i + 1}
                        style={{ border: selectedTags.indexOf(tag) > -1 ? "1px solid #ea725b" : "" }}
                        checked={selectedTags.indexOf(tag) > -1}
                        onChange={(checked) => !(disabledTags.indexOf(tag) > -1) && handleChange(tag, checked)}
                        className={cx(
                          !(disabledTags.indexOf(tag) > -1) ? '' :
                            styles.checkableTag,
                          `${selectedTags.indexOf(tag) > -1
                            ? styles.selectedTag
                            : ""
                          }`
                        )}
                      >
                        {selectedTags.indexOf(tag) > -1 ? (
                          <>
                            <CloseOutlined className={styles.closeIcon} />
                            <span style={{ fontSize: "12pt" }}>{
                              tag.replace(/\/-/, ' ')
                            }</span>
                          </>
                        ) : (
                          <>{
                            tag.replace(/\/s/g, ' ')
                          }</>
                        )}
                      </CheckableTag>
                    ))}
                    {/* <CheckableTag className={cx(styles.selectedTag, "")} style={{ border: "1px solid #ea725b", color: "#ea725b" }}>Sample</CheckableTag> */}
                    <div className={styles.isDesktopF}>
                    <Dropdown className="float-right" overlay={menu}>
                      <a
                        // className="ant-dropdown-link"
                        onClick={(e) => e.preventDefault()}
                      >
                        {defaultSort.type === "asc" ? (
                          <img className="mr-2" src={ShortIcon} alt="" />
                        ) : (
                          <img className="mr-2" src={ShortIcon} alt="" />
                        )}
                        Sort By <img className="ml-2 arrow-IconImg" src={arrowDownIcon} />{" "}
                      </a>
                    </Dropdown>
                    </div>
                    {/* </div> */}
                  </div>
                </div>

              </div>
            </Col>
            {/* <Col className="shortFilter--main" span={24}>
              <div className="short-by-list">
                <Dropdown overlay={menu}>
                  <a
                    className="ant-dropdown-link"
                    onClick={(e) => e.preventDefault()}
                  >
                    {defaultSort.type === "asc" ? (
                      <img className="mr-2" src={ShortIcon} alt="" />
                    ) : (
                      <img className="mr-2" src={ShortIcon} alt="" />
                    )}
                    Sort By <img className="ml-2 arrow-IconImg" src={arrowDownIcon} />{" "}

                  </a>
                </Dropdown>
              </div>
            </Col> */}
          </Row>
        </div>
      </div>
    </>
  );
};
export const TravelItineraryCards = (props) => {
  const { routeData } = props;
  return (
    <div className={styles.TravelItineraryCardsBackground}>
      {routeData &&
        routeData.map((item) => {
          return <TravelItinerary key={item.id} platform={props.platform} queryParams={props.queryParams} data={item} firstFiltered={routeData[0]} routes={routeData} />;
        })}
    </div>
  );
};

CruiseRoutesCards.TravelItineraryCards = TravelItineraryCards;

export default CruiseRoutesCards;
