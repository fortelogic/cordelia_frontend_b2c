import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux"
import { useHistory, Link } from "react-router-dom"
import { Row, Col, Button, Radio, Space } from "antd";
import { ArrowLeftOutlined } from "@ant-design/icons";
import ProgressStepper from "../ProgressStepper/ProgressStepper";
import Card from "../UI/Card";
import classes from "./BookingFlowCards.module.scss";
import cx from "classnames";
import { ReactComponent as BedIcon } from "../../assets/icons/bed-icon.svg";
import { ReactComponent as ArrowDownIcon } from "../../assets/icons/arrowDown-icon.svg";
import localStore from "../../utils/localStore";
import DepartureIcon from "../../assets/img/departure-icon.svg";
import ArrivalIcon from "../../assets/img/arrival-icon.svg";
import BookingLine from "../../assets/icons/booking-page-line.svg"
import moment from 'moment'
import Edit from "../../assets/icons/Edit.svg"
import { bookingsummary } from "../../redux/actions/roomAction"
import { getCabins } from "../../redux/actions/cabinAction"
import { setBookingStatus } from "../../redux/actions/bookingAction"
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded'
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import CancellationPolicy from "../../components/Policies/cancellationPolicy"
import RechedulePolicy from "../../components/Policies/reschedulePolicy"
import LocalStore from '../../utils/localStore'
import ReactGA from "react-ga"


export const BookingFlowCards = () => {
  return <></>;
};

export const ProgressBar = (props) => {
  const history = useHistory();
  const [bookingBlock, setModifyBookingBlock] = useState(false)
  // const booking = useSelector((state) => state.booking)
  // const cabins = useSelector((state) => state.allcabins)
  const booking = localStore.get("booking")
  const cabins = localStore.get("cabins")

  const calcu = (key) => {
    switch (key) {
      case "rooms":
        let rooms = 0
        if (cabins !== undefined && cabins.length > 0) {
          for (let i = 0; i < cabins.length; i++) {
            rooms += cabins[i].room ? cabins[i].room.length : 0
          }
        }
        return rooms
      case "guests":
        let guests = 0
        if (cabins !== undefined && cabins.length > 0) {
          for (let i = 0; i < cabins.length; i++) {
            guests += cabins ? cabins[i].guest ? cabins[i].guest.total : 0 : 0
          }
        }
        return guests
      case "cabins":
        if (cabins !== undefined && cabins.length > 0) {
          let str = ''
          let trimmedArray = [];
          let values = [];
          let value;
          let count;

          for (let i = 0; i < cabins.length; i++) {
            if (cabins[i].hasOwnProperty("detail")) {
              value = cabins[i]["detail"]["category_id"];

              if (values.indexOf(value) === -1) {
                trimmedArray.push({ name: cabins[i].name, count: (count === undefined ? 0 : count) + 1 })
                values.push(value)
              } else {
                trimmedArray.find((item) => item.name === cabins[i].name ? item.count = item.count + 1 : item.count = item.count + 0)
              }
            }
          }

          trimmedArray.map((item) => str += `${item.count}x${item.name},`)
          if (!str.includes('x')) return `Cabins not selected`; else return str
        } else {
          return `Cabins not selected`
        }
      default:
        return key
    }
  }

  const modifyBookingData = () => {
    const allports = []

    for (let i = 0; i < booking[0].ports.length; i++) {
      allports.push(booking[0].ports[i].port.name)
    }

    ReactGA.event({
      category: 'Modify Booking',
      action: 'User modified the booking',
      value: 'Modify-' + booking[0].nightCount + 'N-' + allports.join("-").toLowerCase(),
      label: 'Modify-' + booking[0].nightCount + 'N-' + allports.join("-").toLowerCase()
    });
  }

  return (
    <>
      <Row className={classes.progressContainer}>
        <Col className="col-4 col-lg-1 order-2 order-lg-1 p-0">
          <Button
            type="primary arrow-left-btn"
            className="w-auto"
            onClick={() => {
              history.goBack();
            }}
          >
            <ArrowLeftOutlined />
          </Button>
        </Col>
        <Col className="col-12 col-lg-9 order-1 order-lg-2 pl-0 pr-0 pt-4 progress-bar-a">
          <ProgressStepper current={props.current} />
        </Col>
        <Col
          className="col-8 col-lg-2 order-3 order-lg-3 p-0"
          style={{ textAlign: "right" }}
        >
          <Button type="primary" onClick={() => { setModifyBookingBlock(!bookingBlock); modifyBookingData() }} className="btn-modify">
            Modify Booking
          </Button>
        </Col>
      </Row>
      {bookingBlock &&
        <>
          <div className={classes.modifyBlockDesktop}>
            <div className="col">
              <div className="col-12">
                <p className="pt-3" style={{ fontSize: "14pt" }}><b>Which Step would you like to modify ?</b></p>
              </div>
              <div className="row mb-5" style={{ boxShadow: "10px #000000" }}>
                <div className="col-3 p-0">
                  <Row className={`p-3 ${classes.blockselection}`} justify="space-between">
                    <Col className="col-10 p-0">
                      <p className="mt-2 w-100 text-center" style={{ lineHeight: "5px" }}>Selected Cruise</p>
                      <p className="mt-2 w-100 text-center" style={{ lineHeight: "5px" }}><b>{booking && booking[0].name}</b></p>
                    </Col>
                    {
                      booking &&
                      <Col className="p-0">
                        <Link to="/cruise-routes">
                          <img className={`mt-3 ${classes.editIcon}`} src={Edit} />
                        </Link>
                      </Col>
                    }
                  </Row>
                </div>
                <div className="col-3 p-0">
                  <Row className={`p-3 ${classes.blockselection}`} justify="space-between">
                    <Col className="col-10 p-0">
                      <p className="mt-2 w-100 text-center" style={{ lineHeight: "5px" }}>Rooms & Guests</p>
                      <p className="mt-2 w-100 text-center" style={{ lineHeight: "5px" }}><b>{calcu("rooms")} Rooms, {calcu("guests")} Guests</b></p>
                    </Col>
                    {
                      calcu("guests") > 0 &&
                      <Col className="p-0">
                        <Link to="/cabin-selection">
                          <img className={`mt-3 ${classes.editIcon}`} src={Edit} />
                        </Link>
                      </Col>
                    }
                  </Row>
                </div>
                <div className="col-3 p-0">
                  <Row className={`p-3 ${classes.blockselection}`} justify="space-between">
                    <Col className="col-10 p-0">
                      <p className="mt-2 w-100 text-center" style={{ lineHeight: "5px" }}>Cabins</p>
                      <p className="mt-2 w-100 text-center" style={{ lineHeight: "5px" }}><b>{calcu("cabins")}</b></p>
                    </Col>
                    {
                      calcu("cabins").includes("x") &&
                      <Col className="p-0">
                        <Link to="/room-type">
                          <img className={`mt-3 ${classes.editIcon}`} src={Edit} />
                        </Link>
                      </Col>
                    }
                  </Row>
                </div>
                <div className="col-3 p-0">
                  <Row className={`p-3 ${classes.blockselection}`} justify="space-between">
                    <Col className="col-10 p-0">
                      <p className="mt-2 w-100 text-center" style={{ lineHeight: "5px" }}>Guests</p>
                      <p className="mt-2 w-100 text-center" style={{ lineHeight: "5px" }}><b>{calcu("guests")} Guests Declared</b></p>
                    </Col>
                    {
                      calcu("guests") > 0 &&
                      <Col className="p-0">
                        <Link to="/deck-selection">
                          <img className={`mt-3 ${classes.editIcon}`} src={Edit} />
                        </Link>
                      </Col>
                    }
                  </Row>
                </div>
              </div>
            </div>
          </div>

          <div className={classes.modifyBlockMobile}>
            <div className="row">
              <div className="col-12">
                <p style={{ fontSize: "12pt" }}><b>Which Step would you like to modify ?</b></p>
              </div>
              <div className="row mb-5" style={{ boxShadow: "10px #000000" }}>
                <div className="col-12">
                  <Row className={`p-3 mx-2 ${classes.blockselection}`} justify="space-between">
                    <Col className="col-10 p-0">
                      <p className="mt-2" style={{ lineHeight: "5px" }}>Selected Cruise</p>
                      <p className="mt-2" style={{ lineHeight: "5px" }}><b>{booking && booking[0].name}</b></p>
                    </Col>
                    {
                      booking &&
                      <Col className="p-0">
                        <Link to="/cruise-routes">
                          <img className={`mt-3 ${classes.editIcon}`} src={Edit} />
                        </Link>
                      </Col>
                    }
                  </Row>
                </div>
                <div className="col-12">
                  <Row className={`p-3 mx-2 ${classes.blockselection}`} style={{ borderBottom: "1px solid gray" }} justify="space-between">
                    <Col className="col-10 p-0">
                      <p className="mt-2" style={{ lineHeight: "5px" }}>Rooms & Guests</p>
                      <p className="mt-2" style={{ lineHeight: "5px" }}><b>{calcu("rooms")} Rooms, {calcu("guests")} Guests</b></p>
                    </Col>
                    {
                      calcu("guests") > 0 &&
                      <Col className="p-0">
                        <Link to="/cabin-selection">
                          <img className={`mt-3 ${classes.editIcon}`} src={Edit} />
                        </Link>
                      </Col>
                    }
                  </Row>
                </div>
                <div className="col-12">
                  <Row className={`p-3 mx-2 ${classes.blockselection}`} style={{ borderBottom: "1px solid gray" }} justify="space-between">
                    <Col className="col-10 p-0">
                      <p className="mt-2" style={{ lineHeight: "5px" }}>Cabins</p>
                      <p className="mt-2" style={{ lineHeight: "5px" }}><b>{calcu("cabins")}</b></p>
                    </Col>
                    {
                      calcu("cabins").includes("x") &&
                      <Col className="p-0">
                        <Link to="/room-type">
                          <img className={`mt-3 ${classes.editIcon}`} src={Edit} />
                        </Link>
                      </Col>
                    }
                  </Row>
                </div>
                <div className="col-12">
                  <Row className={`p-3 mx-2 ${classes.blockselection}`} justify="space-between">
                    <Col className="col-10 p-0">
                      <p className="mt-2" style={{ lineHeight: "5px" }}>Guest</p>
                      <p className="mt-2" style={{ lineHeight: "5px" }}><b>2 Guests Declared</b></p>
                    </Col>
                    {
                      calcu("guests") > 0 &&
                      <Col className="p-0">
                        <Link to="/deck-selection">
                          <img className={`mt-3 ${classes.editIcon}`} src={Edit} />
                        </Link>
                      </Col>
                    }
                  </Row>
                </div>
              </div>
            </div>
          </div>
        </>
      }
    </>
  );
};

export const BookingSummary = (props) => {

  const cabins = useSelector(state => state.allcabins)

  const [canceltionPolicyStatus, setCancelationPolicyStatus] = useState(false)
  const [cancellationPolicy, setCancellationPolicy] = useState(null);
  const [reschedulePolicy, setReschedulePolicy] = useState(null);

  useEffect(() => {
    const Id = localStore.get("itineraryID");
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/api/itineraries/${Id}/policies`, {
      method: 'GET',
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((res) => res.json())
      .then((res) => {
        setCancellationPolicy(res.cancellation);
        setReschedulePolicy(res.reschedule);
      }).catch((err) => {
        alert("Error Fetching Cancellation and Reschedule Policies");
      });
  }, [])


  return (
    <div className={`${classes.bookingSummary}`}>
      <div className={classes.heading}>Booking Summary</div>
      <div className={cx(classes.row, "no-gutters")}>
        <div className={cx("col-md-6 col-12 mt-4 pr-2")}>
          <p className={classes.sectionHeading}>Booking Details</p>
          <BookingDetails>
            <BookingDetails.Footer>
              <div className="col-12 row" onClick={() => setCancelationPolicyStatus(!canceltionPolicyStatus)}>
                <p className={cx(classes.cancellation, "col-10")}>Cancellation and Reschedule policy</p>
                <p className="col-2">{canceltionPolicyStatus ? <UpOutlined /> : <DownOutlined />}</p>
              </div>
              {canceltionPolicyStatus && <>
                <CancellationPolicy cancellationPolicy={cancellationPolicy} />
                <RechedulePolicy reschedulePolicy={cancellationPolicy} />
              </>}

            </BookingDetails.Footer>
          </BookingDetails>
        </div>
        <div className={cx(classes.priceDetailCard, "col-md-6 col-12 mt-4")}>
          {/* <div onClick={e => setPriceDetail(!showPrice)} className={classes.priceDetailAccordion}>
            <p className={classes.sectionHeading}>Price Details</p>
            <div style={{ transform: [{ rotate: "90deg" }] }}><ArrowDownIcon /></div>
          </div> */}
          <p className={classes.sectionHeading}>Price Details</p>
          <PriceDetails priceDetails={props.bookings} />
        </div>
      </div>
    </div >
  );
};

const BookingDetails = (props) => {
  const cabins = useSelector(state => state.allcabins)
  let booking = localStore.get("booking")
  const dispatch = useDispatch()


  return (
    <Card className={cx(classes.bookingDetail)}>
      <div className={classes.cardContent}>
        <div className="ship-detail" style={{ marginBottom: "20px" }}>
          <p className={`mb-0 ${classes.secondaryText}`}>Ship</p>
          <h5 className={`font-weight-bold ${classes.primaryText}`}>{booking && booking.length > 0 ? booking[0].ship.name : ''}</h5>
        </div>

        <div className={cx("row borderline--summary arrivalDepartureDesktop")} style={{ marginBottom: "20px" }}>
          <div className={cx("col-6")}>
            <div className="icon-summry-book">
              <img src={DepartureIcon} style={{ height: '30px', width: '30px' }} />
            </div>
            <div className={cx(classes.secondaryText)}>Departure</div>
            <div className={cx(classes.primaryText, "font-weight-bold")}>
              {booking && booking.length ? booking[0].ports.map((val, i) => val.type == "ORIGIN" ? val.port.name : '') : ''}
            </div>
            <div className={cx(classes.primaryText, "font-weight-bold")}>
              {booking && booking.length ? moment(booking[0].startTime).format('llll') : ''}
            </div>
          </div>
          <div className={cx("col-6")}>
            <div className="icon-summry-book">
              <img src={ArrivalIcon} style={{ height: '30px', width: '30px' }} />
            </div>
            <div className={cx(classes.secondaryText)}>Arrival</div>
            <div className={cx(classes.primaryText, "font-weight-bold")}>
              {booking && booking.length ? booking[0].ports.map((val, i) => val.type == "DESTINATION" ? val.port.name : '') : ''}
            </div>
            <div className={cx(classes.primaryText, "font-weight-bold")}>
              {booking && booking.length ? moment(booking[0].endTime).format('llll') : ''}
            </div>
          </div>
        </div>

        <div className={cx("row borderline--summary arrivalDepartureMobile")} style={{ marginBottom: "20px" }}>
          <div className={cx("row")}>
            <div className="icon-summry-book col-3 justify-content-center">
              <img src={DepartureIcon} style={{ height: '30px', width: '30px' }} />
              <div className="row">
                <img src={BookingLine} className="mx-auto" style={{ height: "80px" }} />
              </div>
            </div>
            <div className="col-9">
              <div className={cx(classes.secondaryText)}>Departure</div>
              <div className={cx(classes.primaryText, "font-weight-bold")}>
                {booking && booking.length ? booking[0].ports.map((val, i) => val.type == "ORIGIN" ? val.port.name : '') : ''}
              </div>
              <div className={cx(classes.primaryText, "font-weight-bold")}>
                {booking && booking.length ? moment(booking[0].startTime).format('llll') : ''}
              </div>
            </div>
          </div>
          <div className={cx("row")}>
            <div className="icon-summry-book col-3">
              <img src={ArrivalIcon} style={{ height: '30px', width: '30px' }} />
            </div>
            <div className="col-9">
              <div className={cx(classes.secondaryText)}>Arrival</div>
              <div className={cx(classes.primaryText, "font-weight-bold")}>
                {booking && booking.length ? booking[0].ports.map((val, i) => val.type == "DESTINATION" ? val.port.name : '') : ''}
              </div>
              <div className={cx(classes.primaryText, "font-weight-bold")}>
                {booking && booking.length ? moment(booking[0].endTime).format('llll') : ''}
              </div>
            </div>
          </div>
        </div>
        <div className="cabinsDesktop">
          {cabins.map((item, index) => (
            <div key={index} className={cx(classes.cabinDetails)} style={{ marginBottom: "20px" }}>
              <div className={cx("row")} style={{ marginBottom: "8px" }}>
                <div className={cx("col-1")}>
                  <BedIcon />
                </div>
                <div className={cx("col-10")}>
                  <div className={cx(classes.secondaryText)}>{`${item.name}`}</div>
                </div>
              </div>
              <div className={cx("row")}>
                <div className={cx("col-1")}></div>
                <div className={cx("col-10", classes.primaryText, "font-weight-bold")}>
                  <div>{`${item && item.detail.name}`}</div>
                  <div>Deck No: {`${item && item.deck.id}`}</div>
                  <div>Room No: {`${item && item.room && item.room[0] && item.room[0].roomno}`}</div>
                  <div>{`${item.guest.total}`} Guests</div>
                </div>
              </div>
            </div>
          ))}
        </div>
        <div className="cabinsMobile row">
          <div className={cx("")} style={{ marginRight: '15px' }}>

            <BedIcon />
          </div>
          {cabins.map((item, index) => (
            <div key={index} className="col-5 p-0 ">
              <div className={cx(classes.cabinDetails)} style={{ marginBottom: "20px" }}>
                <div className={cx("")} style={{ marginBottom: "8px" }}>

                  <div className={cx("col-12 ")}>
                    <div className={cx(classes.secondaryText)}>{`${item.name}`}</div>
                  </div>
                </div>
                <div className={cx("")}>
                  <div className={cx("col-12 ", classes.primaryText, "font-weight-bold")}>
                    <div>{`${item && item.detail.name}`}</div>
                    <div>Deck No: {`${item && item.deck.id}`}</div>
                    <div>Room No: {`${item && item.room && item.room[0] && item.room[0].roomno}`}</div>
                    <div>{`${item.guest.total}`} Guests</div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>

        {props.children}
      </div>
    </Card>
  );
};

BookingDetails.Footer = (props) => {
  return <div className={props.className}>{props.children}</div>;
};

const PriceDetails = (props) => {

  const { border = true } = props
  const bookings = useSelector(state => state.roomReducer.bookingsummary)
  const cabins = useSelector(state => state.allcabins)
  const history = useHistory()
  const [showPriceCard, setPricecardShow] = useState(true)
  const dispatch = useDispatch()

  console.log("Booking Summary On Price Details", bookings)

  useEffect(async () => {
    if (bookings.length == 0) {
      dispatch(bookingsummary(localStore.get('bookingSummary')))
    }
    let data = await dispatch(getCabins());

  }, [])

  const checkCabins = () => {
    let res = []
    cabins.map((data) => {
      res.push(data.detail.hasOwnProperty('price'))
    })
    return !res.includes(false)
  }

  const getCabinTotal = () => {
    let ans = 0;
    cabins.map(data => {
      ans += data.detail.price.total
    })
    return ans
  }

  return (bookings.length != 0 && checkCabins()) && (
    <div>
      <Card border={border} className={`${classes.priceDetail}`}>
        <div className={classes.priceDetailHeader} onClick={() => setPricecardShow(!showPriceCard)}>
          <span className={classes.priceCardHeading}>{border ? 'PRICE DETAILS' : 'Price Details'}</span>
          <div><ArrowDownIcon /></div>
        </div>
        {showPriceCard &&
          <div className={classes.priceCardContent}>
            <div className={`${classes.section}`}>

              {
                cabins.map((items, index) => {
                  // console.log("Cabin Name", items.name)
                  return <div key={index}>
                    <p className="mt-3"><b>{items.name}</b></p>
                    {items.detail.price.individual.map((item, index) => {
                      return (<div key={index} className={classes.heading, "mt-3"}>
                        <div className={classes.sectionRow}>
                          <span>{`${item.type}`}</span>
                          <span>{`₹${item.total}`}</span>
                        </div>
                        <div className={classes.subHeading}>
                          <div className={classes.sectionRow}>
                            <span>Cabin Fare:</span>
                            <span>{`₹${item.fare}`}</span>
                          </div>
                          <div className={classes.sectionRow}>
                            <span>Other Charges:</span>
                            <span>{`₹${item.portCharges + item.gratuity}`}</span>
                          </div>
                          {/* <div className={classes.sectionRow}>
                            <span>Port Charge:</span>
                            <span>{`₹${item.portCharges}`}</span>
                          </div>
                          <div className={classes.sectionRow}>
                            <span>Gratuity:</span>
                            <span>{`₹${item.gratuity}`}</span>
                          </div> */}
                        </div>
                        
                        <div className={`${classes.section}`}>
                          <div className={classes.heading}>
                            <div className={classes.sectionRow}>
                              <span>Promo Code</span>
                              <span>&#8377;0</span>
                            </div>
                          </div>
                          <div className={classes.heading}>
                            <div className={classes.sectionRow}>
                              <span>Shore Excursion</span>
                              <span>{bookings && bookings.booking.shore_excursion_total}</span>
                            </div>
                          </div>
                          <div className={classes.subHeading}>
                            <div className={classes.sectionRow}>
                              <span>Cabin Fare:</span>
                              <span>&#8377;0</span>
                            </div>
                          </div>
                        </div>

                        <div className={`${classes.section}`}>
                          <div className={classes.heading}>
                            <div className={classes.sectionRow}>
                              <span>Sub Total</span>
                              <span>{`₹${item.fare}`}</span>
                            </div>
                          </div>
                          <div className={classes.subHeading}>
                            <div className={classes.sectionRow}>
                              <span>Cabin Fare:</span>
                              <span>{bookings.booking.cabin_fare}</span>
                            </div>
                          </div>
                        </div>

                        <div className={`${classes.section}`}>
                          <div className={classes.heading}>
                            <div className={classes.sectionRow}>
                              <span>Taxes</span>
                              <span>{bookings.tax}</span>
                            </div>
                          </div>
                          <div className={classes.subHeading}>
                            <div className={classes.sectionRow}>
                              <span>GST:</span>
                              <span>{bookings.booking.gst}</span>
                            </div>
                          </div>
                        </div>

                      </div>)
                    })}
                  </div>
                })
              }
            </div>
          </div>
        }
        {/* <PriceDetailFooter text="Grand Total" value={`${getCabinTotal()}`} /> */}
        {border && <PriceDetailFooter text="Grand Total" value={`${bookings.booking.total}`} />}
      </Card >
    </div>
  );
};

const BillingDetails = (props) => {
  const booking = localStore.get("bookingSummary")
  const shoresUpdated = localStore.get("updatedShores")
  const { havePan, havePanChange, panno, changePanNo, panname, changePanName, paymentOption, setPaymentOption } = props;
  const { haveGst, gstin, haveGstName, haveMobilenumber, changeHaveGst, changeGstin, changeHaveGstName, changeHaveMobilenumber } = props

  const [value, setValue] = React.useState(1);

  const onChange = e => {
    setValue(e.target.value);
  };
  return (
    <Card className={`${classes.billingDetail}`}>
      <div className={classes.cardContent}>
        <div className={`${classes.section}`}>
          <div className={cx(classes.heading, classes.label)}>Apply Promo Code</div>
          <div className={cx("input-group mb-3", classes.formGroup)}>
            <input type="text" className={cx("form-control")} placeholder="EX: HAPPY769" aria-label="Promo code" aria-describedby="basic-addon2" />
            <div className={cx("input-group-append", classes.inputAppendBtn)}>
              <span className="input-group-text" id="basic-addon2">Apply</span>
            </div>
          </div>
        </div>

        <div className={`${classes.section}`}>
          <div className={classes.flex}>
            <div className={cx(classes.heading, classes.label, "mr-4")}>Do You have pan card?</div>
            <Radio.Group onChange={havePanChange} value={havePan}>
              <Radio value={1}>Yes</Radio>
              <Radio value={2}>NO</Radio>
            </Radio.Group>
          </div>
          {havePan == 1 && <div className={cx("row")}>
            <div className="col-lg-6 mt-2">
              <input type="text" className="form-control" onChange={changePanNo} value={panno} placeholder="Enter your Pan number" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
            <div className="col-lg-6 mt-2">
              <input type="text" className="form-control" onChange={changePanName} value={panname} placeholder="Enter your Pan name" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
          </div>}
        </div>


        <div className={`${classes.section}`}>
          <div className={classes.flex}>
            <div className={cx(classes.heading, classes.label, "mr-4")}>Do You have GSTIN?</div>
            <Radio.Group onChange={changeHaveGst} value={haveGst}>
              <Radio value={1}>Yes</Radio>
              <Radio value={2}>NO</Radio>
            </Radio.Group>
          </div>
          {haveGst == 1 && <div className={cx("row")}>
            <div className="col-lg-4 mt-2">
              <input type="text" className="form-control" onChange={changeGstin} value={gstin} placeholder="GSTIN" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
            <div className="col-lg-4 mt-2">
              <input type="text" className="form-control" onChange={changeHaveGstName} value={haveGstName} placeholder="Name" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
            <div className="col-lg-4 mt-2">
              <input type="text" className="form-control" onChange={changeHaveMobilenumber} value={haveMobilenumber} placeholder="Mobile Number" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
          </div>}
        </div>
        <hr className={classes.dashed} />
        <div className={classes.section}>
          <h6 className="font-weight-bold">Select Payment Option</h6>
          <div className={"row px-2"}>
            <div className="col-sm-6 p-1">
              <div className={cx(paymentOption === "BookBy100" ? classes.selectedPaymentOption : "", classes.paymentOptions, "card")} onClick={() => setPaymentOption("BookBy100")}>
                <div className="card-body mb-0">
                  <CheckCircleRoundedIcon className={cx(classes.checkCircleRound)} />
                  <h6 className="text-sm font-weight-bold">Book Now By Paying 100% </h6>
                  <p className="mb-0">Paying upfront fullpayment gets you exciting offers and lot more.</p>
                </div>
              </div>
            </div>
            {
              booking.hasOwnProperty("payment_option_id") && booking.payment_option_id ?
                <div className="col-sm-6 p-1">
                  <div className={cx(paymentOption === "BookBy25" ? classes.selectedPaymentOption : "", classes.paymentOptions, "card")} onClick={() => setPaymentOption("BookBy25")}>
                    <div className="card-body">
                      <CheckCircleRoundedIcon className={cx(classes.checkCircleRound)} />
                      <h6 className="text-sm font-weight-bold"> Reserve Now By Paying 25% </h6>
                      <p className="mb-0">Paying upfront fullpayment gets you exciting offers and lot more</p>
                    </div>
                  </div>
                </div>
                : ""
            }
          </div>
        </div>
      </div>
      {/* <PriceDetailFooter text={paymentOption === "BookBy25" ? `Partial Amount Payable` : `Amount Payable`} value={shoresUpdated ? shoresUpdated.total : booking.total} /> */}
      <PriceDetailFooter text={paymentOption === "BookBy25" ? `Partial Amount Payable` : `Amount Payable`} value={paymentOption === "BookBy25" ? booking.partial_payable_amount : booking.total} />
    </Card>
  );
}

const PriceDetailFooter = (props) => {
  const { text, value } = props;
  return (
    <div className={`${classes.cardFooter}`}>
      <div>
        <span>{text}</span>
      </div>
      <div>
        <span>&#8377;{value}</span>
      </div>
    </div>
  );
};

export const PaymentSumarry = (props) => {
  const { havePan, havePanChange, panno, changePanNo, panname, changePanName, paymentOption, setPaymentOption } = props;
  const { haveGst, gstin, haveGstName, haveMobilenumber, changeHaveGst, changeGstin, changeHaveGstName, changeHaveMobilenumber } = props

  const [showPrice, setPriceShow] = useState(false)

  return (
    <div className={`${classes.bookingSummary}`}>
      <div className={classes.heading}>Payment</div>
      <div className={cx(classes.row, "no-gutters")}>
        <div className={cx("col-md-6 col-12 mt-4 pr-2")}>
          <p className={classes.sectionHeading}>Booking Details</p>
          <BookingDetails>
            <div onClick={() => setPriceShow(!showPrice)}>
              <BookingDetails.Footer className={classes.paymentDetailFooter}>
                {/* <div>Price Details</div>
                <div><ArrowDownIcon /></div> */}
              </BookingDetails.Footer>
            </div>
            <PriceDetails border={false} />
          </BookingDetails>
        </div>
        <div className={cx("col-md-6 col-12 mt-4 pl-2")}>
          <p className={classes.sectionHeading}>Billing Details</p>
          <BillingDetails
            havePan={havePan}
            havePanChange={havePanChange}
            panno={panno}
            changePanNo={changePanNo}
            panname={panname}
            changePanName={changePanName}
            haveGst={haveGst}
            gstin={gstin}
            haveGstName={haveGstName}
            haveMobilenumber={haveMobilenumber}
            changeHaveGst={changeHaveGst}
            changeGstin={changeGstin}
            changeHaveGstName={changeHaveGstName}
            changeHaveMobilenumber={changeHaveMobilenumber}
            paymentOption={paymentOption}
            setPaymentOption={setPaymentOption}
          />
        </div>
      </div>
    </div>
  );
}

BookingFlowCards.ProgressBar = ProgressBar;
BookingFlowCards.BookingSummary = BookingSummary;
BookingFlowCards.PaymentSumarry = PaymentSumarry;
export default BookingFlowCards;
