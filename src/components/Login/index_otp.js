import React, { useState } from 'react'
import { useDispatch } from "react-redux"
import Modal from "../Modal"
import { Button } from "antd"
import { CloseOutlined } from "@ant-design/icons"
import { authenticate } from "../../redux/actions/authAction"
import cx from "classnames";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { useForm } from "react-hook-form";
import { phone, email, first_name, last_name, country_code } from "../../utils/validations";
import Timer from "../Timer/index";
import OtpInput from 'react-otp-input';
import styles from "./login.module.css"
import CounrtyCodes from "./countryCodes.json"
import axios from "axios"
import { useHistory } from "react-router-dom";

const firstSchema = Joi.object({
    phone,
    email,
    first_name,
    last_name,
    country_code
});

const LoginOtp = ({ isLoginModalShow, setLoginModalShow }) => {


    let history = useHistory();
    const [user, setUser] = React.useState({ phone_number: "", country_code: "+91", email: "" })
    const [sending, setSending] = useState(false);
    const [resending, setResending] = useState(false);
    const [canResend, setCanResend] = useState(false);
    const [verifyData, setVerifyOTP] = useState(null);
    const [otp, setOTP] = useState("");
    const [error, setOtpError] = useState(null);
    const [verifying, setVerifyingOtp] = useState(false);
    const [userDetails, setUserDetails] = React.useState({ firstname: "", lastname: "", countrycode: "+91", phone: "" })

    const dispatch = useDispatch()

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: joiResolver(firstSchema),
    });

    const setUsersData = (e) => {
        // const { name, value } = e.target
        // setUser((state) => ({
        //     ...state,
        //     [name]: value,
        // }))
    }

    const setLoggedInUser = (e) => {
        dispatch(authenticate(user))
        modalCloseHandler()
    }

    const modalCloseHandler = async () => {
        await setLoginModalShow(false)
    }


    const onSubmit = (data, e) => {
    e.preventDefault()
    if (verifyData) {
        verifyOtp(data);
        return false;
    }
    setSending(true);
    let dataObj = {
        first_name: data.first_name,
        last_name: data.last_name,
        phone_number: data.phone,
        country_code: data.country_code,
    };

    fetch(`${process.env.REACT_APP_API_ENDPOINT}/api/send_login_otp`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify(dataObj),
    }).then((response) => response.json())
        .then((response) => {
            setVerifyOTP(true);
        }).catch((err) => err);
    setSending(false);
    }

    const handleChange = (otp) => setOTP(otp);

    const handleResend = (data) => {
        if (resending) return;
        setResending(true);
        let dataObj = {
            first_name: data.first_name,
            last_name: data.last_name,
            phone_number: data.phone,
            country_code: data.country_code,
        };
        fetch(`${process.env.REACT_APP_API_ENDPOINT}/api/send_login_otp`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(dataObj),
        })
            .then((response) => response.json())
            .then((response) => {
                setVerifyOTP(true);
            })
            .catch((err) => err);
    };

    const verifyOtp = (data) => {
        setOtpError(null);
        if (verifying) {
            return false;
        }
        if (!otp) {
            setOtpError("Please fill the OTP");
            return false;
        }
        setVerifyingOtp(true);
        let dataObj = {
            "otp": otp,
            "phone_number": data.phone,
            "country_code": data.country_code
        };


        fetch(`${process.env.REACT_APP_API_ENDPOINT}/api/verify_otp`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(dataObj),
        })
            .then((response) => {
                if (response.ok) {
                    return response.json()
                }
                throw Error("Verification failed");
            }
            )
            .then((response) => {
                localStorage.setItem("auth_otp", JSON.stringify(response))
                modalCloseHandler()
                history.push({
                    pathname: '/my-bookings',
                })
                return false;
            }).catch((err) => setOtpError("OTP verification failed"));
        setVerifyingOtp(false);
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault()
        let obj = {
            name: userDetails.firstname + " " + userDetails.lastname,
            phone_number: userDetails.phone,
            country_code: userDetails.countrycode
        }
        const { data } = await axios.post(`${process.env.REACT_APP_API_ENDPOINT}/api/send_otp`, obj, {})
        localStorage.setItem("auth", JSON.stringify(data.data.auth))
        const auth = localStorage.getItem("auth")
        if (auth) {
            modalCloseHandler()
        } else {
            console.log("Login Failed")
        }
    }

    const inputChange = (e) => {
        setUserDetails(state => ({
            ...state,
            [e.target.name]: e.target.value
        }))
        console.log("User", userDetails)
    }

    return (
        <Modal show={isLoginModalShow} onClose={modalCloseHandler} >
            <div className="card-body p-0 position-relative">
                <div className="modal-close modalmap-close">
                    <CloseOutlined style={{ color: '#EA725B' }} onClick={modalCloseHandler} />
                </div>
                <div className="modal-content p-0 m-0">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="modal-body mx-3">
                            <p className="login-modal-header">Please enter your contact details to continue</p>
                            <div className="row">
                                <div className="col-6">
                                    <div className="md-form mb-4">
                                        <input
                                            type="text"
                                            name="first_name"
                                            className="form-control validate"
                                            placeholder="Enter your firstname"
                                            defaultValue={user.first_name}
                                            {...register("first_name", {
                                                required: "Required",
                                            })}
                                        />
                                        {errors.first_name && "Please enter your first name"}
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className="md-form mb-4">
                                        <input
                                            type="text"
                                            name="last_name"
                                            className="form-control validate"
                                            placeholder="Enter your lastname"
                                            defaultValue={user.last_name}
                                            {...register("last_name", {
                                                required: "Required",
                                            })}

                                        />
                                        {errors.last_name && "Please enter your last name"}
                                    </div>
                                </div>
                            </div>
                            <div className="md-form mb-4">
                                <input
                                    type="email"
                                    name="email"
                                    className="form-control validate"
                                    placeholder="Enter your email"
                                    defaultValue={user.email}
                                    {...register("email", {
                                        required: "Required",
                                    })}
                                />
                                {errors.email && "Please enter your email"}
                            </div>
                            <div className="row md-form mb-4">
                                <div className="col-3 col-md-3 col-xl-2">
                                    <select
                                        name="country_code"
                                        id="country_code"
                                        {...register("country_code", {
                                            required: "Required",
                                        })}
                                        defaultValue={user.country_code}
                                        className={cx(styles.selectStyle, "form-control validate")}
                                    >
                                        {CounrtyCodes.map(code => {
                                            return (<option key={code} value={code}>{code}</option>)
                                        })}
                                    </select>
                                </div>
                                <div className="col-9 col-md-9 col-xl-10">
                                    <input
                                        type="text"
                                        name="phone"
                                        className="form-control validate"
                                        placeholder="Enter your phone number"
                                        defaultValue={user.phone}
                                        {...register("phone", {
                                            required: "Required",
                                        })}
                                    />
                                    {errors.phone && "Please enter your mobile number"}
                                </div>
                            </div>
                            {verifyData &&
                                <div>
                                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                                        <p className="text-color-2">Enter The Verification Code</p>
                                        <Timer initialSeconds="60" />
                                    </div>
                                    <OtpInput
                                        containerStyle={cx(styles.containerStyle)}
                                        inputStyle={cx(styles.inputStyle)}
                                        value={otp}
                                        onChange={handleChange}
                                        numInputs={4}
                                    />
                                </div>
                            }

                            <div className="text-center my-8 resend-otp">
                                {canResend ?
                                    <span icon="fal fa-sms" >
                                        {resending
                                            ? "Re-sending OTP, please check your SMS..." : <>Did not get OTP? <a className="cursor-pointer" onClick={handleSubmit(handleResend)}>Resend the OTP</a></>}
                                    </span> : null}
                            </div>

                            <div className="md-form mb-4 mt-4 row justify-content-center">
                                {!verifyData ? <button className={styles.SendOtp}>Send OTP</button> : <button className={styles.SendOtp}>Verify OTP</button>}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Modal>
    )
}

export default LoginOtp
