import React from 'react'
import "./ourdestination.scss"
import { Button } from "antd"
import Slider from 'react-slick'
import Banner from '../../assets/img/banner.png'
// import Diver from '../../assets/img/diver.png'
// import Goa from '../../assets/img/goa.png'
// import Bridge from '../../assets/img/bridge.png'
// import Kochi from '../../assets/img/kochi.png'
// import Div from '../../assets/img/diu.png'
// import Buddha from '../../assets/img/colombo.png'
// import Colombo from "../../assets/img/our-destination/colombo-mobile.png"
// import Diu from "../../assets/img/our-destination/diu.png"
// import Lakshadweep from "../../assets/img/our-destination/lakshadweep-mobile.png"
// import Mumbai from "../../assets/img/our-destination/mumbai.png"
// import KochiMobile from "../../assets/img/our-destination/kochi.png"
// import GoaMobile from "../../assets/img/our-destination/goa.png"
import WeekendCruise from "../../assets/img/home-page/seacation/seacation-one.png"
import WeddingOnWaves from "../../assets/img/home-page/seacation/seacation-two.png"
import OffsideAndEvents from "../../assets/img/home-page/seacation/seaction-three.png"
import PerfectHoliday from "../../assets/img/home-page/seacation/seaction-four.png"
import RomenticGetaway from "../../assets/img/home-page/seacation/seaction-five.png"
import SpecialSailings from "../../assets/img/home-page/seacation/seaction-six.png"



const OurDestination = ({ isMobile }) => {

    const settings = {
        infinite: true,
        slidesToShow: 3,
        // className: "center",
        // centerMode: true,
        centerPadding: "0px",
        speed: 300,
        cssEase: 'linear',
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                },
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    return (
        <>
            <div className="our-destination-desktop">
                <h2>SEACATION FOR <b>EVERY OCCASION</b></h2>
                <div style={{ display: 'flex', flexDirection: 'column', maxHeight: '860px', flexWrap: 'wrap' }}>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        {/* style={{ height: '300px', width: '380px', background: '#ccc', borderRadius: '5px', margin: '10px' }} */}
                        <img src={WeekendCruise}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "24px", fontWeight: "bold" }}>WEEKEND CRUISES</p>
                            <div className="seacation-text-buttons">
                                <p className="text-white">Your best weekend getaway awaits!</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="col explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/weekend-cruise" }}
                                >
                                    Know More
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={PerfectHoliday}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "24px", fontWeight: "bold" }}>Perfect Holiday</p>
                            <div className="seacation-text-buttons">
                                <p className="text-white">A Holiday like none other!</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/cruise-routes" }}
                                >
                                    Know More
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={WeddingOnWaves}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "24px", fontWeight: "bold" }}>WEDDING ON WAVES</p>
                            <div className="seacation-text-buttons">

                                <p className="text-white">The ultimate wedding destination in the middle of the ocean</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/groups-and-events/wedding-cruises" }}
                                >
                                    Know More
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        <img src={RomenticGetaway}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "24px", fontWeight: "bold" }}>ROMANTIC GETAWAY</p>

                            <div className="seacation-text-buttons">

                                <p className="text-white">Set Sail to celebrate Love</p>

                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/cruise-gateway" }}
                                >
                                    Know More
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        <img src={OffsideAndEvents}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "24px", fontWeight: "bold" }}>OFFSITES AND EVENTS</p>
                            <div className="seacation-text-buttons">
                                <p className="text-white">Events An offbeat experience for your offsite</p>

                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
                                >
                                    Know More
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={SpecialSailings}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "24px", fontWeight: "bold" }}>SPECIAL SAILINGS</p>
                            <div className="seacation-text-buttons">

                                <p className="text-white">Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc.</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
                                >
                                    Know More
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="our-destination-mobile mt-5">
                <h3 className="ml-3">SEACATION FOR <b>EVERY OCCASION</b></h3>
                <Slider className="primary-slider" {...settings} id={Date.now()} >
                    <div className="position-relative rounded-lg overflow-hidden" style={{ height: '500px', width: '380px', background: '#ccc', borderRadius: '5px' }} src={Banner}>
                        <img src={WeekendCruise}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination text-left" style={{ fontSize: "24px", fontWeight: "bold" }}>WEEKEND CRUISES</p>
                            <div className="seacation-text-buttons">
                                <p className="text-white">Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc.</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="col explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
                                >
                                    Explore Now
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden" style={{ height: '500px', width: '380px', background: '#ccc', borderRadius: '5px', margin: '10px' }} >
                        <img src={WeddingOnWaves}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination text-left" style={{ fontSize: "24px", fontWeight: "bold" }}>PERFECT HOLIDAY</p>
                            <div className="seacation-text-buttons">

                                <p className="text-white">Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc.</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
                                >
                                    Explore Now
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden" style={{ height: '500px', width: '380px', background: '#ccc', borderRadius: '5px', margin: '10px' }}>
                        <img src={OffsideAndEvents}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination text-left" style={{ fontSize: "24px", fontWeight: "bold" }}>WEDDING ON WAVES</p>
                            <div className="seacation-text-buttons">

                                <p className="text-white">Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc.</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
                                >
                                    Explore Now
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden" style={{ height: '500px', width: '380px', background: '#ccc', borderRadius: '5px', margin: '10px' }} src={Banner}>
                        <img src={PerfectHoliday}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination text-left" style={{ fontSize: "24px", fontWeight: "bold" }}>ROMANTIC GETAWAY</p>
                            <div className="seacation-text-buttons">

                                <p className="text-white">Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc.</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
                                >
                                    Explore Now
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden" style={{ height: '500px', width: '380px', background: '#ccc', borderRadius: '5px', margin: '10px' }} src={Banner}>
                        <img src={RomenticGetaway}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination text-left" style={{ fontSize: "24px", fontWeight: "bold" }}>OFFSITES AND EVENTS</p>
                            <div className="seacation-text-buttons">

                                <p className="text-white">Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc.</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
                                >
                                    Explore Now
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden" style={{ height: '500px', width: '380px', background: '#ccc', borderRadius: '5px', margin: '10px' }} >
                        <img src={SpecialSailings}></img>
                        <div className="position-absolute text-center w-100 destination-images-content">
                            <p className="destination text-left" style={{ fontSize: "24px", fontWeight: "bold" }}>SPECIAL SAILINGS</p>
                            <div className="seacation-text-buttons">

                                <p className="text-white">Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc.</p>
                                <Button
                                    type="primary"
                                    size="large"
                                    className="explore-more-btn"
                                    style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
                                    onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
                                >
                                    Explore Now
                                </Button>
                            </div>
                        </div>
                    </div>
                </Slider>
                <div className="text-center">
                    <a href={'#'}>View All</a>
                </div>
                {/* </div> */}
            </div>
        </>
    )
}

export { OurDestination }
