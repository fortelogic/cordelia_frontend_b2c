import Slider from 'react-slick';
import React from "react";

function TrustBadge({ isMobile, trustBadges }) {
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 8,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          dots: true,
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: false,
        },
      },
      {
        breakpoint: 767,
        settings: {
          dots: true,
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (<Slider {...settings} arrows={false} className="primary-slider p-0 my-5">{trustBadges.map(({ description, mobileSrc, src, title }, idx) => (
    <div className="trust-badge__container" key={idx}>
      <div className="trust-badge__card" key={idx}>
        <figure>
          <img alt="cordelia cruise" src={isMobile ? mobileSrc : src} />
        </figure>
        <h4>{title}</h4>
        <p>{description}</p>
      </div>
    </div>))}</Slider>)
}

TrustBadge = React.memo(TrustBadge);

export { TrustBadge };
