import React from "react";
import HtmlParser from "react-html-parser";
import Slider from "react-slick";
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { Button } from "antd"

function NextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div className={className} style={{ ...style }} onClick={onClick}>
      {">"}
    </div>
  );
}

function PrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div className={className} style={{ ...style }} onClick={onClick}>
      {"<"}
    </div>
  );
}

const settings = {
  infinite: true,
  slidesToShow: 3,
  // className: "center",
  // centerMode: true,
  // centerPadding: "100px",
  speed: 300,
  cssEase: "linear",
  dots: true,
  arrows: true,
  nextArrow: <ChevronRightIcon />,
  prevArrow: <ChevronLeftIcon />,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
      },
    },
    {
      breakpoint: 767,
      settings: {
        arrows: false,
        dots: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        initialSlide: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

function CardsCarouselMobile({ heading, items, config, isMobile, isOccasionEvents = false }) {
  return (
    <section className="cards-carousel">
      {/* <section className="cards-carousel" style={{ margin: isMobile && isOccasionEvents ? "0px -18px" : "" }}> */}
      <div className="cards-carousel__header">
        <h3 className="cards-carousel__header__title">{HtmlParser(heading)}</h3>
      </div>
      <div className="cards-carousel__slider">
        <Slider className="primary-slider" {...settings} {...config} id={Date.now()}>
          {items.map((item, idx) => {
            const {
              author = null,
              bottomLink = null,
              date = null,
              description = null,
              src = null,
              subTitle = null,
              tags = null,
              title = null,
            } = item;
            return (
              <div key={item.title + idx}>
                <div className="cards-carousel__slider__card">
                  {src && (
                    <figure>
                      <img alt="cordelia cruises" src={src} />
                    </figure>
                  )}

                  <div className="pt-3">
                    <div className="cards-carousel__slider__card__body__top">
                      <h4 className="cards-carousel__slider__card__body__title">
                        {title}
                      </h4>
                      {subTitle && (
                        <p className="cards-carousel__slider__card__body__subtitle">
                          {subTitle}
                        </p>
                      )}
                      {!subTitle && author && !item.avatar && (
                        <div className="cards-carousel__slider__card__body__inline">
                          <span className="cards-carousel__slider__card__body__inline__author">
                            {author}
                          </span>
                          <span className="cards-carousel__slider__card__body__inline__date">
                            {date}
                          </span>
                        </div>
                      )}
                      {tags && !description && (
                        <ul className="cards-carousel__slider__card__body__tags">
                          {tags.map(({ name, src }, idx) => (
                            <li key={name + idx}>
                              <img alt={"cordelia cruises " + name} src={src} />
                              <span>{name}</span>
                            </li>
                          ))}
                        </ul>
                      )}
                      {!tags && description && (
                        <p
                          className={`cards-carousel__slider__card__body__description ${item.avatar ? "quotes" : ""
                            }`}
                        >
                          {description}
                        </p>
                      )}
                    </div>
                    <div className="cards-carousel__slider__card__body__bottom">
                      {!bottomLink && (
                        <>
                          {item.price && (
                            <div className="cards-carousel__slider__card__body__bottom__price">
                              <span>Starting from</span>
                              <strong>{item.price}</strong>
                            </div>
                          )}
                          {item.link && (
                            <div className="cards-carousel__slider__card__body__bottom__link">
                              <a href={item.link.url} target={item.link.target}>
                                {item.link.text}
                              </a>
                              <ArrowForwardIcon className="primary-color pb-1 ml-1" />
                            </div>
                          )}
                          {item.avatar && (
                            <div className="cards-carousel__slider__card__body__bottom__avatar">
                              <figure>
                                <img alt="cordelia cruises" src={item.avatar} />
                              </figure>
                              <div>
                                <strong>jane Cooper</strong>
                                <span>Mumbai, India</span>
                              </div>
                            </div>
                          )}
                        </>
                      )}
                      {item.url && (
                        <button className="py-1 it-section__button border" onClick={() => { window.open(item.url, item.target) }}>Know More</button>
                      )}
                      {bottomLink && (
                        <a
                          className="cc-button cc-button--primary"
                          href={bottomLink.url}
                          target={bottomLink.target}
                        >
                          {bottomLink.text}
                        </a>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    </section>
  );
}

// CardsCarousel = React.memo(CardsCarousel);

export { CardsCarouselMobile };
