import React from 'react';
import { Button, message } from 'antd';

function Newsletter({ isMobile }) {
  const info = () => {
    message.info('You have been subscribed successfully!');
  };
  return (
    <section className={`newsletter-section ${isMobile ? '' : ''}`}>
      <h3 className="d-none d-md-block">
        SUBSCRIBE TO <strong>OUR NEWSLETTER</strong>
      </h3>
      <div className={` ${isMobile ? '' : 'row'}`}>
        <div className="newsletter-section__top col-12 col-md-6 d-md-flex  align-items-center" style={{ flex: 1 }}>
          <h3 className="d-md-none">
            SUBSCRIBE TO <strong>OUR NEWSLETTER</strong>
          </h3>
          <p className="m-md-0">Be the first to know all our updates & offers</p>
        </div>
        <div className="col-12 col-md-6">
          <form className="newsletter-section__form" style={{ flex: 1 }}>
            <div className="newsletter-section__form__input-group" style={{ border: 'none' }}>
              <input placeholder="Your email address" type="email" style={{ border: 'none' }} />
              <Button type="primary" style={{ height: '40px' }} onClick={info}>
                SUBSCRIBE
              </Button>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
}

Newsletter = React.memo(Newsletter);

export { Newsletter };
