import React, { useState, useEffect } from "react";
import HtmlParser from "react-html-parser";
import { Button } from "antd";
import Destination from "../../assets/img/home-page/destination.png"

function ImageSwitcher({ isMobile, item, id, imageComponent, history, customStyle }) {
  const [index, setIndex] = useState([1]);
  const { image } = item;
  const { tagImages } = item;
  return (
    <section className="it-section">
      <div className={isMobile ? "it-section__top p-0 pt-4 pb-2" : "it-section__top"}>
        <h3>{HtmlParser(item.title)}</h3>
      </div>
      {image && (!image.position || image.position === "left") && (
        <div className="it-section__image">
          <img
            alt="Cordelia cruises"
            src={isMobile ? Destination : tagImages[index]}
          />
        </div>
      )}
      <div className={isMobile ? "it-section__tags p-0 py-3" : "it-section__tags"} style={customStyle && !isMobile ? customStyle : null}>
        <div>
          <h3>
            {HtmlParser(item.title)}
            {/* OUR <strong>DESTINATIONS</strong> */}
          </h3>
          <ul style={{ paddingLeft: "0px" }}>
            {item.tags.map((tag, id) => {
              return (
                <li
                  className={`${id == index ? "it-section__tags--active" : ""}`}
                  key={id}
                  onClick={() => { setIndex(id) }}
                >
                  {tag}
                </li>
              );
            })}
          </ul>
          {item.description && <div>{item.description}</div>}
        </div>
        <div>
          <button className="py-1 it-section__button border" onClick={() => { window.open(item.url, item.target) }}>
            {!isMobile ? item.btnText : "KNOW MORE"}
          </button>
        </div>
      </div>
      {image && image.position && image.position === "right" && (
        <div className="it-section__image">
          <img
            alt="Cordelia cruises"
            src={isMobile ? image.mobile : image.web}
          />
        </div>
      )}
      {imageComponent && (
        <div className="it-section__image">{imageComponent}</div>
      )}
    </section>
  );
}

ImageSwitcher = React.memo(ImageSwitcher);

export { ImageSwitcher };
