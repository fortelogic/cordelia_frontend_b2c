import classes from "./fourpillars.module.css"
import cx from "classnames";
import HtmlParser from 'react-html-parser'


const FourPillars = ({ heading, items, isMobile }) => {
    return (
        <>
            <section className="cards-carousel">
            <h2 className="ml-3">{HtmlParser(heading)}</h2>
            <div className={cx(classes.pillarBase, "justify-content-center")}>
                {
                    items.map((item, index) => {
                        return (
                            <div className={cx(classes.pillarImageDiv)}>
                                <img className={cx(classes.pillarImage, "m-2")} src={item.image} style={{ borderRadius: "10px 10px 10px 10px" }} />
                                <div className={cx(classes.pillarImageContent)}>
                                    <h4 className={cx(classes.pillarTitle)}>{item.title}</h4>
                                    <span className={cx(classes.pillarDescription)}>{item.description}</span>
                                    <button onClick={()=>{window.location.href=item.url}} className={cx(classes.knowMorePillar)}>Know More</button>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
            </section>
        </>
    );
}

export { FourPillars }