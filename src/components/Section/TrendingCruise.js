import React from 'react'
import "./trending-cruise.scss"
import { Button } from "antd"
import Slider from 'react-slick'
import Banner from '../../assets/img/banner.png'
import Diver from '../../assets/img/diver.png'
import Goa from '../../assets/img/goa.png'
import Bridge from '../../assets/img/bridge.png'
import Kochi from '../../assets/img/kochi.png'
import Div from '../../assets/img/diu.png'
import Buddha from '../../assets/img/colombo.png'
import Colombo from "../../assets/img/our-destination/colombo-mobile.png"
import Diu from "../../assets/img/our-destination/diu.png"
import Lakshadweep from "../../assets/img/our-destination/lakshadweep-mobile.png"
import Mumbai from "../../assets/img/our-destination/mumbai.png"
import KochiMobile from "../../assets/img/our-destination/kochi.png"
import GoaMobile from "../../assets/img/our-destination/goa.png"

const TrendingCruise = ({ isMobile }) => {

    const settings = {
        infinite: true,
        slidesToShow: 3,
        // className: "center",
        // centerMode: true,
        centerPadding: "0px",
        speed: 300,
        cssEase: 'linear',
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                },
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    return (
        <>
            <div className="our-destination-desktop">
                <h1>TRENDING <b>CRUISE DESTINATIONS</b></h1>
                <div style={{ display: 'flex', flexDirection: 'column', maxHeight: '860px', flexWrap: 'wrap' }}>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        {/* style={{ height: '300px', width: '380px', background: '#ccc', borderRadius: '5px', margin: '10px' }} */}
                        <img src={Diver}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>LAKSHADWEEP</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">LAKSHADWEEP</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        With the picture-perfect flawless combination of awe-inspiring land
                                        and seascapes, Lakshadweep is a destination that outshines its
                                        reputation.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={Kochi}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>KOCHI</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">KOCHI</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        Kochi is popular as a gateway to discover God’s own Country, Kerala.
                                        Kochi has brilliantly upheld the distinct cultural and historical
                                        identity of the state.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={Goa}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>GOA</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">GOA</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        A backpacker’s paradise and a weekend travel hub, Goa is famous for
                                        its tropical vibe, young identity, and cultural adaptations.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        <img src={Buddha}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>COLOMBO</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">CHENNAI</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        Chennai will surprise you with the diverse combination of art,
                                        religion, and cultural identity. Chennai is a brilliant reflection
                                        of culinary brilliance, cosmopolitan vibes, and traditions.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        <img src={Div}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>DIU</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">DIU</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        The powerhouse of natural beauty, culture and colonial architecture,
                                        Diu is one of the must-visit weekend getaways from Mumbai.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={Bridge}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>MUMBAI</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">MUMBAI</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        With the picture-perfect flawless combination of awe-inspiring land
                                        and seascapes, Lakshadweep is a destination that outshines its
                                        reputation.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="text-center">
                    <a href={'#'}>View All</a>
                </div>
            </div>
            <div className="our-destination-mobile">
                <h1>TRENDING <b>CRUISE DESTINATIONS</b></h1>
                <Slider className="primary-slider" {...settings} id={Date.now()} >
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        {/* style={{ height: '300px', width: '380px', background: '#ccc', borderRadius: '5px', margin: '10px' }} */}
                        <img src={Diver}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>LAKSHADWEEP</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">LAKSHADWEEP</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        With the picture-perfect flawless combination of awe-inspiring land
                                        and seascapes, Lakshadweep is a destination that outshines its
                                        reputation.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={Kochi}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>KOCHI</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">KOCHI</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        Kochi is popular as a gateway to discover God’s own Country, Kerala.
                                        Kochi has brilliantly upheld the distinct cultural and historical
                                        identity of the state.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={Goa}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>GOA</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">GOA</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        A backpacker’s paradise and a weekend travel hub, Goa is famous for
                                        its tropical vibe, young identity, and cultural adaptations.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        <img src={Buddha}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>COLOMBO</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">CHENNAI</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        Chennai will surprise you with the diverse combination of art,
                                        religion, and cultural identity. Chennai is a brilliant reflection
                                        of culinary brilliance, cosmopolitan vibes, and traditions.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even" src={Banner}>
                        <img src={Div}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>DIU</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">DIU</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        The powerhouse of natural beauty, culture and colonial architecture,
                                        Diu is one of the must-visit weekend getaways from Mumbai.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-relative rounded-lg overflow-hidden our-destination-images-even">
                        <img src={Bridge}></img>
                        <div className="position-absolute w-100 destination-images-content">
                            <p className="destination" style={{ fontSize: "20px", fontWeight: "bold" }}>MUMBAI</p>
                        </div>
                        <div className="darkness-on-cruise">
                            <div className="darkness-on-cruise-content position-relative">
                                <div className="darkness-on-cruise-content-wrapper">
                                    <h4 className="darkness-on-cruise-content-title">MUMBAI</h4>
                                    <div className="text-white darkness-on-cruise-content-description">
                                        With the picture-perfect flawless combination of awe-inspiring land
                                        and seascapes, Lakshadweep is a destination that outshines its
                                        reputation.
                                    </div>
                                    <div className="text-left mt-5">
                                        <a
                                            href="/cruise-routes"
                                            className="view-cruises"
                                        >
                                            View Cruise
                                        </a>
                                        <a
                                            href="/cruise-destinations/cruise-to-lakshadweep"
                                            className="explore-more"
                                        >
                                            Explore Now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Slider>
                <div className="text-center">
                    <a href={'#'}>View All</a>
                </div>
                {/* </div> */}
            </div>
        </>
    )
}

export { TrendingCruise }
