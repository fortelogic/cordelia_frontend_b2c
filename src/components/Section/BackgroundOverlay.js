import React from "react";
// import mobileSrc from '../../assets/img/cruise-transparent_mobile.png';
import bg from "../../assets/img/home-page/cruise-transparent.png";
import mobilebg from "../../assets/img/home-page/cruise-transparent_mobile.png";
import Container from "../Container";
import { Button } from "antd";
import { useHistory } from "react-router-dom";

function BackgroundOverlay({ isMobile, history }) {
  // bo-section - background-overlay-section
  const src = bg;
  const mobileSrc = mobilebg;
  return (
    <section className="bo-section">
      <Container>
        <div className="bo-section__top">
          <h3>
            THE <strong>EMPRESS</strong>
          </h3>
          <p>
            Lorem Ipsum is a simply dummy text used in typing industry since
            1500’s.
          </p>
          {/* <Button
            type="primary"
            size="large"
            style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px", marginTop: isMobile ? "30px" : "50px" }}
            onClick={() => { window.location.href = "https://cordelia.fortelogic.in/empress-page" }}
          >
            KNOW MORE
          </Button> */}
          <Button
            type="primary"
            size="large"
            style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px", marginTop: isMobile ? "30px" : "50px" }}
            onClick={() => { history.push("/aboutship") }}
          >
            KNOW MORE
          </Button>
        </div>
      </Container>
      <div className="bo-section__image">
        <img
          alt="Cordelia cruises"
          style={{ width: "100%" }}
          src={isMobile ? mobileSrc : src}
        />
      </div>
    </section>
  );
}

BackgroundOverlay = React.memo(BackgroundOverlay);

export { BackgroundOverlay };
