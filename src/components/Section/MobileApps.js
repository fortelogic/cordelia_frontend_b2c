import React from 'react';

function MobileApps() {
  return (
    <section className="mobile-apps">
      <figure>
        <img alt="" src="" />
      </figure>
      <div>
        <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
        <p>You can now book faster on your fingertips</p>
      </div>
    </section>
  );
}

MobileApps = React.memo(MobileApps);
export { MobileApps };
