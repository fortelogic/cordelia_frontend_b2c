import React from "react";
import Rotator from "react-rotating-text";
import Slider from "react-slick";
import "./index.scss";
import { Button } from "antd";
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

const settings = {
  infinite: true,
  slidesToShow: 1,
  speed: 300,
  cssEase: "linear",
  dots: false,
  arrows: true,
  nextArrow: <ChevronRightIcon />,
  prevArrow: <ChevronLeftIcon />,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
      },
    },
    {
      breakpoint: 767,
      settings: {
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

function OverlayCard({ isMobile, items, history }) {
  // overlayCard section - oc-section
  return (
    <section className="oc-section">
      <div className="cards-carousel__slider">
        <Slider className="primary-slider" {...settings} id={Date.now()}>
          {items.map((item, idx) => (
            <div className="oc-section__vertical-align__card" key={idx}>
              <img
                alt="cordelia cruises"
                src={isMobile ? item.image.mobile : item.image.web}
              // style={{ width: "100%", height: "25vh" }}
              />
              <div className="oc-section__vertical-align__card__content">
                <h5>{item.title}</h5>
                <p>{item.description}</p>
                <Button type="primary" className="d-none d-md-block" onClick={() => { window.open(item.url, item.target) }}>View Offers</Button>
              </div>
            </div>
          ))}
        </Slider>
      </div>
      {/* <div className="oc-section__vertical-align oc-section-aligin">
        <div className="oc-section__vertical-align__card">
          <img alt="cordelia cruises" src={isMobile ? mobile_src_one : web_src_one} />
          <div className="oc-section__vertical-align__card__content">
            <h5>Healthy Waves</h5>
            <p>Enjoy safe holidays with healthy waves</p>
          </div>
        </div>
        <div className="oc-section__vertical-align__card">
          <img alt="cordelia cruises" src={isMobile ? mobile_src_two : web_src_two} />
          <div className="oc-section__vertical-align__card__content">
            <h5>Kids sail free</h5>
            <p>Enjoy safe holidays with healthy waves</p>
          </div>
        </div>
        <div className="oc-section__vertical-align__card">
          <img alt="cordelia cruises" src={isMobile ? mobile_src_three : web_src_three} />
          <div className="oc-section__vertical-align__card__content">
            <h5>book by paying ₹500</h5>
            <p>Enjoy safe holidays with healthy waves</p>
          </div>
        </div>
      </div> */}
    </section>
  );
}

OverlayCard = React.memo(OverlayCard);

export { OverlayCard };
