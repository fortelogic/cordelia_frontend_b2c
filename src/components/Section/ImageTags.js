import React from "react";
import HtmlParser from "react-html-parser";
import { Button } from "antd";

function ImageTags({ isMobile, item, id, imageComponent, history, customStyle }) {
  const { image } = item;
  return (
    <section className="it-section">
      <div className="it-section__top">
        <h3>{HtmlParser(item.title)}</h3>
      </div>
      {image && (!image.position || image.position === "left") && (
        <div className="it-section__image">
          <img
            alt="Cordelia cruises"
            src={isMobile ? image.mobile : image.web}
          />
        </div>
      )}
      <div className="it-section__tags" style={customStyle && !isMobile ? customStyle : null}>
        <div>
          <h3>
            {HtmlParser(item.title)}
            {/* OUR <strong>DESTINATIONS</strong> */}
          </h3>
          {item.tags && (
            <ul style={{ paddingLeft: "0px" }}>
              {item.tags.map((tag, id) => {
                return (
                  <li
                    className={`${id == 2 ? "it-section__tags--active" : ""}`}
                    key={id}
                  >
                    {tag}
                  </li>
                );
              })}
            </ul>
          )}
          {item.description && <div>{item.description}</div>}
        </div>
        <div>
          <button className="py-1 it-section__button border"  onClick={() => { window.open(item.url, item.target) }}>
            {item.btnText}
          </button>
        </div>
      </div>
      {image && image.position && image.position === "right" && (
        <div className="it-section__image">
          <img
            alt="Cordelia cruises"
            src={isMobile ? image.mobile : image.web}
          />
        </div>
      )}
      {imageComponent && (
        <div className="it-section__image">{imageComponent}</div>
      )}
    </section>
  );
}

ImageTags = React.memo(ImageTags);

export { ImageTags };
