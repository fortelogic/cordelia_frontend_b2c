import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import cx from "classnames";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import pageStyle from "./wowoffer.module.css";
import { Button } from "antd"
import HtmlParser from 'react-html-parser'
import pageStyle from "../../views/home/wowoffer.module.css";

// const Offercomponent = ({ DeparturePortsData }) => {
const WowOffers = ({ heading, items, isMobile }) => {

    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: false,
                },
            },
            {
                breakpoint: 767,
                settings: {
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    const PlaceCard = ({ data, wrapperclass }) => {
        return (
            <div
                className={cx("flex flex-col justify-content-center", pageStyle["this-destination-box"], wrapperclass)}
                style={{ backgroundImage: `url(${data.image})` }}
            >
                <div className={cx(pageStyle["this-description-container"], "flex flex-col justify-end")}>
                    <h3 className={cx(pageStyle["titile"], "text-white")}>{data.title}</h3>
                    <h3 className={cx(pageStyle["offer"], "text-white")}>{data.offer}</h3>
                </div>
            </div>
        );
    };

    return (
        <section className="oc-section px-sm-2 px-0">
            <div className="cards-carousel__header p-0">
                {
                    <h3 className={cx(pageStyle["title"], "cards-carousel__header__title")}>{HtmlParser(heading)}</h3>
                }
            </div>
            <div className="cards-carousel__slider p-0">
                <Slider {...settings} arrows={false} className="primary-slider p-0">
                    {items.map((data, index) => (
                        <PlaceCard key={index} data={data} wrapperclass={pageStyle["placecard"]} />
                    ))}
                </Slider>
            </div>
        </section>
    )
}

export { WowOffers }