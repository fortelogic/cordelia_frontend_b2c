import React from 'react';
import Rotator from 'react-rotating-text';
import Image from '../../assets/img/cordelia-cruises-content-image-section.png';
import MobileImage from '../../assets/img/cordelia-cruises-content-image-section-mobile.png';
import { Button } from "antd";

function ContentImage({ isMobile }) {
  //ci - content-image

  return (
    <section className="ci-section">
      <div className="ci-section__content">
        <h3>
          Experience
          <span>
            <b><Rotator items={['Lakshadweep', 'Diu', 'Mumbai', 'Colombo']} color="#ea725b" /></b>
          </span>
          like never before
        </h3>
        <p>Welcome aboard India’s only premium cruise liner, Cordelia Cruises.</p>
        <strong>Now you can just Book with 5000 INR. </strong>
      </div>
      <div className="ci-section__image">
        <figure>
          <img alt="cordelia cruises" src={isMobile ? MobileImage : Image} />
        </figure>
        <div className="ci-section__image__overlay">
          <p>
            Odio quam nulla posuere tempus, ornare amet ultricies. Vivamus semper cras egestas vestibulum massa metus
            mauris.
          </p>
          {
            !isMobile ? <Button ghost className="primary-button" onClick={() => { window.open("https://cordelia.fortelogic.in/cruise-deals", "_blank") }}>Avail Now</Button> : <p ghost className="primary-button" onClick={() => { window.open("https://cordelia.fortelogic.in/cruise-deals", "_blank") }}>Avail Now</p>
          }
        </div>
      </div>
    </section>
  );
}

ContentImage = React.memo(ContentImage);

export { ContentImage };
