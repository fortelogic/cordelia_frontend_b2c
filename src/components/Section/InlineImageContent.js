import React from 'react';
import { Button } from "antd"

function InlineImageContent({ btnText, description, isMobile, image, target, title, url }) {
  //ci - content-image

  return (
    <section className={`inline-section ${image.position}`}>
      <div className="inline-section__content">
        <div>
          <h3>{title}</h3>
          <p>{description}</p>
        </div>
        <div>
          {/* <a className="cc-button cc-button--primary" href={url} target={target}>
            {btnText}
          </a> */}
          <Button
            type="primary"
            size="large"
            style={{ height: isMobile ? "36px !important" : "40px", width: isMobile ? "140px" : "150px" }}
          >
            KNOW MORE
          </Button>
        </div>
      </div>
      <div className="inline-section__image">
        <figure>
          <img alt="cordelia cruises" src={image.web} />
        </figure>
      </div>
    </section>
  );
}

InlineImageContent = React.memo(InlineImageContent);

export { InlineImageContent };
