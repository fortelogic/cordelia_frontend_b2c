import React from "react";
import classes from "./Card.module.scss";

const Card = (props) => {
  const { onClick, border = true, style = {} } = props;
  return (
    <div
      style={style}
      className={`${border ? classes.card : classes.cardWB} ${props.className}`}
      onClick={onClick ? onClick : (e) => e.preventDefault()}
    >
      {props.children}
    </div>
  );
};

export default Card;
