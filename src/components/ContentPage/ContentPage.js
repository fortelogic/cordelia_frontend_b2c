import Header from "../Header/HeaderHome";
import Footer from "../Footer/Footer";

const ContentPage = ({ title, contents, banners, description = "" }) => (
  <>
    <Header className="mb-5" bgPurple/>
    <main className="bg-auto">
      <div className="p-4">
        <h3>{title}</h3>
        {contents.map((line, index) => (
          <div key={index}>
            { line.title && <h4>{ line.title }</h4>}
            <p>{line.content}</p>
            <br/>
          </div>
        ))}
      </div>
    </main>
    <Footer />
  </>
);

export default ContentPage;