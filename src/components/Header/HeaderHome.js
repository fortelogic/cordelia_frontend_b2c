import React, { useState, useEffect } from "react";
import styles from "./HeaderHome.module.scss";
import Logo from "../../assets/img/logo.png";
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import Container from "../Container";
import { useHistory, Link } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu'
import CancelIcon from '@material-ui/icons/Cancel';
import MoreHorizOutlinedIcon from '@material-ui/icons/MoreHorizOutlined';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Button, Select, Menu as AntdMenu, Dropdown, Avatar, Divider, Alert } from "antd";
// import Banner from "../../assets/img/banner.png";
import Banner from "../../assets/img/cruise-routes/cruise-banner-mobile.png";
// import bannerMobile from '../../assets/img/mobile_banner.png';
import bannerMobile from '../../assets/img/cruise-routes/cruise-banner-desktop.png';
import cx from "classnames";
import UserImg from "../../assets/img/mobile-menu/avatar.png"
import CruiseIcon from "../../assets/img/mobile-menu/cruise.svg"
import DestinationIcon from "../../assets/img/mobile-menu/destination.svg"
import PromotionIcon from "../../assets/img/mobile-menu/promotion.svg"
import BookingIcon from "../../assets/img/mobile-menu/bookings.svg"
import ContactIcon from "../../assets/img/mobile-menu/contact.svg"
import BlogIcon from "../../assets/img/mobile-menu/blog.svg"
import LogoutIcon from "../../assets/img/mobile-menu/logout.svg"
import Login from "../../components/Login"
import LoginOtp from "../../components/Login/index_otp"
import LocalStorage from "../../utils/localStore"
import { authenticate } from "../../redux/actions/authAction"
import { useDispatch } from "react-redux"
import moment from 'moment'
import MapIcon from "../../assets/img/header/map.png"
import OnboardActivityIcon from "../../assets/img/header/onboard-activity.png"
import DiningIcon from "../../assets/img/header/dining-icon.png"
import StateroomIcon from "../../assets/img/header/stateroom-icon.png"
import ShoreexcursionIcon from "../../assets/img/header/shore-excursion.png"

import FindCruiseIcon from "../../assets/img/header/find-cruise-icon.png"
import CordeliaPlannerIcon from "../../assets/img/header/cordelia-planner-icon.png"
import DeparturePortsIcon from "../../assets/img/header/departure-ports-icon.png"
import FAQIcon from "../../assets/img/header/faq-icon.png"
import OffersIcon from "../../assets/img/header/offers-icon.png"
import MyBookingsIcon from "../../assets/img/header/my-bookings-icon.png"
import WebCheckInIcon from "../../assets/img/header/web-check-in-icon.png"




function Header(props) {
  const { mybookingflag, setMybookingFlag } = props
  const [update, setUpdate] = useState(false);
  const history = useHistory();
  const { Option } = Select;
  const dispatch = useDispatch()
  const [isLoginModalShow, setLoginModalShow] = React.useState(false)
  const [isLoginOtpModalShow, setLoginOtpModalShow] = React.useState(false)
  const queryParams = new URLSearchParams(window.location.search)
  const auth = LocalStorage.getItem("auth")
  const [selectedMenu, setSelectedMenu] = useState(0)

  // useEffect(() => {
  //   const auth = LocalStorage.get("auth")
  //   if (auth.hasOwnProperty("auth") && auth.hasOwnProperty("user")) {
  //     dispatch(authenticate(auth.user))
  //   } else {
  //     setLoginModalShow(true)
  //   }
  // }, [])

  const logout = () => {
    localStorage.clear();
    window.location = "/";
    return null
  }


  const menu = (
    <AntdMenu style={{ padding: "5px 10px 0px" }}>
      {
        auth === undefined || auth === null ?
          <AntdMenu.Item onClick={() => setLoginModalShow(true)}>
            <h6>Login</h6>
          </AntdMenu.Item>
          :
          <AntdMenu.Item onClick={() => logout()}>
            <h6>Logout</h6>
          </AntdMenu.Item>
      }
    </AntdMenu >
  )

  const exploreMenu = (
    <div className={styles.megaMenu}>
      <div className={styles.megaMenuContainer}>
        <div className={styles.megaMenuContent}>
          <div className={styles.contentItem}>
            <img src={MapIcon} style={{ height: "20px", width: "20px" }} />
            <a href={`/itinerary`}>
              <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Destinations</span>
            </a>
          </div>
          <div className={styles.contentItem}>
            <img src={OnboardActivityIcon} style={{ height: "20px", width: "20px" }} />
            <a href={`/itinerary`}>
              <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Onboard Activity</span>
            </a>
          </div>
          <div className={styles.contentItem}>
            <img src={DiningIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Dining</span>
          </div>
          <div className={styles.contentItem}>
            <img src={StateroomIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Staterooms</span>
          </div>
          <div className={styles.contentItem}>
            <img src={ShoreexcursionIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Shore Excursion</span>
          </div>
        </div>
      </div>
    </div>
  )

  const planMenu = (
    <div className={styles.megaMenu}>
      <div className={styles.megaMenuContainer}>
        <div className={styles.megaMenuContent}>
          <div className={styles.contentItem}>
            <img src={FindCruiseIcon} style={{ height: "20px", width: "20px" }} />
            <a href={`/cruise-routes`}>
              <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Find Cruise</span>
            </a>
          </div>
          <div className={styles.contentItem}>
            <img src={CordeliaPlannerIcon} style={{ height: "20px", width: "20px" }} />
            <a href={`/cruise-routes`}>
              <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Cordelia Planner</span>
            </a>
          </div>
          <div className={styles.contentItem}>
            <img src={DeparturePortsIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Departure Ports</span>
          </div>
          <div className={styles.contentItem}>
            <img src={FAQIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>FAQ</span>
          </div>
          <div className={styles.contentItem}>
            <img src={OffersIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Offers Icon</span>
          </div>
        </div>
      </div>
    </div>
  )

  const manageMenu = (
    <div className={styles.megaMenu}>
      <div className={styles.megaMenuContainer}>
        <div className={styles.megaMenuContent}>
          <div className={styles.contentItem}>
            <img src={MyBookingsIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>My Bookings</span>
          </div>
          <div className={styles.contentItem}>
            <img src={WebCheckInIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Web Check In</span>
          </div>
          <div className={styles.contentItem}>
            <img src={DeparturePortsIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Valet Service</span>
          </div>
          <div className={styles.contentItem}>
            <img src={FAQIcon} style={{ height: "20px", width: "20px" }} />
            <span style={{color:"black", fontSize: "10pt", fontWeight: "bolder", marginTop: "5px", marginLeft: "10px" }}>Lost & Found</span>
          </div>
        </div>
      </div>
    </div>
  )

  // const manageMenu = (
  //   <div className={styles.megaMenu}>
  //     <div className={styles.megaMenuContainer}>
  //       <div className={styles.megaMenuContent}>
  //         <div className={styles.contentItem}>
  //           <img src={MyBookingsIcon} style={{ height: "20px", width: "20px" }} />
  //           <span style={{ fontSize: "10pt", fontWeight: "bolder", marginTop: "5px" }}>My Bookings</span>
  //         </div>
  //         <div className={styles.contentItem}>
  //           <img src={WebCheckInIcon} style={{ height: "20px", width: "20px" }} />
  //           <span style={{ fontSize: "10pt", fontWeight: "bolder", marginTop: "5px" }}>Web Check In</span>
  //         </div>
  //       </div>
  //     </div>
  //   </div>
  // )

  React.useEffect(() => {

    const auth = JSON.parse(localStorage.getItem('auth'))
    // if (auth != undefined) {
    //   if ((!(queryParams.get("platform") && queryParams.get("platform") !== null && queryParams.get("platform") === "true") || auth.temp_token) && auth.temp_token != null) {
    //     if (window.location.pathname === "/my-bookings") {
    //       setLoginModalShow(true)
    //     }
    //   }
    // }

    // if (auth == undefined) {

    // const auth_otp = JSON.parse(localStorage.getItem('auth_otp'))
    const auth_otp = JSON.parse(localStorage.getItem('auth'))
    if (auth_otp == undefined || auth_otp == null) {
      if (queryParams.get("platform") && queryParams.get("platform") !== null && queryParams.get("platform") === "true" || (window.location.pathname == "/" || window.location.pathname == "/cruise-routes" || window.location.pathname == "/post-quotation")) {
        // if (window.location.pathname === "/my-bookings") {
        //   setLoginModalShow(true)
        // } else {
        //   setLoginModalShow(false)
        // }
      } else {
        setLoginModalShow(true)
      }

      // (queryParams.get("platform") && queryParams.get("platform") !== null && queryParams.get("platform") === "true") || (window.location.pathname == "/" || window.location.pathname == "/cruise-routes" || window.location.pathname == "/post-quotation") || props.noAuth && setLoginModalShow(true)
    }
    else {
      let date1 = moment(new Date(), 'YYYY/MM/DD HH:mm');
      let date2 = moment(moment.unix(auth_otp.exp).format('YYYY/MM/DD HH:mm'));
      var seconds = moment.duration(date2.diff(date1)).asSeconds();
      if (date1 > date2) {
        localStorage.removeItem('auth_otp')
        setLoginModalShow(true);
      }
    }
  }, []);

  const handleeMybookings = () => {
    const auth_otp = JSON.parse(localStorage.getItem('auth_otp'))
    if (auth_otp == undefined || auth_otp == null) {
      setLoginOtpModalShow(true);
    } else {
      history.push({
        pathname: '/my-bookings',
      })
    }
  }

  const renderCorrespondingMenu = (selectedMenu) => {
    switch(selectedMenu) {
      case 0:
        return exploreMenu
      case 1:
        return planMenu
      case 2:
        return manageMenu
      default:
        return exploreMenu
    }
  }

  return (
    <div className={`Header ${props.className ? props.className : ""}`}>
      {update ? <><Container strict={true} wrapperClass="bg-white">
        <div className="row p-1 align-items-center">
          <div className="col-10">
            <span>Cordelia Cruises safety program - Healthy waves.</span><br />
            <span className={cx(styles.textIndicator, "font-weight-bold")}>Learn more about current health protocol and travel requirements.</span>
          </div>
          <div className="col-2 text-right"><CancelIcon style={{ color: "#EA725B" }} onClick={() => { setUpdate(false) }} /></div>
        </div>
      </Container></> : null}


      {/* {
        isLoginModalShow === true ? <Login isLoginModalShow={isLoginModalShow} setLoginModalShow={setLoginModalShow} /> :
          (queryParams.get("platform") && queryParams.get("platform") !== null && queryParams.get("platform") === "true") || (window.location.pathname == "/" || window.location.pathname == "/cruise-routes" || window.location.pathname == "/post-quotation") || props.noAuth ? "" : <Login isLoginModalShow={isLoginModalShow} setLoginModalShow={setLoginModalShow} />
      } */}

      {
        (queryParams.get("platform") && queryParams.get("platform") !== null && queryParams.get("platform") === "true") || (window.location.pathname == "/cruise-routes" || window.location.pathname == "/post-quotation") || props.noAuth ? "" : <Login isLoginModalShow={isLoginModalShow} setLoginModalShow={setLoginModalShow} mybookingflag={mybookingflag} setMybookingFlag={setMybookingFlag} />
        // (queryParams.get("platform") && queryParams.get("platform") !== null && queryParams.get("platform") === "true") || (window.location.pathname == "/" || window.location.pathname == "/cruise-routes" || window.location.pathname == "/post-quotation") || props.noAuth ? "" : <Login isLoginModalShow={isLoginModalShow} setLoginModalShow={setLoginModalShow} />
      }

      <LoginOtp isLoginModalShow={isLoginOtpModalShow} setLoginModalShow={setLoginOtpModalShow} mybookingflag={mybookingflag} setMybookingFlag={setMybookingFlag} />
      <header className={cx(styles.siteHeader, props.bgPurple ? `bg-purple` : "")} style={{ position: props.cruiseRoute ? "absolute" : "relative", right: "0", width: "100%" }}>
        <Container>
          <div className={styles.siteIdentity}>
            {
              (queryParams.get("platform") && queryParams.get("platform") !== null && queryParams.get("platform") === "true" ? <img src={Logo} alt="Site Name" /> : <a href="/"> <img src={Logo} alt="Site Name" /> </a>)
            }
          </div>
          <nav className={styles.siteNavigation}>
            <div className={styles.menu}>
              {/* <MenuIcon style={{ color: "white" }}/> */}
              {/* <AccountCircleIcon style={{ color: "white", marginRight: "10px" }} /> */}
              <div>
                {
                  (queryParams.get("platform") && queryParams.get("platform") !== null && queryParams.get("platform") === "true")
                    ? "" :
                    <Menu
                      className={styles.customMenu}
                      customBurgerIcon={<MenuIcon />}
                      customCrossIcon={<CloseIcon htmlColor={"#EA725B"} />}
                      burgerButtonClassName={styles.menuButton}
                      crossButtonClassName={styles.crossButton}
                      overlayClassName={styles.menuOverlay}
                      menuClassName={styles.menuList}
                      itemListClassName={styles.menuItemList}
                      right>
                      {props.noAuth ? null : <div className="row d-flex align-items-center mb-4">
                        <div className="col-3">
                          <Avatar size={64} src={UserImg} />
                        </div>
                        <div className="col-9 pl-2">
                          <h6 className="mb-0">Nilam Patel</h6>
                          <p className="mb-0 text-gray">ID: 278903</p>
                        </div>
                      </div>}
                      <div>
                        {props.noMulticurrency ? null : <div className="row col-12 d-flex align-items-center justify-content-between mb-2">
                          <div className="col-5">
                            <p className={styles.currencyMobile}>Currency</p>
                          </div>
                          <div className="col-4">
                            <Select defaultValue="inr" className={cx(styles.currencySelectorMobile, "currencySelectorMobile")}>
                              <Option value="inr">₹ INR </Option>
                              <Option value="usd">$ USD</Option>
                            </Select>
                          </div>
                        </div>}
                      </div>
                      {!props.webcheckin ? <>
                      <Link to={"/cruise-routes"} className={styles.menuItem}>
                        <img className="mr-3" src={CruiseIcon} />
                        Find a cruise </Link>
                        <Divider style={{ width: "90%", margin: "15px 0px" }} />
                        <a href="/cruise-destinations" className={styles.menuItem}>
                          <img className="mr-3" src={DestinationIcon} />
                          Destinations</a>
                        <Divider style={{ width: "90%", margin: "15px 0px" }} />
                        <a href="/cruise-deals" className={styles.menuItem}>
                          <img className="mr-3" src={PromotionIcon} />
                          PROMOTIONS</a>
                        <Divider style={{ width: "90%", margin: "15px 0px" }} />
                        <a href="https://www.cordeliacruises.com/contact" className={styles.menuItem}>
                          <img className="mr-3" src={ContactIcon} />
                          CONTACT</a>
                        <Divider style={{ width: "90%", margin: "15px 0px" }} />
                        {!props.webcheckin && <><a href="#" className={styles.menuItem}>
                          <img className="mr-3" src={BookingIcon} />
                          MY BOOKINGS</a>
                          <Divider style={{ width: "90%", margin: "15px 0px" }} /></>}
                        <Link to="/web-check-in/booking-details" className={styles.menuItem}>
                          <img className="mr-3" src={DestinationIcon} />
                          WEB CHECK IN </Link>
                        <Divider style={{ width: "90%", margin: "15px 0px" }} />
                        {props.noAuth ? null : <a href="#" className={styles.menuItem}>
                          <img className="mr-3" src={LogoutIcon} />
                          LOGOUT</a>}</> :
                        <><a href="/web-check-in/booking-details" className={styles.menuItem}>
                          <img className="mr-3" src={DestinationIcon} />
                          WEB CHECK IN </a>
                          <Divider style={{ width: "90%", margin: "15px 0px" }} /></>}
                    </Menu>
                }
              </div>
            </div>
            <ul className={styles.nav}>
              {!props.webcheckin && <>
                <li className={styles.underli} onClick={() => setSelectedMenu(0)}>
                  <Dropdown arrow={true} overlay={exploreMenu} placement="bottomCenter">
                    <a target="_blank">
                      <span className={selectedMenu === 0 ? styles.selectedMenu : styles.notSelectedMenu}>
                        EXPLORE
                      </span>
                    </a>
                  </Dropdown>
                </li>
                <li className={styles.underli} onClick={() => setSelectedMenu(1)}>
                  <Dropdown arrow={true} overlay={planMenu} placement="bottomCenter">
                    <a target="_blank">
                      <span className={selectedMenu === 1 ? styles.selectedMenu : styles.notSelectedMenu}>
                        PLAN
                      </span>
                    </a>
                  </Dropdown>
                </li>
                <li className={styles.underli} onClick={() => setSelectedMenu(2)}>
                  <Dropdown arrow={true} overlay={manageMenu} placement="bottomCenter">
                    <a target="_blank">
                      <span className={selectedMenu === 2 ? styles.selectedMenu : styles.notSelectedMenu}>
                        MANAGE
                      </span>
                    </a>
                  </Dropdown>
                </li>
                <li className={styles.underli}>
                  <a href={`/contact`} target="_blank">
                    <span className={styles.notSelectedMenu}>
                      CONTACT
                    </span>
                  </a>
                </li>
                <li className={styles.underli}>
                  <a href={`/`} target="_blank">
                    <span className={styles.notSelectedMenu}>
                      REGISTER
                    </span>
                  </a>
                </li>
                <li className={styles.underli}>
                  <a href={`/`} target="_blank">
                    <span className={styles.notSelectedMenu}>
                      LOGIN
                    </span>
                  </a>
                </li>
                </>
              }
              {/* <li>
                <Button onClick={() => window.location = '/web-check-in/booking-details'}>WEB CHECK IN</Button>
              </li>
              {props.noMulticurrency ? null : <li>
                <Select defaultValue="inr" className="select-transparent">
                  <Option value="inr">₹ INR </Option>
                  <Option value="usd">$ USD</Option>
                </Select>
              </li>} */}

              {/* {auth === undefined || auth === null ? <li>
                  <Dropdown mouseLeaveDelay={2} overlay={menu} placement="bottomLeft">
                    <AccountCircleIcon style={{ color: "white", cursor: "pointer" }} />
                  </Dropdown>
                </li> : <li onClick={() => logout()}>
                  <span className={styles.logoutOption}>Logout</span>
                </li>
              } */}
            </ul>
          </nav>
        </Container>
      </header>
      <div className={styles.submenu}>
      <div className={styles.submenuContainer}>
        {renderCorrespondingMenu(selectedMenu)}
      </div>
      </div>
      {props.isBanner ? <img src={props.isMobile ? Banner : bannerMobile} alt="Site Name" className={styles.banner} /> : null}
    </div >
  );
}

export default Header