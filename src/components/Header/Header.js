import styles from "./Header.module.scss";
import Logo from "../../assets/img/logo.png";
import Banner from "../../assets/img/banner.png";
import bannerMobile from '../../assets/img/banner_mobile.png';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import Container from "../Container";
import { Link } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu'

function Header(props) {
  return (
    <div className="Header">
      <header className={styles.siteHeader}>
        <Container>
          <div className={styles.siteIdentity}>
            <a href="/">
              <img src={Logo} alt="Site Name" />
            </a>
          </div>
          <nav className={styles.siteNavigation}>
            <div className={styles.menu}>
              {/* <MenuIcon style={{ color: "white" }}/> */}
              <div>
                <Menu
                  className={styles.customMenu}
                  customBurgerIcon={<MenuIcon />}
                  customCrossIcon={<CloseIcon />}
                  burgerButtonClassName={styles.menuButton}
                  crossButtonClassName={styles.crossButton}
                  overlayClassName={styles.menuOverlay}
                  menuClassName={styles.menuList}
                  itemListClassName={styles.menuItemList}
                  right>
                  <Link to={"/cruise-routes"} className={styles.menuItem}>Find a cruise </Link>
                  <Link to="/destination" className={styles.menuItem}>Destinations </Link>
                  <a href="#" className={styles.menuItem}>My Bookings</a>
                  <a href="#" className={styles.menuItem}>Blog</a>
                  <a href="#" className={styles.menuItem}>Contact</a>
                </Menu>
              </div>
            </div>
            <ul className={styles.nav}>
              <li>
                <Link to={"/cruise-routes"}>Find a cruise</Link>
              </li>
              <li>
                <Link to="/destination">Destinations </Link>
              </li>
              <li>
                <a href="#">My Bookings</a>
              </li>
              <li>
                <a href="#">Blog</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
        </Container>
      </header>
      <img src={props.isMobile ? bannerMobile : Banner} alt="Site Name" className={styles.banner} />
    </div>
  );
}

export default Header;
