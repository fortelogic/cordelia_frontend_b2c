import React from 'react';
import { useHistory } from "react-router-dom"
import banner from '../../assets/img/banner.png';
import bannerMobile from '../../assets/img/banner_mobile.png';

function Jumbotron({ isMobile, noContent, history }) {

  return (
    <section className="cc-jumbotron">
      <figure>
        <img alt="cordelia cruises" src={isMobile ? bannerMobile : banner} />
      </figure>
      {!noContent && <div className="cc-jumbotron__content">
        <h3><span style={{ display: 'block' }}>This September,</span> sail to India’s only coral island chain, Lakshadweep</h3>
        <a className="cc-button cc-button--outline text-white" onClick={() => history.push("/cruise-destinations")}> Know More</a>
      </div>}
    </section >
  );
}

Jumbotron = React.memo(Jumbotron);

export { Jumbotron };
