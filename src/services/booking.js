import { client } from "./index";
import LocalStorage from "../utils/localStore"

export const check_availability = async (id, params, type = null) => {
  const auth = LocalStorage.getItem("auth")
  if (type) {
    return await client.post(
      `https://staging.cordeliacruises.com/api/bookings/${id}/check_availability`,
      params,
      {
        headers: { 'Authorization': 'Bearer ' + auth.token }
      }
    )
  } else {
    return await client.post(
      `/api/itineraries/${id}/check_availability`,
      params,
      {
        headers: { 'Authorization': 'Bearer ' + auth.token }
      }
    )
  }
}