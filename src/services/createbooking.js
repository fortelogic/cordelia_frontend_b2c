import { client } from "./index"
import LocalStorage from "../utils/localStore";


export const createbooking = async (params, itineraryID, type = null) => {
    const auth = LocalStorage.getItem("auth")
    if (type) {
        return await client.post(`https://staging.cordeliacruises.com/api/bookings`, { "variables": { "input": { "rooms": params }, "itineraryID": itineraryID } }, { headers: { Authorization: `Bearer ` + auth.token } })

    } else {
        return await client.post("/api/bookings", { "variables": { "input": { "rooms": params }, "itineraryID": itineraryID } }, { headers: { Authorization: `Bearer ` + auth.token } })

    }
}