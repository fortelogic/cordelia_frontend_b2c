import { client } from "./index";

export const getPaymentDetails = (params, type = null) => {

    if (type === "quotation") {
        return client.post("https://staging.cordeliacruises.com/api/payments", params, {})

    } else {
        return client.post("api/payments", params, {})
    }
}

