import { client } from "../index"
import LocalStorage from "../../utils/localStore"


export const completePostQuotation = (itemid) => {
    return client.post(`https://staging.cordeliacruises.com/api/quotations/${itemid}/payment_link`, {}, {})
}