import { client } from "../index"
import LocalStorage from "../../utils/localStore"


export const sendQuotationValues = (itemid) => {

    // const auth = LocalStorage.getItem("auth")

    // return client.post(`https://staging.cordeliacruises.com/api/quotations/${itemid}/payment_link`, {
    //     headers: {
    //         Authorization: `Bearer ${auth.token}`,
    //     }
    // })

    return client.post(`https://staging.cordeliacruises.com/api/quotations/${itemid}/payment_link`, {}, {})
}