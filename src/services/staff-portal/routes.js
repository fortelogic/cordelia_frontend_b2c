import { client } from "../index"
import LocalStorage from "../../utils/localStore"


export const filterWithDatesService = (dateOne, dateTwo) => {

    const auth = LocalStorage.getItem("auth")

    return client.get(`https://staff.staging.cordeliacruises.com/staff_portal/routes?start_date=${dateOne}&to_date=${dateTwo}`, {
        headers: {
            Authorization: `Bearer ${auth.token}`,
        }
    })
}