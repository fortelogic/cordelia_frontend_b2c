import { client } from "../index"
import LocalStorage from "../../utils/localStore"


export const postQuotationBooking = (bookingid, authtoken) => {

    const requestOptions = {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ 'quote_tok': authtoken })
    }
    fetch(`https://staging.cordeliacruises.com/api/quotations/${bookingid}/booking`, requestOptions)
        .then((res) => {
            if (res.status === 401) {
                localStorage.clear();
            }
            if (res.ok) {
                return res.json();
            } else {
                throw new Error("Something went wrong");
            }
        })
        .then((data) => {
            if (data.status == 'failed') {
                console.log("No Data")
            } else {
                console.log(data.booking)
            }
        })
}