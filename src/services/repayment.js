import { client } from "./index"
import LocalStorage from "../utils/localStore";


export const getRePaymentDetails = (params) => {
    const auth = LocalStorage.getItem("auth")
    return client.post("api/payments/repay_due_amount", params, { headers: { Authorization: `Bearer ` + auth.token } })
}

