import { client } from "./index"
import LocalStorage from "../utils/localStore"

export const bookingid = async (bookingid, params, type = null) => {

    const auth = LocalStorage.getItem("auth")

    if (type) {

        return await client.post(`https://staging.cordeliacruises.com/api/bookings/${bookingid}/complete`, { "rooms": params }, { headers: { Authorization: `Bearer ` + auth.token } })
    } else {
        
        if(params.length > 1) {
            let anele = params.shift()
        }

        return await client.put(`/api/bookings/${bookingid}`, {
            "variables": {
                "input": {
                    "rooms": params
                }
            }
        }, { headers: { Authorization: `Bearer ` + auth.token } })
    }
}