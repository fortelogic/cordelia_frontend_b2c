import { client } from "./index"
import LocalStorage from "../utils/localStore"

export const getBooking = (params) => {
    // const auth = LocalStorage.get("auth")

    // return client.get(`/api/bookings/${params}/details?web_checkin=true`, { headers: { Authorization: `Bearer ` + auth.auth.auth.token } })
    return client.get(`/api/bookings/${params}/details?web_checkin=true`)
}

export const saveGuestDeclaration = (params) => {
    // const auth = LocalStorage.get("auth")

    // return client.post(`/api/bookings/save_declaration`, params, { headers: { Authorization: `Bearer ` + auth.auth.auth.token } })
    return client.post(`/api/bookings/save_declaration`, params)
}

export const saveGuestDetails = (params) => {
    // const auth = LocalStorage.get("auth")

    // return client.post(`/api/bookings/update_guests`, params, { headers: { Authorization: `Bearer ` + auth.auth.auth.token } })
    return client.post(`/api/bookings/update_guests`, params )
}

export const getBoardingPass = (params) => {
    // const auth = LocalStorage.get("auth")

    // return client.get(`/api/bookings/boarding_passes?booking_id=${params}`, { headers: { Authorization: `Bearer ` + auth.auth.auth.token } })
    return client.get(`/api/bookings/boarding_passes?booking_id=${params}`)
}

export const shareBoardingPass = (params) => {
    // const auth = LocalStorage.get("auth")

    // return client.post(`/api/bookings/update_guests`, params, { headers: { Authorization: `Bearer ` + auth.auth.auth.token } })
    return client.post(`/api/bookings/share_boarding_pass`, params )
}