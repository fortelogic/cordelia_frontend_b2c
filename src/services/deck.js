import { client } from "./index";
import LocalStorage from "../utils/localStore";

export const getDecks = (itineraryId, catid = null, type = null) => {
  const auth = LocalStorage.getItem("auth")

  // console.log("Sawal", catid)

  if (type) {
    return client.get(`https://staging.cordeliacruises.com/api/itineraries/${itineraryId}/decks/?category_id=${catid}`, {
      headers: {
        Authorization: `Bearer ${auth.token}`,
      }
    })

    // return client.get(`https://staging.cordeliacruises.com/api/itineraries/714d0e18-d851-4c69-95f7-de1aaf560a9a/decks/?category_id=2c39eafe-3b58-403e-8421-b1528f7cd12e`, {
    //   headers: {
    //     Authorization: `Bearer ${auth.token}`,
    //   }
    // })

  } else {
    return client.get(`api/itineraries/${itineraryId}/decks?category_id=${catid}`, {
      headers: {
        Authorization: `Bearer ${auth.token}`,
      }
    })
  }
}