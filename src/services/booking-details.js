import { client } from "./index";
import LocalStorage from "../utils/localStore"

const bookingdetails = async (bookingid) => {
    const auth = LocalStorage.getItem("auth")
    return await client.get(
        `/api/bookings/${bookingid}/details`,
        {
            headers: { 'Authorization': 'Bearer ' + auth.token }
        }
    )
}

export default bookingdetails