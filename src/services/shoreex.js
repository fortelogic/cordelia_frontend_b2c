import { client } from "./index"
import LocalStorage from "../utils/localStore"

export const shoreExcursions = async (id = null) => {

    if (id) {
        const auth = LocalStorage.getItem("auth")
        const booking = LocalStorage.get("bookingSummary")

        return client.get(`api/bookings/${id}/shore_excursions`, { headers: { Authorization: `Bearer ` + auth.token } })
    } else {
        const auth = LocalStorage.getItem("auth")
        const booking = LocalStorage.get("bookingSummary")

        return client.get(`api/bookings/${booking.booking.id}/shore_excursions`, { headers: { Authorization: `Bearer ` + auth.token } })
    }
}

export const updateShoreExcursionsService = (params, id = null) => {
    if (id) {
        const auth = LocalStorage.getItem("auth")
        const booking = LocalStorage.get("bookingSummary")

        return client.post(`api/bookings/${id}/update_shore_excursions`, params, { headers: { Authorization: `Bearer ` + auth.token } })
    } else {
        const auth = LocalStorage.getItem("auth")
        const booking = LocalStorage.get("bookingSummary")

        return client.post(`api/bookings/${booking.booking.id}/update_shore_excursions`, params, { headers: { Authorization: `Bearer ` + auth.token } })
    }
}