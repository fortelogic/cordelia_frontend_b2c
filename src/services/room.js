import { client } from "./index";
import LocalStorage from "../utils/localStore"


// export const getRooms = (params) => client.get(`api/itineraries/${params.itineraryID}/decks/${params.deckId}/layout`, { headers: { Authorization: `Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYTIyOTIwZTgtNmVhMi00Zjk3LWFjOWUtZDk5YWExYjE5OTZhIiwidGVtcF90b2tlbiI6dHJ1ZSwiZXhwIjoxNjI3ODkxNjMzfQ.k8FIzoGDyuIPPz4cTGk8O5POj8sZ0Ia0i5oNSSptzNc` } }, params)
export const getRooms = (params, type = null) => {
    const auth = LocalStorage.getItem("auth")
    if (type) {
        return client.get(`https://staging.cordeliacruises.com/api/itineraries/${params.itineraryID}/decks/${params.deckId}/layout?beds=${params.beds}&category_id=${params.category_id}`, { headers: { Authorization: `Bearer ` + auth.token } })
    } else {
        return client.get(`api/itineraries/${params.itineraryID}/decks/${params.deckId}/layout?beds=${params.beds}&category_id=${params.category_id}`, { headers: { Authorization: `Bearer ` + auth.token } })
    }
}