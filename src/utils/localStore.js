const setItem = (key, value) => {
  const val = JSON.stringify(value);
  localStorage.setItem(key, val);
};

const getItem = (key) => {
  let item = localStorage.getItem(key);
  return JSON.parse(item);
};

//return store if exist otherwise create new store and return that
const get = (key = "") => {
  let store = getItem("store");
  if (!store) {
    store = {};
    setItem("store", store);
  }
  return key.toString().trim().length ? store[key] : store;
};

//add the key value pair in store
const add = (key, value) => {
  let store = get();
  if (store) {
    store[key] = value;
    setItem("store", store);
  }
};

const remove = (key) => {
  let store = get();
  if (store) {
    delete store[key];
    setItem("store", store);
  }
};

export default {
  get,
  add,
  remove,
  getItem,
  setItem,
};
