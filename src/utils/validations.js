import Joi from "joi";

export const phone_pattern = /^(\+[0123456789]{2,4}[ ])?[0123456789]{10,12}$/;
export const phone_pattern_ten_digit = /^(\+[\d]{2,4}[ ])?[\d]{10}$/;

export const ValidatePan=(input)=>{
    var PanNumber = new RegExp("[A-Z]{5}[0-9]{4}[A-Z]{1}");
    if (PanNumber.test(input.toUpperCase())) {
        return true;
    } else {
        return false;
    }
}

export const ValidateVoterID=(input)=>{
    var VoterID = new RegExp("^[a-zA-Z]{3}[0-9]{7}$");
    if (VoterID.test(input)) {
        return true;
    } else {
        return false;
    }
}

export const ValidateAadhar=(input)=>{
    var Aadhar = new RegExp("^[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{4}$");
    if (Aadhar.test(input)) {
        return true;
    } else {
        return false;
    }
}

export const ValidateEmail=(input)=>{
    var Email = new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");
    if (Email.test(input)) {
        return true;
    } else {
        return false;
    }
}

export const email = Joi.string()
  .email({ minDomainSegments: 2, tlds: { allow: false } })
  .required();

export const phone = Joi.string().pattern(phone_pattern).required();

export const first_name = Joi.string().min(2).required();
export const last_name = Joi.string().min(2).required();
export const country_code = Joi.string().min(2).required();