import * as actions from "../actionTypes/authActionType"
import localStore from "../../utils/localStore"

export default (state = {}, action) => {
    switch (action.type) {
        case actions.AUTHENTICATE_USER:
            localStore.add("auth", action.payload)
            return {
                ...state,
                ...action.payload
            }
        case actions.DEAUTHENTICATE_USER:
            return {
                ...state,
            }
        default:
            return state
    }
}