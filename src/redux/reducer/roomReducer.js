import * as actionType from '../actionTypes/roomActionType';
import localStore from "../../utils/localStore";

export default (state = { selectedRooms: [], bookingsummary: [], allGuests: [] }, action) => {
    switch (action.type) {
        case actionType.GET_ROOMS:
            return {
                ...state,
                roomsData: action.payload
            };

        case actionType.SELECT_ROOMS:
            return {
                ...state,
                selectedRooms: [...state.selectedRooms, action.payload]
            }

        case actionType.REMOVE_ROOMS:
            return {
                ...state,
                selectedRooms: state.selectedRooms.filter((item, index) => item.itinerary_room_id !== action.payload.itinerary_room_id)
            }

        case actionType.BOOKING_SUMMARY:
            localStore.add("bookingSummary", action.payload);
            return {
                ...state,
                bookingsummary: action.payload
            }

        case actionType.ALL_GUESTS:
            localStore.add("allguests", state.allGuests.concat(action.payload))
            return {
                ...state,
                allGuests: state.allGuests.concat(action.payload)
            }
    }
    return state;
}

