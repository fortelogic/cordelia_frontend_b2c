import * as actionType from "../actionTypes/deckActionType";

export default (state = [], action) => {
  switch (action.type) {
    case actionType.GET_DECKS: {
      return [...action.payload];
    }
    default:
      return state;
  }
};
