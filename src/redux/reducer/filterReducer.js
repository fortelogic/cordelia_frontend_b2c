import * as actionType from "../actionTypes/filterActionType";

export default (state = [], action) => {

  switch (action.type) {
    case actionType.GET_FILTER: {
      return state;
    }
    case actionType.SET_FILTER: {
      return { filter: action.payload };
    }
    default:
      return state;
  }
};
