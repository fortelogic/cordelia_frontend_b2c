import * as actionType from "../actionTypes/shoreActionType"
import localStore from "../../utils/localStore"

export default (state = {}, action) => {
    switch (action.type) {
        case actionType.SHORE_EX_LIST:
            return {
                ...state,
                shores: action.payload
            }
        case actionType.UPDATE_SHORE_EX:
            const { data } = action.payload
            localStore.add("updatedShores", data)
            return {
                ...state,
                shoresUpdated: data
            }
        default:
            return state
    }
}