import { combineReducers } from "redux";
import authReducer from "./authReducer";
import routeReducer from "./routeReducer";
import cabinsReducer from "./cabinsReducer";
import bookingReducer from "./bookingReducer";
import deckReducer from "./deckReducer";
import roomReducer from "./roomReducer"
import filterReducer from "./filterReducer"
import shoreReducer from "./shoreReducer"

export default combineReducers({
  auth: authReducer,
  route: routeReducer,
  allcabins: cabinsReducer,
  booking: bookingReducer,
  deckCollection: deckReducer,
  roomReducer: roomReducer,
  filterReducer: filterReducer,
  shores: shoreReducer,
});
