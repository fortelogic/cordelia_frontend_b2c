import localStore from "../../utils/localStore";
import * as actionType from "../actionTypes/cabinsActionType";

const defaultState = [
  {
    id: Date.now().toString(),
    name: "Cabin 1",
    detail: {},
    guest: {
      total: 1,
      adults: 1,
      children: 0,
      infants: 0,
    },
    deck: '',
    room: [],
    fisrtGuest: []
  },
];

const MAX_GUEST_COUNT = 4;
const MIN_GUEST_COUNT = 0;

export default (state = defaultState, action) => {
  switch (action.type) {
    case actionType.GET_CABINS:
      let cabins = localStore.get("cabins");
      return cabins
        ? cabins
        : [
          {
            id: Date.now().toString(),
            name: "Cabin 1",
            guest: {
              total: 1,
              adults: 1,
              children: 0,
              infants: 0,
            },
          },
        ];

    case actionType.ADD_CABIN:
      let cabin = {
        id: Date.now().toString(),
        name: `Cabin ${state.length + 1}`,
        guest: {
          total: 1,
          adults: 1,
          children: 0,
          infants: 0,
        },
      };
      return [...state, cabin];

    case actionType.REMOVE_CABIN: {
      let updatedCabinList = [...state].filter(
        (cabin) => cabin.id !== action.payload.cabinId
      );
      updatedCabinList.map((data, index) => {
        if (!data.hasOwnProperty('detail'))
          updatedCabinList[index].name = `Cabin ${index + 1}`
      })
      if (action.payload.deleteFromStore) {
        localStore.add("cabins", updatedCabinList);
      }
      return updatedCabinList;
    }
    case actionType.ADD_GUEST: {
      let { selectedCabin, guestType } = action.payload;
      return state.map((cabin) => {
        if (
          cabin.id === selectedCabin.id &&
          cabin.guest.total < MAX_GUEST_COUNT &&
          guestType in cabin.guest
        ) {
          cabin.guest[guestType] += 1;
          cabin.guest.total += 1;
        }
        return cabin;
      });
    }
    case actionType.SET_CABIN_NAME: {
      let { selectedCabin, name, detail } = action.payload;
      let filteredCabin = state.map((cabin) => {
        if (
          cabin.id === selectedCabin.id
        ) {
          cabin.name = name
          cabin.detail = detail
        }
        return cabin;
      });
      localStore.add("cabins", filteredCabin);
      return filteredCabin
    }

    case actionType.ADD_DECK: {
      let { selectedCabin, deck } = action.payload;
      let filteredCabin = state.map((cabin) => {
        if (
          cabin.id === selectedCabin.id
        ) {
          cabin.deck = deck
        }
        if (cabin.hasOwnProperty("room")) {
          cabin["room"] = []
        }
        console.log("Khali", cabin)
        return cabin;
      });
      localStore.add("cabins", filteredCabin);
      return filteredCabin
    }

    case actionType.ADD_ROOM: {
      let { selectedCabin } = action.payload;
      let filteredCabin = state.map((cabin) => {
        if (
          cabin.id === selectedCabin
        ) {
          cabin.room = [action.payload]
        }
        return cabin;
      });
      localStore.add("cabins", filteredCabin);
      return filteredCabin
    }

    case actionType.REMOVE_ROOM: {
      let { selectedCabin } = action.payload;
      let filteredCabin = state.map((cabin) => {
        if (cabin.id === selectedCabin) {
          cabin.room = cabin.room.filter((item) => item.itineraryID === action.payload.itinerary_room_id)
        }
        return cabin;
      });
      localStore.add("cabins", filteredCabin);
      return filteredCabin
    }

    // case actionType.ADD_ROOM: {
    //   let { room } = action.payload;
    //   return ({
    //     ...state,
    //     room: room
    //   })
    // }
    case actionType.REMOVE_GUEST: {
      let { selectedCabin, guestType } = action.payload;
      return state.map((cabin) => {
        if (
          cabin.id === selectedCabin.id &&
          cabin.guest.total > MIN_GUEST_COUNT &&
          guestType in cabin.guest
        ) {
          cabin.guest[guestType] -= 1;
          cabin.guest.total -= 1;
        }
        return cabin;
      });
    }

    case actionType.REMOVE_INVALID_CABINS: {
      let cabins = state.filter((cabin) => cabin.guest.total > 0);
      localStore.add("cabins", cabins);
      return cabins;
    }

    default:
      return state;
  }
};
