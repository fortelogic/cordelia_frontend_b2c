import * as actionType from "../actionTypes/bookingActionType";
import localStore from "../../utils/localStore";

export default (state = {}, action) => {
  switch (action.type) {
    case actionType.SELECTED_DATE:
      if (state.itineraryID === action.payload.itineraryID) {
        localStore.remove("itineraryID")
        return {
          ...state,
          itineraryID: "",
        };
      }
      return {
        ...state,
        ...action.payload,
      };
    case actionType.GET_BOOKING_STATUS:
      const itineraryID = localStore.get("itineraryID");
      return {
        ...state,
        itineraryID: itineraryID,
      };
    case actionType.SET_BOOKING_STATUS: {
      let itineraryID = localStore.get("itineraryID");
      if (itineraryID !== action.payload.itineraryID || itineraryID === undefined) {
        localStore.remove("cabins")
        localStore.add("itineraryID", action.payload.itineraryID)
        localStore.add("booking", action.payload.booking)
      }
      return {
        ...state,
        ...action.payload,
      };
    }
    case actionType.SET_BOOKING_AVAILABILITY: {
      return {
        ...state,
        availability: action.payload,
      };
    }

    default:
      return state;
  }
};
