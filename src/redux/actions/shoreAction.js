import { SHORE_EX_LIST, UPDATE_SHORE_EX } from "../actionTypes/shoreActionType"
import { shoreExcursions, updateShoreExcursionsService } from "../../services/shoreex"

export const shoreExcursionList = (id = null) => async (dispatch) => {
    // const response = await shoreExcursions()
    const response = await shoreExcursions(id)
    return dispatch({
        type: SHORE_EX_LIST,
        payload: response.data ? response.data : null
    })
}

export const updateShoreExcursions = (params, id = null) => async (dispatch) => {
    // console.log("This Params", params)
    const shores = await updateShoreExcursionsService(params, id)
    return dispatch({
        type: UPDATE_SHORE_EX,
        payload: shores
    })
}