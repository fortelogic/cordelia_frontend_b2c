import * as apis from "../../services/room";
import * as actionType from "../actionTypes/roomActionType";

export const getRooms = (params, type = null) => async (dispatch) => {
    const response = await apis.getRooms(params, type);
    if (response) {
        if (response.statusText === "OK") {
            return dispatch({
                type: actionType.GET_ROOMS,
                payload: response.data,
            });
        }
    }
    return response
};

export const selectRooms = (params) => async (dispatch) => {
    return dispatch({
        type: actionType.SELECT_ROOMS,
        payload: params
    })
}

export const removeRooms = (params) => async (dispatch) => {
    return dispatch({
        type: actionType.REMOVE_ROOMS,
        payload: params
    })
}

export const bookingsummary = (params) => async (dispatch) => {
    return dispatch({
        type: actionType.BOOKING_SUMMARY,
        payload: params
    })
}

export const allGuests = (params) => async (dispatch) => {
    return dispatch({
        type: actionType.ALL_GUESTS,
        payload: params
    })
}