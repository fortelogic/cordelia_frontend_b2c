import * as apis from "../../services/route";
import * as actionType from "../actionTypes/routeActionType";

export const getRoutes = (params) => async (dispatch) => {
  const response = await apis.getRoutes(params);
  const { defaultSort } = params
  if (response) {
    if (response.statusText === "OK") {
      let routeData = response.data.data.routes
      if (defaultSort) {
        routeData = routeData.sort((a, b) => {
          if (defaultSort.type === "asc") {
            return a.itineraries[0][defaultSort.value] <
              b.itineraries[0][defaultSort.value]
              ? -1
              : 1;
          } else {
            return a.itineraries[0][defaultSort.value] <
              b.itineraries[0][defaultSort.value]
              ? 1
              : -1;
          }
        });
      }
      return dispatch({
        type: actionType.GET_ROUTES,
        payload: routeData,
      });
    }
  }
  return response;
};
