import * as actionType from "../actionTypes/filterActionType";

export const getFilter = () => async (dispatch) => {
  dispatch({
    type: actionType.GET_FILTER,
  });
};

export const setFilter = (filter) => async (dispatch) => {
  dispatch({
    type: actionType.SET_FILTER,
    payload: filter,
  });
};
