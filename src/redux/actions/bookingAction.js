import * as apis from "../../services/booking";
import * as actionType from "../actionTypes/bookingActionType";

export const setSelectedDate = (selectedDate) => async (dispatch) => {
  dispatch({
    type: actionType.SELECTED_DATE,
    payload: selectedDate,
  });
};

export const checkAvailability = (Id, parama, type = null) => async (dispatch) => {
  const response = await apis.check_availability(Id, parama, type)
  if (response.status == 200) {
    dispatch({
      type: actionType.SET_BOOKING_AVAILABILITY,
      payload: response.data.data.checkAvailability,
    })
  }
  return response
}

export const getBookingStatus = () => async (dispatch) => {
  dispatch({
    type: actionType.GET_BOOKING_STATUS,
  });
};

export const setBookingStatus = (bookingStatus) => async (dispatch) => {
  dispatch({
    type: actionType.SET_BOOKING_STATUS,
    payload: bookingStatus,
  });
};
