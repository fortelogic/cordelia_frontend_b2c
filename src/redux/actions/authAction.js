import * as apis from "../../services/auth";
import * as actionType from "../actionTypes/authActionType";

export const authenticate = (params) => async (dispatch) => {
  const response = await apis.authenticate(params)
  console.log("Response", response)
  if (response.data.hasOwnProperty("data")) {
    localStorage.setItem("isLoggedIn", true)
    dispatch({
      type: actionType.AUTHENTICATE_USER,
      payload: {
        user: params,
        auth: response.data.data
      },
    })
    return response.data;
  }
  return response
}

export const deauthenticate = () => async (dispatch) => {

}

