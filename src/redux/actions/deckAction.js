import * as apis from "../../services/deck";
import * as actionType from "../actionTypes/deckActionType";

export const getDecks = (itineraryId, catid = null, type = null) => async (dispatch) => {
  const response = await apis.getDecks(itineraryId, catid, type)
  if (response) {
    if (response.statusText === "OK") {
      const data = response.data.sort((a, b) => (a.id < b.id ? -1 : 1));
      dispatch({
        type: actionType.GET_DECKS,
        payload: data,
      });
    }
  }
};

// export const setDeck = (deck) => async (dispatch) => {
//   dispatch({
//     type: actionType.SET_DECK,
//     payload: deck,
//   });

// };
