import * as apis from "../../services/route";
import * as actionType from "../actionTypes/cabinsActionType";
import localStore from "../../utils/localStore";

export const getCabins = () => async (dispatch) => {
  return dispatch({
    type: actionType.GET_CABINS,
  });
};

export const addCabin = () => async (dispatch) => {
  return dispatch({
    type: actionType.ADD_CABIN,
  });
};

export const setCabinName = (payload) => async (dispatch) => {
  return dispatch({
    type: actionType.SET_CABIN_NAME,
    payload: payload,
  });
};

export const addDeck = (payload) => async (dispatch) => {
  return dispatch({
    type: actionType.ADD_DECK,
    payload: payload,
  });
};

export const addRoom = (payload) => async (dispatch) => {
  return dispatch({
    type: actionType.ADD_ROOM,
    payload: payload
  });
};

export const removeRoom = (payload) => async (dispatch) => {
  return dispatch({
    type: actionType.REMOVE_ROOM,
    payload: payload
  });
};


export const removeCabin =
  (cabinId, deleteFromStore = false) =>
    async (dispatch) => {
      return dispatch({
        type: actionType.REMOVE_CABIN,
        payload: { cabinId: cabinId, deleteFromStore },
      });
    };

export const addGuestToSelectedCabin = (payload) => async (dispatch) => {
  return dispatch({
    type: actionType.ADD_GUEST,
    payload: payload,
  });
};

export const removeGuestFromSelectedCabin = (payload) => async (dispatch) => {
  return dispatch({
    type: actionType.REMOVE_GUEST,
    payload: payload,
  });
};

export const removeInvalidCabins = () => async (dispatch) => {
  return dispatch({
    type: actionType.REMOVE_INVALID_CABINS,
  });
};