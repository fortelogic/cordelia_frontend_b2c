import React,{useEffect, Component, useContext, useState} from "react";
import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container";
import Card from '../../components/UI/Card';
import cx from "classnames";
import styles from "./my-booking-details.module.css";
import LocalStorage from "../../utils/localStore"
import moment from 'moment'
import { ShareAltOutlined, DownloadOutlined, PrinterOutlined, DownOutlined, UpOutlined } from '@ant-design/icons';
import {
    useHistory,
    withRouter,
    useLocation
  } from "react-router-dom";
  import DepartureIcon from "../../assets/img/departure-icon.svg";
  import Loader from "../../components/Loading/LoadingIcon";

var bookingID = null;
class MyBookingsDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookingData:[],
            loading: true
        }
    }

    componentDidMount() {
        const { match, location, history } = this.props;
        bookingID = location.state.id;
        this.setState({bookingIdState:bookingID})
        this.detailApi(bookingID)
    }

    detailApi = (bookingID) => {
        const auth = JSON.parse(localStorage.getItem("auth_otp"))
        fetch(`${process.env.REACT_APP_API_ENDPOINT}/api/bookings/${bookingID}/details`,
        {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: `Bearer ${auth.token}`,
            },
        }).then((res) => {
            if(res.status == 401) {
                throw new Error("Something went wrong");
            }
            if (res.ok) {
              // console.log('hdjshd')
                return res.json();
            } else {
                throw new Error("Something went wrong");
            }
          })
          .then((response) => {
                console.log(response)
                this.setState({bookingData: response})
                this.setState({loading:false})
                // setBookingData(response.bookings);
                // setCompleteData(response)
                // setLoading(false);
          })
          .catch((error) => {

          });
    }

    render() {
        return(<>
            {!this.state.loading && <MyBookingsDetailsComponents bookingData={this.state.bookingData}/>}
            {this.state.loading &&  <Loader />}
            </>
        )
    }
}


export default MyBookingsDetails;

const MyBookingsDetailsComponents = ({bookingData}) => {
    console.log(bookingData)
    const [showPersonalData, setShowPersonalData] = useState(false)
    const [showPriceData, setShowPriceData] = useState(true)

    return (
        <div className="Home">
            <Header bgPurple />
            <Container>
                <div className={cx("col-12 col-md-12 mt-4 mb-4")}>
                    <div className="row">
                        <div className="col-lg-9 col-sm-12">
                            <p className={styles.myBooking}>MY BOOKING</p>
                        </div>
                        <div className="col-lg-3 col-sm-12">
                            <div className="row pl-5 align-items-center justify-content-around">
                                <p className={cx(styles.IconsCon,"cursor-pointer")}><span>Share</span> <ShareAltOutlined /></p>
                                <p className={cx(styles.IconsCon,"cursor-pointer")}><span>Print</span> <PrinterOutlined /></p>
                                <a href={bookingData.booking.invoice_url} target="_blank"><p className={cx(styles.IconsCon,"cursor-pointer")}><span>Download</span> < DownloadOutlined/></p></a>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-7 col-sm-12">
                            <p className={styles.header}>Booking Summary</p>
                            <Card>
                                <div>
                                    <div className={cx(styles.cruisenightContainer, "p-4")}>
                                        <div className="col-12 col-md-12 row">
                                            <div className="col-6">
                                                <h5 className={cx(styles.nightHeader)}>{bookingData.booking.itinerary.nights} Night Cruise</h5>
                                                <h6 className={cx(styles.subnightHeader)}>{moment(bookingData.booking.itinerary.start_time).format("dddd, D MMMM YYYY")}</h6>
                                            </div>
                                            <div className={cx(styles.statuscontainer,"col-6")}>
                                                <h5 className={bookingData.booking.status == "CONFIRMED" ? styles.status : styles.statusOrange}>{bookingData.booking.status}</h5>
                                                <h6 className={cx(styles.bookingID)}>Booking ID: {bookingData.booking.number}8</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={cx(styles.portdetails,"col-12 p-4 row")}>
                                            <div className={cx(styles.borderStyle,"col-4 pb-4")}>
                                                    <p className={styles.subheading}>Cruise Start</p>
                                                    <h5 className={styles.startPlace}>{bookingData.booking.itinerary.ports[0].city}</h5>
                                                    <h6 className={styles.startDate}>{moment(bookingData.booking.itinerary.start_time).format("dddd, D MMMM YYYY")}</h6>
                                                    <h6 className={styles.startPlaceSub}>{bookingData.booking.itinerary.ports[0].city}</h6>
                                                    <p className={cx(styles.getDirection, "pt-2")}>Get Direction</p>
                                            </div>
                                            <div className={cx(styles.borderStyle,"col-4 pb-4 row pt-4")}>
                                                <p className="pt-2" style={{fontWeight: 'bold', color:'#EA725B'}}>- - - - - - - - -</p>
                                                <img src={DepartureIcon} style={{ height: '40px', width: '40px', display:"flex", justifyContent:'center' }} />
                                                <p className="pt-2" style={{fontWeight: 'bold', color:'#EA725B'}}>- - - - - - - - -</p>
                                            </div>
                                            <div className={cx(styles.borderStyle,"col-4 pb-4")}>
                                                    <p className={styles.subheading}>Cruise End</p>
                                                    <h5 className={styles.startPlace}>{bookingData.booking.itinerary.ports[bookingData.booking.itinerary.ports.length-1].city}</h5>
                                                    <h6 className={styles.startDate}>{moment(bookingData.booking.itinerary.end_time).format("dddd, D MMMM YYYY")}</h6>
                                                    <h6 className={styles.startPlaceSub}>{bookingData.booking.itinerary.ports[bookingData.booking.itinerary.ports.length-1].city}</h6>
                                            </div>
                                    </div>
                                    <div className={cx(styles.portdetails,"col-12 pl-4 pr-4 row")}>
                                        <div className={cx(styles.borderStyle,"col-12")}>
                                            <p className={styles.subheading}>Itinerary</p>
                                            <p className={styles.itineraryDetails}>{bookingData.booking.itinerary.name}</p>
                                        </div>
                                    </div>
                                    <p className={cx(styles.subheadingCabin,"pt-4")}>Cabin Details</p>
                                        <div className={cx(styles.portdetails,"col-12 pl-4 pr-4 row")}>
                                            {bookingData.booking.rooms.map((data, i) => {
                                                return(
                                                    <div className="col-lg-6 col-sm-12">
                                                        <p className={styles.cabinTitle}>Cabin: {i+1}</p>
                                                        <div className={styles.cabinDetailsCont}>
                                                            <p className={styles.cabinDetails}>Catagory: <span>{data.category}</span></p>
                                                            <p className={styles.cabinDetails}>Deck No: <span>{data.deck_no}</span></p>
                                                        </div>
                                                        <div className={styles.cabinDetailsCont}>
                                                            <p className={styles.cabinDetails}>Room No: <span>{data.number}</span></p>
                                                            <p className={styles.cabinDetails}>Total Guest: <span>{data.guests.length}</span></p>
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                </div>
                            </Card>
                        </div>
                        <div className="col-lg-5 col-sm-12">
                            <p className={styles.header}>Guest Details</p>
                            <Card>
                                <div>
                                    <div className={cx(styles.cruisenightContainer, "p-4")}>
                                        <div className={cx(styles.marginRemover,"col-12 col-md-12 row")}>
                                            <div className={cx(styles.marginRemover,"col-8")}>
                                                <h5 className={cx(styles.nightHeader)}>{bookingData.booking.guests} Adults</h5>
                                                <h6 className={cx(styles.subnightHeader)}>{bookingData.booking.guest_count.adult} Adult {bookingData.booking.guest_count.child > 0 ? "| "+ bookingData.booking.guest_count.child + "Child": null } {bookingData.booking.guest_count.infant > 0 ? "| "+bookingData.booking.guest_count.infant + "Infant": null }</h6>
                                            </div>
                                            <div className={cx(styles.marginRemover, "col-4")}>
                                                <p className={styles.arrowUpAndDown} onClick={()=>setShowPersonalData(!showPersonalData)}>
                                                    {showPersonalData ? <UpOutlined /> :<DownOutlined />}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                    {showPersonalData && <div className={cx(styles.marginRemover,"col-12 row p-4")}>
                                            {bookingData.booking.rooms.map((data,i) => {
                                                return(
                                                    <CabinDetails data={data} index={i}/>
                                                )
                                            })}
                                        </div>}
                                    </div>
                                </div>
                                
                            </Card>
                            <p className={cx(styles.header,"pt-4")}>Pricing Details</p>
                            <Card>
                                <div>
                                    <div className={cx(styles.cruisenightContainer, "p-2")}>
                                        <div className={cx(styles.marginRemover,"col-12 col-md-12 row")}>
                                            <div className="col-10">
                                                <h5 className={cx(styles.nightHeader,"pt-2")}>Grand total  ₹ {bookingData.booking.total_amnt}</h5>
                                                {bookingData.booking.status == "RESERVED" &&  <h5 className={cx(styles.nightsubHeader,"pt-2")}>Due Amount ₹ {bookingData.booking.pending_amount}</h5>}
                                            </div>
                                            <div className={cx(styles.arrowUpAndDown,"col-2")}>
                                                <p onClick={()=>setShowPriceData(!showPriceData)}>
                                                    {showPriceData ? <UpOutlined /> :<DownOutlined />}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    {showPriceData && <div className="col-12 row p-4">
                                        <div className={cx("col-8")}>
                                            {bookingData.booking.guests_details.map((data) => {
                                                return(
                                                    <>
                                                        <p className={styles.mainHead}>Adult</p>
                                                        <p className={styles.subHead}>Cabin Fare:</p>
                                                        <p className={styles.subHead}>Port Charges</p>
                                                        <p className={styles.subHead}>Gratuity:</p>
                                                    </>
                                                )
                                            })}
                                            <p className={styles.mainHead}>Sub Total</p>
                                            <p className={styles.mainHead}>Taxes:</p>
                                            <p className={styles.subHead}>GST:</p>
                                        </div>
                                        <div className={cx(styles.marginRemover,"col-4")}>
                                            {bookingData.booking.guests_details.map((data) => {
                                                return(
                                                    <>
                                                        <p className={styles.mainHeadPrice}>₹{data.total}</p>
                                                        <p className={styles.subHeadPrice}>₹{data.fare}</p>
                                                        <p className={styles.subHeadPrice}>₹{data.portCharges}</p>
                                                        <p className={styles.subHeadPrice}>₹{data.gratuity}</p>
                                                    </>
                                                )
                                            })}
                                            <p className={styles.mainHeadPrice}>₹{bookingData.booking.total_amnt}</p>
                                            <p className={styles.mainHeadPrice}>₹{bookingData.booking.gst}</p>
                                            <p className={styles.subHeadPrice}>₹{bookingData.booking.gst}</p>
                                        </div>
                                    </div>}
                                </div>
                                
                            </Card>
                        </div>
                    </div>
                    <div className={cx(styles.BottomButtonContainer,"col-12")}>
                        <button className={styles.rescheduleButton}>RESCHEDULE</button>
                        <button className={styles.cancelButton}>CANCEL BOOKING</button>
                    </div>

                </div>
            </Container>
            <Footer />
        </div>
    )
}

const CabinDetails = ({data, index}) => {
    const [details, setDetails] = useState(false)
    return (
        <>
            <div className={cx(styles.marginRemover, styles.borderStyle, "col-12 row")}>
                <div className="col-9">
                        <p>Cabin{index+1} : <span className={styles.codeInt}>{data.code}</span> </p>
                </div>
                <div className={cx(styles.arrowUpAndDownSmall ,"col-3")} onClick={() => {setDetails(!details)}}>
                        {details ? <UpOutlined/> :<DownOutlined />}
                </div>
            </div>
            {details && <div className={cx(styles.marginRemover, styles.borderStyle, "col-12 row")}>
                   {data.guests.map((items, i) => {
                       return(
                            <div className="col-lg-6 col-sm-12 mt-4 mb-4">
                                <p className={styles.guestTitle}>Guest {i+1} - {items.type}</p>
                                <p className={styles.guestName}>{items.name}</p>
                                <p className={styles.guestTitle}>{items.gender}</p>
                                <p className={styles.guestTitle}>{items.email}</p>
                                <p className={styles.guestTitle}>{items.phone}</p>
                            </div>
                       )
                   })} 
            </div>}
        </>
    )
}
