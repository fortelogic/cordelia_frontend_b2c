import ContentPage from "../../components/ContentPage/ContentPage";

let contents = [
    { title: null, content: "Cordelia Cruises by Waterways Leisure Tourism Pvt Ltd is India’s premium cruise liner. Waterways Leisure Tourism Pvt Ltd is a venture by the Dream Hotel Group." },
    { title: null, content: "Cordelia, a symbol of the daughter of the ocean in Latin is an embodiment of luxury, style, and substance. Its course is decided by the classic combination of elegance and free-spiritedness to go beyond the ordinary and discover new horizons." },
    { title: null, content: "Cordelia aspires to promote and drive the cruise culture in India through experiences that are stylish, luxurious, and most importantly, inherently Indian. Cordelia is a cruise liner for Indians catering to the way Indians love to holiday." },
    { title: null, content: "We will be commencing our domestic and international sailings from the mid of 2021. Our guests will be sailing to destinations like Sri Lanka, Lakshadweep, Goa, Diu, Chennai, and Kochi." }
];

let banners = ["/images/about_bg.webp", "/images/about_bg.webp"];

const TOSS = () => (
    <ContentPage
        title = "About Us"
        contents = { contents }
        banners = { banners }>
    </ContentPage>
);

export default TOSS;