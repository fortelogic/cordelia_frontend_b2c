import { Row, Col } from 'antd'
import ImageMain1 from '../../assets/img/main-img1.png'
import ImageMain2 from '../../assets/img/main-img2.png'
import ImageMain3 from '../../assets/img/main-img3.png'
import ImageMain4 from '../../assets/img/main-img4.png'
import VideoImage from '../../assets/img/videoImage.png'
import VideoImage1 from '../../assets/img/videoImage1.png'
import VideoImage2 from '../../assets/img/videoImage2.png'
import VideoImage3 from '../../assets/img/videoImage3.png'
import SlideImage1 from '../../assets/img/slide-image1.png'
import SlideImage2 from '../../assets/img/slide-image2.png'
import SlideImage3 from '../../assets/img/slide-image3.png'
import SlideImage4 from '../../assets/img/slide-image4.png'
import DisplayImage1 from '../../assets/img/displayImage1.png'
import DisplayImage2 from '../../assets/img/displayImage2.png'
import PlayBtn from '../../../assets/img/PlayBtn.js'
import CalenderIcon from '../../../assets/img/calenderIcon.js'
import Carousel from "react-multi-carousel";
import Quote from '../../assets/img/quote.svg'
import ArrowLeft from '../../../assets/img/ArrowLeft'
import ArrowRight from '../../../assets/img/ArrowRight'

import * as Section from "../../../components/Section";
import { testimonials } from "../../home/feed";

// import "react-multi-carousel/lib/styles.css";
import './index.scss'
import { FormatQuote, FormatQuoteRounded } from '@material-ui/icons'
import Container from "../../../components/Container";


const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 8,
        showDots: false,
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3,
        settings: {
            showDots: false,
        }

    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        showDots: true,

    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        showDots: true,
    }
};

const responsive2 = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 8
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 4
    },
    tablet: {
        breakpoint: { max: 1024, min: 767 },
        items: 2
    },
    mobile: {
        breakpoint: { max: 767, min: 0 },
        items: 1
    }
};


function MainContent() {

    const array = [
        {
            heding: 'Departure from Mumbai',
            mainImage: ImageMain1,
            videoImage: VideoImage,
        },
        {
            mainImage: ImageMain2,
            videoImage: VideoImage1,
            heding: 'Explore Lakshadweep'
        },
        {
            mainImage: ImageMain3,
            videoImage: VideoImage2,
            heding: 'Explore Goa'
        },
        {
            mainImage: ImageMain4,
            videoImage: VideoImage3,
            heding: 'Day at sea (Onboard Activities)'
        },
    ]

    const CustomDot = ({ onMove, index, onClick, active }) => {
        return (
            active ? <div className="custom-dots-active"></div> : <div className="custom-dots"></div>
        );
    };

    const ButtonGroup = ({ next, previous, goToSlide, ...rest }) => {
        const { carouselState: { currentSlide } } = rest;
        return (
            <div className="carousel-button-group">
                <div className={`${currentSlide === 0 ? 'disable' : ''} iconOut leftIcon`} onClick={() => previous()} >
                    <ArrowLeft />
                </div>
                <div className="iconOut" onClick={() => next()} >
                    <ArrowRight />
                </div>
            </div>
        );
    };

    return (
        <>
            {
                array.map(val => {
                    return (
                        <>
                            <Row justify={"space-between"} className="trip-stops">
                                <Col className="heading-text">{val.heding}</Col>
                                <Row className="conatentIcon">
                                    <CalenderIcon />
                                    <p className="day-text mt-1">Day 1</p>
                                </Row>
                            </Row>
                            <Row className="main-image">
                                <Col md={24} lg={24} className="mainImage">
                                    <img className="rounded" src={val.mainImage} />
                                </Col>
                            </Row>
                            <Row className="videoDes">
                                <Col sm={24} md={14}>
                                    <p className="stop-description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. Ullamcorper eget ultrices ultrices lorem sed auctor diam auctor suspendisse. Consectetur velit tortor curabitur scelerisque elit. Sit nisl felis erat imperdiet neque eu cursus lectus senectus. Quis nec odio quis nulla auctor eget..
                                    </p>
                                </Col>
                                <Col sm={24} md={2}>
                                </Col>
                                <Col sm={24} md={8}>
                                    <div className="videoContainer">
                                        <img src={val.videoImage} />
                                        <div className="videoContainerIcon">
                                            <PlayBtn />
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <Container wrapperClass="carousel-cont">
                                <div className="content-carousel">
                                    <p className="title-text">Things to do in Lakshadweep</p>
                                    <Carousel renderDotsOutside={true} slidesToSlide={1} centerPadding={0} arrows={false} dotListClass="dlc" showDots={true} customDot={<CustomDot />} customButtonGroup={<ButtonGroup />} itemClass="sliderList2" sliderClass="sliderUl" responsive={responsive}>
                                        <div className="sliderContent">
                                            <img className="cardImage" src={SlideImage1} />
                                            <div className="sliderText">
                                                <p className="cardTitle">
                                                    Mumbai City Tour
                                                </p>
                                                <p className="cardDescription">
                                                    Vitae nulla volutpat sapien mattis risus nisl, pharetra ultricies. Dictum nam iaculis sit amet accumsan, aliquet bibendum.
                                                    Vitae nulla volutpat sapien mattis risus nisl, pharetra ultricies. Dictum nam iaculis sit amet accumsan, aliquet bibendum.
                                                </p>
                                            </div>
                                        </div>
                                        <div className="sliderContent">
                                            <img className="cardImage" src={SlideImage2} />
                                            <div className="sliderText">
                                                <p className="cardTitle">
                                                    Lorem ipsum dolor sit
                                                </p>
                                                <p className="cardDescription">
                                                    Vitae nulla volutpat sapien mattis risus nisl, pharetra ultricies. Dictum nam iaculis sit amet accumsan, aliquet bibendum.
                                                </p>
                                            </div>
                                        </div>
                                        <div className="sliderContent">
                                            <img className="cardImage" src={SlideImage3} />
                                            <div className="sliderText">
                                                <p className="cardTitle">
                                                    Lorem ipsum dolor sit
                                                </p>
                                                <p className="cardDescription">
                                                    Vitae nulla volutpat sapien mattis risus nisl, pharetra ultricies. Dictum nam iaculis sit amet accumsan, aliquet bibendum.
                                                </p>
                                            </div>
                                        </div>
                                        <div className="sliderContent">
                                            <img className="cardImage" src={SlideImage3} />
                                            <div className="sliderText">
                                                <p className="cardTitle">
                                                    Lorem ipsum dolor sit
                                                </p>
                                                <p className="cardDescription">
                                                    Vitae nulla volutpat sapien mattis risus nisl, pharetra ultricies. Dictum nam iaculis sit amet accumsan, aliquet bibendum.
                                                </p>
                                            </div>
                                        </div>

                                    </Carousel>
                                </div>
                            </Container>
                        </>
                    )
                })
            }

            <div className="sliderMain-mobile">
                <h2 className="travellers-text">
                    TRAVELLER'S <b className="ml-2"> SPEAK </b>
                </h2>
                <p className="test-text">Testimonials</p>
                <Section.CardsCarousel testimonial={true} {...testimonials} />
                {/* <Carousel renderDotsOutside={true} arrows={false} dotListClass="dlc" showDots={true} customDot={<CustomDot />} customButtonGroup={<ButtonGroup />} itemClass="sliderList2" responsive={responsive2}>
                    <div className="sliderContentFooter">
                        <div className="sliderFooterText">
                            <img className="quote-img" src={Quote}></img>

                            <p className="testimonial-text">
                                Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.”
                            </p>
                            <img className="userImage" src={DisplayImage1} />
                            <div className="mt-5">
                                <h6 className="un-text">
                                    Peter Parker
                                </h6>
                                <span className="user-address">Mumbai, India</span>
                            </div>
                        </div>
                    </div>
                    <div className="sliderContentFooter">
                        <div className="sliderFooterText">
                            <img className="quote-img" src={Quote}></img>

                            <p className="testimonial-text">
                                Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.”
                            </p>
                            <img className="userImage" src={DisplayImage1} />
                            <div className="mt-5">
                                <h6 className="un-text">
                                    Cody Fisher
                                </h6>
                                <span className="user-address">Mumbai, India</span>
                            </div>
                        </div>
                    </div>
                    <div className="sliderContentFooter">
                        <div className="sliderFooterText">
                            <img className="quote-img" src={Quote}></img>

                            <p className="testimonial-text">
                                Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.”
                            </p>
                            <img className="userImage" src={DisplayImage2} />
                            <div className="mt-5">
                                <h6 className="un-text">
                                    Wade Warren
                                </h6>
                                <span className="user-address">Mumbai, India</span>
                            </div>
                        </div>
                    </div>
                </Carousel> */}
            </div>

        </>
    );
}

export default MainContent