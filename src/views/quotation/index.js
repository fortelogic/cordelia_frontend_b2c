import React from 'react'
import styles from "./quotation.module.css"
import cx from "classnames"
import { sendQuotationValues } from "../../services/staff-portal/quotation"

const Quotation = () => {

    const queryParams = new URLSearchParams(window.location.search)

    React.useEffect(async () => {
        await sendQuotationValues(queryParams.get("item_id"))
    }, [])

    return (
        <main className={cx(styles.quotationThankyouPage, "text-center mt-5")}>
            <div className={cx(styles.quotationThankyouPage, "text-center mt-5")}>
                <h1 className={cx(styles.thankyouText)}>Thank You!</h1>
                <p>We have forwarded payment link to your mail ID please check and make payment!</p>
                <p className={cx(styles.connectUs)}>CONNECT WITH US!</p>
            </div>
        </main>
    )
}

export default Quotation
