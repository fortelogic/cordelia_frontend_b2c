import { useState, useEffect } from "react";
import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";
import FAQView, { FAQItem } from "../../components/View/FAQView";
import ReactHtmlParser from "react-html-parser";

const FAQ = () => {

  const queryParams = new URLSearchParams(window.location.search)

  let faqString = [
    {
      question: "Are guests allowed to smoke inside cabins?",
      answer:
        "Guests are not permitted to smoke inside cabins. There are designated smoking spaces on board the cruise.",
    },
    {
      question:
        "Are there wheelchairs available for senior citizens and differently abled guests?",
      answer:
        "Wheelchairs are available on board the ship. Guests should notify cruise ship staff, incase of a wheelchair requirement one day prior to sailing.",
    },
    {
      question: "Is food permitted inside cabins?",
      answer:
        "There are dedicated areas onboard the cruise ship for various meals and eating, however, guests are not allowed to eat inside the cabins.",
    },
    {
      question: "Are there Halal food option on board the cruise ship?",
      answer:
        "Pork or Beef are not served onboard, food served on board is Halal and a certificate proving the same can be provided.",
    },
    {
      question: "Are there inter-connecting rooms available?",
      answer:
        "There are inter-connecting rooms within the interior and ocean view stateroom categories.",
    },
    {
      question: "Do balcony cabins come with 4 berths?",
      answer:
        "Occupancy for 4 guests  is available in two stateroom categories – Interior and Ocean View.  Triple occupancy is available in Balcony, Suite and Chairman suite.",
    },
    {
      question: "What is the size of suite’s balcony?",
      answer: "The suite balcony size is 225 sq. ft.",
    },
    {
      question: "How many swimming pools are there on board? ",
      answer:
        "There are two pools onboard, Out of which one is dedicated for kids.",
    },
    {
      question: "Am I allowed to bring packaged foods/snacks/tuck on board?",
      answer:
        "Packaged foods, snacks, or any other kind of tuck is not allowed on board.",
    },
    {
      question: "Is this cruise ship couple friendly? ",
      answer: "Yes, this cruise ship is couple friendly.",
    },
    {
      question: "What are the embarkation timings?",
      answer:
        "All sailings have different embarkation timings. Guests will be notified of embarkation timings and other important details before sailing. ",
    },
    {
      question: "Will lunch be provided if I report 4 hours early?",
      answer:
        "Depending upon the embarkation time, food facilities will be provided onboard.",
    },
    {
      question: "Are pets allowed on board?",
      answer: "Pets are not allowed on board.",
    },
    {
      question:
        "If my child is below 6 months of age, can I bring him/her onboard?",
      answer: "Infants below 6 months are not allowed onboard. ",
    },
    {
      question: "Am I allowed to carry baby food onboard?",
      answer: "Yes, you can carry baby food onboard",
    },
    {
      question:
        "What if my child is 26 months old, is he/she allowed onboard free or is it chargeable?",
      answer:
        "Any infant above 2 years of age is considered as a child. Extra beds are chargeable.",
    },
    {
      question: "If a lady is pregnant, she’s is 24 weeks is she allowed?",
      answer: "<b>The Carrier cannot for health and safety reasons carry pregnant Guests of 24 weeks or more at the time of embarkation or any time during the sailing. The Carrier reserves the right to request a medical certificate at any stage of pregnancy and to refuse passage if the Carrier and/or the Master are not satisfied that the Guest will be safe during the passage</b>",
    },
    {
      question: "What is the boarding pass for? ",
      answer:
        "A boarding pass is a document provided to facilitate check-in, giving a guest permission to enter the port and to board the cruise ship. It identifies the guests name, the cruise number, and the date & scheduled time for departure. ",
    },
    {
      question: "Are Hookah's/ Sheesha’s available on board?",
      answer: "Hookah’s, Sheeha’ & E-Cigarettes are not permitted onboard.",
    },
    {
      question: "What is the cancellation policy? ",
      answer: `Please Refer to the cancelation policy. <a className="text-j-magenta" target="_blank" href="/cancellation-policy">Click To Know More</a>`,
    },
    {
      question: "If I cancel my cruise before 60 days, will I get 100% refund?",
      answer: `Refer to the refund policy. <a className="text-j-magenta" target="_blank" href="/cancellation-policy">Click To Know More</a>`,
    },
    {
      question: "What is Cordelia Academy? ",
      answer:
        "We have an amazing range of offerings from arts & crafts area, scavenger hunts, jewelers making, air hockey, karaoke & lot more. A palace of fun filled with excitement & joy. Our experienced staffs will always be available for any assistance. All kids need to be fully potty trained, no diapers or pullups allowed in the facility Is water free in Cabins.",
    },
    {
      question: "Is free drinking water available in the cabins?   ",
      answer:
        "Every stateroom is provided with 2 bottles of water (500ml each). And you may fill it with the dispenser available on the specific decks. ",
    },
    {
      question: "Can I bring along  extra bottles of water with me?",
      answer:
        "It is not allowed to carry any consumable items onboard. Guests need to purchase drinking water onboard.",
    },
    {
      question: "My visa is expiring within 6 months. Can I travel?",
      answer:
        "You can travel before your  visa expires, however if your passport is expiring within 6 months then a new visa cannot be provided.",
    },
    {
      question: "What does the price of my cruise include?",
      answer:
        "The price of your cruise vacation includes your accommodations, majority of  dining options, entertainment & most onboard activities. You will receive a detailed list of inclusions and exclusions during your booking process. ",
    },
    {
      question: "I am a single traveler, will the prices be different for me? ",
      answer:
        "Prices of the cruise are per person per day based on double occupancy in INR & vary based on the date of reservation, sailing date & state room category. Please refer to our pricing policy for single travellers.",
    },
    {
      question: "What is not included in the cruise fare?",
      answer:
        "Your cruise fare does not include air transportation, transfers, optional shores & land excursions, meals & accommodations ashore, certain beverages, casino gaming, gratuities, internet wifi, in-room telephone calls, specialty restaurants, purchases from ship stores, or items of a personal nature such as medical services, laundry, spa treatments or salon services. Cruise fare does not include Cruise Taxes, Fees, & Port Charges.",
    },
    {
      question:
        "What is the minimum age for purchasing or consuming alcohol on the Cruise?",
      answer:
        "Guests must be at least 21 years of age to be eligible to consume soft liquor and at least 25 years of age to consume hard liquor.",
    },
    {
      question:
        "Is there an official dress code guideline followed onboard for pools, public areas, casinos, and fine dining restaurants. Is National/traditional attire permitted onboard? ",
      answer: `Though there is no official dress code to be followed onboard, guests are encouraged to wear smart casuals around ship areas. Passengers must strictly wear swim suits and trunks when using the pool. When out on the open sea, the evenings tend to get quite chilly, which is why it is recommended that guests bring along a sweater or a warm jacket. 
    <br></br>
    <p style={{color:"red"}}>There is on-board policy and Casino policy in place, we highly recommend guest to read those policies.</p>`,
    },
    {
      question: "What are the timings for the onboard Casino and bar? ",
      answer:
        "The bars onboard are open as per usual timings, however the onboard Casino will only open once the cruise ship crosses 12 nautical miles. The Casino opens 1 hour post sailing and closes 1 hour prior to arrival. ",
    },
    {
      question:
        "Will I need to carry my own luggage to my cabins during embarkation and disembarkation? Will porterage be provided during sailings?",
      answer:
        "All heavy baggage can be handed over to porterage,  and will be delivered to your cabin I hour after sailing. Porters will be there to assist however guests needs to carry there own luggage after scanning. Luggage Trolleys will also be available at the terminal.",
    },
    {
      question:
        "Casino chips can be purchased onboard using Credit Card, Charge Cards or cash? All cards accepted VISA/Master?",
      answer: "Cash, Credit Card, Master & Visa are accepted onboard",
    },
    {
      question:
        "What are the prices of onboard wifi packages? What is the costing of my onboard wifi, and what is the speed like?  ",
      answer:
        "Wifi Package Prices vary with every sailing. Prices for packages start from 20$ onwards. You will be provided with more details once you commence your booking process. ",
    },
    {
      question: "What are smoking regulations?",
      answer:
        "Smoking is strictly prohibited in all staterooms and stateroom balconies. Cordelia Cruises reserves the right to levy a fine upto US $ 1000 (or its equivalent) should guests be found smoking outside the designated areas for smoking of the vessel and may also lead to forthwith disembarkation. Throwing cigarette butts over the side of the ship is also strictly prohibited and shall attract heavy fines.",
    },
    {
      question: "Can I pay for expenses using cash onboard ?",
      answer:
        "Upon embarkation, you will be provided with a  key-Card. This card can be recharged as per your wish and is what needs to be used at all onboard facilities. You can upload credit on to your key-card using Debit card, Credit Card, or Cash. If there any credit left on your key card at the end of your trip, that amount will be refunded back to you.",
    },
    {
      question: "What are the Visa Requirements For Sri Lanka?",
      answer: "Guests with Indian Passport need to obtain the same online",
    },
    {
      question: "What are the Safety Measures for COVID 19?",
      answer: `<ul style={{listStyle: "unset"}}>
            <li>Temperature checks & signed declarations by guests are mandatory,</li>
            <li>Guest bedrooms and linens are sanitized & disinfected regularly,</li>
            <li>Common public areas & touch points are sanitized & sterilized hourly.</li>
            <li>Self service in the dining areas is suspended, areas for recreation & entertainment facilities are sanitized pre & post usage.</li>
            <li>Air filters & coils are checked and replaced as required to ensure healthy air quality in cabins.</li>
            <li>24*7 medical centre onboard with an isolation room is available onboard</li>
            <li>All our frontline crew members wear a protective gear and undergo regular temperature checks.</li>
            <li>To ensure safety, the water is checked several times during the day.</li>
            <li>Outside food and beverages is not allowed on board.</li>
          </ul>`,
    },
    {
      question: "What destinations can I take a cruise to?",
      answer: `Our cruise is a destination in itself which allows you to explore a plethora of dinning options, activities & unique experiences. You can book 2,3,5,7 Night cruises to Goa, Mumbai, Diu, Lakshadweep, Chennai, Kochi, Trincomalee, Colombo, Galle.
          <br></br>
          <a className="text-j-magenta" target="_blank" href="/cruise-destinations">Click To Know More</a>`,
    },
    {
      question: "Are there any restrictions on what I cannot bring onboard?",
      answer: `You cannot bring any item that is dangerous or illegal. Drugs, arms, guns, explosives, animals and flammable items (E - Cigarette, Hookahs) fall into this category. So be sure you do not bring these.
          Outside liquor & food is not allowed.
          High voltage devices like hair irons, clothing Iron, electric kettle, hair dryers aren't allowed.`,
    },
    {
      question: "What is the Luggage Policy?",
      answer:
        "Guests are encouraged to limit their luggage quantity. Each Guest is permitted to carry luggage to a maximum of 3 bags per person, with the condition that each bag weighs less than 20 kgs, and total weight should not exceed 50 kgs. In the event of total weight of the baggage exceeding 50 kgs, excess baggage will be charged at the rate of Rs. 300 per kilogram.",
    },
    {
      question: "Port Information",
      answer: `<a className="text-j-magenta" target="_blank" href="https://goo.gl/maps/PokVubX3xdu7EXAj6">Mumbai: Green Gate Internation Cruise Terminal, Shoorji Vallabhdas Rd, Fort, Mumbai, Maharashtra 400001</a>
          <br></br>
          <a className="text-j-magenta" target="_blank" href="https://goo.gl/maps/qfuh7HKdR5NrgeV57">GOA: Marmugao Port Trust Colony, Vasco da Gama, Goa</a>`,
    },
    {
      question: "Is Web-Check In Madatory?",
      answer: `Web check-in is mandatory for all our guests. Web check-in helps to generate boarding pass which needs to be produced at the gate & terminal to enter the restricted areas and get onboard.
          Please get in touch with our support team in case of any issues with Web check-in.`,
    },
    {
      question: "What happens if I fall ill while onboard the cruise?",
      answer:
        "We have a medical centre onboard, with a registered Doctor and nurses on board to take care of you . Please note that professional medical services are not provided for fee.",
    },
    {
      question: "What should I do incase I lose my access card?",
      answer:
        "The guest is requested to report this issue to the reception counter and collect the duplicate key card. Charges may apply on the same.",
    },
    {
      question: "What Currency can we use onboard ?",
      answer:
        "USD & INR are accepted. ROE for USD will be considered as on date on board.",
    },
    {
      question: "Would there be any language barrier faced on board?",
      answer:
        "Cordelia Cruise multi-lingual crew hail majorly from India and other nations, all the members of the speak English. Chances are that you will most certainly find someone who will be more than happy to chat with you in your own language.",
    },
    {
      question: "Are there shopping options onboard?",
      answer: "Yes, we do have shopping on board ",
    },
    {
      question: "How to book shore Excursions?",
      answer:
        `You can book shore excursion online on our website <a className="text-j-magenta" href="https://www.cordeliacruises.com" target="_blank">www.cordeliacruises.com</a>. In case you have missed to book the shore excursions online. Can book the same at our shore excursion desk onboard.`,
    },
    {
      question: "What is the minimum size for a group?",
      answer:
        "We consider 16 staterooms as the minimum number required for a group booking.",
    },
    {
      question: "Are laundry services available on board?",
      answer:
        "Yes, Laundry services are available on board, on chargeable basis",
    },
    {
      question:
        "Do you'll have an itinerary/events schedule to guide us, which show/ activity  is happening when?",
      answer:
        "Onboard the cruise you will get a day planner which contains schedule of all activities.",
    },
    {
      question: "Can I upgrade my stateroom category onboard?",
      answer:
        "Stateroom category can be upgraded onboard as per the availability with additional charges.",
    },
    {
      question: "Are beverage packages available onboard ?",
      answer:
        "Yes, the guests can purchase their preferred beverage package on board.",
    },
    {
      question: "Can guests bring aerial drones onboard?",
      answer:
        "Getting Aerial drone on board are subject to prior permissions from the Port Authorities, CISF & Ship Board Team",
    },
    {
      question: "What travel documents are required to bring onboard?",
      answer: `You need to complete your web check-in using our website/mobile site/application and download your boarding pass. 
          For domestic sailings along with your boarding pass please carry any original copy of government recognised identity proof from the following -  Aadhaar Card, Passport, Driving License, Voter ID, Pan Card.
          For International Sailings you will need a Boarding Pass, Passport & Visa.
          Incase of an infant for domestic travel please carry an original birth certificate.
          For International sailings, passport is mandatory.`,
    },
    {
      question: "Is Jain food available?",
      answer:
        "The dedicated Jain Cuisine service whips up an array of authentic Jain dishes for Breakfast/Lunch & Dinner. All meals prepared with keeping the cultural values in mind.",
    },
    {
      question: "Are Spa facilities included in my package?",
      answer:
        "SPA is one of the most recommended experiences onboard, you need to pay and avail this services.",
    },
    {
      question: "What are the Disembarkation Process?",
      answer: `To ensure a smooth disembarkation process, similar to check in & embarkation, times for disembarkation are also staggered. 
          Guests will be advised provided allocated time to disembark. This will usually occur on the last full day of your cruise (the day before you're due to disembark).
          Disembarkation is normally completed within 3 hours of the ships arrival, however, this can vary. For this reason, if you have post cruise flights, we recommend your flight departs no earlier than 5 to 7 hours after your cruise is scheduled to arrive in port.`,
    },
    {
      question: "What do I do in case I forgot a belonging on board?",
      answer: `Will recommend you to have a checklist to not forget anything on the cruise. If you happen to forget something on the cruise please get in touch with our customer support department. You can write to us at <a className="text-j-magenta" href="mailto:support@cordeliacruises.com">support@cordeliacruises.com</a>`,
    },
    {
      question: "Am I allowed to carry my medicines on board?",
      answer:
        "Yes, you can carry all kinds of generic & prescribed medicines. Please carry the prescription/medical file incase of any specific prescribed medicines.",
    },
  ];
  const [searchString, setSearchString] = useState("");
  const [faqs, setFaqs] = useState(faqString);
  let BackupFAQ = "";
  useEffect(() => {
    BackupFAQ = faqString;
  }, null);


  const handleSearch = (e) => {
    setSearchString(e.target.value)
    let searchIndex = e.target.value;
    faqString = BackupFAQ;
    faqString = faqString.filter((element) => {
      return (element.question.toLowerCase().includes(searchIndex.toLowerCase()) || (element.answer.toLowerCase().includes(searchIndex.toLowerCase())));
    });
    setFaqs(faqString);
  };

  return (
    <>
      <Header className="mb-5" bgPurple platform={queryParams.get("platform")} />
      <main className="bg-auto">
        <div className="p-4">
          <div style={{ margin: "2rem auto", width: "98%" }}>
            <input
              className="search-bar"
              placeholder="Search"
              type="input"
              value={searchString}
              onChange={(e) => handleSearch(e)}
            />
          </div>
          <FAQView title="FAQs" icon="far fa-money-check">
            {faqs.map((k) => (<FAQItem title={k.question}>{ReactHtmlParser(k.answer)}</FAQItem>))}
          </FAQView>
        </div>
      </main>
      {/* <Footer /> */}
      {
        (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
      }
    </>
  );
};

export default FAQ;
