import { useEffect, useState } from "react"
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux"
import Header from '../../components/Header/HeaderHome';
import Footer from '../../components/Footer/Footer';
import Card from '../../components/UI/Card';
import Carousel from '../../components/UI/Carousel';
import './shore.scss';
import CardbgImg from '../../assets/img/bg-package-card.png';
import Slider1 from '../../assets/img/imageSlide1.jpg';
import Beach from '../../assets/img/beach.jpg';
import Packge1 from '../../assets/img/packge1.jpg';
import Packge2 from '../../assets/img/packge2.jpg';
import Packge3 from '../../assets/img/packge3.jpg';
import { shoreExcursionList, updateShoreExcursions } from "../../redux/actions/shoreAction"
import localStore from "../../utils/localStore";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { allGuests, bookingsummary } from "../../redux/actions/roomAction"
import { bookingid } from "../../services/bookingid"
import Activity from "../../assets/img/destination/activity.png"
import Insurance from "../../assets/img/destination/insurance.png"
import Car from "../../assets/img/destination/Vector.png"
import bookingdetails from "../../services/booking-details"
import { getRePaymentDetails } from "../../services/repayment"


function Home() {
  const history = useHistory()
  const dispatch = useDispatch()
  const [defaultPort, setDefaultPort] = useState(0)

  const booking = localStore.get("bookingSummary")
  const allguests = localStore.get("allguests")
  const bookingSummary = useSelector(state => state.roomReducer.bookingsummary)
  const guests = useSelector(state => state.roomReducer.allGuests)
  const [allshores, setShores] = useState({ "shore_excursions": [] })

  const queryParams = new URLSearchParams(window.location.search)

  const shores = useSelector((state) => state.shores)
  const shoresUpdated = localStore.get("updatedShores")

  useEffect(async () => {
    if (queryParams.get("bookingid")) {
      const data = await dispatch(shoreExcursionList(queryParams.get("bookingid")))
    } else {
      const data = await dispatch(shoreExcursionList())
      // console.log("Here shores", shores)
    }
  }, [defaultPort])

  useEffect(async () => {
    if (queryParams.get("bookingid")) {
      // console.log("By Vi")
      await dispatch(updateShoreExcursions(allshores, queryParams.get("bookingid")))
      await dispatch(shoreExcursionList(queryParams.get("bookingid")))
      const response = await bookingdetails(queryParams.get("bookingid"))
      dispatch(bookingsummary(response.data.booking))
      console.log("Query From", response)
    } else {
      await dispatch(updateShoreExcursions(allshores))
      await dispatch(shoreExcursionList())
      console.log("Here guests", allguests)
      const response = await bookingid(booking.booking.id, allguests)
      // const response = await bookingid(booking.booking.id, allguests[0])
      console.log("Booking Response", response)
      dispatch(bookingsummary(response.data))
    }
  }, [allshores])


  const doContinue = async () => {
    if (guests.length > 0 && guests[0].guests && guests[0].guests[0] && guests[0].guests[0]) {
      // dispatch(allGuests(guests[0].guests[0]))
      // const response = await bookingid(booking.booking.id, allguests)
      const response = await bookingid(booking.booking.id, guests)
      dispatch(bookingsummary(response.data))
      queryParams.get("platform") === "true" ? history.push('/payment-details?platform=true') : history.push('/payment-details')
    }

    if (queryParams.get("bookingid")) {
      const params = {
        "variables": {
          "input": {
            "contact": {
              "first_name": booking.contact.name.split(" ")[0],
              "last_name": booking.contact.name.split(" ")[1],
              "email": booking.contact.email,
              "phoneNumber": booking.contact.phone_number,
              "gstin": null,
              "gstin_name": null,
              "gstin_phone": null,
              "pan": null,
              "pan_name": null
            },
            "paymentInfo": {
              "plan": "",
              "voucherNumber": "",
              "promoCode": "",
              "first_name": booking.contact.name.split(" ")[0],
              "last_name": booking.contact.name.split(" ")[1],
              "email": booking.contact.email
            }
          }
        },
        "booking_id": queryParams.get("bookingid"),
        "payment_for": "shore_excursions"
      }

      const paymentDetails = await getRePaymentDetails(params)

      let { pg_data } = paymentDetails.data

      const form = document.createElement("form")
      form.setAttribute("method", "post")
      form.setAttribute("action", pg_data.payu_url)

      for (var key in pg_data.payu_body) {
        if (pg_data.payu_body.hasOwnProperty(key)) {
          const hiddenField = document.createElement("input")
          hiddenField.setAttribute("type", "hidden")
          hiddenField.setAttribute("name", key)
          hiddenField.setAttribute("value", pg_data.payu_body[key])
          form.appendChild(hiddenField)
        }
      }

      document.body.appendChild(form)
      form.submit()
    }
  }

  const addShoreExcursion = async (item, shorePort) => {

    let shoreobj = { "id": item.id, "port_id": shorePort.port_id, "guests": [] }

    if (queryParams.get("bookingid")) {
      for (let i = 0; i < booking.rooms.length; i++) {
        for (let j = 0; j < booking.rooms[i].guests.length; j++) {
          shoreobj.guests.push({ "id": booking.rooms[i].guests[j].id, "transfer_type_id": item.transfer_types[0].id })
        }
      }
    } else {

      for (let i = 0; i < booking.rooms[0].booked_data.guest_data.length; i++) {
        shoreobj.guests.push({ "id": booking.rooms[0].booked_data.guest_data[i].id, "transfer_type_id": item.transfer_types[0].id })
      }
    }

    // Single shore for each port
    if (allshores.shore_excursions.filter(item => item.id === shoreobj.id).length > 0) {
      const filtered = allshores.shore_excursions.filter(ele => ele.id !== shoreobj.id)
      setShores((state) => ({
        ...state,
        shore_excursions: filtered
      }))
    } else {
      if (allshores.shore_excursions.filter(item => item.id !== shoreobj.id && item.port_id === shoreobj.port_id).length > 0) {

        setShores((state) => ({
          ...state,
          shore_excursions: [...state.shore_excursions.filter((ele) => ele.port_id !== shoreobj.port_id), shoreobj]
        }))
      } else {
        setShores((state) => ({
          ...state,
          shore_excursions: [...state.shore_excursions, shoreobj]
        }))
      }
    }
  }


  return (
    <div className="App">
      <Header className="mb-4" bgPurple platform={queryParams.get("platform")} />
      <Button
        type="primary arrow-left-btn ml-4"
        className="w-auto"
        onClick={() => {
          history.goBack();
        }}
      >
        <ArrowLeftOutlined />
      </Button>
      <div className="packag--save--section">
        <div className="container">
          <div className="row my-5">
            <div className="col-12 col-lg-8">
              <Card className="cardpackage--bg mb-4 mb-lg-0 overflow-hidden">
                <div className="full--bgcard-view" style={{ backgroundImage: `url(${CardbgImg})` }}>
                  <div className="package--choose-txt">
                    <h3>Choose PACKAGES and Save upto 25%</h3>
                    <p>
                      Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat
                      duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.1
                    </p>
                  </div>
                </div>
              </Card>
            </div>
            <div className="col-12 col-lg-4">
              {
                // (bookingSummary && bookingSummary.hasOwnProperty("booking") && bookingSummary.booking.hasOwnProperty("shore_excursion_total")) &&
                <Card className="cardbox--tablecard">
                  <div className="card-body">
                    <div className="cardtable--bookitems">
                      <h3 className="pricedtl--title">Price Details</h3>
                      <table className="table tableprice--dtl">

                        {
                          queryParams.get("bookingid") ? `` :
                            <tbody>
                              <tr className="tr-bookview">
                                <td><b>Total Booking Amount</b></td>
                                <td><b>₹ {bookingSummary.total - bookingSummary.booking.shore_excursion_total}</b></td>
                              </tr>
                              {
                                bookingSummary.booking.shore_excursion_total > 0 &&
                                <>
                                  <tr className="tr-bookview">
                                    <td><b>Shore Excursion</b></td>
                                    <td><b>₹ {bookingSummary.booking.shore_excursion_total - bookingSummary.booking.shore_excursion_gst}</b></td>
                                  </tr>
                                  <tr className="tr-bookview">
                                    <td><b>Shore Excursion GST</b></td>
                                    <td><b>₹ {bookingSummary.booking.shore_excursion_gst}</b></td>
                                  </tr>
                                </>
                              }
                              {
                                bookingSummary.discount > 0 &&
                                <tr className="tr-bookview">
                                  <td className="text-success"><b>Discount</b></td>
                                  <td className="text-success"><b>-₹ {bookingSummary.discount}</b></td>
                                </tr>
                              }
                              <tr className="tr-bookview">
                                <td className="total-amt--book pt-2">Total Payable Amount</td>
                                <td className="extra--book-td pt-2">₹ {shoresUpdated !== undefined ? shoresUpdated.booking.total : booking.booking.total}</td>
                              </tr>
                            </tbody>
                        }

                        {/* {
                          bookingSummary.hasOwnProperty("id") &&
                            queryParams.get("bookingid") ?
                            <tbody>
                              <tr className="tr-bookview">
                                <td><b>Total Booking Amount</b></td>
                                <td><b>₹ {bookingSummary.total_amnt}</b></td>
                              </tr>
                              {
                                bookingSummary.shore_excursion_total_price > 0 &&
                                <>
                                  <tr className="tr-bookview">
                                    <td><b>Shore Excursion</b></td>
                                    <td><b>₹ {bookingSummary.shore_excursion_total_price - bookingSummary.gst}</b></td>
                                  </tr>
                                  <tr className="tr-bookview">
                                    <td><b>Shore Excursion GST</b></td>
                                    <td><b>₹ {bookingSummary.gst}</b></td>
                                  </tr>
                                </>
                              }
                              {
                                bookingSummary.discount > 0 &&
                                <tr className="tr-bookview">
                                  <td className="text-success"><b>Discount</b></td>
                                  <td className="text-success"><b>-₹ {bookingSummary.discount}</b></td>
                                </tr>
                              }
                              <tr className="tr-bookview">
                                <td className="total-amt--book pt-2">Total Payable Amount</td>
                                <td className="extra--book-td pt-2">₹ {shoresUpdated !== undefined ? shoresUpdated.booking.total : booking.booking.total}</td>
                              </tr>
                            </tbody>
                            : <tbody>
                              <tr className="tr-bookview">
                                <td><b>Total Booking Amount</b></td>
                                <td><b>₹ {bookingSummary.total - bookingSummary.booking.shore_excursion_total}</b></td>
                              </tr>
                              {
                                bookingSummary.booking.shore_excursion_total > 0 &&
                                <>
                                  <tr className="tr-bookview">
                                    <td><b>Shore Excursion</b></td>
                                    <td><b>₹ {bookingSummary.booking.shore_excursion_total - bookingSummary.booking.shore_excursion_gst}</b></td>
                                  </tr>
                                  <tr className="tr-bookview">
                                    <td><b>Shore Excursion GST</b></td>
                                    <td><b>₹ {bookingSummary.booking.shore_excursion_gst}</b></td>
                                  </tr>
                                </>
                              }
                              {
                                bookingSummary.discount > 0 &&
                                <tr className="tr-bookview">
                                  <td className="text-success"><b>Discount</b></td>
                                  <td className="text-success"><b>-₹ {bookingSummary.discount}</b></td>
                                </tr>
                              }
                              <tr className="tr-bookview">
                                <td className="total-amt--book pt-2">Total Payable Amount</td>
                                <td className="extra--book-td pt-2">₹ {shoresUpdated !== undefined ? shoresUpdated.booking.total : booking.booking.total}</td>
                              </tr>
                            </tbody>
                        } */}
                      </table>
                      <div className="text-md-right text-center">
                        <button className="btn btn-proceed" onClick={doContinue}>
                          PROCEED
                        </button>
                      </div>
                    </div>
                  </div>
                </Card>
              }
            </div>
          </div>
        </div>
      </div>
      <div className="tab-section--wraper pb-5">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="main-head-view pb-3">
                <h3 className="heading-h3">
                  EXPLORE <strong>ACTIVITIES</strong>
                </h3>
              </div>
            </div>
            <div className="col-12">
              <div className="main-full-content-wrapper">
                <ul className="nav-tab-pills mb-5">
                  {
                    shores.shores &&
                    shores.shores.ports.map((item, index) => {
                      return item.shore_excursions.length > 0 && <li key={index} className="nav-items--main active" onClick={e => setDefaultPort(index)}>
                        <button className={defaultPort === index ? "navLink--tab" : "navLink--tab--deactive"} onClick={e => setDefaultPort(index)}>
                          {item.port_name}
                        </button>
                      </li>
                    })
                  }
                </ul>

                <div className="content-body-wrapper-tab">
                  <div className="wigite-conten--main">

                    {
                      shores.shores && shores.shores.ports.length > 0 &&
                      shores.shores.ports[defaultPort].shore_excursions.map((item, indexdown) => {
                        return <div key={indexdown} className="row-grid-List">
                          <div className="ent-card carousel-parent">
                            <div className="carousel-image">
                              <Carousel id={"slide" + indexdown} imgs={item.images} item={item} />
                            </div>
                          </div>
                          <div>
                            <div className="inclusin-flex--wrapContent">
                              <div className="topHead--excurr">
                                <h1>{item.title}</h1>
                                <h6><i className="far fa-clock"></i>{`  ` + item.hours + `  `}HRS</h6>
                              </div>
                              <div className="py-3">
                                <div className="topHead--excurr">
                                  <h4>Inclusion:</h4>
                                </div>
                                <div className="inclusion-icons">
                                  <div className="inclusion">
                                    <img className="inclusion-icon" src={Car} />
                                    <span>Transfer</span>
                                  </div>
                                  <div className="inclusion">
                                    <img className="inclusion-icon" src={Activity} />
                                    <span>Activity</span>
                                  </div>
                                  <div className="inclusion">
                                    <img className="inclusion-icon" src={Insurance} />
                                    <span>Insurance</span>
                                  </div>
                                  <div className="inclusion inc-text">
                                    <span>More Inclusions</span>
                                  </div>
                                </div>
                              </div>
                              <div className="price-container">
                                <div className="row align-items-center justify-content-between">
                                  <p className="ml-3"><b>Transfered By: </b> <br /><span>SIC Tour (Coach)</span></p>
                                  <div className="mr-4 pr-1">
                                    <div className="row align-items-baseline">
                                      <h4 className="font-weight-bold">{` ₹ ` + item.adult_price + ` / `}</h4>
                                      <br />
                                      <br />
                                      <span>Adult</span>
                                    </div>
                                    <div className="row align-items-baseline">
                                      <h4 className="font-weight-bold">{` ₹ ` + item.child_price + ` / `}</h4>
                                      <span>Child</span>
                                    </div>
                                  </div>
                                </div>
                                <div className="">

                                  <button type="text" className={`btn ${item.selected ? "btn-remove" : "btn-add"}`} onClick={e => addShoreExcursion(item, shores.shores.ports[defaultPort])}>
                                    {item.selected ? "REMOVE" : "ADD"}
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      })
                    }
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="package-section--wraper pb-5">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="main-head-view pb-3">
                <h3 className="heading-h3">
                  EXPLORE <strong>SPA PACKAGES</strong>
                </h3>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-md-4">
              <div className="package-col-Img">
                <img src={Packge1} />
                <div className="package--name-posit">
                  <h4 className="package--title">SILVER PACKAGE</h4>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-4">
              <div className="package-col-Img">
                <img src={Packge2} />
                <div className="package--name-posit">
                  <h4 className="package--title">GOLD PACKAGE</h4>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-4">
              <div className="package-col-Img">
                <img src={Packge3} />
                <div className="package--name-posit">
                  <h4 className="package--title">COUPLE PACKAGE</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <Footer /> */}
      {
        (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
      }
    </div >
  );
}

export default Home;
