import React, { useState, useEffect } from "react";
import BoardingPassComponent from "../../components/BoardingPass/CheckIn"
import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container/index";
import styles from "./boardingpass.module.css"
import { getBoardingPass, getBooking } from '../../services/webcheckin'
import { useParams } from 'react-router-dom';

function BoardingPass(props) {
  const [passData, setPassData] = useState();
  const [booking, setBooking] = useState();
  let { id } = useParams();
  let guestArray = []
  const queryParams = new URLSearchParams(window.location.search)



  useEffect(async () => {
    const responsePassData = await getBoardingPass(id)
    setPassData(responsePassData.data)
    const responsebookingData = await getBooking(id)
    setBooking(responsebookingData.data.booking)
  }, [])


  // const mergeArray=(array1,array2)=>{
  //   let merged = [];
  //   for(let i=0; i<arr1.length; i++) {
  //     merged.push({
  //      ...array1[i], 
  //      ...(array2.find((itmInner) => itmInner.id === array1[i].id))}
  //     );
  //   }
  //   console.log(merged,"nik-log merged")
  // }

  const findPassDetails = (array, id) => {
    let obj = array.find(o => o.guest_id === id);
    return obj
  }
  return (
    <>
    {
      (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Header className="mb-5" bgPurple noAuth noMulticurrency webcheckin />)
    }
      <Container wrapperClass={styles.waveBg}>
        <div className="px-3 px-md-0">
          <h3 className={styles.heading}>Web Check-IN</h3></div>
        {booking ? booking.rooms.map((room, index) => {
          return (
            room.guests.map(guest => {
              console.log(guest, "nik-log obj")
              return (
                <BoardingPassComponent guestData={guest} itineraryData={booking.itinerary} roomData={room} passData={findPassDetails(passData, guest.id)} refId={booking.number} bookingId={booking.id} />
              )
            }))
        }) : <div className="my-5 py-5"><div className="spinner-border d-flex justify-content-center mx-auto" role="status">
          <span className="sr-only">Loading...</span>
        </div></div>}
      </Container>
      {
        (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
      }
    </>
  );
}

export default BoardingPass;
