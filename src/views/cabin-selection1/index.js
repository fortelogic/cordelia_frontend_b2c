import React, { useEffect, useState, useRef, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getCabins,
  addCabin,
  removeCabin,
  addGuestToSelectedCabin,
  removeGuestFromSelectedCabin,
  removeInvalidCabins,
} from "../../redux/actions/cabinAction";
import Header from "../../components/Header/HeaderHome";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Card from "../../components/UI/Card";
import Footer from "../../components/Footer/Footer";
import DeleteIcon from "../../assets/img/delete-icon.svg";
import BedIcon from "../../assets/img/bed-icon.svg";
import Minus from "../../assets/img/minus.svg";
import Plus from "../../assets/img/plus.svg";
import bookinImgBanner from "../../assets/img/sidebaner-booking.jpg";
import bookinImgBannerMobile from "../../assets/img/sidebaner-booking-Mob.png";
import "./cabinsec.scss";
import cx from "classnames";
import { useHistory } from "react-router-dom";
import Container from "../../components/Container";
import SelectedCabinList from "../cabin-selection/SelectedCabinList";
import { ReactComponent as DiscountIcon } from '../../assets/icons/discount.svg';


const MAX_GUEST_COUNT = 4;

const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)

function RoomSelection() {
  const dispatch = useDispatch();
  let history = useHistory();
  const [selectedCabinIndex, setSelectedCabinIndex] = useState(0);
  const cabinSection = useRef(null)
  const executeScroll = () => scrollToRef(cabinSection)
  const queryParams = new URLSearchParams(window.location.search)

  const { cabinList } = useSelector((state) => ({
    cabinList: state.allcabins,
  }));

  //run on render
  useEffect(async () => {
    window.scrollTo(0, 0);
    await dispatch(getCabins());
    executeScroll()
  }, []);

  useEffect(async () => {
    // setSelectedCabinIndex(cabinList.length - 1)
  }, [cabinList]);

  let selectRoom = async () => {
    await dispatch(removeInvalidCabins())

    let rem = ''
    for (let i = 0; i < cabinList.length; i++) {
      rem += cabinList[i].name.split(" ").join("_") + '_'
      rem += 'A_' + cabinList[i].guest.adults + '_'
      rem += 'C_' + cabinList[i].guest.children + '_'
      rem += 'I_' + cabinList[i].guest.infants + '_'
    }

    // console.log("Cabin List", cabinList)

    // queryParams.get("platform") === "true" ? history.push("/room-type?platform=true") : history.push('/room-type')
    // queryParams.get("platform") === "true" ? history.push("/room-type?platform=true") : history.push('/room-type')

    queryParams.get("platform") === "true" ?
      history.push(`/room-type?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&platform=true`) :
      history.push(`/room-type?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}`)
  };

  const addCabinHandler = async (e) => {
    if (selectedCabinIndex < 14) {
      e.preventDefault();
      await dispatch(addCabin())
    } else {
      console.log("Not allowed anymore")
    }
  }


  const removeCabinHandler = async (cabinId) => {
    if (cabinList.length === 1) return;
    await dispatch(removeCabin(cabinId));
  };

  const addGuestHandler = (guestType) => {
    const selectedCabin = cabinList[selectedCabinIndex];
    if (selectedCabin) {
      dispatch(
        addGuestToSelectedCabin({
          selectedCabin: selectedCabin,
          guestType: guestType,
        })
      );
    }
  };

  const checkCanbins = () => {
    let res = true
    if (cabinList.length < 0)
      res = false
    else
      cabinList.map(item => { if (!(item.guest.total > 0)) { res = false } })
    return !res
  }

  const removeGuestHandler = (guestType) => {
    const selectedCabin = cabinList[selectedCabinIndex];
    if (selectedCabin) {
      dispatch(
        removeGuestFromSelectedCabin({
          selectedCabin: selectedCabin,
          guestType: guestType,
        })
      );
    }
  };
  return (
    <div className="Home">
      <Header bgPurple platform={queryParams.get("platform")} />
      <div className="cabin-wrapper-body mb-5 pb-5">
        <div className="container">
          <BookingFlowCards.ProgressBar />
        </div>
        <div className="cabin-section mb-4" ref={cabinSection}>
          <div className="container">
            <div className="row justify-content-between">
              {/* <div className="col-12 col-lg-7 col-xl-8 d-md-none mb-4">
                <div className="booknow--bannerImg">
                  <img src={bookinImgBanner} className={cx('offerImageWeb')} />
                  <img src={bookinImgBannerMobile} className={cx('offerImageMob')} />
                </div>
              </div> */}
              <div className="d-flex col-6 col-md-12 align-items-center heading-of-cabins">
                <p className="heading-cabin-guests"><b>Add Cabin & Guests</b></p>
              </div>
              <div className="col-6 d-md-none pt-2">
                <div className="btn-card--bth">
                  <button
                    onClick={addCabinHandler}
                    type="button"
                    className="btn btn-cardadd add-cabin"
                  >
                    Add Cabin
                  </button>
                </div>
              </div>
            </div>
            <div className="row d-none d-md-flex">
              {cabinList.map((cabin, index) => {
                return (
                  <div className="col-12 col-md-6 col-lg-3" key={cabin.id}>
                    <div
                      onClick={() => setSelectedCabinIndex(index)}
                      className={`cabin-boxgrid ${index == selectedCabinIndex ? " selected-cabin" : "shadow"
                        }`}
                    >
                      <h4 className="title-cabin">{cabin.name}</h4>
                      <span className="guest--view">
                        Guest: {cabin.guest.total}
                      </span>
                      {cabin.name == "Cabin 1" ? null : (
                        <div className="delticon">
                          <button
                            onClick={(e) => { e.stopPropagation(); setSelectedCabinIndex(0); removeCabinHandler(cabin.id); }}
                            type="button"
                            className="btn btn-delt"
                          >
                            <img src={DeleteIcon} />
                          </button>
                        </div>
                      )}
                    </div>
                  </div>
                );
              })}
              <div className="col-12 col-md-6 col-lg-3">
                <div className="btn-card--bth">
                  <button
                    onClick={addCabinHandler}
                    type="button"
                    className="btn btn-cardadd add-cabin"
                  >
                    Add Cabin
                  </button>
                </div>
              </div>
            </div>
            <Container>
              <div className="row d-md-none pt-1">
                <div className="crow overflow-auto">
                  {cabinList.map((cabin, index) => {
                    return (
                      <div className="col cabin-slider" key={cabin.id}>
                        <div
                          onClick={() => setSelectedCabinIndex(index)}
                          className={`cabin-boxgrid ${index == selectedCabinIndex ? " selected-cabin" : null
                            }`}
                        >
                          <h4 className="title-cabin">{cabin.name}</h4>
                          <span className="guest--view">
                            Guest: {cabin.guest.total}
                          </span>
                          {cabin.name == "Cabin 1" ? null : (
                            <div className="delticon">
                              <button
                                onClick={(e) => { e.stopPropagation(); setSelectedCabinIndex(0); removeCabinHandler(cabin.id); }}
                                type="button"
                                className="btn btn-delt"
                              >
                                <img src={DeleteIcon} />
                              </button>
                            </div>
                          )}
                        </div>
                      </div>
                    );
                  })}
                </div>
                <div className="col-12 col-md-6 col-lg-3 d-none d-md-block">
                  <div className="btn-card--bth">
                    <button
                      onClick={addCabinHandler}
                      type="button"
                      className="btn btn-cardadd add-cabin"
                    >
                      Add Cabin
                    </button>
                  </div>
                </div>
              </div>
            </Container>
          </div>
        </div>
        <div className="cabin--section--main">
          <div className="container">
            <h4 className="font-weight-bold">How many guests for your cabin?</h4>
            <p>
              <img src={BedIcon} className="mr-2" />
              Cabin can accomodate 2,3 or 4 Guests
            </p>
            <div className="row mb-4">
              <div className="passenger-row">
                {/* <Card className="">
                  <div className="card-body">
                    <div className="cabinadd--field">
                      <h4>
                        {cabinList[selectedCabinIndex] &&
                          cabinList[selectedCabinIndex].name}
                      </h4>
                      <h4 className="font-weight-bold">How many guests for your cabin?</h4>
                      <p>
                        <img src={BedIcon} className="mr-2" />
                        Cabin can accomodate 2,3 or 4 Guests
                      </p>
                      <div className="listMore--items--vat">
                        <ul className="mb-5">
                          <li>
                            <div className="widgit--guest--list">
                              <div className="iconbutton-comn">
                                <button
                                  disabled={
                                    cabinList[selectedCabinIndex] &&
                                    (cabinList[selectedCabinIndex].guest
                                      .total === 1 ||
                                      cabinList[selectedCabinIndex].guest
                                        .adults === 1)
                                  }
                                  onClick={() => removeGuestHandler("adults")}
                                  type="button"
                                  className={cx(
                                    "btn",
                                    `${cabinList[selectedCabinIndex] &&
                                      (cabinList[selectedCabinIndex].guest
                                        .total === 1 ||
                                        cabinList[selectedCabinIndex].guest
                                          .adults === 1)
                                      ? "btn-minus"
                                      : "btn-plus"
                                    }`
                                  )}
                                >
                                  <img src={Minus} />
                                </button>
                              </div>
                              <div className="items--filltxt">
                                <span className="fst-txtnv">
                                  {cabinList[selectedCabinIndex] &&
                                    cabinList[selectedCabinIndex].guest.adults}
                                </span>
                                <span className="fst-txtnv adultxt">
                                  Adults
                                </span>
                                <span className="scnd-txtnv">12 & Above</span>
                              </div>
                              <div className="iconbutton-comn">
                                <button
                                  disabled={
                                    cabinList[selectedCabinIndex] &&
                                    cabinList[selectedCabinIndex].guest
                                      .total === MAX_GUEST_COUNT
                                  }
                                  onClick={() => addGuestHandler("adults")}
                                  type="button"
                                  className={cx(
                                    "btn",
                                    `${cabinList[selectedCabinIndex] &&
                                      cabinList[selectedCabinIndex].guest
                                        .total === MAX_GUEST_COUNT
                                      ? "btn-minus"
                                      : "btn-plus"
                                    }`
                                  )}
                                >
                                  <img src={Plus} />
                                </button>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="widgit--guest--list">
                              <div className="iconbutton-comn">
                                <button
                                  disabled={
                                    cabinList[selectedCabinIndex] &&
                                    (cabinList[selectedCabinIndex].guest
                                      .total === 0 ||
                                      cabinList[selectedCabinIndex].guest
                                        .children === 0)
                                  }
                                  onClick={() => removeGuestHandler("children")}
                                  type="button"
                                  className={cx(
                                    "btn",
                                    `${cabinList[selectedCabinIndex] &&
                                      (cabinList[selectedCabinIndex].guest
                                        .total === 0 ||
                                        cabinList[selectedCabinIndex].guest
                                          .children === 0)
                                      ? "btn-minus"
                                      : "btn-plus"
                                    }`
                                  )}
                                >
                                  <img src={Minus} />
                                </button>
                              </div>
                              <div className="items--filltxt">
                                <span className="fst-txtnv">
                                  {cabinList[selectedCabinIndex] &&
                                    cabinList[selectedCabinIndex].guest
                                      .children}
                                </span>
                                <span className="fst-txtnv adultxt">
                                  Children
                                </span>
                                <span className="scnd-txtnv">
                                  2 Years - 12 Years
                                </span>
                              </div>
                              <div className="iconbutton-comn">
                                <button
                                  disabled={
                                    cabinList[selectedCabinIndex] &&
                                    cabinList[selectedCabinIndex].guest
                                      .total === MAX_GUEST_COUNT
                                  }
                                  onClick={() => addGuestHandler("children")}
                                  type="button"
                                  className={cx(
                                    "btn",
                                    `${cabinList[selectedCabinIndex] &&
                                      cabinList[selectedCabinIndex].guest
                                        .total === MAX_GUEST_COUNT
                                      ? "btn-minus"
                                      : "btn-plus"
                                    }`
                                  )}
                                >
                                  <img src={Plus} />
                                </button>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="widgit--guest--list">
                              <div className="iconbutton-comn">
                                <button
                                  disabled={
                                    cabinList[selectedCabinIndex] &&
                                    (cabinList[selectedCabinIndex].guest
                                      .total === 0 ||
                                      cabinList[selectedCabinIndex].guest
                                        .infants === 0)
                                  }
                                  onClick={() => removeGuestHandler("infants")}
                                  type="button"
                                  className={cx(
                                    "btn",
                                    `${cabinList[selectedCabinIndex] &&
                                      (cabinList[selectedCabinIndex].guest
                                        .total === 0 ||
                                        cabinList[selectedCabinIndex].guest
                                          .infants === 0)
                                      ? "btn-minus"
                                      : "btn-plus"
                                    }`
                                  )}
                                >
                                  <img src={Minus} />
                                </button>
                              </div>
                              <div className="items--filltxt">
                                <span className="fst-txtnv">
                                  {cabinList[selectedCabinIndex] &&
                                    cabinList[selectedCabinIndex].guest.infants}
                                </span>
                                <span className="fst-txtnv adultxt">
                                  Infants
                                </span>
                                <span className="scnd-txtnv">
                                  6 Month - 2 Years
                                </span>
                              </div>
                              <div className="iconbutton-comn">
                                <button
                                  disabled={
                                    cabinList[selectedCabinIndex] &&
                                    cabinList[selectedCabinIndex].guest
                                      .total === MAX_GUEST_COUNT
                                  }
                                  onClick={() => addGuestHandler("infants")}
                                  type="button"
                                  className={cx(
                                    "btn",
                                    `${cabinList[selectedCabinIndex] &&
                                      cabinList[selectedCabinIndex].guest
                                        .total === MAX_GUEST_COUNT
                                      ? "btn-minus"
                                      : "btn-plus"
                                    }`
                                  )}
                                >
                                  <img src={Plus} />
                                </button>
                              </div>
                            </div>
                          </li>
                        </ul>
                        <p>
                          * Infants below 6 months are not allowed to travel
                        </p>
                      </div>
                    </div>
                  </div>
                </Card> */}


                <div className="adults-section">
                  <h4 className="font-weight-bold">Adult</h4>
                  <p>12 Years & Above</p>
                  <div className="adults-box">
                    <div className="widgit--guest--list">
                      <div className="iconbutton-comn">
                        <button
                          disabled={
                            cabinList[selectedCabinIndex] &&
                            (cabinList[selectedCabinIndex].guest
                              .total === 1 ||
                              cabinList[selectedCabinIndex].guest
                                .adults === 1)
                          }
                          onClick={() => removeGuestHandler("adults")}
                          type="button"
                          className={cx(
                            "btn",
                            `${cabinList[selectedCabinIndex] &&
                              (cabinList[selectedCabinIndex].guest
                                .total === 1 ||
                                cabinList[selectedCabinIndex].guest
                                  .adults === 1)
                              ? "btn-minus"
                              : "btn-plus"
                            }`
                          )}
                        >
                          {/* <img src={Minus} /> */}
                          <i className="fa fa-minus" />
                        </button>
                      </div>
                      <div className="items--filltxt">
                        <span className="fst-txtnv">
                          {cabinList[selectedCabinIndex] &&
                            cabinList[selectedCabinIndex].guest.adults}
                        </span>
                        {/* <span className="fst-txtnv adultxt">
                          Adults
                        </span>
                        <span className="scnd-txtnv">12 & Above</span> */}
                      </div>
                      <div className="iconbutton-comn">
                        <button
                          disabled={
                            cabinList[selectedCabinIndex] &&
                            cabinList[selectedCabinIndex].guest
                              .total === MAX_GUEST_COUNT
                          }
                          onClick={() => addGuestHandler("adults")}
                          type="button"
                          className={cx(
                            "btn",
                            `${cabinList[selectedCabinIndex] &&
                              cabinList[selectedCabinIndex].guest
                                .total === MAX_GUEST_COUNT
                              ? "btn-minus"
                              : "btn-plus"
                            }`
                          )}
                        >
                          <i className="fa fa-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="children-section">
                  <h4 className="font-weight-bold">Child</h4>
                  <p>2 Years - 12 Years*</p>
                  <div className="adults-box">
                    <div className="widgit--guest--list">
                      <div className="iconbutton-comn">
                        <button
                          disabled={
                            cabinList[selectedCabinIndex] &&
                            (cabinList[selectedCabinIndex].guest
                              .total === 0 ||
                              cabinList[selectedCabinIndex].guest
                                .children === 0)
                          }
                          onClick={() => removeGuestHandler("children")}
                          type="button"
                          className={cx(
                            "btn",
                            `${cabinList[selectedCabinIndex] &&
                              (cabinList[selectedCabinIndex].guest
                                .total === 0 ||
                                cabinList[selectedCabinIndex].guest
                                  .children === 0)
                              ? "btn-minus"
                              : "btn-plus"
                            }`
                          )}
                        >
                          <i className="fa fa-minus" />
                        </button>
                      </div>
                      <div className="items--filltxt">
                        <span className="fst-txtnv">
                          {cabinList[selectedCabinIndex] &&
                            cabinList[selectedCabinIndex].guest
                              .children}
                        </span>
                      </div>
                      <div className="iconbutton-comn">
                        <button
                          disabled={
                            cabinList[selectedCabinIndex] &&
                            cabinList[selectedCabinIndex].guest
                              .total === MAX_GUEST_COUNT
                          }
                          onClick={() => addGuestHandler("children")}
                          type="button"
                          className={cx(
                            "btn",
                            `${cabinList[selectedCabinIndex] &&
                              cabinList[selectedCabinIndex].guest
                                .total === MAX_GUEST_COUNT
                              ? "btn-minus"
                              : "btn-plus"
                            }`
                          )}
                        >
                          <i className="fa fa-plus" />
                        </button>
                      </div>
                    </div>
                  </div>

                  <div className="">
                    <DiscountIcon />Kids up to 12 years of age can sail free on any of our cruises till February 2022
                  </div>
                </div>

                <div className="infants-section">
                  <h4 className="font-weight-bold">Infant</h4>
                  <p>6 Months to 2 Years (infants below 6 onths are not allowed to travel)</p>
                  <div className="adults-box">
                    <div className="widgit--guest--list">
                      <div className="iconbutton-comn">
                        <button
                          disabled={
                            cabinList[selectedCabinIndex] &&
                            (cabinList[selectedCabinIndex].guest
                              .total === 0 ||
                              cabinList[selectedCabinIndex].guest
                                .infants === 0)
                          }
                          onClick={() => removeGuestHandler("infants")}
                          type="button"
                          className={cx(
                            "btn",
                            `${cabinList[selectedCabinIndex] &&
                              (cabinList[selectedCabinIndex].guest
                                .total === 0 ||
                                cabinList[selectedCabinIndex].guest
                                  .infants === 0)
                              ? "btn-minus"
                              : "btn-plus"
                            }`
                          )}
                        >
                          {/* <img src={Minus} /> */}
                          <i className="fa fa-minus" />
                        </button>
                      </div>
                      <div className="items--filltxt">
                        <span className="fst-txtnv">
                          {cabinList[selectedCabinIndex] &&
                            cabinList[selectedCabinIndex].guest.infants}
                        </span>
                      </div>
                      <div className="iconbutton-comn">
                        <button
                          disabled={
                            cabinList[selectedCabinIndex] &&
                            cabinList[selectedCabinIndex].guest
                              .total === MAX_GUEST_COUNT
                          }
                          onClick={() => addGuestHandler("infants")}
                          type="button"
                          className={cx(
                            "btn",
                            `${cabinList[selectedCabinIndex] &&
                              cabinList[selectedCabinIndex].guest
                                .total === MAX_GUEST_COUNT
                              ? "btn-minus"
                              : "btn-plus"
                            }`
                          )}
                        >
                          {/* <img src={Plus} /> */}
                          <i className="fa fa-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              {/* <div className="col-12 col-lg-7 col-xl-8 d-none d-md-block">
                <div className="booknow--bannerImg">
                  <img src={bookinImgBanner} />
                </div>
              </div> */}
            </div>
            <div className="row">
              <div className="col-12">
                <div className="text-center room-btnview">
                  <button
                    onClick={selectRoom}
                    disabled={
                      checkCanbins()
                    }
                    className="ant-btn ant-btn-primary ant-btn-lg cabin-selection-button"
                  >
                    Select Room Type
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <Footer /> */}
      {
        (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
      }
    </div>
  );
}

export default RoomSelection;
