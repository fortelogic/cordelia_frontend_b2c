import React, { Component } from "react";
import "./index.scss";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Home from "./home/index"
import CruiseRoutes from "./cruise-routes/index"
import RoomSelection from "./room-selection/index"
import CabinSelection from "./cabin-selection/index"
import CabinSelection1 from "./cabin-selection1/index"
import ThankYou from "./thank-you/index"
import GuestSelection from "./guest-selection/index"
import DeckSelection from "./deck-selection/index"
import BookingSummary from "./booking-summary/index"
import GuestDetails from "./guest-details/index"
import ShoreExcursions from "./shore-excursions/index"
import PaymentDetails from "./payment-details/index"
import ConfirmedBookingDetails from "./confirmed-booking-details/index"
import My404Component from "./404"
import HomeComponent from './HomeComponent/index'
import Itinerary1 from './itinerary/5N-MUM-KOC-LAK-MUM'
import WebCheckInComponent from "./web-check-in/index"
import BoardingPassComponent from "./boarding-pass"
import BookingDetailsComponent from "./booking-details"
import Aboutship from "./about-ship"
import FAQ from "./faq";
import AboutUs from "./about-us"
import MyBookings from "./my-bookings"
import MyBookingsDetails from "./my-bookings-details"
import CruiseDestination from "./cruise-destination"
import Contactus from "./contact-us"
import PrivacyPolicy from "./privacy-policy"
import CruiseDeals from "./cruise-deals/cruise-deals";
import PostQuotation from "./post-quotation";
import PostQuotationPayment from "./post-quotation/post-quotation-payment";
import Quotation from "../views/quotation"
import ReactGA from "react-ga"
import BookingSuccessPage from "./booking-success";
import HealthyWaves from "./healthy-waves"
import OnboardPolicy from "./onboard-policy"

class App extends Component {

  render() {

    // Jay's tracking Id
    // const TRACKING_ID = "UA-209939585-1"

    // Sai sir's tracking id
    const TRACKING_ID = "UA-189041422-2"
    ReactGA.initialize(TRACKING_ID)

    return (
      <Router>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/cruise-routes' component={CruiseRoutes} />
          <Route path='/room-selection' component={RoomSelection} />
          <Route path='/room-type' component={CabinSelection} />
          <Route path='/cabin-selection' component={CabinSelection1} />
          <Route path='/guest-details' component={GuestSelection} />
          <Route path='/thank-you' component={ThankYou} />
          <Route path='/deck-selection' component={DeckSelection} />
          <Route path='/booking-summary' component={BookingSummary} />
          <Route path='/guest-selection' component={GuestSelection} />
          <Route path='/itinerary' component={HomeComponent} />
          <Route path='/5N-MUM-KOC-LAK-MUM' component={Itinerary1} />
          <Route path='/payment-details' component={PaymentDetails} />
          <Route path='/confirmed-booking-details' component={ConfirmedBookingDetails} />
          <Route path='/destination' component={ShoreExcursions} />

          <Route path="/post-quotation" component={PostQuotation} />
          <Route path="/quotations" component={Quotation} />
          <Route path="/quotations-payment-mode" component={PostQuotationPayment} />

          <Route path="/booking/success" component={BookingSuccessPage} />
          {/* <Route path="/" component={ } /> */}

          <Route path="/cruise-destinations" component={CruiseDestination} />
          <Route path='/aboutship' component={Aboutship} />
          <Route path="/contact" component={Contactus} />
          <Route path="/cruise-deals" component={CruiseDeals} />
          {/* <Route path="/privacy-policy" component={() => { window.location = "https://cordelia.fortelogic.in/privacy-policy"; return null }} /> */}

          <Route path="/healthy-waves" component={HealthyWaves} />
          <Route path="/onboard-policy" component={OnboardPolicy} />
          <Route path="/privacy-policy" component={PrivacyPolicy} />
          <Route path="/tnc" component={() => { window.location = "https://cordelia.fortelogic.in/tnc"; return null }} />
          <Route path="/blog" component={() => { window.location = "https://blog.cordeliacruises.com"; return null }} />

          <Route path='/web-check-in/booking-details' component={BookingDetailsComponent} />
          <Route path='/web-check-in/guest-details/:refNumber' component={WebCheckInComponent} />
          <Route path='/web-check-in/boarding-pass/:id' component={BoardingPassComponent} />
          <Route path='/faq' component={FAQ} />
          <Route path='/about-us' component={AboutUs} />
          <Route path='/my-bookings' component={MyBookings} />
          <Route path='/my-bookings-details' component={MyBookingsDetails} />
          <Route path='/404' component={My404Component} />
          <Redirect from='*' to='/404' />
        </Switch>
      </Router>
    );
  }
}

export default App;
