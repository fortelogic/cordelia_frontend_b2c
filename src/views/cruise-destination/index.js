import React from 'react'
import cx from "classnames"
import styles from "./cruise-destination.module.css"
import Header from "../../components/Header/HeaderHome"
import Container from "../../components/Container/index"
import Footer from "../../components/Footer/Footer"
import FooterButtonSection from "../../components/UI/FooterButtonSection"
import ReactHtmlParser from "react-html-parser"
import Slider from "react-slick"
import { Button } from "antd"

import Lakshdweep from "../../assets/img/our-destination/lakshadweep-trending.png"
import Chennai from "../../assets/img/our-destination/chennai.jpg"
import Kochi from "../../assets/img/our-destination/kochi.png"
import Mumbai from "../../assets/img/our-destination/mumbai.png"
import Goa from "../../assets/img/our-destination/goa.png"
import Diu from "../../assets/img/our-destination/diu.png"
import MumbaiDeparture from "../../assets/img/lakshdweep-page/mumbai.jpg"
import CruiseInspirationOne from "../../assets/img/destination-page/cruise_inspiration.webp"
import CruiseInspirationTwo from "../../assets/img/destination-page/cruise_inspiration_2.webp"
import CruiseInspirationThree from "../../assets/img/destination-page/cruise_inspiration_3.webp"

import { TrendingCruise } from "../../components/Section/TrendingCruise.js"



const OccasionsData = {
    title: "CRUISE <strong>INSPIRATION</strong>",
    description:
        "Onboard the Empress, there are unlimited options for the event attendees and their families to relax and recharge.",
    list: [
        {
            category: "Travel Inspiration",
            image: CruiseInspirationOne,
            date: "7th July",
            title: "A Staycation that takes you places",
            description:
                "Regular staycations have passed, it’s time for offbeat weekend getaways from Mumbai with India’s premium cruise liner – Cordelia Cruises – A city on the sea where the journey is as exciting as the destination.",
            link:
                "https://blog.cordeliacruises.com/2021/07/07/the-best-staycation-in-mumbai-a-staycation-that-takes-you-places/",
        },
        {
            category: "Destinations",
            image: CruiseInspirationTwo,
            date: "12th July",
            title: "Is Lakshadweep the Maldives of India?",
            description:
                "Lakshadweep is located 400 kilometers west of mainland India, just north of the Maldives. The archipelago is made up of 36 beautiful islands, a promising cluster surrounded by shallow sandbanks and pristine coral reefs. Agatti, Bangaram, and Kadmat are the core paradises of Lakshadweep, located in the central part of the archipelago.",
            link:
                "https://blog.cordeliacruises.com/2021/07/13/is-lakshadweep-the-maldives-of-india/",
        },
        {
            category: "Destinations",
            image: CruiseInspirationThree,
            date: "10th July",
            title: "Destination weddings get a makeover with cruise weddings",
            description:
                "Taking your weddings vows in the middle of the ocean is sure to be a memorable experience and one to cherish for years to come. Wonder how a cruise wedding can be better than a regular destination wedding?",
            link:
                "https://blog.cordeliacruises.com/2021/07/07/destination-weddings-get-a-major-makeover-with-cruise-weddings/",
        },
    ],
};


const DeparturePortsData = {
    title: "CRUISE <strong>DEPARTURE PORTS</strong>",
    description:
        "Your cruise adventure begins here. Set sail on an unforgettable cruise journey from these port destinations.",
    ports: [
        {
            title: "MUMBAI",
            description: (
                <>
                    <div className="text-white pb-10">
                        From colonial architecture to high-tech skyscrapers, Mumbai’s
                        skyline is an interesting reflection of history and modernity.
                        Popularly known as the Maximum City, Mumbai is filled with iconic
                        tourist attractions, local and fine-dine restaurants, shopping
                        malls, lanes and beautiful beaches
                    </div>
                    <div className="mt-5">
                        <a
                            href="/cruise-routes"
                            className={styles.viewCruises}
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-mumbai"
                            className={cx(styles.exploreMore)}
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: MumbaiDeparture,
        },
        {
            title: "KOCHI",
            description: (
                <>
                    <div className="text-white pb-10">
                        Kochi, formerly known as Cochin is Kerala’s commercial capital, and
                        a cosmopolitan city which has brilliantly upheld the distinct
                        cultural and historical identity of the state. A modern reflection
                        of India’s new identity, Kochi is an interesting mix of hilly areas
                        and backwaters attracting travelers and traders.
                    </div>
                    <div className="mt-5">
                        <a
                            href="/cruise-routes"
                            className={styles.viewCruises}
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-kochi"
                            className={cx(styles.exploreMore)}
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Kochi,
        },
        {
            title: "GOA",
            description: (
                <>
                    <div className="text-white pb-10">
                        This Konkan state in the country’s southwestern coast is an
                        interesting mix of sun, sand, and spice. Whether you’re traveling
                        with your family and friends or traveling solo, Goa’s versatility
                        never fails to charm.
                    </div>
                    <div className="mt-5">
                        <a
                            href="/cruise-routes"
                            className={styles.viewCruises}
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-goa"
                            className={cx(styles.exploreMore)}
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Goa,
        },
        {
            title: "CHENNAI",
            description: (
                <>
                    <div className="text-white pb-10">
                        Adorned gracefully by numerous beaches, temples, and museums,
                        Chennai is a brilliant reflection of culinary brilliance,
                        cosmopolitan vibes, and traditions. The Marina beach in Chennai is
                        the world's second-largest beach. Your visit to Chennai is
                        incomplete without savoring the authentic South Indian delicacies.
                    </div>
                    <div className="text-left grid grid-cols-1 md:grid-cols-2 gap-4">
                        <a
                            href="/cruise-routes"
                            className={styles.viewCruises}
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-chennai"
                            className={cx(styles.exploreMore)}
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Chennai,
        },
    ],
};

const TrendingDestinationData = {
    title: "TRENDING <strong>CRUISE DESTINATIONS</strong>",
    description:
        "Discover the beauty of Lakshadweep, the charm of Goa, witness the beauty of Diu, the aura of Kochi, and the cultural spirit of Chennai on a cruise vacation with Cordelia Cruises. Be it weekend getaways from Mumbai or family holidays to Lakshadweep, there’s a cruise holiday for every occasion. Choose a Mumbai to Goa cruise or cruise from Mumbai to Diu; cruise from Mumbai and Kochi to Lakshadweep or simply choose to explore the beauty of Kerala on a cruise holiday from Mumbai to Kochi.",
    ports: [
        {
            title: "LAKSHADWEEP",
            description: (
                <>
                    <div className="text-white">
                        With the picture-perfect flawless combination of awe-inspiring land
                        and seascapes, Lakshadweep is a destination that outshines its
                        reputation.
                    </div>
                    <div className="text-left grid grid-cols-1 md:grid-cols-2 gap-4">
                        <a
                            href="/cruise-routes"
                            className={cx(styles.viewCruises)}
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-lakshadweep"
                            className={cx(styles.exploreMore)}
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Lakshdweep,
        },
        {
            title: "KOCHI",
            description: (
                <>
                    <div className="text-base pb-10">
                        Kochi is popular as a gateway to discover God’s own Country, Kerala.
                        Kochi has brilliantly upheld the distinct cultural and historical
                        identity of the state.
                    </div>
                    <div className="text-left grid grid-cols-1 md:grid-cols-2 gap-4">
                        <a
                            href="/cruise-routes"
                            className={styles.viewCruises}
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-kochi"
                            className={cx(styles.exploreMore)}
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Kochi,
        },
        {
            title: "GOA",
            description: (
                <>
                    <div className="text-base pb-10">
                        A backpacker’s paradise and a weekend travel hub, Goa is famous for
                        its tropical vibe, young identity, and cultural adaptations.
                    </div>
                    <div className="text-left grid grid-cols-1 md:grid-cols-2 gap-4">
                        <a
                            href="/cruise-routes"
                            className={styles.viewCruises}
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-goa"
                            className={cx(styles.exploreMore)}
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Goa,
        },
        {
            title: "CHENNAI",
            description: (
                <>
                    <div className="text-base pb-10">
                        Chennai will surprise you with the diverse combination of art,
                        religion, and cultural identity. Chennai is a brilliant reflection
                        of culinary brilliance, cosmopolitan vibes, and traditions.
                    </div>
                    <div className="text-left grid grid-cols-1 md:grid-cols-2 gap-4">
                        <a
                            href="/cruise-routes"
                            className=" view-cruises-btn rounded-small transition duration-300 ease-in-out py-3  px-6 font-bold uppercase text-sm bg-j-orange text-j-white text-center"
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-chennai"
                            className=" view-cruises-btn rounded-small transition duration-300 ease-in-out py-3  px-6 font-bold uppercase text-sm border border-white text-j-white text-center"
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Chennai,
        },
        {
            title: "DIU",
            description: (
                <>
                    <div className="text-base pb-10">
                        The powerhouse of natural beauty, culture and colonial architecture,
                        Diu is one of the must-visit weekend getaways from Mumbai.
                    </div>
                    <div className="text-left grid grid-cols-1 md:grid-cols-2 gap-4">
                        <a
                            href="/cruise-routes"
                            className=" view-cruises-btn rounded-small transition duration-300 ease-in-out py-3  px-6 font-bold uppercase text-sm bg-j-orange text-j-white text-center"
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-diu"
                            className=" view-cruises-btn rounded-small transition duration-300 ease-in-out py-3  px-6 font-bold uppercase text-sm border border-white text-j-white text-center"
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Diu,
        },
        {
            title: "MUMBAI",
            description: (
                <>
                    <div className="text-base pb-10">
                        India’s most fascinating melting pot and the city of dreams,
                        Mumbai’s spirit is eternal.
                    </div>
                    <div className="text-left grid grid-cols-1 md:grid-cols-2 gap-4">
                        <a
                            href="/cruise-routes"
                            className=" view-cruises-btn rounded-small transition duration-300 ease-in-out py-3  px-6 font-bold uppercase text-sm bg-j-orange text-j-white text-center"
                        >
                            View Cruise
                        </a>
                        <a
                            href="/cruise-destinations/cruise-to-mumbai"
                            className=" view-cruises-btn rounded-small transition duration-300 ease-in-out py-3  px-6 font-bold uppercase text-sm border border-white text-j-white text-center"
                        >
                            Explore Now
                        </a>
                    </div>
                </>
            ),
            image: Mumbai,
        },
    ],
};

const PageBottom = ({ pagestyles }) => {
    const sectionStyles = pagestyles || styles;

    return (
        <div
            className={cx(
                sectionStyles["social-media-area"],
                sectionStyles["wedding-page-section"],
                sectionStyles["pageSection"],
                sectionStyles["social-custom-align"]
            )}
        >
            <div className={cx(sectionStyles["social-media-area-inner"])}>
                <div className={cx(sectionStyles["social-media-area-heading-group"])}>
                    <h2
                        className={cx(sectionStyles["wedding-page-title"], "text-center")}
                    >
                        FOLLOW US ON <strong>SOCIAL MEDIA</strong>
                    </h2>
                    <h6
                        className={cx(
                            sectionStyles["wedding-page-subtitle"],
                            "text-center"
                        )}
                    >
                        Follow us for the latest updates and offers
                    </h6>
                </div>
                <div className="text-center">
                    <ul className={cx(sectionStyles["social-media-list"])}>
                        <li>
                            <a
                                href="https://www.facebook.com/cordeliacruises"
                                target="_blank"
                                aria-label="Facebook"
                                rel="noreferrer"
                            >
                                <i className="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.instagram.com/cordeliacruises_india/?hl=en"
                                target="_blank"
                                aria-label="Facebook"
                                rel="noreferrer"
                            >
                                <i className="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.linkedin.com/company/cordelia-cruises"
                                target="_blank"
                                aria-label="Facebook"
                                rel="noreferrer"
                            >
                                <i className="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://twitter.com/CordeliaCruises"
                                target="_blank"
                                aria-label="Facebook"
                                rel="noreferrer"
                            >
                                <i className="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

const CruiseDestination = () => {

    const queryParams = new URLSearchParams(window.location.search)

    const settingsInterior = {
        infinite: true,
        slidesToShow: 3,
        // className: "center",
        // centerMode: true,
        // centerPadding: "100px",
        speed: 300,
        cssEase: 'linear',
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                },
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    const cruisePortSliderSettings = {
        infinite: false,
        slidesToShow: 4,
        speed: 300,
        cssEase: "linear",
        dots: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 2,
                    infinite: false,
                },
            },
            {
                breakpoint: 767,
                settings: {
                    infinite: false,
                    slidesToShow: 1,
                    speed: 300,
                    cssEase: "linear",
                    dots: true,
                    arrows: false,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    infinite: false,
                    slidesToShow: 1,
                    speed: 300,
                    cssEase: "linear",
                    dots: true,
                    arrows: false,
                },
            },
        ],
    };

    return (
        <div className="Home">
            <Header bgPurple platform={queryParams.get("platform")} />
            <div className={cx(styles.bannerSection)}>
                <div className={cx(styles.bannerContent)}>
                    <h1 className={cx(styles.bannerTitle)}>CRUISE DESTINATIONS</h1>
                    <p className={cx(styles.bannerSubTitle)}>EVERY DESTINATION HAS A STORY TO TELL</p>
                </div>
            </div>
            <Container strict="true">
                <div className={cx(styles.vacationParagraph)}>
                    <p className={cx(styles.vacationParagraphText)}>A cruise vacation with Cordelia Cruises takes you to some of the most gorgeous Indian and internaltional destinations. A perfect journey deserves a perfect destination. From the wild and unexplored beauty of Lakshadweep to the popular beaches of Goa and the stunning landscapes of Kochi, every cruise destination has a story to tell. Explore the flavors, the natural beauty, and cultures of the destinations. Cruise holidays let you experience destinations in different ways. Choose your favorite cruise destination and start planning.</p>
                </div>

                <div className={cx(styles.trendingCruisesDesktop)}>
                    <TrendingCruise />
                    {/* <h1 className={cx(styles.trendingCruisesDesktopHeading)}>{ReactHtmlParser(TrendingDestinationData.title)}</h1>
                    <p className={cx(styles.trendingCruisesDesktopDescription)}>{ReactHtmlParser(TrendingDestinationData.description)}</p>
                    <div className={cx(styles.trendingCruisesDesktopImageStack)}>
                        {
                            TrendingDestinationData && TrendingDestinationData.ports.map((item, index) => {
                                return <div key={index}>
                                    <img className={cx(styles.trendingCruisesDesktopImage)} src={item.image} />
                                </div>
                            })
                        }
                    </div> */}
                </div>

                <div className={cx(styles.trendingCruisesMobile)}>

                </div>
            </Container>
            {/* <Container strict="true">
                <div className={cx(styles.cruiseDeparturePorts)}>
                    <h1 className={cx(styles.cruiseDeparturePortsTitle)}>{ReactHtmlParser(DeparturePortsData.title)}</h1>
                    <p className={cx(styles.cruiseDeparturePortsDescription)}>{ReactHtmlParser(DeparturePortsData.description)}</p>

                    <Slider {...cruisePortSliderSettings} className={cx(styles.cruiseDeparturePortsSlider)}>
                        {
                            DeparturePortsData.ports.map((item, index) => {
                                return <div key={index} className={cx(styles.cruiseDeparturePorts)}>
                                    <img src={item.image} style={{ width: "100%", objectFit: "cover" }} />
                                </div>
                            })
                        }
                    </Slider>
                </div>
            </Container> */}
            <div className={cx(styles.cruiseDeparturePorts)}>
                <Container strict="true">
                    <h1 className={cx(styles.cruiseDeparturePortsTitle)}>{ReactHtmlParser(DeparturePortsData.title)}</h1>
                    <p className={cx(styles.cruiseDeparturePortsDescription)}>{ReactHtmlParser(DeparturePortsData.description)}</p>
                </Container>
                <Slider className={cx(styles.cruiseDepartureSlider, "m-3")} {...cruisePortSliderSettings} id={Date.now()}>
                    {
                        DeparturePortsData.ports.map((item, index) => {
                            return <div key={index} className={cx(styles.cruiseDeparturePortsSlider, "position-relative")}>
                                <img src={item.image} className={cx(styles.departurePortImage)} />
                                <div className={cx(styles.cruiseDeparturePortName)}>
                                    <h5 className={cx(styles.cruiseDeparturePortNameText)}>{item.title}</h5>
                                </div>
                                <div className={cx(styles.cruisePortContent)}>
                                    <h5 className={cx(styles.cruisePortContentTitle)}>{item.title}</h5>
                                    <p className={cx(styles.cruisePortContentDescription)}>{item.description}</p>
                                </div>
                            </div>
                        })
                    }
                </Slider>
            </div>

            <div className={cx(styles.occasions)}>
                <Container strict="true">
                    <h1 className={styles.occasionsTitle}>{ReactHtmlParser(OccasionsData.title)}</h1>
                    <p className={cx(styles.occasionDescription)}>{OccasionsData.description}</p>
                </Container>
                <Slider className="primary-slider mt-5" {...settingsInterior} id={Date.now()}>
                    {OccasionsData.list.map((item, idx) => {
                        return (
                            <div key={item.title + idx}>
                                <div className="cards-carousel__slider__card">
                                    {item.image && (
                                        <figure>
                                            <img alt="cordelia cruises" src={item.image} />
                                        </figure>
                                    )}

                                    <div className="cards-carousel__slider__card__body text-left">
                                        <div className="cards-carousel__slider__card__body__top" style={{ height: "210px" }}>
                                            <h4 className="cards-carousel__slider__card__body__title">{item.title}</h4>
                                            <h6 className={cx(styles.occasionCategory)}>{item.category}</h6>
                                            {item.description && (
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    {item.description}
                                                </p>
                                            )}
                                        </div>
                                        <div className="cards-carousel__slider__card__body__bottom text-center justify-content-center mt-5">
                                            <a href={item.link} target="_blank" className="cc-button cc-button--primary text-white">
                                                KNOW MORE
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </Slider>
                <a className={cx(styles.inspiration)} href="https://blog.cordeliacruises.com/" target="_blank">
                    <p className={cx(styles.getMoreExpiration)}>Get More inspiration</p>
                </a>
            </div>

            <div>
                <PageBottom />
            </div>

            <FooterButtonSection />
            {/* <Footer /> */}
            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
            }
        </div>
    )
}

export default CruiseDestination
