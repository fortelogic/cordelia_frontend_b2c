import { useState, useEffect, useRef } from "react"
import { useSelector, useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"
import Header from "../../components/Header/HeaderHome";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Card from "../../components/UI/Card";
import Footer from "../../components/Footer/Footer";
import DeleteIcon from "../../assets/img/delete-icon.svg";
import BedIcon from "../../assets/img/bed-icon.svg";
import Minus from "../../assets/img/minus.svg";
import Plus from "../../assets/img/plus.svg";
import "./guest.scss";
import { bookingid } from "../../services/bookingid"
import { allGuests, bookingsummary } from "../../redux/actions/roomAction"
import { getCabins } from "../../redux/actions/cabinAction"
import localStore from "../../utils/localStore";
import completePostQuotation from "../../utils/localStore"
import CounrtyCodes from "../../components/Login/countryCodes.json"
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { DropdownDate } from "react-dropdown-date";
import moment from "moment"


// guests[0].guests[0]

function RoomSelection() {

  var bookingDetails = useSelector((state) => {
    return state.allcabins
  })

  const cabins = useSelector((state) => {
    return state.allcabins;
  });

  const booking = useSelector((state) => state.roomReducer.bookingsummary.booking)

  const history = useHistory();
  const dispatch = useDispatch();
  const [selectedCabin, setSelectedCabin] = useState(0);
  const [selectedGuest, setSelectedGuest] = useState(0);
  const [guests, setGuests] = useState([])
  const [errors, setFormErrors] = useState([])
  const [startDate, setStartDate] = useState(new Date());
  const queryParams = new URLSearchParams(window.location.search)


  useEffect(async () => {
    window.scrollTo(0, 0);
    dispatch(getCabins());
    dispatch(bookingsummary(localStore.get('bookingSummary')))
  }, []);

  useEffect(async () => {

    if (checkCabins()) {
      let arr = []
      let arrtwo = []
      bookingDetails.map((itemup, indexup) => {
        let roomId = itemup.room ? itemup.room[0].itinerary_room_id : '';
        arr.push({ priceKey: itemup.detail.priceKey, roomId: roomId, guests: [] });
        arrtwo.push({ priceKey: itemup.detail.priceKey, roomId: roomId, guests: [] });

        itemup.detail.price.individual.map((itemdown, indexdown) => {
          // arr[indexup]["guests"].push({ firstname: "", lastname: "", gender: "", dob: "", phoneNumber: "", email: "" })
          arr[indexup]['guests'].push({
            name: '',
            lastname: "",
            type: itemdown.type,
            discountCategory: null,
            email: '',
            countryCode: '+91',
            phoneNumber: '',
            gender: '',
            dateOfBirth: '1991-01-01',
            country: 'India',
            state: 'Karnataka',
            citizenship: 'India',
          });

          arrtwo[indexup]['guests'].push({
            name: '',
            lastname: "",
            type: itemdown.type,
            discountCategory: null,
            email: '',
            countryCode: '+91',
            phoneNumber: '',
            gender: '',
            dateOfBirth: '',
            country: 'India',
            state: 'Karnataka',
            citizenship: 'India',
          });

        });
      });
      // setGuests(Object.create(arr))
      // setFormErrors(Object.create(arr))

      setGuests(arr)
      setFormErrors(arrtwo)
    } else {
      // history.push("/room-selection")
    }
  }, [bookingDetails])

  // const validate = () => {
  //   console.log(guests)
  //   let res = []
  //   guests.map(data => data.guests.map(item => {
  //     res.push(item.name != '')
  //     res.push(item.email != '')
  //     res.push(item.gender != '')
  //     res.push(item.countryCode != '')
  //     res.push(item.phoneNumber != '')
  //     res.push(item.dateOfBirth != '')
  //   })
  //   )
  //   console.log(!res.includes(false));
  //   return !res.includes(false)
  // }

  const validation = () => {
    const minus = [...errors]
    const errorstate = []

    const firstnameLastnamePattern = /^[A-Za-z]+$/
    const phonenumberpattern = /^[0-9]+$/



    minus.map((data, indexup) => {
      data.guests.map((item, indexdown) => {
        if (guests[indexup]["guests"][indexdown]["name"] === "" || guests[indexup]["guests"][indexdown]["name"] === undefined) {
          minus[indexup].guests[indexdown]["name"] = "Please fill firstname"
          errorstate.push(false)
        } else {
          if (!firstnameLastnamePattern.test(guests[indexup]["guests"][indexdown]["name"])) {
            minus[indexup].guests[indexdown]["name"] = "Firstname has to be valid"
            errorstate.push(false)
          } else {
            minus[indexup].guests[indexdown]["name"] = ""
          }
        }

        if (guests[indexup]["guests"][indexdown]["lastname"] === "" || guests[indexup]["guests"][indexdown]["lastname"] === undefined) {
          minus[indexup].guests[indexdown]["lastname"] = "Please fill lastname"
          errorstate.push(false)
        } else {
          if (!firstnameLastnamePattern.test(guests[indexup]["guests"][indexdown]["lastname"])) {
            minus[indexup].guests[indexdown]["lastname"] = "Lastname has to be valid"
            errorstate.push(false)
          } else {
            minus[indexup].guests[indexdown]["lastname"] = ""
          }
        }

        if (guests[indexup]["guests"][indexdown]["gender"] === "" || guests[indexup]["guests"][indexdown]["gender"] === undefined) {
          minus[indexup].guests[indexdown]["gender"] = "Please select gender"
          errorstate.push(false)
        } else {
          minus[indexup].guests[indexdown]["gender"] = ""
        }

        if (guests[indexup]["guests"][indexdown]["dateOfBirth"] === "" || guests[indexup]["guests"][indexdown]["dateOfBirth"] === undefined) {
          minus[indexup].guests[indexdown]["dateOfBirth"] = "Please select date of birth"
          errorstate.push(false)
        } else {
          minus[indexup].guests[indexdown]["dateOfBirth"] = ""
          if (guests[indexup]["guests"][indexdown]["type"] === "CHILD") {
            let today = moment()
            let givendate = moment(guests[indexup]["guests"][indexdown]["dateOfBirth"])
            if (today.diff(givendate, 'days') > 4380) {
              minus[indexup].guests[indexdown]["dateOfBirth"] = "Age of the child can be maximum 12 years"
              errorstate.push(false)
            } else {
              minus[indexup].guests[indexdown]["dateOfBirth"] = ""
            }
          }

          if (guests[indexup]["guests"][indexdown]["type"] === "ADULT") {
            let today = moment()
            let givendate = moment(guests[indexup]["guests"][indexdown]["dateOfBirth"])
            if (today.diff(givendate, 'days') < 4380) {
              minus[indexup].guests[indexdown]["dateOfBirth"] = "Age of the adult can not be less then 12 years"
              errorstate.push(false)
            } else {
              minus[indexup].guests[indexdown]["dateOfBirth"] = ""
            }
          }
        }

        if (guests[indexup]["guests"][indexdown]["phoneNumber"] === "" || guests[indexup]["guests"][indexdown]["phoneNumber"] === undefined) {
          minus[indexup].guests[indexdown]["phoneNumber"] = "Please fill phone number"
          errorstate.push(false)
        } else {
          minus[indexup].guests[indexdown]["phoneNumber"] = ""
          if (!phonenumberpattern.test(guests[indexup]["guests"][indexdown]["phoneNumber"])) {
            minus[indexup].guests[indexdown]["phoneNumber"] = "Phonenumber has to be valid"
            errorstate.push(false)
          } else {
            if (guests[indexup]["guests"][indexdown]["phoneNumber"].length > 10) {
              minus[indexup].guests[indexdown]["phoneNumber"] = "Phonenumber can't be more then 10 digits"
              errorstate.push(false)
            } else {
              minus[indexup].guests[indexdown]["phoneNumber"] = ""
            }
          }
        }

        if (guests[indexup]["guests"][indexdown]["email"] === "" || guests[indexup]["guests"][indexdown]["email"] === undefined) {
          minus[indexup].guests[indexdown]["email"] = "Please fill email"
          errorstate.push(false)
        } else {
          minus[indexup].guests[indexdown]["email"] = ""
        }
      })
    })
    setFormErrors(minus)

    if (errorstate.length > 0) {
      return false
    } else {
      return true
    }
  }

  const checkCabins = () => {
    let res = []
    cabins.map((data) => {
      res.push(data.detail.hasOwnProperty('price'))
    })
    return !res.includes(false)
  }

  const guestselection = async () => {
    if (queryParams.get("from")) {
      if (queryParams.get("from") == "booking-success") {
        if (guests[0].guests && guests[0].guests[0] && guests[0].guests[0]) {
          dispatch(allGuests(guests[0].guests[0]))
          const response = await bookingid(booking.id, guests, "staging")
          queryParams.get("platform") === "true" ?
            history.push(`/my-bookings?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&from=booking-success&platform=true`) :
            history.push(`/my-bookings?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&from=booking-success`)

        }
      }
    } else {
      if (guests[0].guests && guests[0].guests[0] && guests[0].guests[0]) {
        dispatch(allGuests(guests[0].guests[0]))
        const response = await bookingid(booking.id, guests)
        queryParams.get("platform") === "true" ? history.push('/payment-details?platform=true') : history.push('/payment-details')
      }
    }
  }

  const addOnSelection = async () => {
    if (guests[0].guests && guests[0].guests[0] && guests[0].guests[0]) {
      await dispatch(allGuests(guests))
      const response = await bookingid(booking.id, guests)
      dispatch(bookingsummary(response.data))
      queryParams.get("platform") === "true" ? history.push('/destination?platform=true') : history.push('/destination')
    }
  }

  const setDataInState = async (property, indexup, indexdown, e) => {
    const values = [...guests];
    guests[indexup].guests[indexdown][property] = e.target.value;
    setGuests(values);
  };


  const formatDate = (date) => {
    // formats a JS date to 'yyyy-mm-dd'
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  };

  return checkCabins() && (
    <div className="Home">
      <Header bgPurple platform={queryParams.get("platform")} />
      {/* <div className="container">
        <BookingFlowCards.ProgressBar />
      </div> */}
      <div className="cabin-section">
        <div className="container">
          <BookingFlowCards.ProgressBar current={2} />
        </div>
        <div className="cabin-section">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-3 ">
                <div className="heading--main">
                  <h3 className="title--main mb-4">Cabins</h3>
                  <h3 className="title--main-a mb-4">Guest Details</h3>
                </div>
              </div>
            </div>
            <div className="cabins-container">
              {bookingDetails &&
                bookingDetails.map((item, index) => {
                  return (
                    <>
                      <div className={'col-8 col-md-6 col-xl-3 p-0 pr-3'} onClick={() => setSelectedCabin(index)}>
                        <div
                          className={'cabin-boxgrid'}
                          style={{ border: selectedCabin == index ? '1px solid #EA725B' : 'none' }}
                        >
                          <h4 className="title-cabin ">{item.name.toUpperCase()}</h4>
                          <span className="guest--view">Guests : {item.guest.total}</span>
                          <span className="guest--view">Deck No.: {item.deck.id}</span>
                          <span className="guest--view">Room No.: {item.room[0].roomno}</span>
                          <div className="delticon">
                            <button type="button" className="btn btn-delt">
                              <img src={DeleteIcon} />
                            </button>
                          </div>
                        </div>
                      </div>
                    </>
                  );
                })}
            </div>
          </div>
        </div>
        <div className="cabin--section--main pb-5">
          <div className="container">
            <h3 className="title--main mt-3">Guests Details</h3>
            <div className="row">
              {/* <div className="col-12 col-md-3">
              <div className="heading--main">
                <h3 className="title--main mb-4">Cabins</h3>
              </div>
            </div> */}

              <div className="cabins-container ml-3 mt-3">
                {bookingDetails.map((single, indexup) => {
                  return (
                    indexup == selectedCabin &&
                    single.detail.price.individual.map((item, indexdown) => {
                      return (
                        <p
                          className={indexdown == selectedGuest ? 'selected-text-size px-3' : 'text-size px-3'}
                          onClick={(e) => setSelectedGuest(indexdown)}
                        >
                          Guest {`${indexdown + 1}`}
                        </p>
                      );
                    })
                  );
                })}
              </div>
            </div>
            <div className="is-mobile">
              <div className="row">
                {bookingDetails.map((single, indexup) => {
                  return (
                    indexup == selectedCabin &&
                    single.detail.price.individual.map((item, indexdown) => {
                      return (
                        indexdown == selectedGuest && (
                          <div className="col-12 col-lg-4">
                            <Card onClick={() => { }}>
                              <div className="card-body">
                                <div className="formcard--cabin">
                                  <h4 className="small--title mb-3">
                                    Guest {indexdown + 1}: <span className="text-capitalize">{item.type}</span>
                                  </h4>
                                  <div className="">
                                    <form className="">
                                      <div className="form-group form-row mb-0">
                                        <div className="col-12 col-md-6 mb-3">
                                          <input
                                            type="text"
                                            placeholder="First Name"
                                            className="form-control guest-input"
                                            value={guests[indexup] ? guests[indexup]['guests'][indexdown]['name'] : ''}
                                            onChange={(e) => setDataInState('name', indexup, indexdown, e)}
                                          />
                                          {/* <span style={{ color: "red" }}>{errors[indexup]['guests'][indexdown]['name']}</span> */}
                                        </div>
                                        <div className="col-12 col-md-6 mb-3">
                                          <input
                                            type="text"
                                            placeholder="Last Name"
                                            className="form-control guest-input"
                                            value={
                                              guests[indexup] ? guests[indexup]['guests'][indexdown]['lastname'] : ''
                                            }
                                            onChange={(e) => setDataInState('lastname', indexup, indexdown, e)}
                                          />
                                          {/* <span style={{ color: "red" }}>{errors[indexup]['guests'][indexdown]['lastname']}</span> */}
                                        </div>
                                        <p className="notice ml-2">Name should as per the passport</p>
                                      </div>
                                      <div className="form-group form-row">
                                        <div className="col-12">
                                          <select
                                            className="form-control guest-input"
                                            value={
                                              guests[indexup] ? guests[indexup]['guests'][indexdown]['gender'] : ''
                                            }
                                            onChange={(e) => setDataInState('gender', indexup, indexdown, e)}
                                          >
                                            <option>Select your Gender</option>
                                            <option>Male</option>
                                            <option>Female</option>
                                          </select>
                                          {/* <span style={{ color: "red" }}>{errors[indexup]['guests'][indexdown]['gender']}</span> */}
                                        </div>
                                      </div>
                                      <div className="form-group form-row">
                                        <div className="col-12">
                                          {/* <input
                                            type="date"
                                            onKeyDown={(e) => e.preventDefault()}
                                            placeholder=""
                                            className="form-control guest-input guest-input-date"
                                            value={
                                              guests[indexup] ? guests[indexup]['guests'][indexdown]['dateOfBirth'] : ''
                                            }
                                            onChange={(e) => setDataInState('dateOfBirth', indexup, indexdown, e)}
                                          /> */}
                                          <DropdownDate className="form-control guest-input guest-input-date" selectedDate={guests[indexup] ? guests[indexup]['guests'][indexdown]['dateOfBirth'] : '1999-04-06'} onDateChange={(date) => { console.log("Noxy", date); setDataInState('dateOfBirth', indexup, indexdown, { target: { value: formatDate(date) } }) }} />
                                          <span style={{ color: "red" }}>{errors[indexup] ? errors[indexup]['guests'][indexdown]['dateOfBirth'] : ``}</span>

                                        </div>
                                      </div>
                                      <div className="form-group form-row">
                                        <div className="col-12">
                                          <div className="d-flex number--txt form-control p-0">
                                            {/* <input
                                              type="text"
                                              placeholder="+91"
                                              className="form-control-num numcontrol--v guest-input"
                                              value={
                                                guests[indexup]
                                                  ? guests[indexup]['guests'][indexdown]['countryCode']
                                                  : ''
                                              }
                                              onChange={(e) => setDataInState('countryCode', indexup, indexdown, e)}
                                            /> */}
                                            <select
                                              className="form-control-num numcontrol--v guest-input"
                                              onChange={(e) => setDataInState('countryCode', indexup, indexdown, e)}
                                              value={"+91"}
                                            >
                                              {CounrtyCodes.map(code => {
                                                return (<option key={code} value={code}>{code}</option>)
                                              })}
                                            </select>

                                            {/* <span style={{ color: "red" }}>{errors[indexup]['guests'][indexdown]['countryCode']}</span> */}
                                            <input
                                              type="text"
                                              placeholder="10 Digit Mobile Number"
                                              className="form-control-num guest-input"
                                              value={
                                                guests[indexup]
                                                  ? guests[indexup]['guests'][indexdown]['phoneNumber']
                                                  : ''
                                              }
                                              onChange={(e) => setDataInState('phoneNumber', indexup, indexdown, e)}
                                            />
                                            {/* <span style={{ color: "red" }}>{errors[indexup]['guests'][indexdown]['phoneNumber']}</span> */}
                                          </div>
                                        </div>
                                      </div>
                                      <div className="form-group form-row">
                                        <div className="col-12">
                                          <input
                                            type="email"
                                            placeholder="Email Address"
                                            className="form-control guest-input"
                                            value={guests[indexup] ? guests[indexup]['guests'][indexdown]['email'] : ''}
                                            onChange={(e) => setDataInState('email', indexup, indexdown, e)}
                                          />
                                          {/* <span style={{ color: "red" }}>{errors[indexup]['guests'][indexdown]['email']}</span> */}
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </Card>
                          </div>
                        )
                      );
                    })
                  );
                })}
              </div>
            </div>
            <div className="isn-t-mobile">
              <div className="custom-guests-forms">
                {bookingDetails.map((single, indexup) => {
                  return (
                    indexup == selectedCabin &&
                    single.detail.price.individual.map((item, indexdown) => {
                      return (
                        <div className="col-4 p-0 pr-4">
                          <Card onClick={() => { }}>
                            <div className="card-body">
                              <div className="formcard--cabin">
                                <h4 className="small--title mb-3">
                                  Guest {indexdown + 1}: <span className="text-capitalize">{item.type.toLowerCase()}</span>
                                </h4>
                                <div className="">
                                  <form className="">
                                    <div className="form-group form-row mb-0">
                                      <div className="col-12 col-md-6 mb-3">
                                        <input
                                          type="text"
                                          placeholder="First Name"
                                          className="form-control guest-input"
                                          value={
                                            guests[indexup] ? guests[indexup]['guests'][indexdown]['name'] : ''
                                          }
                                          onChange={(e) => setDataInState('name', indexup, indexdown, e)}
                                        />
                                        <span style={{ color: "red" }}>{errors[indexup] ? errors[indexup]['guests'][indexdown]['name'] : ''}</span>
                                      </div>
                                      <div className="col-12 col-md-6 mb-3">
                                        <input
                                          type="text"
                                          placeholder="Last Name"
                                          className="form-control guest-input"
                                          value={
                                            guests[indexup] ? guests[indexup]['guests'][indexdown]['lastname'] : ''
                                          }
                                          onChange={(e) => setDataInState('lastname', indexup, indexdown, e)}
                                        />
                                        <span style={{ color: "red" }}>{errors[indexup] ? errors[indexup]['guests'][indexdown]['lastname'] : ""}</span>
                                      </div>
                                      <p className="notice ml-2">Name should as per the passport</p>
                                    </div>
                                    <div className="form-group form-row">
                                      <div className="col-12">
                                        <select
                                          className="form-control guest-input"
                                          value={guests[indexup] ? guests[indexup]['guests'][indexdown]['gender'] : ''}
                                          onChange={(e) => setDataInState('gender', indexup, indexdown, e)}
                                        >
                                          <option>Select your Gender</option>
                                          <option>Male</option>
                                          <option>Female</option>
                                        </select>
                                        <span style={{ color: "red" }}>{errors[indexup] ? errors[indexup]['guests'][indexdown]['gender'] : ""}</span>
                                      </div>
                                    </div>
                                    <div className="form-group form-row">
                                      <div className="col-12">

                                        {/* <DatePicker className="form-control guest-input" dateFormat="yyyy/MM/dd" dateFormat="dd/mm/yyyy" selected={guests[indexup] ? guests[indexup]['guests'][indexdown]['dateOfBirth'] : ''} onChange={(e) => setDataInState('dateOfBirth', indexup, indexdown, e)} /> */}

                                        {/* <input
                                          type="date"
                                          onKeyDown={(e) => e.preventDefault()}
                                          placeholder=""
                                          className="form-control guest-input guest-input-date"
                                          value={
                                            guests[indexup] ? guests[indexup]['guests'][indexdown]['dateOfBirth'] : ''
                                          }
                                          onChange={(e) => setDataInState('dateOfBirth', indexup, indexdown, e)}
                                        /> */}

                                        {/* <input
                                          type="date"
                                          placeholder=""
                                          className="form-control guest-input guest-input-date"
                                          value={
                                            guests[indexup] ? guests[indexup]['guests'][indexdown]['dateOfBirth'] : ''
                                          }
                                          onChange={(e) => setDataInState('dateOfBirth', indexup, indexdown, e)}
                                        /> */}

                                        <DropdownDate className="form-control guest-input guest-input-date" selectedDate={guests[indexup] ? guests[indexup]['guests'][indexdown]['dateOfBirth'] : '1999-04-06'} onDateChange={(date) => { console.log("Noxy", date); setDataInState('dateOfBirth', indexup, indexdown, { target: { value: formatDate(date) } }) }} />
                                        <span style={{ color: "red" }}>{errors[indexup] ? errors[indexup]['guests'][indexdown]['dateOfBirth'] : ""}</span>
                                      </div>
                                    </div>
                                    <div className="form-group form-row">
                                      <div className="col-12">
                                        <div className="d-flex number--txt form-control p-0">
                                          {/* <input
                                            type="text"
                                            placeholder="+91"
                                            className="form-control-num numcontrol--v guest-input"
                                            value={
                                              guests[indexup] ? guests[indexup]['guests'][indexdown]['countryCode'] : ''
                                            }
                                            onChange={(e) => setDataInState('countryCode', indexup, indexdown, e)}
                                          /> */}
                                          <select
                                            className="form-control-num numcontrol--v guest-input"
                                            onChange={(e) => setDataInState('countryCode', indexup, indexdown, e)}
                                            value={"+91"}
                                          >
                                            {CounrtyCodes.map(code => {
                                              return (<option key={code} value={code}>{code}</option>)
                                            })}
                                          </select>
                                          <input
                                            type="text"
                                            placeholder="10 Digit Mobile Number"
                                            className="form-control-num guest-input"
                                            value={
                                              guests[indexup] ? guests[indexup]['guests'][indexdown]['phoneNumber'] : ''
                                            }
                                            onChange={(e) => setDataInState('phoneNumber', indexup, indexdown, e)}
                                          />
                                        </div>
                                        <span style={{ color: "red" }}>{errors[indexup] ? errors[indexup]['guests'][indexdown]['phoneNumber'] : ""}</span>
                                      </div>
                                    </div>
                                    <div className="form-group form-row">
                                      <div className="col-12">
                                        <input
                                          type="email"
                                          placeholder="Email Address"
                                          className="form-control guest-input"
                                          value={guests[indexup] ? guests[indexup]['guests'][indexdown]['email'] : ''}
                                          onChange={(e) => setDataInState('email', indexup, indexdown, e)}
                                        />
                                        <span style={{ color: "red" }}>{errors[indexup] ? errors[indexup]['guests'][indexdown]['email'] : ""}</span>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </Card>
                        </div>
                      );
                    })
                  );
                })}
              </div>
            </div>
            {
              queryParams.get("from") ?
                queryParams.get("from") == "booking-success" ? <div className="row my-5">
                  <div className="col-12 ">
                    <div className="choose-addon text-center d-flex justify-content-center">
                      <button className="btn btn-continue-btn m-1" onClick={() => { if (validation()) { guestselection() } }}>
                        Complete
                      </button>
                    </div>
                  </div>
                </div> :
                  <div className="row my-5">
                    <div className="col-12 ">
                      <div className="choose-addon text-center d-flex justify-content-center">
                        <button className="btn btn-choose-btn m-1" onClick={() => { if (validation()) { addOnSelection() } }}>
                          Choose Add-On
                        </button>
                        <button className="btn btn-continue-btn m-1" onClick={() => { if (validation()) { guestselection() } }}>
                          Proceed To Pay
                        </button>
                      </div>
                    </div>
                  </div> :
                <div className="row my-5">
                  <div className="col-12 ">
                    <div className="choose-addon text-center d-flex justify-content-center">
                      <button className="btn btn-choose-btn m-1" onClick={() => { if (validation()) { addOnSelection() } }}>
                        Choose Add-On
                      </button>
                      <button className="btn btn-continue-btn m-1" onClick={() => { if (validation()) { guestselection() } }}>
                        Proceed To Pay
                      </button>
                    </div>
                  </div>
                </div>
            }
          </div>
        </div>
        {/* <Footer /> */}
        {
          (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
        }
      </div>
    </div>
  );
}

export default RoomSelection;
