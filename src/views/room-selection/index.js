import { useState, useEffect } from "react"
import Header from "../../components/Header/HeaderHome";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container";
import SelectedCabinList from "../cabin-selection/SelectedCabinList";
import { getCabins } from "../../redux/actions/cabinAction";
import { useDispatch, useSelector } from "react-redux";
// import Heading from "../../components/Room-Selection/Heading";
// import Availablevisible from "../../components/Room-Selection/Availablevisible";
// import Pagecontent from "../../components/Room-Selection/Pagecontent";

// Bootstrap Designs
import Heading from "../../components/Room-Selection-Boot/Heading"
import Availablevisible from "../../components/Room-Selection-Boot/Availablevisible"
import Pagecontent from "../../components/Room-Selection-Boot/Pagecontent"
import "./index.scss"

function RoomSelection() {
	const dispatch = useDispatch();
	const [selectedCabin, setSelectedCabin] = useState('')
	const queryParams = new URLSearchParams(window.location.search)
	const [focusedCabinId, setFocusCabinId] = useState("");


	// const { cabins } = useSelector((state) => ({
	// 	cabins: state.allcabins,
	// }));

	// useEffect(async () => {
	// 	let data = await dispatch(getCabins());
	// }, []);

	// useEffect(async () => {
	// 	if(selectedCabin != '')
	// 	console.log('Dta',selectedCabin);
	// 	if(selectedCabin == '')
	// 	setSelectedCabin(cabins[0])
	// }, [cabins]);

	useEffect(async () => {
		window.scrollTo(0, 0);
	}, []);

	const getDefaultSelectedCabin = (cabin) => {
		setSelectedCabin(cabin)
	}

	const onSelectCabinHandler = (cabin) => {
		setSelectedCabin(cabin)
	};

	return (
		<div className="Home">
			<Header bgPurple platform={queryParams.get("platform")} />
			<Container>
				<div className="room-selection-container">
					<BookingFlowCards.ProgressBar current={1} />
				</div>
				{true ?
					<>
						<Container strict={true}>
							<SelectedCabinList
								getSelectedCabin={getDefaultSelectedCabin}
								isCabinPage={false}
								deckNumbers={["03", "07"]}
								className="selectedCabinMB"
								onSelect={onSelectCabinHandler}
								fromSelectionPage={true}
								setFocusCabinId={setFocusCabinId}
							/>
						</Container>
					</> : null}

				<Container>
					<Heading selectedCabin={selectedCabin} />
				</Container>
				<Availablevisible />
				{selectedCabin.detail ?
					selectedCabin.detail.priceKey ?
						<Pagecontent selectedCabin={selectedCabin} queryParams={queryParams} platform={queryParams.get("platform")} />
						: null
					: null}

			</Container>
			{/* <Footer /> */}
			{
				(queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
			}
		</div>
	);
}

export default RoomSelection;
