import React, { useEffect, useState } from "react"
import Banner from "../../assets/img/healthy-waves/healthy-waves.png"
import BannerMobile from "../../assets/img/healthy-waves/healthy-waves-mobile-banner.png"
import Header from "../../components/Header/HeaderHome"
import Footer from "../../components/Footer/Footer"
import cx from "classnames"
import classes from "./healthy-waves.module.css"
import Badge from "../../assets/img/healthy-waves/safety-bookmark.png"
import { trustBadgesHealthyWaves } from "../home/feed"
import * as Section from "../../components/Section"
import Container from '../../components/Container'
import ReactPlayer from "react-player";
import Line from "../../assets/img/healthy-waves/line-healthy.png"
import HealthyCheck from "../../assets/img/healthy-waves/healthy-check.png"



const HealthyWaves = () => {

    const queryParams = new URLSearchParams(window.location.search)

    const [isMobile, setMobile] = React.useState(false);



    React.useEffect(() => {
        _isMobileScreen();
        window.addEventListener('resize', _isMobileScreen);
        return () => {
            window.removeEventListener('resize', _isMobileScreen);
        };
    }, []);


    const _isMobileScreen = () => {
        console.log("is Mobille", isMobile)
        console.log("window size", window.innerWidth)
        // if (window.innerWidth <= 768) {
        //     setMobile(true);
        //     return;
        // }

        if (window.innerWidth <= 814) {
            setMobile(true);
            return;
        }
        setMobile(false);
    };

    return (
        <div className={cx(classes.healthyWave)}>
            <Header bgPurple platform={queryParams.get("platform")} />
            <div className={cx(classes.mainBannerDiv)}>
                <img src={Banner} className={cx(classes.mainBannerImg)} />
                <div className={cx(classes.bannerContent)}>
                    <h1 className={cx(classes.bannerContentHeading)}>HEALTHY WAVES</h1>
                    <h5 className={cx(classes.bannerContentSubHeading)}>CRUISE WITH CONFIDENCE</h5>
                </div>
            </div>

            <div className={cx(classes.mainBannerDivMobile)}>
                <img src={BannerMobile} className={cx(classes.mainBannerImgMobile)} />
                <div className={cx(classes.bannerContentMobile)}>
                    <h1 className={cx(classes.bannerContentHeadingMobile)}>HEALTHY WAVES</h1>
                    <h5 className={cx(classes.bannerContentSubHeadingMobile)}>CRUISE WITH CONFIDENCE</h5>
                </div>
            </div>

            <div className={cx(classes.saftyAssuranceSection)}>
                <h1 className={cx(classes.saftyAssuranceHeading)}>YOUR SAFETY IS <b>OUR PRIORITY</b></h1>
                <p className={cx(classes.saftyAssuranceParagraph)}>Given that safety is paramount and a non-negotiable factor especially in today’s times, Cordelia has implemented all the standard protocols for its crew members and guests. Health protocols designed to make your seacation safe and memorable!</p>
                {/* <div className={cx(classes.safetyBadges)}>
                    <div className={cx(classes.badge)}>
                        <img className={cx(classes.badgeImage)} src={Badge} />
                        <p className={cx(classes.badgeTitle)}>100% Fresh, Filtered Air</p>
                        <p className={cx(classes.badgeDescription)}>Our HVAC system continuously intakes 100% fresh ocean air, filtering it for supply to all indoor rooms and areas.</p>
                    </div>
                </div> */}
                <Container>
                    <Section.TrustBadge isMobile={isMobile} trustBadges={trustBadgesHealthyWaves} />
                </Container>

                <div className={cx(classes.youtubeVideoSection)}>
                    <div className="video-container">
                        <ReactPlayer
                            width={'1200px'}
                            className="react-player"
                            controls={true}
                            playing={true}
                            light={Banner}
                            url="https://www.youtube.com/watch?v=8CE0BNv8dWw"
                        />
                    </div>
                </div>

                <div className={cx(classes.protocolSection)}>
                    <div className={cx(classes.protocolSectionLineDiv)}>
                        <img className={cx(classes.protocolSectionLine)} src={Line} width="2px" />
                    </div>
                    <div className={cx(classes.protocolSectionLeft)}>
                        <img className={cx(classes.protocolSectionLeftIcon)} src={HealthyCheck} />
                        <div className={cx(classes.mobileCheckImageContainer)}>
                            <img src={HealthyCheck} className={cx(classes.mobileCheckImage)} />
                        </div>
                        <h4 className="font-weight-bold">PRE-EMBARKATION</h4>
                        <p>Guests without a negative RTPCR test report not prior to 48 hours, will be denied boarding and consider as a no-show.</p>
                        <p>Negative RTPCR test reports should be with a valid bar code, generated by ICMR, will be accepted.</p>
                        <p>Children below the age of 12 years do not require an RTPCR test report.</p>
                        <p>Guest who have completed their vaccination will be required to carry a copy of their vaccination certificate.</p>
                    </div>

                    <div className={cx(classes.protocolSectionRight)}>
                        <img className={cx(classes.protocolSectionRightIcon)} src={HealthyCheck} />
                        <div className={cx(classes.mobileCheckImageContainer)}>
                            <img src={HealthyCheck} className={cx(classes.mobileCheckImage)} />
                        </div>
                        <h4 className="font-weight-bold">ONLINE CHECK-IN</h4>
                        <p>Guests without a negative RTPCR test report not prior to 48 hours, will be denied boarding and consider as a no-show.</p>
                        <p>Negative RTPCR test reports should be with a valid bar code, generated by ICMR, will be accepted.</p>
                        <p>Children below the age of 12 years do not require an RTPCR test report.</p>
                        <p>Guest who have completed their vaccination will be required to carry a copy of their vaccination certificate.</p>
                    </div>

                    <div className={cx(classes.protocolSectionLeft)}>
                        <img className={cx(classes.protocolSectionLeftIcon)} src={HealthyCheck} />
                        <div className={cx(classes.mobileCheckImageContainer)}>
                            <img src={HealthyCheck} className={cx(classes.mobileCheckImage)} />
                        </div>
                        <h4 className="font-weight-bold">ARRIVAL AT THE PORT GATE</h4>
                        <p>Guests without a negative RTPCR test report not prior to 48 hours, will be denied boarding and consider as a no-show.</p>
                        <p>Negative RTPCR test reports should be with a valid bar code, generated by ICMR, will be accepted.</p>
                        <p>Children below the age of 12 years do not require an RTPCR test report.</p>
                        <p>Guest who have completed their vaccination will be required to carry a copy of their vaccination certificate.</p>
                    </div>

                    <div className={cx(classes.protocolSectionRight)}>
                        <img className={cx(classes.protocolSectionRightIcon)} src={HealthyCheck} />
                        <div className={cx(classes.mobileCheckImageContainer)}>
                            <img src={HealthyCheck} className={cx(classes.mobileCheckImage)} />
                        </div>
                        <h4 className="font-weight-bold">TRANSFERS FROM THE GATE TO THE TERMINAL</h4>
                        <p>Guests without a negative RTPCR test report not prior to 48 hours, will be denied boarding and consider as a no-show.</p>
                        <p>Negative RTPCR test reports should be with a valid bar code, generated by ICMR, will be accepted.</p>
                        <p>Children below the age of 12 years do not require an RTPCR test report.</p>
                        <p>Guest who have completed their vaccination will be required to carry a copy of their vaccination certificate.</p>
                    </div>

                    <div className={cx(classes.protocolSectionLeft)}>
                        <img className={cx(classes.protocolSectionLeftIcon)} src={HealthyCheck} />
                        <div className={cx(classes.mobileCheckImageContainer)}>
                            <img src={HealthyCheck} className={cx(classes.mobileCheckImage)} />
                        </div>
                        <h4 className="font-weight-bold">CRUISE TERMINAL</h4>
                        <p>Guests without a negative RTPCR test report not prior to 48 hours, will be denied boarding and consider as a no-show.</p>
                        <p>Negative RTPCR test reports should be with a valid bar code, generated by ICMR, will be accepted.</p>
                        <p>Children below the age of 12 years do not require an RTPCR test report.</p>
                        <p>Guest who have completed their vaccination will be required to carry a copy of their vaccination certificate.</p>
                    </div>

                    <div className={cx(classes.protocolSectionRight)}>
                        <img className={cx(classes.protocolSectionRightIcon)} src={HealthyCheck} />
                        <div className={cx(classes.mobileCheckImageContainer)}>
                            <img src={HealthyCheck} className={cx(classes.mobileCheckImage)} />
                        </div>
                        <h4 className="font-weight-bold">EMBARKATION</h4>
                        <p>Guests without a negative RTPCR test report not prior to 48 hours, will be denied boarding and consider as a no-show.</p>
                        <p>Negative RTPCR test reports should be with a valid bar code, generated by ICMR, will be accepted.</p>
                        <p>Children below the age of 12 years do not require an RTPCR test report.</p>
                        <p>Guest who have completed their vaccination will be required to carry a copy of their vaccination certificate.</p>
                    </div>
                </div>

                <div className={cx(classes.downloadProtocolSection)}>
                    <i className="fa fa-download" style={{ color: "#ea725b", fontSize: "15px" }}></i>
                    <h6 className={cx(classes.downloadProtocolText, "ml-3")}>Download Safety Protocols</h6>
                </div>

            </div>
        </div>
    )
}

export default HealthyWaves
