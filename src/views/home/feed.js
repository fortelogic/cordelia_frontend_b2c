import CruiseLandPackage1 from '../../assets/img/home-page/cruise-land-packages-1.png';
import Hotel from '../../assets/img/home-page/tag-hotel.png';
import Activites from '../../assets/img/home-page/tag-activities.png';
import Transfer from '../../assets/img/home-page/tag-car.png';
import CruiseLandPackage2 from '../../assets/img/home-page/cruise-land-packages-2.png';
import CruiseLandPackage3 from '../../assets/img/home-page/cruise-land-packages-3.png';
import OcassionWedding from '../../assets/img/home-page/seacation-for-every-occasion-wedding.png';
import OcassionOffsite from '../../assets/img/home-page/seacation-for-every-occasion-offsites-and-events.png';
import OcassionWeekend from '../../assets/img/home-page/seacation-for-every-occasion-weekend-cruises.png';
import SafeSail from '../../assets/img/home-page/safe-sail.png';
import SafeSailMobile from '../../assets/img/home-page/safe-sail_mobile.png';
import PremiumCruise from '../../assets/img/home-page/premium-cruise.png';
import PremiumCruiseMobile from '../../assets/img/home-page/premium-cruise_mobile.png';
import BestOffers from '../../assets/img/home-page/best-offers.png';
import BestOffersMobile from '../../assets/img/home-page/best-offers_mobile.png';
import CruiseInspirationDiu from '../../assets/img/home-page/1-cruise-inspirations-diu.png';
import CruiseInspirationKochi from '../../assets/img/home-page/2-cruise-inspirations-kochi.png';
import CruiseInspirationGoa from '../../assets/img/home-page/3-cruise-inspirations-goa.png';
import Testimonial1 from '../../assets/img/home-page/testimony-1.png';
import Testimonial2 from '../../assets/img/home-page/testimony-2.png';
import Testimonial3 from '../../assets/img/home-page/testimony-3.png';
import CordeliaAppstore from '../../assets/img/home-page/cordelia-appstore.png';
import CordeliaScreenshot from '../../assets/img/home-page/cordelia-screenshot.png';
import CordeliaScreenshotMobile from '../../assets/img/home-page/cordelia-screenshot_mobile.png';
import CordeliaPlaystore from '../../assets/img/home-page/cordelia-playstore.png';
import CordeliaQr from '../../assets/img/home-page/cordelia-qrcode.png';
import bg from '../../assets/img/home-page/cordelia-cruises-destination.png';
import kochibg from '../../assets/img/home-page/kochi-destination.png';
import mobilebg from '../../assets/img/home-page/cordelia-cruises-destination_mobile.png';
import web_src_one from '../../assets/img/home-page/cordelia-cruise-overlay-card-1.png';
import web_src_two from '../../assets/img/home-page/cordelia-cruise-overlay-card-1.png';
import web_src_three from '../../assets/img/home-page/cordelia-cruise-overlay-card-1.png';
import mobile_src_one from '../../assets/img/cordelia-cruise-overlay-card-1_mobile.png';
import mobile_src_two from '../../assets/img/cordelia-cruise-overlay-card-2_mobile.png';
import mobile_src_three from '../../assets/img/cordelia-cruise-overlay-card-3_mobile.png';
import WowOne from "../../assets/img/offer-section/wow-1.png"
import WowThree from "../../assets/img/offer-section/wow-3.png"
import WowFour from "../../assets/img/offer-section/wow-4.png"
import PillarOne from "../../assets/img/home-page/fourpillars/pillar-one.png"
import PillarTwo from "../../assets/img/home-page/fourpillars/pillar-two.png"
import PillarThree from "../../assets/img/home-page/fourpillars/pillar-three.png"
import PillarFour from "../../assets/img/home-page/fourpillars/pillar-four.png"
import BsOne from "../../assets/img/iternary-1.jpg"
import BsTwo from "../../assets/img/iternary-2.jpg"
import BsThree from "../../assets/img/iternary-3.jpg"
import OceanView from "../../assets/img/ocean_view_card.webp"
import Suite from "../../assets/img/suite-web.webp"
import Balcony from "../../assets/img/balcony_card.webp"
import WeekendCruise from "../../assets/img/home-page/seacation/seacation-one.png"


import OneBestSellingCruise from '../../assets/img/home-page/best-selling-cruises-1.png';
import TwoBestSellingCruise from "../../assets/img/home-page/bestsellingcruises/orig_shutterstock_594407522 1.png"
import ThreeBestSellingCruise from "../../assets/img/home-page/bestsellingcruises/shutterstock_399212785 - Copy 1.png"


export const cruiseLandPackages = {
  heading: 'CRUISE + LAND <strong>PACKAGES</strong>',
  items: [
    {
      link: {
        arrow: true,
        target: '_blank',
        text: 'Explore More',
        url: 'https://cordelia.fortelogic.in/cruise-deals/cruise-land-vacation',
      },
      price: '₹25,025',
      subTitle: '2N Mumbai-Kochi Cruise + 3N Kerala Land Holidays',
      src: CruiseLandPackage1,
      tags: [
        { name: 'Hotel', src: Hotel },
        { name: 'Activities', src: Activites },
        { name: 'Transfer', src: Transfer },
      ],
      title: '8 NIGHT FABULOUS KERALA',
    },
    {
      link: {
        arrow: true,
        target: '_blank',
        text: 'Explore More',
        url: 'https://cordelia.fortelogic.in/cruise-deals/cruise-land-vacation',
      },
      price: '₹25,025',
      subTitle: '2N Mumbai-Kochi Cruise + 3N Kerala Land Holidays',
      src: CruiseLandPackage2,
      tags: [
        { name: 'Hotel', src: Hotel },
        { name: 'Activities', src: Activites },
        { name: 'Transfer', src: Transfer },
      ],
      title: '8 NIGHT FABULOUS KERALA',
    },
    {
      link: {
        arrow: true,
        target: '_blank',
        text: 'Explore More',
        url: 'https://cordelia.fortelogic.in/cruise-deals/cruise-land-vacation',
      },
      price: '₹25,025',
      subTitle: '2N Mumbai-Kochi Cruise + 3N Kerala Land Holidays',
      src: CruiseLandPackage3,
      tags: [
        { name: 'Hotel', src: Hotel },
        { name: 'Activities', src: Activites },
        { name: 'Transfer', src: Transfer },
      ],
      title: '8 NIGHT FABULOUS KERALA',
    },
    {
      link: {
        arrow: true,
        target: '_blank',
        text: 'Explore More',
        url: 'https://cordelia.fortelogic.in/cruise-deals/cruise-land-vacation',
      },
      price: '₹25,025',
      subTitle: '2N Mumbai-Kochi Cruise + 3N Kerala Land Holidays',
      src: CruiseLandPackage3,
      tags: [
        { name: 'Hotel', src: Hotel },
        { name: 'Activities', src: Activites },
        { name: 'Transfer', src: Transfer },
      ],
      title: '8 NIGHT FABULOUS KERALA',
    },
  ],
  viewAllLink: 'javascript:void(0)', // temporary link
};

export const bestSellingCruises = {
  heading: 'BEST <strong>SELLING CRUISES</strong>',
  description: `<p>Plan your much-awaited getaway with our fabulous deals on cruise holidays. From special launch fares to flexibility in refunds and cancellations, our offers and services ensure that you cruise with us confidently. Whether it’s a family cruise holiday to Lakshadweep or Sri Lanka, or getaway with friends to Goa, there’s a deal for every occasion.</p>`,
  Title: 'BEST  <strong>SELLING CRUISES</strong>',
  link: {
    arrow: false,
    target: '',
    text: 'Explore Itinerary',
    url: '/cruise-routes',
  },
  shortDescription:
    'Plan your much-awaited getaway with our fabulous deals on cruise holidays. From special launch fares to flexibility in refunds and cancellations, our offers and services ensure that you cruise with us confidently.',
  items: [
    {
      description: 'Cruise to Lakshadweep from Mumbai for 5 nights to experience the best of cruise and land holidays. ',
      link: {
        arrow: true,
        target: '_blank',
        text: 'Explore Itinerary',
        url: '/cruise-routes',
      },
      subTitle: 'Mumbai - Kochi - Lakshadweep - Mumbai',
      src: BsOne,
      title: '5 NIGHTS LAKSHADWEEP',
    },
    {
      description: 'Cruise bookings have never been this easy. Just select your itinerary, date, and cruise to Lakshadweep.',
      link: {
        arrow: true,
        target: '_blank',
        text: 'Explore Itinerary',
        url: '/cruise-routes',
      },
      subTitle: 'Mumbai  - Lakshadweep - Goa - Mumbai',
      src: BsTwo,
      title: '5 NIGHTS LAKSHADWEEP',
    },
    {
      description: (<>Experience the best of Kerala and Lakshadweep on this cruise.<br/><br/> </>),
      link: {
        arrow: true,
        target: '_blank',
        text: 'Explore Itinerary',
        url: '/cruise-routes',
      },
      subTitle: 'Cochin & Mumbai cruise',
      src: BsThree,
      title: '3 NIGHTS LAKSHADWEEP',
    },
  ],
};

export const cardOverlay = {
  items: [
    {
      title: 'Healthy Waves',
      description: 'Enjoy safe holidays with healthy waves',
      image: {
        mobile: mobile_src_one,
        web: web_src_one,
      },
      target: '_blank',
      url: 'https://cordelia.fortelogic.in/cruise-deals',
    },
    {
      title: 'Kids sail free',
      description: 'Enjoy safe holidays with healthy waves',
      image: {
        mobile: mobile_src_two,
        web: web_src_two,
      },
      target: '_blank',
      url: 'https://cordelia.fortelogic.in/cruise-deals',
    },
    {
      title: 'book by paying ₹500',
      description: 'Enjoy safe holidays with healthy waves',
      image: {
        mobile: mobile_src_three,
        web: web_src_three,
      },
      target: '_blank',
      url: 'https://cordelia.fortelogic.in/cruise-deals',
    },
  ],
};

export const occasionEvents = {
  heading: 'SEACATION FOR <strong>EVERY OCCASION</strong>',
  items: [
    {
      description:
        'Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc. Ipsum praesent tempor est ligula et leo. Nulla natoque tempus sem ultrices egestas nam suscipit pellentesque.',
      btnText: 'Explore More',
      image: {
        mobile: OcassionWedding,
        web: OcassionWedding,
        position: 'left',
      },
      src: OcassionWedding,
      title: 'Wedding VOWS with the ocean VIEWS',
      target: '_blank',
      url: 'https://cordelia.fortelogic.in/groups-and-events/wedding-cruises',
    },
    {
      description:
        'Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc. Ipsum praesent tempor est ligula et leo. Nulla natoque tempus sem ultrices egestas nam suscipit pellentesque.',
      btnText: 'Explore More',
      image: {
        mobile: OcassionOffsite,
        web: OcassionOffsite,
        position: 'right',
      },
      src: OcassionOffsite,
      title: 'Offsites and Events',
      target: '_blank',
      url: 'https://cordelia.fortelogic.in/groups-and-events/corporate-cruises',
    },
    {
      description:
        'Vitae, eget sit integer tincidunt. Risus dignissim elementum, bibendum est nunc, vel sem vitae, nunc. Ipsum praesent tempor est ligula et leo. Nulla natoque tempus sem ultrices egestas nam suscipit pellentesque.',
      btnText: 'Explore More',
      image: {
        mobile: OcassionWeekend,
        web: OcassionWeekend,
        position: 'left',
      },
      src: OcassionWeekend,
      title: 'Weekend Cruises',
      target: '_blank',
      url: 'https://cordelia.fortelogic.in/weekend-cruises',
    },
  ],
};

export const OurDestination = {
  title: 'Our <strong>Destination</strong>',
  image: {
    mobile: mobilebg,
    web: bg,
  },
  tags: [
    'Diu',
    'Mumbai',
    'Goa',
    'Cochin',
    'Chennai',
    'Lakshadweep',
    'Male',
    'Colombo',
    'Trincomalee',
    'Jaffna',
    'Galle',
  ],
  tagImages: [bg, kochibg, bg, kochibg, bg, kochibg, bg, kochibg, bg, kochibg],
  newTags: [
    { title: 'Diu', image: { web: bg, mobile: mobilebg } },
    { title: 'Goa', image: { web: bg, mobile: mobilebg } },
    { title: 'Cochin', image: { web: bg, mobile: mobilebg } },
    { title: 'Chennai', image: { web: bg, mobile: mobilebg } },
    { title: 'Lakshadweep', image: { web: bg, mobile: mobilebg } },
    { title: 'Male', image: { web: bg, mobile: mobilebg } },
    { title: 'Colombo', image: { web: bg, mobile: mobilebg } },
    { title: 'Trincomalee', image: { web: bg, mobile: mobilebg } },
    { title: 'Galle', image: { web: bg, mobile: mobilebg } },
  ],
  btnText: 'Explore All',
  url: 'https://cordelia.fortelogic.in/cruise-destinations',
};

export const trustBadges = [
  {
    mobileSrc: SafeSailMobile,
    src: SafeSail,
    title: '100% Fresh, Filtered Air',
    description:
      'Our HVAC system continuously intakes 100% fresh ocean air, filtering it for supply to all indoor rooms and areas.',
  },
  {
    mobileSrc: PremiumCruiseMobile,
    src: PremiumCruise,
    title: 'Face Mask & Social Distancing',
    description:
      'All Guest must wear a face mask or a protective face cover & maintain social distancing to keep yourself & others safe.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Sanitization',
    description:
      'The entire ship is sanitized and disinfected thoroughly prior to sailing, and during regular intervals daily.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Elevated Health Protocols',
    description: 'More doctors and nurses, upgraded facilities, equipment, and sound care plans.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Negative RTPCR Test',
    description: 'A negative RTCPR test report of not prior to 48 hours to be submitted during embarkation.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Mandatory Web Check - in',
    description: 'It is mandatory for all our guests to check-in online. This helps to obtain boarding pass and checkin at port.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Booking for Shore Excursion',
    description: 'Guests pre-registered for shore excursions to be allowed to disembark at port of calls.',
  },  
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Deboarding Protocol',
    description: 'Unvaccinated or single vaccinated guests to submit a negative RTPCR test report on arrival at the port or undergo the test.',
  },
];

export const trustBadgesHealthyWaves = [
  {
    mobileSrc: SafeSailMobile,
    src: SafeSail,
    title: '100% Fresh, Filtered Air',
    description:
      'Our HVAC system continuously intakes 100% fresh ocean air, filtering it for supply to all indoor rooms and areas.',
  },
  {
    mobileSrc: PremiumCruiseMobile,
    src: PremiumCruise,
    title: 'Face Mask & Social Distancing',
    description:
      'All Guest must wear a face mask or a protective face cover & maintain social distancing to keep yourself & others safe.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Sanitization',
    description:
      'The entire ship is sanitized and disinfected thoroughly prior to sailing, and during regular intervals daily.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Elevated Health Protocols',
    description: 'More doctors and nurses, upgraded facilities, equipment, and sound care plans.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Negative RTPCR Test',
    description: 'A negative RTCPR test report of not prior to 48 hours to be submitted during embarkation.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Mandatory Web Check - in',
    description: 'It is mandatory for all our guests to check-in online. This helps to obtain boarding pass and checkin at port.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Booking for Shore Excursion',
    description: 'Guests pre-registered for shore excursions to be allowed to disembark at port of calls.',
  },
  {
    mobileSrc: BestOffersMobile,
    src: BestOffers,
    title: 'Deboarding Protocol',
    description: 'Unvaccinated or single vaccinated guests to submit a negative RTPCR test report on arrival at the port or undergo the test.',
  },
];

export const cruiseInspirations = {
  heading: 'CRUISE <strong>INSPIRATIONS</strong>',
  items: [
    {
      author: 'Admin',
      date: '03-Mar-2021',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.',
      link: {
        target: '_blank',
        text: 'Read More',
        url: 'https://cordelia.fortelogic.in/cruise-destinations/cruise-to-diu',
      },
      src: CruiseInspirationDiu,
      title: 'PLACE TO EXPLORE IN DIU',
    },
    {
      author: 'Admin',
      date: '03-Mar-2021',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.',
      link: {
        target: '_blank',
        text: 'Read More',
        url: 'https://cordelia.fortelogic.in/cruise-destinations/cruise-to-kochi',
      },
      src: CruiseInspirationKochi,
      title: 'PLACE TO EXPLORE IN KOCHI',
    },
    {
      author: 'Admin',
      date: '03-Mar-2021',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.',
      link: {
        target: '_blank',
        text: 'Read More',
        url: 'https://cordelia.fortelogic.in/cruise-destinations/cruise-to-goa',
      },
      src: CruiseInspirationGoa,
      title: 'PLACE TO EXPLORE IN GOA',
    },
    {
      author: 'Admin',
      date: '03-Mar-2021',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.',
      link: {
        target: '_blank',
        text: 'Read More',
        url: 'https://cordelia.fortelogic.in/cruise-destinations/cruise-to-goa',
      },
      src: CruiseInspirationGoa,
      title: 'PLACE TO EXPLORE IN GOA',
    },
  ],
};

export const testimonials = {
  heading: '<strong>TESTIMONIALS</strong>',
  items: [
    {
      author: 'Jane Cooper',
      avatar: Testimonial1,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In habitant faucibus nibh aliquam mi amet dui maecenas tellus. Aliquam sed sit et orci aliquet. Sollicitudin donec et',
      location: 'Mumbai, India',
    },
    {
      author: 'Darrell Steward',
      avatar: Testimonial2,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In habitant faucibus nibh aliquam mi amet dui maecenas tellus. Aliquam sed sit et orci aliquet. Sollicitudin donec et',
      location: 'Gujarat, India',
    },
    {
      author: 'Jane Cooper',
      avatar: Testimonial3,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In habitant faucibus nibh aliquam mi amet dui maecenas tellus. Aliquam sed sit et orci aliquet. Sollicitudin donec et',
      location: 'Mumbai, India',
    },
    {
      author: 'Jane Cooper',
      avatar: Testimonial1,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In habitant faucibus nibh aliquam mi amet dui maecenas tellus. Aliquam sed sit et orci aliquet. Sollicitudin donec et',
      location: 'Mumbai, India',
    },
    {
      author: 'Jane Cooper',
      avatar: Testimonial2,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In habitant faucibus nibh aliquam mi amet dui maecenas tellus. Aliquam sed sit et orci aliquet. Sollicitudin donec et',
      location: 'Mumbai, India',
    },
    {
      author: 'Jane Cooper',
      avatar: Testimonial3,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In habitant faucibus nibh aliquam mi amet dui maecenas tellus. Aliquam sed sit et orci aliquet. Sollicitudin donec et',
      location: 'Mumbai, India',
    },
    {
      author: 'Jane Cooper',
      avatar: Testimonial1,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In habitant faucibus nibh aliquam mi amet dui maecenas tellus. Aliquam sed sit et orci aliquet. Sollicitudin donec et',
      location: 'Mumbai, India',
    },
    {
      author: 'Jane Cooper',
      avatar: Testimonial2,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In habitant faucibus nibh aliquam mi amet dui maecenas tellus. Aliquam sed sit et orci aliquet. Sollicitudin donec et',
      location: 'Mumbai, India',
    },
  ],
};

export const mobileApps = {
  appStore: CordeliaAppstore,
  description:
    'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
  heading: 'India’s first cruise liner booking at your fingertips. Download our App Now!!!',
  mobileheading: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  mobileDesc: 'You can now book faster on your fingertips',
  mobileSrc: CordeliaScreenshotMobile,
  playStore: CordeliaPlaystore,
  src: CordeliaScreenshot,
  qrCode: CordeliaQr,
};

export const wowOffers = {
  heading: 'WOW <strong>OFFERS</strong>',
  items: [
    {
      title: "FREE STATEROOM",
      offer: "UPGRADE**",
      description: (
        <>
          <div className="mt-8">
            <p className="text-white text-base">
              Book with just 25% now, and pay the rest 30 days prior to your sailing date.
            </p>
          </div>
        </>
      ),
      // image: `images/goair/offer1.png`,
      // image: require('../../assets/img/offer-section/wow-1.png'),
      image: OceanView,
    },
    {
      title: "BOOK NOW",
      offer: "PAY LATER**",
      description: (
        <>
          <div className="mt-8">
            <p className="text-white text-base">
              Book with just 25% now, and pay the rest 30 days prior to your sailing date.
            </p>
          </div>
        </>
      ),
      // image: `images/goair/offer2.png`,
      // image: require('../../assets/img/offer-section/wow-2.png'),
      image: Balcony,
    },
    {
      title: "KIDS SAIL",
      offer: "FREE*",
      description: (
        <>
          <div className="mt-8">
            <p className="text-white text-base">
              Book with just 25% now, and pay the rest 30 days prior to your sailing date.
            </p>
          </div>
        </>
      ),
      // image: `images/goair/offer3.png`,
      // image: require('../../assets/img/offer-section/wow-3.png'),
      image: WowThree,
    },
    {
      title: "PERFECT HOLIDAY",
      offer: "PRIVILEGES*",
      description: (
        <>
          <div className="mt-8">
            <p className="text-white text-base">
              Book with just 25% now, and pay the rest 30 days prior to your sailing date.
            </p>
          </div>
        </>
      ),
      // image: `images/goair/offer4.png`,
      // image: require('../../assets/img/offer-section/wow-4.png'),
      image: Suite,
    },
  ],
}

export const fourPillars = {
  heading: 'FOUR <strong>PILLARS</strong>',
  items: [
    {
      title: "The Empress",
      offer: "A City on the Sea",
      description: "A City on the Sea ",
      image: PillarOne,
      url: 'https://cordelia.fortelogic.in/empress-page',
    },
    {
      title: "Entertainment",
      offer: "Upto $250*",
      description: " Be spoilt for choice. Non stop fun for one and all.",
      image: PillarFour,
      url: 'https://cordelia.fortelogic.in/entertainment',
    },
    {
      title: "Food & Dining",
      description: "The world on your plate and the sound of waves.",
      image: PillarThree,
      url: 'https://cordelia.fortelogic.in/cruise-dining',
    },
    {
      title: "Destination",
      offer: "At Price of Interior*",
      description: "Every destination has a story to tell.",
      image: PillarTwo,
      url: 'https://cordelia.fortelogic.in/cruise-destinations',
    },
  ],
}


// export const wowOffers = [
//   {
//     title: "Book Now",
//     offer: "Pay Later*",
//     description: (
//       <>
//         <div className="mt-8">
//           <p className="text-white text-base">
//             Book with just 25% now, and pay the rest 30 days prior to your sailing date.
//           </p>
//         </div>
//       </>
//     ),
//     // image: `images/goair/offer1.png`,
//     // image: require('../../assets/img/offer-section/wow-1.png'),
//     image: WowOne,
//   },
//   {
//     title: "Get Ocean View",
//     offer: "At Price of Interior*",
//     description: (
//       <>
//         <div className="mt-8">
//           <p className="text-white text-base">
//             Book with just 25% now, and pay the rest 30 days prior to your sailing date.
//           </p>
//         </div>
//       </>
//     ),
//     // image: `images/goair/offer2.png`,
//     // image: require('../../assets/img/offer-section/wow-2.png'),
//     image: WowTwo,
//   },
//   {
//     title: "For the first time",
//     offer: "Kids Sail Free*",
//     description: (
//       <>
//         <div className="mt-8">
//           <p className="text-white text-base">
//             Book with just 25% now, and pay the rest 30 days prior to your sailing date.
//           </p>
//         </div>
//       </>
//     ),
//     // image: `images/goair/offer3.png`,
//     // image: require('../../assets/img/offer-section/wow-3.png'),
//     image: WowThree,
//   },
//   {
//     title: "Onboard Credit",
//     offer: "Upto $250*",
//     description: (
//       <>
//         <div className="mt-8">
//           <p className="text-white text-base">
//             Book with just 25% now, and pay the rest 30 days prior to your sailing date.
//           </p>
//         </div>
//       </>
//     ),
//     // image: `images/goair/offer4.png`,
//     // image: require('../../assets/img/offer-section/wow-4.png'),
//     image: WowFour,
//   }
// ]
