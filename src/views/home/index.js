import HtmlParser from 'react-html-parser';
import { Button, Collapse } from 'antd';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getRoutes } from '../../redux/actions/routeAction';
import { useHistory } from 'react-router-dom';
import Anchor from '../../assets/img/anchor.png';
import Wheel from '../../assets/img/wheel.png';

import Slide1 from '../../assets/img/perfect-banner-slide.webp';
import Slide2 from '../../assets/img/dining-banner-slide.webp';
import Slide3 from '../../assets/img/newness-banner-slide.webp';
import Slide4 from '../../assets/img/entertainment-banner-slide.webp';
import Slide5 from '../../assets/img/dj-banner-slide.webp';
import SlideM1 from '../../assets/img/perfect-banner-mobile.webp';
import SlideM2 from '../../assets/img/dining-banner-mobile.webp';
import SlideM3 from '../../assets/img/newness-banner-mobile.webp';
import SlideM4 from '../../assets/img/entertainment-banner-mobile.webp';
import SlideM5 from '../../assets/img/dj-banner-mobile.webp';

import LocalStore from '../../utils/localStore';
import Layout from '../../components/Layout/Layout'

import {
  bestSellingCruises,
  cruiseLandPackages,
  cruiseInspirations,
  occasionEvents,
  testimonials,
  trustBadges,
  cardOverlay,
  OurDestination,
  mobileApps,
  wowOffers,
  fourPillars
} from './feed';
import { Jumbotron } from '../../components/Header';
import SearchComponent from '../../components/SearchComponent/SearchComponent';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/HeaderHome';
import Banner from '../../components/BannerCarousel/Banner';
import NotificationBanner from '../../components/NotificationBanner/NotificationBanner';
import * as Section from '../../components/Section';
import Container from '../../components/Container';

function Home() {
  const dispatch = useDispatch();
  const defaultSort = { value: 'nightCount', type: 'asc' };
  const [ports, setPorts] = useState([]);
  const [isModalShow, setIsModalShow] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [isMobile, setMobile] = React.useState(false);
  let history = useHistory();
  const { Panel } = Collapse;
  const { routeData } = useSelector((state) => ({
    routeData: state.route.routeData,
  }));

  const queryParams = new URLSearchParams(window.location.search)

  const bannerDetails = {
    banners : [
      {
        image:Slide1,
      },
      {
        image:Slide2,
      },
      {
        image:Slide3,
      },
      {
        image:Slide4,
      },
      {
        image:Slide5,
      },
    ],
    mobileBanners : [
      {
        image:SlideM1,
      },
      {
        image:SlideM2,
      },
      {
        image:SlideM3,
      },
      {
        image:SlideM4,
      },
      {
        image:SlideM5,
      },
    ]
  }

  //fetch data on first time render
  useEffect(async () => {
    setIsLoading(true);
    await dispatch(getRoutes({ defaultSort }));
    setIsLoading(false);
    if (queryParams.get("token") !== null && queryParams.get("token") !== undefined && queryParams.get("token_expiry") !== undefined && queryParams.get("token_expiry") !== null) {
      LocalStore.setItem("auth", { token: queryParams.get("token"), exp: queryParams.get("token_expiry") })
    }
  }, []);

  //Run when data is fetched (create data here)
  useEffect(async () => {
    if (routeData) {
      const portHash = {};
      routeData.forEach((route) => {
        route.ports.forEach((item) => {
          if (!portHash[item.port.code]) {
            portHash[item.port.code] = item;
          }
        });
      });
      const portOptions = Object.values(portHash).sort((a, b) => {
        return a.port.name < b.port.name ? -1 : 1;
      });
      setPorts(portOptions);
    }
  }, [routeData]);

  React.useEffect(() => {
    _isMobileScreen();
    window.addEventListener('resize', _isMobileScreen);
    return () => {
      window.removeEventListener('resize', _isMobileScreen);
    };
  }, []);

  const searchHandler = (filter) => {
    queryParams.get("platform") === "true" ?
      history.push(
        `/cruise-routes?sailingFrom=${filter.sailingFrom ? filter.sailingFrom.port.name : ''}&sailingTo=${filter.sailingTo ? filter.sailingTo.port.name : ''
        }&month=${filter.month}&platform=true`
      )
      : history.push(
        `/cruise-routes?sailingFrom=${filter.sailingFrom ? filter.sailingFrom.port.name : ''}&sailingTo=${filter.sailingTo ? filter.sailingTo.port.name : ''
        }&month=${filter.month}`
      )
  };

  const _isMobileScreen = () => {
    if (window.innerWidth <= 768) {
      setMobile(true);
      return;
    }
    setMobile(false);
  };

  const modalCloseHandler = async () => {
    await setIsModalShow(false);
  };

  // Deployment

  return (
    <Layout>
      <div className="home-page">
        <NotificationBanner />
        <div className="fixed-menu">
          <Header transparent={true} platform={queryParams.get("platform")} />
        </div>
        {/* <Jumbotron isMobile={isMobile} history={history} /> */}
        <Banner {...bannerDetails}/>
        <div className="w-md-50 mx-auto">
          {
            queryParams.get("platform") && queryParams.get("platform") == "true" ?
              ``
              : <SearchComponent titleText={true} platform={queryParams.get("platform")} onSearch={searchHandler} ports={ports} style={{ marginTop: isMobile ? '-5rem' : '-3rem' }} />
          }
        </div>
        <Container>
          {/* <p className="text-center mt-2 pt-2">Your Current Location is Mumbai.{isMobile ? <br /> : ""} <em className="pointer font-italic" style={{ color: "#EA725B", cursor: "pointer" }}>Not in Mumbai?</em></p> */}

          <Section.FourPillars {...fourPillars} isMobile={isMobile} />
          <Section.CardsCarousel isMobile={isMobile} isBestSelling={true} {...bestSellingCruises} />
          <Section.Newsletter isMobile={isMobile} />
          <Section.WowOffers {...wowOffers} isMobile={isMobile} />
        </Container>
        {/* <Section.BackgroundOverlay isMobile={isMobile} history={history} /> */}
        <Container>
          <Section.OurDestination isMobile={isMobile} />
          {/* <Section.CardsCarousel isMobile={isMobile} {...cruiseLandPackages} history={history} /> */}
          {isMobile && <img className="wheel-img" src={Wheel} />}
          {/* <Section.OverlayCard isMobile={isMobile} {...cardOverlay} history={history} /> */}
          {isMobile && <img className="anchor-img" src={Anchor} />}
          {!isMobile && <img className="wheel-img" src={Wheel} />}

          {/* {!isMobile && (
            <div className="position-relative justify-content-center px-3">
              <img className="anchor-img" src={Anchor} />
              <h3 className="inline-section__heading">{HtmlParser(occasionEvents.heading)}</h3>
              {occasionEvents.items.map((item, idx) => (
                <Section.InlineImageContent key={idx} {...item} />
              ))}
            </div>
          )} */}

          {/* {isMobile && <Section.CardsCarouselMobile isOccasionEvents={true} isMobile={isMobile} {...occasionEvents} />} */}
          <Section.TrustBadge isMobile={isMobile} trustBadges={trustBadges} />
          {/* <Section.CardsCarousel isMobile={isMobile} {...cruiseInspirations} /> */}
          <Section.CardsCarousel titleMargin={"testimon-text"} isMobile={isMobile} {...testimonials} />
        </Container>

        <Container strict={true}>
          <div className="bg-grey py-5">
            <div className={`app-download-section ${isMobile ? 'banner-row' : 'row'}`}>
              <div className="col-4 col-md-3">
                <img className="mobile-screenshot" src={isMobile ? mobileApps.mobileSrc : mobileApps.src}></img>
              </div>
              <div className="col-8 col-md-6 app-download-section__content">
                {isMobile ? (
                  <h6>{mobileApps.mobileheading}</h6>
                ) : (
                  <h3 className="font-weight-bold">{mobileApps.heading}</h3>
                )}

                <p className="description">{isMobile ? mobileApps.mobileDesc : mobileApps.description}</p>
                <div className="d-md-none">
                  <img src={mobileApps.playStore} />
                  <img src={mobileApps.appStore} />
                </div>
              </div>
              <div className="col-2 text-center d-none d-md-block">
                <div>
                  <img src={mobileApps.qrCode} />
                </div>
                <div>
                  <img src={mobileApps.playStore} />
                </div>
                <div>
                  {' '}
                  <img src={mobileApps.appStore} />
                </div>
              </div>
            </div>
          </div>
        </Container>
        {/* <Container strict={true}>
          <div className="py-5 d-none d-md-block container m-auto">
            <div className="row border-bottom">
              <div className="col">
                <h5 className="font-weight-bold">Top destination</h5>
              </div>
              <div className="col">
                <h5 className="font-weight-bold">Top Itineraries</h5>
              </div>
              <div className="col">
                <h5 className="font-weight-bold">Top Activities</h5>
              </div>
              <div className="col">
                <h5 className="font-weight-bold">Top Ports</h5>
              </div>
            </div>
            <div className="row py-3">
              <div className="col">
                <p>Diu</p>
                <p>Mumbai</p>
                <p>Goa</p>
                <p>Cochin</p>
                <p>Chennai</p>
                <p>Colombo</p>
                <p>Jaffna</p>
                <p>Galle</p>
                <p>Trincomalle</p>
                <p>Lakshadweep</p>
                <p>Male</p>
              </div>
              <div className="col">
                <p>Diu</p>
                <p>Mumbai</p>
                <p>Goa</p>
                <p>Cochin</p>
                <p>Chennai</p>
                <p>Colombo</p>
                <p>Jaffna</p>
                <p>Galle</p>
                <p>Trincomalle</p>
                <p>Lakshadweep</p>
                <p>Male</p>
              </div>
              <div className="col">
                <p>Diu</p>
                <p>Mumbai</p>
                <p>Goa</p>
                <p>Cochin</p>
                <p>Chennai</p>
                <p>Colombo</p>
                <p>Jaffna</p>
                <p>Galle</p>
                <p>Trincomalle</p>
                <p>Lakshadweep</p>
                <p>Male</p>
              </div>
              <div className="col">
                <p>Diu</p>
                <p>Mumbai</p>
                <p>Goa</p>
                <p>Cochin</p>
                <p>Chennai</p>
                <p>Colombo</p>
                <p>Jaffna</p>
                <p>Galle</p>
                <p>Trincomalle</p>
                <p>Lakshadweep</p>
                <p>Male</p>
              </div>
            </div>
          </div>
          <div className="d-md-none" key="1">
            <Collapse expandIconPosition={'right'} ghost>
              <Panel header={<h6 className="font-weight-bold mb-0">Top destination</h6>} key="1">
                <p>Diu</p>
                <p>Mumbai</p>
                <p>Goa</p>
                <p>Cochin</p>
                <p>Chennai</p>
                <p>Colombo</p>
                <p>Jaffna</p>
                <p>Galle</p>
                <p>Trincomalle</p>
                <p>Lakshadweep</p>
                <p>Male</p>
              </Panel>
              <Panel header={<h6 className="font-weight-bold mb-0">Top Itineraries</h6>} key="2">
                <p>Diu</p>
                <p>Mumbai</p>
                <p>Goa</p>
                <p>Cochin</p>
                <p>Chennai</p>
                <p>Colombo</p>
                <p>Jaffna</p>
                <p>Galle</p>
                <p>Trincomalle</p>
                <p>Lakshadweep</p>
                <p>Male</p>
              </Panel>
              <Panel header={<h6 className="font-weight-bold mb-0">Top Activities</h6>} key="3">
                <p>Diu</p>
                <p>Mumbai</p>
                <p>Goa</p>
                <p>Cochin</p>
                <p>Chennai</p>
                <p>Colombo</p>
                <p>Jaffna</p>
                <p>Galle</p>
                <p>Trincomalle</p>
                <p>Lakshadweep</p>
                <p>Male</p>
              </Panel>
              <Panel header={<h6 className="font-weight-bold mb-0">Top Ports</h6>} key="4">
                <p>Diu</p>
                <p>Mumbai</p>
                <p>Goa</p>
                <p>Cochin</p>
                <p>Chennai</p>
                <p>Colombo</p>
                <p>Jaffna</p>
                <p>Galle</p>
                <p>Trincomalle</p>
                <p>Lakshadweep</p>
                <p>Male</p>
              </Panel>
            </Collapse>
          </div>

        </Container> */}
        {/* <Section.MobileApps isMobile={isMobile} /> */}
        {
          (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
        }
      </div>
    </Layout>
  );
}

export default Home;
