import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";

function ConfirmedBookingDetails() {
  return (
    <div className="ConfirmedBookingDetails">
      <Header bgPurple/>
      <Footer/>
    </div>
  );
}

export default ConfirmedBookingDetails;
