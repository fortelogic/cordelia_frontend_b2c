import React, { useState, useEffect, useRef } from "react";
import { Link, useHistory } from "react-router-dom"
import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";
import { Row, Col, Button } from 'antd'
import MainContent from './MainContent'
import ImageMain from '../../assets/img/main-img1.png'
import VideoImage from '../../assets/img/videoImage.png'
import './index.scss'
import { Collapse } from 'antd';
import Container from "../../components/Container";
import { ArrowLeftOutlined, MenuOutlined } from "@ant-design/icons";
import Coconut from "../../assets/img/coconuttree.png"
import Line from "../../assets/icons/line.svg"
import Pin from "../../assets/icons/pin.svg"
import Plus from "../../assets/icons/plus.svg"
import Minus from "../../assets/icons/minus.svg"
import ComponentBanner from "../../assets/img/homecomponent/homecomponent-banner.png"

import ComponentMobileBanner from "../../assets/img/homecomponent/ComponentHomeMobile.png"

import DayOneImage from "../../assets/img/homecomponent/day-one-image.png"
import DayOneImageMobile from "../../assets/img/homecomponent/day-one-image-mobile.png"
import BannerLine from "../../assets/img/homecomponent/line.png"
import BannerLineMobile from "../../assets/img/homecomponent/banner-line-mobile.png"

import calanderIcon from '../../assets/icons/calander-Icon.svg';
import { CalendarTodayOutlined } from "@material-ui/icons";
import NaturalHaven from "../../assets/img/homecomponent/natural-haven.png"
import Culinary from "../../assets/img/homecomponent/culinary.png"
import Culture from "../../assets/img/homecomponent/culture.png"
import Slider from 'react-slick'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'

import DayThreeImage from "../../assets/img/homecomponent/day-three-image.png"
import DayThreeImageMobile from "../../assets/img/homecomponent/day-three-image-mobile.png"

import NaturalHavenTwo from "../../assets/img/homecomponent/natural-haven-two.png"
import CulinaryTwo from "../../assets/img/homecomponent/culinary-two.png"
import CultureTwo from "../../assets/img/homecomponent/culture-two.png"

import DayFiveImage from "../../assets/img/homecomponent/day-five-image.png"
import DayFiveImageMobile from "../../assets/img/homecomponent/day-five-image-mobile.png"

import NaturalHavenThree from "../../assets/img/homecomponent/natural-haven-three.png"
import CulinaryThree from "../../assets/img/homecomponent/culinary-three.png"
import CultureThree from "../../assets/img/homecomponent/culture-three.png"

import DayTwoToFour from "../../assets/img/homecomponent/day-two-to-four.png"
import DayTwoToFourMobile from "../../assets/img/homecomponent/day-two-to-four-mobile.png"

import CruiseExpOne from "../../assets/img/homecomponent/cruise-exp-one.png"
import CruiseExpTwo from "../../assets/img/homecomponent/cruise-exp-two.png"
import CruiseExpThree from "../../assets/img/homecomponent/cruise-exp-three.png"
import CruiseExpFour from "../../assets/img/homecomponent/cruise-exp-four.png"
import HtmlParser from 'react-html-parser'
import pageStyle from "../../views/home/wowoffer.module.css";
import cx from "classnames";
import {
    testimonials
} from '../home/feed';
import * as Section from '../../components/Section';
const { Panel } = Collapse



const settings = {
    infinite: true,
    slidesToShow: 3,
    // className: "center",
    // centerMode: true,
    // centerPadding: "100px",
    speed: 300,
    cssEase: 'linear',
    dots: false,
    arrows: true,
    nextArrow: <ChevronRightIcon />,
    prevArrow: <ChevronLeftIcon />,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
            },
        },
        {
            breakpoint: 767,
            settings: {
                arrows: false,
                dots: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                initialSlide: 1,
            },
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            },
        },
    ],
};

const settingsCruiseExp = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                dots: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: false,
            },
        },
        {
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1,
                initialSlide: 1,
            },
        },
        {
            breakpoint: 480,
            settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
            },
        },
    ],
};


function HomeComponent() {

    const queryParams = new URLSearchParams(window.location.search)

    const [accord, setAccord] = useState(9999)
    const [heroVisible, setHeroVisible] = useState(true)
    const ref = useRef()
    const [isMobile, setMobile] = React.useState(false);

    const [dayOneAccord, setDayOneAccord] = useState(true)
    const [dayTwoAccord, setDayTwoAccord] = useState(false)
    const [dayThreeAccord, setDayThreeAccord] = useState(false)
    const [dayFourAccord, setDayFourAccord] = useState(false)
    const [dayFiveAccord, setDayFiveAccord] = useState(false)
    const [daySixAccord, setDaySixAccord] = useState(false)


    React.useEffect(() => {
        _isMobileScreen();
        window.addEventListener('resize', _isMobileScreen);
        return () => {
            window.removeEventListener('resize', _isMobileScreen);
        };
    }, []);

    const _isMobileScreen = () => {
        if (window.innerWidth <= 768) {
            setMobile(true);
            return;
        }
        setMobile(false);
    };


    useEffect(() => {
        const observer = new IntersectionObserver(
            ([entry]) => {

                if (entry.isIntersecting) {
                    //do your actions here
                    setHeroVisible(true)
                    // console.log('Its Visible')
                } else {
                    setHeroVisible(false)
                    // console.log("Its not visible")
                }
            },
            {
                root: null,
                rootMargin: "0px",
                threshold: 0.1
            }
        );
        if (ref.current) {
            observer.observe(ref.current)
        }
    }, [ref])

    const faqs = [
        "How to book a weekend cruise?",
        "What are the best weekend getaways near me?",
        "How to plan short weekend trips?",
        "Can I book a cruise for family weekend getaways?",
        "How to get the best weekend cruise deals?",
        "Whats the best weekend cruise option for me?",
    ]

    const openCloseAccord = (e, index) => {
        if (accord === index) {
            setAccord(9999)
        } else {
            setAccord(index)
        }
    }

    return (
        <div className="Home">
            <Header bgPurple platform={queryParams.get("platform")} />
            <div className="itinerary-page-banner">
                <img src={ComponentBanner} />
                <div className="itinerary-page-banner-content">
                    <h1 className="itinerary-page-banner-night-count">5 NIGHTS</h1>
                    <div className="itinerary-page-banner-cruise-dates">
                        <CalendarTodayOutlined style={{ color: "white", fontSize: "12pt" }} /> <span>Sat 18 Jan' 21 -- Thu 23 Jan' 21</span>
                    </div>
                    <h2 className="itinerary-page-banner-destination">Shrilanka Extravaganza</h2>
                    <div className="itinerary-page-banner-itinerary-map">
                        <div className="itinerary-page-banner-day-circle mr-3">1</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Chennai</span>
                        <img className="banner-line mr-3" src={BannerLine} />

                        <div className="itinerary-page-banner-day-circle mr-3">2</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Day At Sea</span>
                        <img className="banner-line mr-3" src={BannerLine} />

                        <div className="itinerary-page-banner-day-circle mr-3">3</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Colombo</span>
                        <img className="banner-line mr-3" src={BannerLine} />

                        <div className="itinerary-page-banner-day-circle mr-3">4</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Day At sea</span>
                        <img className="banner-line mr-3" src={BannerLine} />

                        <div className="itinerary-page-banner-day-circle mr-3">5</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Male</span>
                        <img className="banner-line mr-3" src={BannerLine} />

                        <div className="itinerary-page-banner-day-circle mr-3">6</div>
                        <span className="itinerary-page-banner-day-destination">Chennai</span>
                    </div>
                </div>
            </div>

            <div ref={ref} className="itinerary-page-banner-mobile">
                <img src={ComponentMobileBanner} />
                <div className="hero-section">
                    <h4 className="night-count">5 NIGHTS</h4>
                    <div className="itinerary-page-banner-cruise-dates">
                        <CalendarTodayOutlined style={{ color: "white", fontSize: "12pt" }} /> <span>Sat 18 Jan' 21 -- Thu 23 Jan' 21</span>
                    </div>
                    <h2 className="itinerary-page-banner-destination">Shrilanka Extravaganza</h2>
                    <div className="itinerary-page-banner-itinerary-map">
                        <div className="itinerary-page-banner-day-circle mr-3">1</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Chennai</span>
                    </div>
                    <img className="banner-line mr-3" src={BannerLineMobile} />
                    <div className="itinerary-page-banner-itinerary-map">
                        <div className="itinerary-page-banner-day-circle mr-3">2</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Day at Sea</span>
                    </div>
                    <img className="banner-line mr-3" src={BannerLineMobile} />
                    <div className="itinerary-page-banner-itinerary-map">
                        <div className="itinerary-page-banner-day-circle mr-3">3</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Colombo</span>
                    </div>
                    <img className="banner-line mr-3" src={BannerLineMobile} />
                    <div className="itinerary-page-banner-itinerary-map">
                        <div className="itinerary-page-banner-day-circle mr-3">4</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Day at Sea</span>
                    </div>
                    <img className="banner-line mr-3" src={BannerLineMobile} />
                    <div className="itinerary-page-banner-itinerary-map">
                        <div className="itinerary-page-banner-day-circle mr-3">5</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Male</span>
                    </div>
                    <img className="banner-line mr-3" src={BannerLineMobile} />
                    <div className="itinerary-page-banner-itinerary-map">
                        <div className="itinerary-page-banner-day-circle mr-3">6</div>
                        <span className="itinerary-page-banner-day-destination mr-3">Chennai</span>
                    </div>
                </div>
            </div>

            <div className={`row ${heroVisible ? "head-cont-fixed shadow-lg" : "head-cont shadow-lg"}`}>
                <div className="col destination-section-one">
                    <h3 className="destination-title">Lakshadweep & Goa Cruise</h3>
                    <h3 className="starting-amount"><span className="from-text">FROM</span>  <span className="amount-from-starting">₹ 27000<small> / Person</small></span></h3>
                </div>
                <div className="col book-container destination-section-two">
                    <Link to="cabin-selection">
                        <p className="book-now-text">BOOK NOW</p>
                        {/* <a href="#" className="book-text">BOOK NOW</a> */}
                    </Link>
                </div>
            </div>

            <div className="cruise-info-box shadow-lg">
                <div className="cruise-info-box-left-block">
                    <h1 className="cruise-info-box-destinations">Lakshadweep & Goa Cruise</h1>
                    <p className="cruise-info-box-destinations-date">Sat, 21 November 2021</p>
                </div>
                <div className="cruise-info-box-center-block">
                    <p className="from-price">FROM</p>
                    <h1 className="cruise-info-box-destinations price-text">
                        ₹ 27000 / <small className="">Person</small>
                    </h1>
                    <p className="cruise-info-box-destinations-date">+ Taxes and other fees</p>
                </div>
                <div className="cruise-info-box-right-block">
                    <Button className="btn btn-primary book-now-button">BOOK NOW</Button>
                </div>
            </div>

            <div className="day-wise-section">
                <div className="itinerary-page-day-1">
                    <h1 className="title-and-things-of-destination">Departure From Chennai</h1>
                    {
                        isMobile ?
                            <img className="day-one-image-mobile" src={DayOneImageMobile} />
                            : <img className="itinerary-page-image" src={DayOneImage} />
                    }
                    <div className="itinerary-page-day-1-content">
                        <h1 className="day-one-text"> Day 1 </h1>
                        <h5 className="day-one-title">Departure From Chennai</h5>
                        <h5 className="day-one-dates">18 Jan' 21 08:00 AM</h5>
                        <p className="day-one-description-text">Quis nec odio quis nulla auctor eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. </p>
                        <button className="know-more-iti" onClick={() => setDayOneAccord(!dayOneAccord)}>{dayOneAccord === true ? `Know Less` : `Know More`}</button>
                    </div>
                    <div className="itinerary-page-day-1-content-mobile">
                        <h1 className="day-one-text"> Day 1 </h1>
                        <h5 className="day-one-title">Departure From Chennai</h5>
                        <h5 className="day-one-dates">18 Jan' 21 08:00 AM</h5>
                        <p className="day-one-description-text">Quis nec odio quis nulla auctor eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. </p>
                        <button className="know-more-iti" onClick={() => setDayOneAccord(!dayOneAccord)}>{dayOneAccord === true ? `Know Less` : `Know More`}</button>
                    </div>

                    <div className={dayOneAccord ? `` : `d-none`}>
                        <h1 className="title-and-things-of-destination">Things to do in Chennai</h1>
                        <div className="things-to-do-slider">
                            <div className="cards-carousel__slider">
                                <Slider className="slick-slider primary-slider slick-initialized" {...settings} id={Date.now()} >
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={NaturalHaven} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">NATURAL HAVEN</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    With white beaches, vibrant coral reefs and an incredible marine life, Lakshadweep is home to the most spectacular views and experiences. India’s smallest union territory has beautifully preserved its natural habitat through responsible tourism. f you’re looking for a holiday with the right combination of sun, sand, and serenity, then Lakshadweep is the place to be.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={Culinary} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">CULINARY</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    The local cuisine is heavily influenced by traditional Kerala flavors, with the extensive use of coconut as a curry blend. Gorge on delicacies such Mus Kavaab, a delicious spicy fried fish, or fish pakodas, or Maasu podichat. There are ample choices for vegetarians as well, such as sambar, and avial. If you’ve got a sweet tooth, try the coconut bondas and moah appams,
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={Culture} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">CULTURE</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    Lakshadweep draws a great amount of cultural influence from the state of Kerala, the traces of which can be found in the local diet. The main language spoken here is Malayalam, and much of the island's folklore and customs reflect a mix of Islamic and Keralan traditions. Parichakali and Kolkai are two folk dance forms native to these islands
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="itinerary-page-day-two-to-four">
                    {/* <h1 className="title-and-things-of-destination p-0">Arrive at Chennai</h1>
                    <p className="heading-description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. Ullamcorper eget ultrices ultrices lorem sed auctor diam auctor suspendisse. Consectetur velit tortor curabitur scelerisque elit. Sit nisl felis erat imperdiet neque eu cursus lectus senectus. Quis nec odio quis nulla auctor eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. Ullamcorper eget ultrices ultrices lorem sed auctor diam auctor suspendisse. Consectetur velit tortor curabitur scelerisque elit. Sit nisl felis erat imperdiet neque eu cursus lectus senectus. Quis nec odio quis nulla auctor eget.
                    </p> */}
                    <h4 className="mt-5 font-weight-bold">Day At Sea</h4>
                    {
                        isMobile ?
                            <img className="day-one-image-mobile p-0" src={DayTwoToFourMobile} />
                            : <img className="itinerary-page-image" src={DayTwoToFour} />
                    }

                    <div className="itinerary-page-day-two-to-four-content">
                        <h1 className="day-one-text"> Day 2</h1>
                        <h5 className="day-one-title">Enjoy Onboard Experience</h5>
                        <h5 className="day-one-dates"> 18 & 20 Jan'21 </h5>
                        <p className="day-one-description-text">Quis nec odio quis nulla auctor eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. </p>
                        <button className="know-more-iti">Know More</button>
                    </div>
                </div>
                <div className="itinerary-page-day-1">
                    <h1 className="title-and-things-of-destination">Explore Colombo</h1>
                    {
                        isMobile ?
                            <img className="day-one-image-mobile" src={DayThreeImageMobile} />
                            : <img className="itinerary-page-image" src={DayThreeImage} />
                    }
                    <div className="itinerary-page-day-1-content">
                        <h1 className="day-one-text"> Day 3 </h1>
                        <h5 className="day-one-title">Explore Colombo</h5>
                        <h5 className="day-one-dates">18 Jan' 21 08:00 AM</h5>
                        <p className="day-one-description-text">Quis nec odio quis nulla auctor eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. </p>
                        <button className="know-more-iti" onClick={() => setDayThreeAccord(!dayThreeAccord)}>{dayThreeAccord === true ? `Know Less` : `Know More`}</button>
                    </div>
                    <div className="itinerary-page-day-1-content-mobile">
                        <h1 className="day-one-text"> Day 3 </h1>
                        <h5 className="day-one-dates">18 Jan' 21</h5>
                        <h5 className="day-one-dates">08:00 AM</h5>
                        <button className="know-more-iti" onClick={() => setDayThreeAccord(!dayThreeAccord)}>{dayThreeAccord === true ? `Know Less` : `Know More`}</button>
                    </div>

                    <div className={dayThreeAccord ? `` : `d-none`}>
                        <h1 className="title-and-things-of-destination">Things to do in Colombo</h1>
                        <div className="things-to-do-slider">
                            <div className="cards-carousel__slider">
                                <Slider className="slick-slider primary-slider slick-initialized" {...settings} id={Date.now()} >
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={NaturalHavenTwo} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">NATURAL HAVEN</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    With white beaches, vibrant coral reefs and an incredible marine life, Lakshadweep is home to the most spectacular views and experiences. India’s smallest union territory has beautifully preserved its natural habitat through responsible tourism. f you’re looking for a holiday with the right combination of sun, sand, and serenity, then Lakshadweep is the place to be.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={CulinaryTwo} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">CULINARY</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    The local cuisine is heavily influenced by traditional Kerala flavors, with the extensive use of coconut as a curry blend. Gorge on delicacies such Mus Kavaab, a delicious spicy fried fish, or fish pakodas, or Maasu podichat. There are ample choices for vegetarians as well, such as sambar, and avial. If you’ve got a sweet tooth, try the coconut bondas and moah appams,
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={CultureTwo} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">CULTURE</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    Lakshadweep draws a great amount of cultural influence from the state of Kerala, the traces of which can be found in the local diet. The main language spoken here is Malayalam, and much of the island's folklore and customs reflect a mix of Islamic and Keralan traditions. Parichakali and Kolkai are two folk dance forms native to these islands
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="itinerary-page-day-two-to-four">
                    <h4 className="mt-5 font-weight-bold">Day At Sea</h4>
                    {
                        isMobile ?
                            <img className="day-one-image-mobile p-0" src={DayTwoToFourMobile} />
                            : <img className="itinerary-page-image" src={DayTwoToFour} />
                    }

                    <div className="itinerary-page-day-two-to-four-content">
                        <h1 className="day-one-text"> Day 4 </h1>
                        <h5 className="day-one-title">Enjoy Onboard Experience</h5>
                        <h5 className="day-one-dates"> 18 & 20 Jan'21 </h5>
                        <p className="day-one-description-text">Quis nec odio quis nulla auctor eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. </p>
                        <button className="know-more-iti">Know More</button>
                    </div>
                </div>

                <div className="itinerary-page-day-1">
                    <h1 className="title-and-things-of-destination">Explore Male</h1>
                    {
                        isMobile ?
                            <img className="day-one-image-mobile" src={DayFiveImageMobile} />
                            : <img className="itinerary-page-image" src={DayFiveImage} />
                    }

                    <div className="itinerary-page-day-1-content">
                        <h1 className="day-one-text"> Day 5 </h1>
                        <h5 className="day-one-title">Explore Male</h5>
                        <h5 className="day-one-dates">18 Jan' 21 08:00 AM</h5>
                        <p className="day-one-description-text">Quis nec odio quis nulla auctor eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur magna platea sodales venenatis. </p>
                        <button className="know-more-iti" onClick={() => setDayFiveAccord(!dayFiveAccord)}>{dayFiveAccord === true ? `Know Less` : `Know More`}</button>
                    </div>
                    <div className="itinerary-page-day-1-content-mobile">
                        <h1 className="day-one-text"> Day 5 </h1>
                        <h5 className="day-one-dates">18 Jan' 21</h5>
                        <h5 className="day-one-dates">08:00 AM</h5>
                        <button className="know-more-iti" onClick={() => setDayFiveAccord(!dayFiveAccord)}> {dayFiveAccord === true ? `Know Less` : `Know More`}</button>
                    </div>
                    <div className={dayFiveAccord ? `` : 'd-none'}>
                        <h1 className="title-and-things-of-destination">Things to do in Colombo</h1>
                        <div className="things-to-do-slider">
                            <div className="cards-carousel__slider">
                                <Slider className="slick-slider primary-slider slick-initialized" {...settings} id={Date.now()} >
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={NaturalHavenThree} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">NATURAL HAVEN</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    With white beaches, vibrant coral reefs and an incredible marine life, Lakshadweep is home to the most spectacular views and experiences. India’s smallest union territory has beautifully preserved its natural habitat through responsible tourism. f you’re looking for a holiday with the right combination of sun, sand, and serenity, then Lakshadweep is the place to be.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={CulinaryThree} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">CULINARY</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    The local cuisine is heavily influenced by traditional Kerala flavors, with the extensive use of coconut as a curry blend. Gorge on delicacies such Mus Kavaab, a delicious spicy fried fish, or fish pakodas, or Maasu podichat. There are ample choices for vegetarians as well, such as sambar, and avial. If you’ve got a sweet tooth, try the coconut bondas and moah appams,
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="cards-carousel__slider__card">
                                        <figure>
                                            <img alt="cordelia cruises" src={CultureThree} />
                                        </figure>
                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">CULTURE</h4>
                                                <p className={`cards-carousel__slider__card__body__description`}>
                                                    Lakshadweep draws a great amount of cultural influence from the state of Kerala, the traces of which can be found in the local diet. The main language spoken here is Malayalam, and much of the island's folklore and customs reflect a mix of Islamic and Keralan traditions. Parichakali and Kolkai are two folk dance forms native to these islands
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h1 className="title-and-things-of-destination text-center">EXPERIENCES ON YOUR CRUISE</h1>
            <p className="experience-text text-center">Discover unlimited experiences onboard and get spoilt for choice</p>
            <div className="things-to-do-slider mt-5">
                <div className="cards-carousel__slider">
                    <Slider className="primary-slider" {...settingsCruiseExp} id={Date.now()} >
                        <div className="cards-carousel__slider__card">
                            <div className="exp-image-with-text">
                                <figure>
                                    <img alt="cordelia cruises" src={CruiseExpOne} />
                                </figure>
                                <h5 className="exp-image-heading">Accommodation</h5>
                            </div>
                        </div>
                        <div className="cards-carousel__slider__card">
                            <div className="exp-image-with-text">
                                <figure>
                                    <img alt="cordelia cruises" src={CruiseExpTwo} />
                                </figure>
                                <h5 className="exp-image-heading">Onboard Dining</h5>
                            </div>
                        </div>
                        <div className="cards-carousel__slider__card">
                            <div className="exp-image-with-text">
                                <figure>
                                    <img alt="cordelia cruises" src={CruiseExpThree} />
                                </figure>
                                <h5 className="exp-image-heading">Club Hopping & Loungign</h5>
                            </div>
                        </div>
                        <div className="cards-carousel__slider__card">
                            <div className="exp-image-with-text">
                                <figure>
                                    <img alt="cordelia cruises" src={CruiseExpFour} />
                                </figure>
                                <h5 className="exp-image-heading">Entertainment</h5>
                            </div>
                        </div>
                    </Slider>
                </div>
            </div>
            <div className={isMobile ? "itinerary-page-testimonials p-0" : "itinerary-page-testimonials"}>
                <Section.CardsCarousel titleMargin={"testimon-text"} {...testimonials} />
            </div>
            <div className="faq-section pb-5">
                <div className="text-center">
                    <p className="faq-title">
                        FAQs
                    </p>
                    <p className="faq-description">
                        Get your questions answered here itself.
                    </p>
                </div>
                <div className="faq-accord">
                    {
                        faqs.map((item, index) => {
                            return (
                                <div className="faq-accord-whole">
                                    <div key={index} className="faq-accord-head py-2">
                                        <p className="faq-accord-title pt-3">{item}</p>
                                        <img className="accrod-sign" onClick={e => openCloseAccord(e, index)} src={accord !== index ? Plus : Minus} />
                                    </div>
                                    {
                                        index === accord &&
                                        <div className="accrod-description">
                                            <p>
                                                To plan short and smart weekend trips, <span>Click here. </span>
                                                Select your itinerary and cruise destination and leave
                                                the hassles of planning to us.
                                            </p>
                                        </div>
                                    }
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            {/* <Footer /> */}
            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
            }
        </div >
    )
}

export default HomeComponent;
