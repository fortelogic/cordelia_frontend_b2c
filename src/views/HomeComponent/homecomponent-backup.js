import { useState, useEffect, useRef } from "react";
import { Link, useHistory } from "react-router-dom"
import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";
import { Row, Col, Button } from 'antd'
import MainContent from './MainContent'
import ImageMain from '../../assets/img/main-img1.png'
import VideoImage from '../../assets/img/videoImage.png'
import './index.scss'
import { Collapse } from 'antd';
import Container from "../../components/Container";
import { ArrowLeftOutlined, MenuOutlined } from "@ant-design/icons";
import Coconut from "../../assets/img/coconuttree.png"
import Line from "../../assets/icons/line.svg"
import Pin from "../../assets/icons/pin.svg"
import Plus from "../../assets/icons/plus.svg"
import Minus from "../../assets/icons/minus.svg"


const { Panel } = Collapse;

const text = (
    <p style={{ paddingLeft: 24 }}>
        Aenean pellentesque viverra morbi id et.
    </p>
);

function HomeComponent() {
    const history = useHistory();
    const [heroVisible, setHeroVisible] = useState(true)
    const ref = useRef();
    const queryParams = new URLSearchParams(window.location.search)


    const [accord, setAccord] = useState(9999)

    const faqs = [
        "How to book a weekend cruise?",
        "What are the best weekend getaways near me?",
        "How to plan short weekend trips?",
        "Can I book a cruise for family weekend getaways?",
        "How to get the best weekend cruise deals?",
        "Whats the best weekend cruise option for me?",
    ]

    const openCloseAccord = (e, index) => {
        if (accord === index) {
            setAccord(9999)
        } else {
            setAccord(index)
        }
    }

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    useEffect(() => {
        const observer = new IntersectionObserver(
            ([entry]) => {

                if (entry.isIntersecting) {
                    //do your actions here
                    setHeroVisible(true)
                    // console.log('Its Visible')
                } else {
                    setHeroVisible(false)
                    // console.log("Its not visible")
                }
            },
            {
                root: null,
                rootMargin: "0px",
                threshold: 0.1
            }
        );
        if (ref.current) {
            observer.observe(ref.current)
        }
    }, [ref])

    return (
        <>
            {/* <div className="heading"> */}
            <Header logoHeight={'75px'} className="itHeader" bgPurple platform={queryParams.get("platform")} />
            {/* </div> */}

            <div ref={ref} id="secondItem" className="hero-section w-100">
                <div className="gradient"></div>
                <div className="total-days pt-5">
                    <div className="header-of-plan pl-4 pb-4">
                        <h6 className="title-text-cruise">5 Nights</h6>
                        <h6>Lakshadweep & Goa Cruise</h6>
                    </div>
                    <div className="plan">
                        <div className="row">
                            <div className="pinline">
                                <img className="pin" src={Pin} />
                                <img className="line" src={Line} />
                            </div>
                            <div>
                                <p className="plan-day">Day 1</p>
                                <p className="plan-place">Departure from Mumbai</p>
                                <p className="date-of-day">Mon, 8 Nov 2021</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="pinline">
                                <img className="pin" src={Pin} />
                                <img className="line" src={Line} />
                            </div>
                            <div>
                                <p className="plan-day">Day 2</p>
                                <p className="plan-place">Day at Sea</p>
                                <p className="date-of-day">Mon, 8 Nov 2021</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="pinline">
                                <img className="pin" src={Pin} />
                                <img className="line" src={Line} />
                            </div>
                            <div>
                                <p className="plan-day">Day 3</p>
                                <p className="plan-place">Explore Lakshadweep</p>
                                <p className="date-of-day">Mon, 8 Nov 2021</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="pinline">
                                <img className="pin" src={Pin} />
                                <img className="line" src={Line} />
                            </div>
                            <div>
                                <p className="plan-day">Day 4</p>
                                <p className="plan-place">Explore Goa</p>
                                <p className="date-of-day">Mon, 8 Nov 2021</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="pinline">
                                <img className="pin" src={Pin} />
                                <img className="line" src={Line} />
                            </div>
                            <div>
                                <p className="plan-day">Day 5</p>
                                <p className="plan-place">Day at Sea</p>
                                <p className="date-of-day">Mon, 8 Nov 2021</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="pinline">
                                <img className="pin" src={Pin} />
                            </div>
                            <div>
                                <p className="plan-day">Day 6</p>
                                <p className="plan-place">Arrival at Mumbai</p>
                                <p className="date-of-day">Mon, 8 Nov 2021</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="headerContent sticky-bar-destination">
                <Row>
                    <Button
                        type="primary arrow-left-button"
                        className="w-auto"
                        onClick={() => {
                            history.goBack();
                        }}
                    >
                        <ArrowLeftOutlined />
                    </Button>
                    <Col xs={24} sm={24} md={24} lg={12} className="headingMain">
                        <h1 className="trip-title mt-3">
                            Lakshadweep & Goa Cruise
                        </h1>
                        {/* <Link to={`/cruise-routes`}>
                            <a>Back</a>
                        </Link> */}
                        <h3>
                            Sat, 21 November 2021
                        </h3>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={12}>
                        <Row>
                            <Col sm={12} md={10}>
                                <h5>
                                    FROM
                                </h5>
                                <h4>
                                    <b>₹27,000</b> <span style={{ textTransform: 'none' }}> / person</span>
                                </h4>
                                <p style={{ lineHeight: '0px' }}>+ Taxes and other fees</p>
                            </Col>
                            <Col xs={24} sm={24} md={14}>
                                <div className="bookBtn">
                                    <h1>
                                        Book now
                                    </h1>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>

            </div>

            <Container wrapperClass="">

                <div className={`row ${heroVisible ? "head-cont-fixed" : "head-cont"}`}>
                    <div className="col">
                        <h3 className="time">5 NIGHTS</h3>
                        <h3 className="date">Sat, 21 Nov 2021</h3>
                    </div>
                    <div className="col">
                        <p className="start-from">STARTING FROM</p>
                        <h3 className="price">₹27,000/ person</h3>
                    </div>
                    <div className="col book-container">
                        {/* <p className="book-text">BOOK NOW</p> */}
                        <Link to="cabin-selection">
                            <a href="#" className="book-text">BOOK NOW</a>
                        </Link>
                    </div>
                </div>

                <div className="top-bar">
                    <MenuOutlined className="menu-button" />
                    <div className="col-10">
                        <span className="destination-name">Lakshadweep & Goa Cruise</span>
                    </div>
                </div>

                <div className="mainContainer mx-2">
                    <MainContent />
                    <div className="collapseContainer">
                        <h2>
                            FAQs
                        </h2>
                        <Collapse bordered={false} defaultActiveKey={['0']}>
                            <Panel header="Lorem ipsum dolor sit amet, consectetur adipiscing elit?" key="1">
                                {text}
                            </Panel>
                            <Panel header="Lorem ipsum dolor sit amet, consectetur adipiscing elit?" key="2">
                                {text}
                            </Panel>
                            <Panel header="Lorem ipsum dolor sit amet, consectetur adipiscing elit?" key="3">
                                {text}
                            </Panel>
                        </Collapse>
                    </div>

                    <div className="faq-section pb-5">
                        <p className="faq-title">
                            FAQs
                        </p>
                        <p className="faq-description">
                            Get your questions answered here itself.
                        </p>
                        <div className="faq-accord">
                            {
                                faqs.map((item, index) => {
                                    return (
                                        <div className="faq-accord-whole">
                                            <div key={index} className="faq-accord-head py-2">
                                                <p className="faq-accord-title pt-3">{item}</p>
                                                <img className="accrod-sign" onClick={e => openCloseAccord(e, index)} src={accord !== index ? Plus : Minus} />
                                            </div>
                                            {
                                                index === accord &&
                                                <div className="accrod-description">
                                                    <p>
                                                        To plan short and smart weekend trips, <span>Click here. </span>
                                                        Select your itinerary and cruise destination and leave
                                                        the hassles of planning to us.
                                                    </p>
                                                </div>
                                            }
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>

                </div>
            </Container>
            <Footer />
        </>
    );
}

export default HomeComponent;
