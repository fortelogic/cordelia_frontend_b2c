import React, { useEffect, useState } from "react";
import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container";
import Card from '../../components/UI/Card';
import cx from "classnames";
import styles from "./my-booking.module.css";
import LocalStorage from "../../utils/localStore"
import moment from 'moment';
import { useHistory } from "react-router-dom";
import DepartureIcon from "../../assets/img/departure-icon.svg";
import Loader from "../../components/Loading/LoadingIcon";


function MyBookings() {
    let history = useHistory();
    const [bookingData, setBookingData] = useState([])
    const [loading, setLoading] = useState(true)
    const [completeData, setCompleteData] = useState([])
    const [isMobile, setMobile] = useState(false)

    const queryParams = new URLSearchParams(window.location.search)
    const [mybookingflag, setMybookingFlag] = useState(false)

    useEffect(() => {
        _isMobileScreen();
        initialApiBookingsDetails()
    }, [mybookingflag])

    const _isMobileScreen = () => {
        if (window.innerWidth <= 768) {
            setMobile(true);
            return;
        }
        setMobile(false);
    };

    const initialApiBookingsDetails = () => {
        const auth = JSON.parse(localStorage.getItem("auth_otp"))
        if(auth == undefined || auth == null) {
            history.push({
                pathname: '/',
            })
        } else {
        // fetch(`${process.env.REACT_APP_API_ENDPOINT}/api/bookings`,
        // fetch(`https://cordelia.fortelogic.in/api/bookings`,
        fetch(`https://cordeliaagent.fortelogic.in/api/bookings`,
            {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    // Authorization: `Bearer ${auth.auth.token}`,
                    Authorization: `Bearer ${auth.token}`,
                },
            }).then((res) => {
                if (res.status == 401) {
                    throw new Error("Something went wrong");
                }
                if (res.ok) {
                    // console.log('hdjshd')
                    return res.json();
                } else {
                    throw new Error("Something went wrong");
                }
            })
            .then((response) => {
                setBookingData(response.bookings);
                setCompleteData(response)
                setLoading(false)
                // setCompleteData(response)
                // setLoading(false);
            })
            .catch((error) => { });
        }
    }

    const handleViewDetails = (id) => {
        history.push({
            pathname: '/my-bookings-details',
            state: { id: id }
        })
    }

    return (
        <div className="Home">
            <Header bgPurple mybookingflag={mybookingflag} setMybookingFlag={setMybookingFlag} isMobile={isMobile} transparent={true} cruiseRoute={true} platform={queryParams.get("platform")} isBanner />
            <Container>
                {!loading && completeData.pagination.total_records == 0 && <p className={cx(styles.noDataFound, "text-center pt-4 pb-4")}>No Result Found</p>}
                {!loading && <h6 className={cx(styles.totalBookings, "mt-4 pl-3")}>Total {completeData.pagination.total_records} Bookings</h6>}
                {!loading && completeData.pagination.total_records > 0 &&
                    <div className={cx(styles.containerMain, "col-12 col-md-12 mt-2")}>
                        {bookingData.map((data) => {
                            return (
                                <div className={cx(styles.card, "col-lg-6 col-md-6 col-sm-12 mt-4 mb-4")}>
                                    <Card>
                                        <div>
                                            <div className={cx(styles.cruisenightContainer, "p-4 pt-4 pb-4")}>
                                                <div className={cx(styles.marginRemove, "col-12 col-md-12 row")}>
                                                    <div className={cx(styles.marginRemove, "col-6 pl-2")}>
                                                        <h5 className={cx(styles.nightHeader)}>{data.itinerary.nights} Night Cruise</h5>
                                                        <h6 className={cx(styles.subnightHeader)}>{moment(data.itinerary.start_time).format("dddd, D MMMM YYYY")}</h6>
                                                    </div>
                                                    <div className={cx(styles.marginRemove, "col-6")}>
                                                        <h5 className={data.status == "CONFIRMED" ? styles.status : styles.statusOrange}>{data.status}</h5>
                                                        <h6 className={cx(styles.bookingID)}>Booking ID: {data.number}</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className={cx(styles.portdetails, "col-12 p-2 pt-4 pb-4")}>
                                                <div className={cx(styles.borderStyle, "col-4 pb-4")}>
                                                    <p>Cruise Start</p>
                                                    <h5 className={styles.startPlace}>{data.itinerary.ports[0].city}</h5>
                                                    <h6 className={styles.startDate}>{moment(data.itinerary.start_time).format("ddd, D MMMM YYYY")}</h6>
                                                    <h5 className={styles.startTime}>{moment(data.itinerary.start_time).locale('en-in').format("LT")}</h5>
                                                    <h5 className={styles.startingPort}>{data.itinerary.ports[0].city}</h5>
                                                </div>
                                                <div className={cx(styles.borderStyle, "col-4 pb-4")}>
                                                    <div className={styles.ship}> <p className="pt-2" style={{ fontWeight: 'bold', color: '#EA725B' }}>- - -</p>
                                                        <img src={DepartureIcon} style={{ height: '30px', width: '30px', display: "flex", justifyContent: 'center' }} />
                                                        <p className="pt-2" style={{ fontWeight: 'bold', color: '#EA725B' }}>- - -</p></div>
                                                </div>
                                                <div className={cx(styles.borderStyle, "col-4 pb-4")}>
                                                    <div className={styles.alignLeftCon}>
                                                        <p>Cruise End</p>
                                                        <h5 className={styles.startPlace}>{data.itinerary.ports[data.itinerary.ports.length - 1].city}</h5>
                                                        <h6 className={styles.startDate}>{moment(data.itinerary.end_time).format("ddd, D MMMM YYYY")}</h6>
                                                        <h5 className={styles.endTime}>{moment(data.itinerary.end_time).locale('en-in').format("LT")}</h5>
                                                        <h5 className={styles.endingPort}>{data.itinerary.ports[data.itinerary.ports.length - 1].city}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className={cx(styles.portdetails, "col-12 col-md-12 bg-j-light-orange p-2")}>
                                                <div className={cx(styles.borderStyle, " col-12")}>
                                                    {data.rooms.map((roomsData, i) => {
                                                        return (
                                                            <div className={styles.innerBorderStyle, "row"}>
                                                                <div className={cx(styles.marginRemove, "col-6 pb-4")}>
                                                                    <h5 className={styles.roomType}>{roomsData.name}</h5>
                                                                    <h6 className={styles.roomNumber}>Room Number: {data.room_details[i].room_no}</h6>
                                                                </div>
                                                                <div className="col-6 pb-4">
                                                                    <h5 className={styles.roomType}>{data.room_details[i].adults_count + data.room_details[i].children_count + data.room_details[i].infants_count} Guests</h5>
                                                                    <h6 className={styles.roomNumber}>{data.room_details[i].adults_count + ` Adult(s)`}   |   {data.room_details[i].children_count + ` Children(s)`}   |   {data.room_details[i].infants_count + ` Infant(s)`}</h6>
                                                                </div>
                                                            </div>
                                                        )
                                                    })}
                                                </div>
                                            </div>
                                            <div className={cx(styles.portdetails, "col-12 col-md-12 bg-j-light-orange p-2")}>
                                                <div className={cx(styles.grandTotal, "col-6")}>
                                                    <h5 className={styles.grandTotal}>Grand total   ₹{Number(data.total_amount).toLocaleString()}</h5>
                                                </div>
                                                <div className={cx(styles.viewDetails, "col-6")}>
                                                    <button className={styles.buttonDetails} onClick={() => handleViewDetails(data.id)}>VIEW DETAILS</button>
                                                </div>
                                            </div>
                                        </div>
                                    </Card>
                                </div>
                            )
                        })}
                    </div>}
                {loading && <Loader />}
            </Container>
            <Footer />
        </div>
    )

}

export default MyBookings;