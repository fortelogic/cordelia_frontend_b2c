import React, { useState } from 'react'
import Header from "../../components/Header/HeaderHome"
import Container from "../../components/Container/index"
import Footer from "../../components/Footer/Footer"
import FooterButtonSection from "../../components/UI/FooterButtonSection"
import Banner from "../../assets/img/about-ship/banner.jpg"
import styles from "./cruise-deals.module.css"
import cx from "classnames";
import * as Section from '../../components/Section'
import { bestSellingCruises } from '../home/feed'
import ReactPlayer from "react-player/youtube"
import Slider from "react-slick"
import ReactHtmlParser from "react-html-parser"
import { Button } from "antd"


import PromotionBanner from "../../assets/img/cruise-deals/promotion_banner.webp"
import BalconyCard from "../../assets/img/cruise-deals/balcony_card.webp"
import KidsSaleFree from "../../assets/img/cruise-deals/kids-sale-free.png"
import OceanViewCard from "../../assets/img/cruise-deals/ocean_view_card.webp"
import BalconyBanner from "../../assets/img/cruise-deals/balcony_banner.webp"
import KidsSaleBanner from "../../assets/img/cruise-deals/kids-free-banner.png"
import OceanViewBanner from "../../assets/img/cruise-deals/ocean_view_card.webp"
import Tick from "../../assets/img/cruise-deals/tick.png"
import Suite from "../../assets/img/cruise-deals/suite-web.webp"

const InteriorTabs = () => {

    const [currentTab, setTab] = useState(0);

    return (<div className={cx(styles.interiorOfferGuestContent)}>
        <div className="row ml-1">
            <div onClick={() => { setTab(0) }} className={currentTab == 0 ? cx(styles.tabActive) : cx(styles.tab)}>
                <p className="text-center pt-4 px-3">Offer Details</p>
            </div>
            <div onClick={() => { setTab(1) }} className={currentTab == 1 ? cx(styles.tabActive) : cx(styles.tab)}>
                <p className="text-center pt-4 px-3">Free Rescheduling</p>
            </div>
            <div onClick={() => { setTab(2) }} className={currentTab == 2 ? cx(styles.tabActive) : cx(styles.tab)}>
                <p className="text-center pt-4 px-3">Cancellation</p>
            </div>
        </div>
        <div className={cx(styles.tabsContent)}>
            {currentTab == 0 && <div>
                <ul className="pt-3">
                    <li className={cx(styles.listitem)}>Limited inventory per sailing. Valid only on first-come, first-served basis. </li>
                    <li className={cx(styles.listitem)}>Offer is valid only for 5 Night sailing.</li>
                    <li className={cx(styles.listitem)}>Booking Validity of this offer till 31st October 2021.</li>
                    <li className={cx(styles.listitem)}>This offer is valid on all stateroom categories.</li>
                    <li className={cx(styles.listitem)}>Valid on sailing till 15th December 2021.</li>
                </ul>
            </div>}
            {currentTab == 1 && <div>
                <ul className="pt-3">
                    <li className={cx(styles.listitem)}>Free rescheduling can be availed only once per booking.</li>
                    <li className={cx(styles.listitem)}>Rescheduling of the booking for more than two times will be chargeable at Rs 5000 per cabin.</li>
                    <li className={cx(styles.listitem)}>The rescheduled itinerary must commence on or before 31st March 2022.</li>
                    <li className={cx(styles.listitem)}>Fare difference, if any will be payable by the customer.</li>
                    <li className={cx(styles.listitem)}>GST will be applicable on any payable amount.</li>
                    <li className={cx(styles.listitem)}>Name change not allowed</li>
                </ul>
                <table className="table table-responsive ml-4 pl-3">
                    <tr className={cx(styles.tableHeader)}>
                        <th>Days Of Departure</th>
                        <th>Reshceduling Fee</th>
                    </tr>
                    <tr>
                        <td>16 days and above</td>
                        <td>NIL Fee + Fare difference if any</td>
                    </tr>
                    <tr>
                        <td>0 to 15 days</td>
                        <td>INR 5000 per stateroom</td>
                    </tr>
                </table>
            </div>}
            {currentTab == 2 && <div>
                <ul className="pt-3">
                    <li className={cx(styles.listitem)}>Cancellation fee will be applicable only on cabin fare.</li>
                    <li className={cx(styles.listitem)}>Port charges & gratuity will be refunded completely.</li>
                    <li className={cx(styles.listitem)}>GST for the refunded amount will also be returned.</li>
                    <li className={cx(styles.listitem)}>Refund will be processed within 10 working days from the cancellation date.</li>
                    <li className={cx(styles.listitem)}>Complete Refund will be made if sailing cancelled due to unforeseen circumstances.</li>
                </ul>
                <table className="table table-responsive ml-4 pl-3">
                    <tr className={cx(styles.tableHeader)}>
                        <th className="col-sm-3">Cancellation Requested</th>
                        <th>Cancellation Fee for Full Pay</th>
                    </tr>
                    <tr>
                        <td>31 days and above to departure</td>
                        <td>25% of stateroom fare</td>
                    </tr>
                    <tr>
                        <td>16 to 30 days to departure</td>
                        <td>50% of stateroom fare</td>
                    </tr>
                    <tr>
                        <td>0 to 15 days to departure</td>
                        <td>100% of total fare</td>
                    </tr>
                    <tr>
                        <td>For No-Show</td>
                        <td>100% of total fare</td>
                    </tr>
                </table>
            </div>}
        </div>
    </div>)

}


const BookNowTabs = () => {

    const [currentTab, setTab] = useState(0);

    return (<div className={cx(styles.interiorOfferGuestContent)}>
        <div className="row ml-1">
            <div onClick={() => { setTab(0) }} className={currentTab == 0 ? cx(styles.tabActive) : cx(styles.tab)}>
                <p className="text-center pt-4 px-3">Offer Details</p>
            </div>
            <div onClick={() => { setTab(1) }} className={currentTab == 1 ? cx(styles.tabActive) : cx(styles.tab)}>
                <p className="text-center pt-4 px-3">Free Rescheduling</p>
            </div>
            <div onClick={() => { setTab(2) }} className={currentTab == 2 ? cx(styles.tabActive) : cx(styles.tab)}>
                <p className="text-center pt-4 px-3">Cancellation</p>
            </div>
        </div>
        <div className={cx(styles.tabsContent)}>
            {currentTab == 0 && <div>
                <ul className="pt-3">
                    <li className={cx(styles.listitem)}>Book with just 25% now, and pay the rest 30 days prior to your sailing date.</li>
                    <li className={cx(styles.listitem)}>The offer is valid on all bookings made till 31st October 2021.</li>
                    <li className={cx(styles.listitem)}>Valid on all sailings.</li>
                </ul>
            </div>}
            {currentTab == 1 && <div>
                <ul className="pt-3">
                    <li className={cx(styles.listitem)}>Free rescheduling can be availed only once per booking.</li>
                    <li className={cx(styles.listitem)}>Rescheduling of the booking for more than two times will be chargeable at Rs 5000 per cabin.</li>
                    <li className={cx(styles.listitem)}>The rescheduled itinerary must commence on or before 31st March 2022.</li>
                    <li className={cx(styles.listitem)}>Fare difference, if any will be payable by the customer.</li>
                    <li className={cx(styles.listitem)}>GST will be applicable on any payable amount.</li>
                    <li className={cx(styles.listitem)}>Name change not allowed</li>
                </ul>
                <table className="table table-responsive ml-4 pl-3">
                    <tr className={cx(styles.tableHeader)}>
                        <th>Days Of Departure</th>
                        <th>Reshceduling Fee</th>
                    </tr>
                    <tr>
                        <td>16 days and above</td>
                        <td>NIL Fee + Fare difference if any</td>
                    </tr>
                    <tr>
                        <td>0 to 15 days</td>
                        <td>INR 5000 per stateroom</td>
                    </tr>
                </table>
            </div>}
            {currentTab == 2 && <div>
                <ul className="pt-3">
                    <li className={cx(styles.listitem)}>Cancellation fee will be applicable only on cabin fare.</li>
                    <li className={cx(styles.listitem)}>Port charges & gratuity will be refunded completely.</li>
                    <li className={cx(styles.listitem)}>GST for the refunded amount will also be returned.</li>
                    <li className={cx(styles.listitem)}>Refund will be processed within 10 working days from the cancellation date.</li>
                    <li className={cx(styles.listitem)}>Complete Refund will be made if sailing cancelled due to unforeseen circumstances.</li>
                </ul>
                <table className="table table-responsive ml-4 pl-3">
                    <tr className={cx(styles.tableHeader)}>
                        <th className="col-sm-3">Cancellation Requested</th>
                        <th>Cancellation Fee for Full Pay</th>
                    </tr>
                    <tr>
                        <td>31 days and above to departure</td>
                        <td>25% of stateroom fare</td>
                    </tr>
                    <tr>
                        <td>16 to 30 days to departure</td>
                        <td>50% of stateroom fare</td>
                    </tr>
                    <tr>
                        <td>0 to 15 days to departure</td>
                        <td>100% of total fare</td>
                    </tr>
                    <tr>
                        <td>For No-Show</td>
                        <td>100% of total fare</td>
                    </tr>
                </table>
            </div>}
        </div>
    </div>)

}

const KidsSailFreeTabs = () => {

    const [currentTab, setTab] = useState(0);

    return (<div className={cx(styles.interiorOfferGuestContent)}>
        <div className="row ml-1">
            <div onClick={() => { setTab(0) }} className={currentTab == 0 ? cx(styles.tabActive) : cx(styles.tab)}>
                <p className="text-center pt-4 px-3">Offer Details</p>
            </div>
            <div onClick={() => { setTab(1) }} className={currentTab == 1 ? cx(styles.tabActive) : cx(styles.tab)}>
                <p className="text-center pt-4 px-3">Offer T&C</p>
            </div>
        </div>
        <div className={cx(styles.tabsContent)}>
            {currentTab == 0 && <div>
                <ul className="pt-3">
                    <li className={cx(styles.listitem)}>Kids up to 12 years of age can sail free on any of our cruises till February 2022.</li>
                    <li className={cx(styles.listitem)}>The offer is valid on all cruise bookings made till 31st October 2021.</li>
                    <li className={cx(styles.listitem)}>The offer is valid on all sailings February 2022.</li>
                </ul>
            </div>}
            {currentTab == 1 && <div>
                <ul className="pt-3">
                    <li className={cx(styles.listitem)}>The offer is valid on all sailings till February 2022.</li>
                    <li className={cx(styles.listitem)}>Port charges, gratuity and taxes as mandated by the government are applicable as per the itinerary.</li>
                    <li className={cx(styles.listitem)}>The child must be accompanied by a parent.</li>
                    <li className={cx(styles.listitem)}>Any additional onboard experiences which are not included in the package will be chargeable.</li>
                    <li className={cx(styles.listitem)}>Port charges, gratuity and GST is applicable for kids between 2-12 years.</li>
                    <li className={cx(styles.listitem)}>For infants ( 6 months to 2 years) port charges and GST applicable.</li>
                    <li className={cx(styles.listitem)}>Name change not allowed.</li>
                    <li className={cx(styles.listitem)}>This offer is applicable only when childs are 3rd and 4th pax in the cabin.</li>
                </ul>
            </div>}
        </div>
    </div>)

}


const CruiseDeals = () => {

    const queryParams = new URLSearchParams(window.location.search)

    return (
        <div className="Home">
            <Header bgPurple platform={queryParams.get("platform")} />
            <div className={cx(styles.promotionBanner, "text-center")}>
                <img src={PromotionBanner} />
                <div className={cx(styles.promotionBannerContent)}>
                    <h1 className={cx(styles.promotionBannerHeading)}> SAVE BIG ON 2021-22 SAILINGS </h1>
                    <br />
                    <div className={cx(styles.promotionBannerSpecification)}>
                        <p className={cx(styles.promotionBannerTextOne)}>
                            Free Stateroom Upgrade* {" "}
                        </p>
                        <span className={cx(styles.promotionBannerTextSeperatorOne)}>|</span>
                        <p className={cx(styles.promotionBannerTextTwo)}>
                            {" "}
                            Book Now. Pay Later*{" "}
                        </p>
                        <span className={cx(styles.promotionBannerTextSeperatorTwo)}>|</span>
                        <p className={cx(styles.promotionBannerTextThree)}>
                            Kids Sail Free*{" "}
                        </p>
                    </div>
                </div>
            </div>
            <div className={cx(styles.offerKnowBox, "text-center")}>
                <p className={cx(styles.offerKnowBoxText)}>To know more about offers, call <strong>022 68811111</strong> or share your details below.</p>
                <div className={cx(styles.offerKnowBoxForm)}>
                    <input className={cx(styles.offerKnowBoxFullname)} placeholder="Fullname" />
                    <input className={cx(styles.offerKnowBoxContactNumber)} placeholder="Phone no" />
                    <input className={cx(styles.offerKnowBoxEmailAddress)} placeholder="Email address" />
                    <Button className={cx(styles.offerKnowBoxSubmitButton)} type="primary">SUBMIT</Button>
                </div>
                <div className={cx(styles.offerKnowBoxCheckbox)}>
                    <input className={cx(styles.offerKnowBoxCheckboxInput)} type="checkbox" />
                    <span className={cx(styles.offerKnowBoxCheckboxText)}>I authorize Cordelia Cruises to contact me with information regarding this query.</span>
                </div>
            </div>
            <div className={cx(styles.dealsOffersSection, "text-center")}>
                <h1 className={cx(styles.dealsOffersSectionHeading)}>OUR BEST CRUISE <strong>DEALS & EXCLUSIVE OFFERS</strong></h1>
                <p className={cx(styles.dealsOffersSectionParaOne)}>It’s time to plan your long-awaited getaway with our fabulous cruise deals and offers. Choose a holiday and a destination that fits your budget, and cruise confidently with our special offers and launch fares. From payment flexibility to free sailings for kids, make the best use of our incredible cruise deals. Change in plan post booking? Don’t worry! Enjoy free cancellations and free rescheduling. Whether it’s a family cruise holiday, or a getaway with friends, there are Cordelia Cruise deals for every occasion.</p>
                <p className={cx(styles.dealsOffersSectionParaTwo)}>Our unparalleled service starts from providing you with the best deals during your booking to the best experiences on your holiday.</p>
                <h2 className={cx(styles.sailingStartFrom)}>Sailing starts from 18th September 2021.</h2>
                <div className={cx(styles.sailingPhotos, "row")}>
                    <div className={cx("col mx-2")} style={{ background: `url(${OceanViewCard})`, height: "393px", backgroundSize: "cover", borderRadius: "10px", position: "relative" }}>
                        <div className={cx(styles.sailingPhotoContent)}>
                            <h1 className={cx(styles.sailingPhotoContentHeading)}>
                                FREE STATEROOM UPGRADE*
                            </h1>
                            <p className={cx(styles.sailingPhotoContentPara)}>Exclusive offer for 5 night sailings</p>
                            <Button
                                type="primary"
                                size="large"
                                className="col explore-more-btn"
                                style={{ height: "40px", width: "150px" }}
                            >
                                <b>Know More</b>
                            </Button>
                        </div>
                    </div>
                    <div className={cx("col mx-2")} style={{ background: `url(${BalconyCard})`, height: "393px", backgroundSize: "cover", borderRadius: "10px" }}>
                        <div className={cx(styles.sailingPhotoContent)}>
                            <h1 className={cx(styles.sailingPhotoContentHeading)}>
                                BOOK NOW. PAY LATER*
                            </h1>
                            <p className={cx(styles.sailingPhotoContentPara)}>Flexible payment & Free Rescheduling</p>
                            <Button
                                type="primary"
                                size="large"
                                className="col explore-more-btn"
                                style={{ height: "40px", width: "150px" }}
                            >
                                <b>Know More</b>
                            </Button>
                        </div>
                    </div>
                    <div className={cx("col mx-2")} style={{ background: `url(${KidsSaleFree})`, height: "393px", backgroundSize: "cover", borderRadius: "10px" }}>
                        <div className={cx(styles.sailingPhotoContent)}>
                            <h1 className={cx(styles.sailingPhotoContentHeading)}>
                                KIDS SAIL FREE*
                            </h1>
                            <p className={cx(styles.sailingPhotoContentPara)}>A deal you shouldn’t miss</p>
                            <Button
                                type="primary"
                                size="large"
                                className="col explore-more-btn"
                                style={{ height: "40px", width: "150px" }}
                            >
                                <b>Know More</b>
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
            <div className={cx(styles.cruiseConfidently, "text-center")}>
                <h1 className={cx(styles.cruiseConfidentlyHeading)}>CRUISE <strong>CONFIDENTLY</strong></h1>
                <h2 className={cx(styles.cruiseConfidentlySubHeading)}>HAPPY WAVES PROGRAM</h2>
                <p className={cx(styles.cruiseConfidentlyParaOne)}> We want you to stay safe and cruise confidently with us. Your safety is our utmost priority. In circumstances where our guests are unable to travel if they are tested positive for Covid-19, or are required to undergo quarantine if a non-traveling family member is tested positive, free rescheduling can be availed. The booking has to be rescheduled at least 48 hours prior to the departure and is subject to fare difference and availability. </p>
                <p className={cx(styles.cruiseConfidentlyParaTwo)}> Guests are required to provide valid authenticate medical certificates/documents to avail this benefit. </p>
            </div>

            <div className={cx(styles.interiorOfferGuest, "text-left")}>
                <h1 className={cx(styles.interiorOfferGuestHeading)}>OFFER FOR INTERIOR GUESTS</h1>
                <p className={cx(styles.interiorOfferGuestParaOne)}> Our Interior stateroom guests are eligible for incredible offers when they book their cruise holidays with full payment. Choose your preferred cruise, make the full payment, and get upgraded from Interior stateroom to Ocean View stateroom for free. Enjoy an Ocean View stateroom at the cost of an Interior room. Cruise deals can’t get better than this! </p>
                <div className={cx(styles.interiorOfferGuestImageAndContent)}>
                    <img className={cx(styles.interiorOfferGuestImage)} src={OceanViewBanner} />
                    <InteriorTabs />
                </div>
            </div>

            <div className={cx(styles.interiorOfferGuest, "text-left")}>
                <h1 className={cx(styles.interiorOfferGuestHeading)}>BOOK NOW. PAY LATER</h1>
                <p className={cx(styles.interiorOfferGuestParaOne)}> We want nothing but the best for you. Reserve your cruise holiday booking with just 25% now, and get the convenience and flexibility to pay the remaining amount 30 days before your sailing date. Enjoy complete peace of mind with the advantage of availing free rescheduling for your booking </p>
                <div className={cx(styles.interiorOfferGuestImageAndContent)}>
                    <img className={cx(styles.interiorOfferGuestImage)} src={BalconyBanner} />
                    <BookNowTabs />
                </div>
            </div>

            <div className={cx(styles.interiorOfferGuest, "text-left")}>
                <h1 className={cx(styles.interiorOfferGuestHeading)}>KIDS SAIL FREE</h1>
                <p className={cx(styles.interiorOfferGuestParaOne)}> Your kids deserve a holiday. A family holiday with Cordelia Cruises is a great idea. We’ve got the best travel deals and vacation packages for your cruise holiday. Kids can sail free onboard Cordelia Cruises. Isn’t it an incredible offer? </p>
                <div className={cx(styles.interiorOfferGuestImageAndContent)}>
                    <img className={cx(styles.interiorOfferGuestImage)} src={KidsSaleBanner} />
                    <KidsSailFreeTabs />
                </div>
            </div>
            <div className={cx(styles.privilegedSuite)}>
                <h1 className={cx(styles.privilegedSuiteHeading)}>EXCLUSIVE PRIVILEGES FOR CORDELIA SUITE GUESTS</h1>
                <div className={cx(styles.privilegedSuiteBlocks)}>
                    <div className={cx(styles.privilegedSuiteBlockOne)}>
                        <img src={Suite} />
                    </div>
                    <div className={cx(styles.privilegedSuiteBlockTwo, "text-center")}>
                        <p className={cx(styles.privilegedSuiteBlockTwoPara)}>Set sail on an amazing cruise holiday amidst pure luxury. Holiday in style and choose from one of our six exclusive state-of-the-art onboard suites to get pampered with the best experiences and services.</p>
                        <div className={cx(styles.privilegedSuiteBlockTwoChecklist)}>
                            <div style={{ display: "flex" }}>
                                <div className={cx(styles.privilegedSuiteCheckbox)}>
                                    <img src={Tick} />
                                </div>
                                <p className={cx(styles.privilegedSuiteChecklistText)}>
                                    Priority embarkation and disembarkation at the beginning and at the end of the cruise.
                                </p>
                            </div>
                            <div style={{ display: "flex" }}>

                                <div className={cx(styles.privilegedSuiteCheckbox)}>
                                    <img src={Tick} />
                                </div>
                                <p className={cx(styles.privilegedSuiteChecklistText)}>
                                    Priority embarkation and disembarkation at the beginning and at the end of the cruise.
                                </p>
                            </div>
                            <div style={{ display: "flex" }}>
                                <div className={cx(styles.privilegedSuiteCheckbox)}>
                                    <img src={Tick} />
                                </div>
                                <p className={cx(styles.privilegedSuiteChecklistText)}>
                                    Priority embarkation and disembarkation at the beginning and at the end of the cruise.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <FooterButtonSection />
            {/* <Footer /> */}
            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
            }
        </div>
    )
}

export default CruiseDeals
