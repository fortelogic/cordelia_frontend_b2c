import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getRoutes, filterRoutes } from '../../redux/actions/routeAction';
import NotificationBanner from '../../components/NotificationBanner/NotificationBanner';
import Header from '../../components/Header/HeaderHome';
import SearchComponent from '../../components/SearchComponent/SearchComponent';
import Footer from '../../components/Footer/Footer';
import CruiseRoutesCards from '../../components/Cards/CruiseRoutesCards';
import './index.scss';
import Loader from '../../components/Loading/LoadingIcon';
import Item from 'antd/lib/list/Item';
import mobileBanner from '../../assets/img/mobile-menu/logout.svg';
import Container from '../../components/Container';
import LocalStore from '../../utils/localStore'


function CruiseRoutes() {
  const dispatch = useDispatch();
  const defaultSort = { value: 'nightCount', type: 'asc' }
  const [routes, setRoutes] = useState([])
  const [filteredData, setFilteredData] = useState(routes)
  const [ports, setPorts] = useState([])
  const [selectedTags, setselectedTags] = useState([])
  const [sortBy, setSortBy] = useState(defaultSort)
  const [isLoading, setIsLoading] = useState(true)
  const [disabledTags, setDisabledTags] = useState([])
  const { routeData } = useSelector((state) => ({
    routeData: state.route.routeData,
  }));
  const [isMobile, setMobile] = useState(false)
  const queryParams = new URLSearchParams(window.location.search)

  let filterData = {
    sailingFrom: queryParams.get("sailingFrom"),
    sailingTo: queryParams.get("sailingTo"),
    month: queryParams.get("month")
  }

  //fetch data on first time render
  useEffect(async () => {
    window.scrollTo(0, 0);
    setIsLoading(true);
    await dispatch(getRoutes({ defaultSort }))
    setIsLoading(false)
  }, []);

  //Run when data is fetched (create data here)
  useEffect(async () => {
    if (routeData) {
      const portHash = {};
      routeData.forEach((route) => {
        route.ports.forEach((item) => {
          if (!portHash[item.port.code]) {
            portHash[item.port.code] = item;
          }
        });
      });
      const portOptions = Object.values(portHash).sort((a, b) => {
        return a.port.name < b.port.name ? -1 : 1;
      });
      setPorts(portOptions);
      setRoutes(routeData);
      setFilteredData(routeData);

      if (filterData.sailingFrom || filterData.sailingTo) {
        preSearchHandler(filterData)
      }

      if (queryParams.get("token") !== null && queryParams.get("token") !== undefined && queryParams.get("token_expiry") !== undefined && queryParams.get("token_expiry") !== null) {
        LocalStore.setItem("auth", { token: queryParams.get("token"), exp: queryParams.get("token_expiry") })
      }
    }
  }, [routeData]);

  //Run when sorting is applied
  useEffect(() => {
    const sortedData = getSortedData(filteredData)
    setFilteredData(sortedData)
  }, [sortBy]);

  React.useEffect(() => {
    _isMobileScreen();
    window.addEventListener('resize', _isMobileScreen);
    return () => {
      window.removeEventListener('resize', _isMobileScreen);
    };
  }, []);

  const _isMobileScreen = () => {
    if (window.innerWidth <= 768) {
      setMobile(true);
      return;
    }
    setMobile(false);
  };

  const onFilterApplied = (nightCountTags) => {
    const sortedData = getSortedData(routes);
    if (nightCountTags.length != 0) {
      setFilteredData(
        sortedData.filter((route) => nightCountTags.includes(route.itineraries[0].nightCount.toString()))
      );
    } else {
      setFilteredData(sortedData);
    }
  };

  const searchHandler = (filter) => {
    const { month, sailingFrom, sailingTo } = filter;
    let searchedResult = (
      month.trim().length
        ? routeData.map((route) => {
          const routeList = { ...route };
          routeList.itineraries = [...routeList.itineraries].filter(
            (itineary) => {
              return (
                new Date(month).getMonth() ===
                new Date(itineary.startTime).getMonth()
              );
            }
          );
          return routeList;
        })
        : routeData
    ).filter((route) => {
      if (route.itineraries.length > 0) {
        const transit = route.itineraries[0].ports.filter(data => data.type == "TRANSIT");
        const [origin, destination] = route.itineraries[0].ports.map((item) => (item.type === 'ORIGIN' || item.type === 'DESTINATION') && item.port.name).filter((x) => x);
        if (sailingFrom && sailingTo) {
          // return origin === sailingFrom.port.name && destination === sailingTo.port.name;

          let flag = [];
          transit.map(data => {
            if (data.port.name == sailingFrom.port.name) {
              flag.push(true);
            }
          })

          return origin === sailingFrom.port.name && destination === sailingTo.port.name || flag.includes(true);

        } else if (sailingFrom) {
          // return origin === sailingFrom.port.name || transit.includes(sailingFrom.port.name);
          let flag = [];
          transit.map(data => {
            if (data.port.name == sailingFrom.port.name) {
              flag.push(true);
            }
          })
          return origin === sailingFrom.port.name || flag.includes(true);
        } else if (sailingTo) {

          let flag = [];
          transit.map(data => {
            if (data.port.name == sailingTo.port.name) {
              flag.push(true);
            }
          })
          return destination === sailingTo.port.name || flag.includes(true);
        } else {
          return true;
        }
      }
      return false;
    });
    setSortBy(defaultSort);
    setselectedTags([]);
    setRoutes(searchedResult);
    setFilteredData(searchedResult);
  };

  const preSearchHandler = (filter) => {
    const { month, sailingFrom, sailingTo } = filter;
    let searchedResult = (
      month.trim().length
        ? routeData.map((route) => {
          const routeList = { ...route };
          routeList.itineraries = [...routeList.itineraries].filter(
            (itineary) => {
              return (
                new Date(month).getMonth() ===
                new Date(itineary.startTime).getMonth()
              );
            }
          );
          return routeList;
        })
        : routeData
    ).filter((route) => {
      if (route.itineraries.length > 0) {
        const transit = route.itineraries[0].ports.filter(data => data.type == "TRANSIT");

        const [origin, destination] = route.itineraries[0].ports
          .map((item) => (item.type === 'ORIGIN' || item.type === 'DESTINATION') && item.port.name)
          .filter((x) => x);

        if (sailingFrom && sailingTo) {

          let flag = [];
          transit.map(data => {
            if (data.port.name == sailingFrom) {
              flag.push(true);
            }
          })

          return origin === sailingFrom && destination === sailingTo || flag.includes(true);
        } else if (sailingFrom) {
          let flag = [];
          transit.map(data => {
            if (data.port.name == sailingFrom) {
              flag.push(true);
            }
          })
          return origin === sailingFrom || flag.includes(true);
        } else if (sailingTo) {
          let flag = [];
          transit.map(data => {
            if (data.port.name == sailingTo) {
              flag.push(true);
            }
          })
          return destination === sailingTo || flag.includes(true);
        } else {
          return true;
        }
      }
      return false;
    })
    setSortBy(defaultSort)
    setselectedTags([])
    setRoutes(searchedResult)
    setFilteredData(searchedResult)
  };

  const getSortedData = (data) => {
    return (
      data &&
      [...data].sort((a, b) => {
        if (sortBy.type === 'asc') {
          return a.itineraries[0].nightCount < b.itineraries[0].nightCount ? -1 : 1;
        } else {
          return a.itineraries[0].nightCount < b.itineraries[0].nightCount ? 1 : -1;
        }
      })
    );
  };

  return (
    <div className="Home">
      <NotificationBanner />
      {
        queryParams.get("platform") && queryParams.get("platform") == "true" ?
          <Header isMobile={isMobile} transparent={true} cruiseRoute={true} platform={queryParams.get("platform")} />
          : <Header isMobile={isMobile} transparent={true} cruiseRoute={true} platform={queryParams.get("platform")} isBanner />
      }
      {
        queryParams.get("platform") && queryParams.get("platform") == "true" ?
          `` : <SearchComponent titleText={true} preFilledData={filterData} platform={queryParams.get("platform")} onSearch={searchHandler} ports={ports} style={{ marginTop: isMobile ? '-5rem' : '-3rem' }} />
      }
      {/* {!isLoading && filteredData.length === 0 ? `` :
        <CruiseRoutesCards
          filterBy={onFilterApplied}
          sortBy={setSortBy}
          defaultSort={sortBy}
          selectedTags={selectedTags}
          onTagSelect={setselectedTags}
          isLoading={isLoading}
        />
      } */}
      {
        filteredData.length > 0 ?
          <CruiseRoutesCards
            filterBy={onFilterApplied}
            sortBy={setSortBy}
            defaultSort={sortBy}
            selectedTags={selectedTags}
            onTagSelect={setselectedTags}
            isLoading={isLoading}
            filteredData={filteredData}
            disabledTags={disabledTags}
            setDisabledTags={setDisabledTags}
            queryParams={queryParams}
          />
          :
          <CruiseRoutesCards
            filterBy={onFilterApplied}
            sortBy={setSortBy}
            defaultSort={sortBy}
            selectedTags={selectedTags}
            onTagSelect={setselectedTags}
            isLoading={isLoading}
            filteredData={`NO DATA`}
            disabledTags={disabledTags}
            setDisabledTags={setDisabledTags}
            queryParams={queryParams}
          />
      }

      {!isLoading ? <CruiseRoutesCards.TravelItineraryCards queryParams={queryParams} platform={queryParams.get("platform")} routeData={filteredData} /> : <Loader />}
      {!isLoading && filteredData.length === 0 ? <div className="row justify-content-center p-5 m-5"><h2>No Cruise Found</h2></div> : ``}
      {
        (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
      }
      {/* <Footer /> */}
    </div>
  );
}

export default CruiseRoutes;
