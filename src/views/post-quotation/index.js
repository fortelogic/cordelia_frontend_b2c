import React from 'react'
import Header from "../../components/Header/HeaderHome"
import Container from "../../components/Container/index"
import Footer from "../../components/Footer/Footer"
import FooterButtonSection from "../../components/UI/FooterButtonSection"
import styles from "./postquotation.module.css"
import cx from "classnames"
import Slider from "react-slick"
import ReactHtmlParser from "react-html-parser"
import { Button } from "antd"
import Modal from "../../components/Modal"
import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css'
import axios from "axios"
import { filterWithDatesService } from "../../services/staff-portal/routes"
import LocalStorage from "../../utils/localStore"



function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

const PostQuotation = () => {


    const queryParams = new URLSearchParams(window.location.search)
    const [dateRange, setDateRange] = React.useState([new Date(), new Date()])
    const [showModal, setModalVisibility] = React.useState(false)

    const filterWithDates = async () => {

        const auth = LocalStorage.getItem("auth")

        // const allroutes = axios.get(`https://staff.staging.cordeliacruises.com/staff_portal/routes?start_date=${formatDate(dateRange[0])}&to_date=${formatDate(dateRange[1])}`, {
        const allroutes = axios.get(`https://cordeliaagent.fortelogic.in/staff_portal/routes?start_date=${formatDate(dateRange[0])}&to_date=${formatDate(dateRange[1])}`, {
            headers: {
                Authorization: `Bearer ${auth.token}`,
            }
        })
    }

    return (
        <div>
            <Header bgPurple platform={queryParams.get("platform")} />

            <div className={cx(styles.postQuotation)}>
                <div className="row">
                    <div className="col">
                        <select id="inputState" className="form-control">
                            <option selected>Select Country</option>
                            <option>India</option>
                        </select>
                    </div>
                    <div className="col">
                        <select id="inputState" className="form-control">
                            <option selected>Select State</option>
                            <option>Maharashtra</option>
                        </select>
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" placeholder="City" />
                    </div>
                </div>

                <div className="row mt-3">
                    <div className="col">
                        <input type="text" className="form-control" placeholder="Inquiry No" />
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" placeholder="Firstname" />
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" placeholder="Lastname" />
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" placeholder="Contact no" />
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" placeholder="Email id" />
                    </div>
                </div>

                <div className="row mt-3">
                    <div className={cx(styles.inputRow)}>
                        <label>Lead Status</label>
                        <select className="form-control">
                            <option>2nd-Attempt</option>
                            <option>3rd-Attempt</option>
                            <option>Quote sent</option>
                            <option>Hot Lead</option>
                            <option>General Enquiry</option>
                            <option>Follow up</option>
                            <option>Payment link sent</option>
                            <option>Quotation Pending</option>
                            <option>Call not answering</option>
                            <option>Call not contactable</option>
                            <option>Potential - No Responses</option>
                            <option>Booked</option>
                            <option>Price High</option>
                            <option>Duplicate</option>
                            <option>Junk</option>
                            <option>Lost of competition</option>
                            <option>Lost of competition</option>
                            <option>Plan Cancelled</option>
                            <option>One way travel</option>
                            <option>Future travel</option>
                            <option>Old Reschedule</option>
                        </select>
                    </div>
                    <div className="col-2">
                        <label>Priority</label>
                    </div>
                    <div className="col-2">
                        <select className="form-control">
                            <option>P0</option>
                            <option>P1</option>
                            <option>P2</option>
                            <option>P3</option>
                        </select>
                    </div>
                    <div className="col">
                        <label>Call back</label>
                    </div>
                    <div className="col">
                        <input type="date" className="form-control" value={new Date()} />
                    </div>
                    <div className="col">
                        <input type="time" className="form-control" />
                    </div>
                </div>

                <h5 className="mt-5">Production Details</h5>
                <p>Quotation</p>
                <div className="row align-center">
                    <div className="">
                        <label>Date of travel</label>
                        {/* <p className={cx(styles.textPickDateRange)} data-toggle="modal" data-target="#exampleModal">Pick date range</p> */}
                        <button type="button" className={cx(styles.textPickDateRange, "btn btn-primary")} onClick={() => setModalVisibility(true)}>Pick date range</button>
                    </div>
                    <div className="d-flex align-items-center px-5 ">
                        <label className="pt-2">Nights:</label>
                        <input type="radio" className="mx-1" />2N
                        <input type="radio" className="mx-1" />3N
                        <input type="radio" className="mx-1" />4N
                        <input type="radio" className="mx-1" />5N
                    </div>
                </div>
                <div className="row align-center">
                    <div className="d-flex align-items-center mr-5">
                        <label className="pt-2">Itineraries:</label>
                        <input type="radio" className="mx-1" />Visit Lakshadweep, At Sea, Mumbai from Kochi
                    </div>
                </div>

                <Modal show={showModal}>
                    <div className="card-body px-3 position-relative mt-2">
                        <h5>Which Date You Want To Travel?</h5>
                        <Calendar selectRange={true} value={dateRange} minDate={new Date()} onChange={e => setDateRange(e)} />
                        <button className={cx(styles.okButtonForDateSelection, "btn btn-default")} onClick={() => { setModalVisibility(false); filterWithDates() }}>OK</button>
                    </div>
                </Modal>
            </div>

            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
            }
        </div >
    )
}

export default PostQuotation
