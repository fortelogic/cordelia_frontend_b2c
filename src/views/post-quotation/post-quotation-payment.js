import { useState, useEffect, createElement } from "react"
import { useDispatch } from "react-redux"
import axios from "axios"
import { useHistory } from "react-router-dom"
import { useSelector } from "react-redux"
import Header from "../../components/Header/HeaderHome";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container";
import FooterButtonSection from "../../components/UI/FooterButtonSection";
import styles from "./post-quotation-payment.module.css"
import cx from "classnames"
import { postQuotationBooking } from "../../services/staff-portal/post-quotation-booking"
import { bookingsummary } from "../../redux/actions/roomAction"
import CountryCode from "../../components/Login/countryCodes.json"
import { getPaymentDetails } from "../../services/payment"
import localStore from "../../utils/localStore";

const PostQuotationPayment = () => {

    const dispatch = useDispatch()
    const [haveGst, SetHaveGst] = useState(1)
    const [gstin, SetGstin] = useState("")
    const [haveGstName, setGstName] = useState("")

    const queryParams = new URLSearchParams(window.location.search)
    const [user, setUserDetails] = useState([{ firstname: "", lastname: "", countrycode: "", phone: "", email: "" }])
    const [cancellationPanel, setCancellationPanel] = useState(false)
    const [personalDetails, setPersonalDetailsPanel] = useState(false)
    const [gstDetails, setGstDetailPanel] = useState(true)
    const [paymentOption, setPaymentOption] = useState("none")

    const booking = useSelector((state) => state.roomReducer.bookingsummary)

    useEffect(async () => {

        const url = queryParams.get("auth")
        const token = url.replace(/\ /g, '+');
        const requestOptions = {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ 'quote_tok': token })
        }

        // await fetch(`https://staging.cordeliacruises.com/api/quotations/${queryParams.get("bookId")}/booking`, requestOptions)
        await fetch(`https://cordeliaagent.fortelogic.in/api/quotations/${queryParams.get("bookId")}/booking`, requestOptions)
            .then((res) => {
                if (res.status === 401) {
                    localStorage.clear();
                }
                if (res.ok) {
                    return res.json();
                } else {
                    throw new Error("Something went wrong");
                }
            }).then((response) => {
                if (response.status == 'failed') {
                    console.log("No Data")
                } else {
                    dispatch(bookingsummary(response))
                }
            })
    }, [])

    const addFormData = (e) => {
        let values = [...user]
        values[0][e.target.name] = e.target.value
        setUserDetails(values)
    }

    const paymentPostQuotation = async () => {

        localStore.add("user", user)
        const params = {
            "variables": {
                "input": {
                    "amount": paymentOption !== "none" ? (paymentOption !== "BookBy100" ? booking.partial_payable_amount : booking.total) : booking.total,
                    "payment_option_id": booking.payment_option_id,
                    "contact": {
                        "first_name": user[0].firstname,
                        "last_name": user[0].lastname,
                        "email": user[0].email,
                        "phoneNumber": user[0].countrycode + " " + user[0].phone,
                        "gstin": null,
                        "gstin_name": haveGstName != '' ? haveGstName : null,
                        "gstin_phone": null,
                        "pan": null,
                        "pan_name": null
                    },
                    "paymentInfo": {
                        "plan": "",
                        "voucherNumber": "",
                        "promoCode": "",
                        "partial": false,
                        "name": user[0].firstname + " " + user[0].lastname,
                        "email": user[0].email
                    }
                }
            },
            "booking_id": booking.booking.id
        }

        const paymentDetails = await getPaymentDetails(params, "quotation")

        let { pg_data } = paymentDetails.data

        const form = document.createElement("form")
        form.setAttribute("method", "post")
        form.setAttribute("action", pg_data.payu_url)

        for (var key in pg_data.payu_body) {
            if (pg_data.payu_body.hasOwnProperty(key)) {
                const hiddenField = document.createElement("input")
                hiddenField.setAttribute("type", "hidden")
                hiddenField.setAttribute("name", key)
                hiddenField.setAttribute("value", pg_data.payu_body[key])
                form.appendChild(hiddenField)
            }
        }

        document.body.appendChild(form)
        form.submit()
    }

    return (
        <div className={cx(styles.page)}>
            <Header />
            <div className="row p-0 m-auto container justify-content-center">
                <div className="col-6 p-3">
                    <div className={cx(styles.postQuotationPayment)}>
                        <div className={cx(styles.postQuotationPaymentSectionBooking)}>
                            <p className={cx(styles.detailHeadingText)}>ORDER ID</p>
                            <p className={cx(styles.detailInfoText)}>{booking.booking && booking.booking.number}</p>

                            <p className={cx(styles.detailHeadingText)}>SHIP</p>
                            <p className={cx(styles.detailInfoText)}>EMPRESS</p>

                            <p className={cx(styles.detailHeadingText)}>ROOM CATEGORY</p>
                            <p className={cx(styles.detailInfoText)}>{booking && booking.room_category}</p>

                            <div className="row">
                                <div className="p-3">
                                    <p className={cx(styles.detailHeadingText)}>DEPARTURE</p>
                                    <p className={cx(styles.detailInfoText)}>Mumbai<br />
                                        Sat, Oct 23, 2021 5:00 PM</p>
                                </div>
                                <div className="py-3">
                                    <p className={cx(styles.detailHeadingText)}>DEPARTURE</p>
                                    <p className={cx(styles.detailInfoText)}>Mumbai<br />
                                        Sat, Oct 23, 2021 5:00 PM</p>
                                </div>
                            </div>
                        </div>
                        <div className={cx(styles.postQuotationPaymentSectionPayment)}>
                        </div>
                        <div className={cx(styles.cancellationHeading, "row")}>
                            <h6 className={cx(styles.cancellationHeadingText, "font-weight-bold")}>Cancellation and Reschedule policy</h6>
                            {
                                cancellationPanel === true ?
                                    <i className={cx(styles.cancellationPointerDown, "fal cursor-pointer text-j-gray-light text-4xl fa-angle-up")} onClick={() => setCancellationPanel(false)}></i>
                                    : <i className={cx(styles.cancellationPointerDown, "fal cursor-pointer text-j-gray-light text-4xl fa-angle-down")} onClick={() => setCancellationPanel(true)}></i>
                            }
                        </div>
                        {
                            cancellationPanel === true ?
                                <div>
                                    <h5 className={cx(styles.cancelFeeHeading)}>CANCELLATION FEE</h5>
                                    <table className={cx(styles.toDeparture, "table mt-3")}>
                                        <thead>
                                            <th>Days To Depature</th>
                                            <th>Fee</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>31 days and above to departure</td>
                                                <td>25% of cabin fare</td>
                                            </tr>
                                            <tr>
                                                <td>16 to 30 days to departure</td>
                                                <td>50% of cabin fare</td>
                                            </tr>
                                            <tr>
                                                <td>0 to 15 days to departure	</td>
                                                <td>100% of total fare</td>
                                            </tr>
                                            <tr>
                                                <td>For No-Show	</td>
                                                <td>100% of total fare</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <p className={cx(styles.portChargeClass)}>
                                        Port charges & gratuity will be refunded completely.
                                        GST for the refunded amount will also be returned.
                                        Refund will be processed within 10 working days from the cancellation date.
                                    </p>

                                    <h5 className={cx(styles.cancelFeeHeading)}>RESCHEDULING FEE</h5>
                                    <table className={cx(styles.toDeparture, "table mt-3")}>
                                        <thead>
                                            <th>Days To Depature</th>
                                            <th>Fee</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>16 days and above</td>
                                                <td>NIL Fee</td>
                                            </tr>
                                            <tr>
                                                <td>0 - 15 days	</td>
                                                <td>INR 5000 per cabin</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <p className={cx(styles.portChargeClass)}>
                                        Rescheduled itinerary must commence on or before 31st March 2022.
                                        Fare difference if any will be payable by the customer.
                                        GST will be applicable on any payable amount.
                                    </p>
                                </div>
                                : ``}
                    </div>
                </div>
                <div className="col-6 p-3">
                    <div className={cx(styles.postQuotationPayment)}>
                        <div className={cx(styles.postQuotationPaymentSectionBooking)}>
                            <div className={cx(styles.billingDetails)}>
                                <div className={cx(styles.billingDetailsHeading, "row")}>
                                    <p className={cx(styles.billingDetailsHeadingText)}>BILLING DETAILS</p>
                                    <i className={cx(styles.cancellationPointerDown, "fal cursor-pointer text-j-gray-light text-4xl fa-angle-up")} onClick={() => setPersonalDetailsPanel(!personalDetails)}></i>
                                </div>
                                {
                                    personalDetails == false ?
                                        <div className={cx(styles.personalDetailsForm, "row p-1")}>
                                            <div className={cx(styles.personalDetailsFormInputContainer, "col p-2")}>
                                                <input className={cx(styles.personalDetailsFormInput, "w-100")} type="text" name="firstname" value={user.firstname} onChange={addFormData} placeholder="First name" />
                                            </div>
                                            <div className={cx(styles.personalDetailsFormInputContainer, "col p-2")}>
                                                <input className={cx(styles.personalDetailsFormInput, "w-100")} type="text" name="lastname" value={user.lastname} onChange={addFormData} placeholder="Last name" />
                                            </div>
                                            <div className={cx(styles.personalDetailsFormInputContainer, "col-12 p-2")}>
                                                <select className={cx(styles.personalDetailsFormInput, "col-2 w-100 h-100")} name="countrycode" value={user.countrycode} onChange={addFormData}>
                                                    {CountryCode.map(code => {
                                                        return (<option key={code} value={code}>{code}</option>)
                                                    })}
                                                </select>
                                                <input className={cx(styles.personalDetailsFormInput, "col-10 w-100")} type="text" name="phone" value={user.phone} onChange={addFormData} placeholder="Phone number" />
                                            </div>
                                            <div className={cx(styles.personalDetailsFormInputContainer, "col-12 p-2")}>
                                                <input className={cx(styles.personalDetailsFormInput, "w-100")} type="text" name="email" value={user.email} onChange={addFormData} placeholder="Email address" />
                                            </div>
                                        </div> :
                                        ``
                                }
                            </div>
                            <div className={cx(styles.billingDetails)}>
                                <div className={cx(styles.billingDetailsHeading, "row")}>
                                    <p className={cx(styles.billingDetailsHeadingText)}>I HAVE A GST NUMBER</p>
                                    <i className={cx(styles.cancellationPointerDown, "fal cursor-pointer text-j-gray-light text-4xl fa-angle-up")} onClick={() => setGstDetailPanel(!gstDetails)}></i>
                                </div>
                                {
                                    gstDetails == false ?
                                        <div className={cx(styles.personalDetailsForm, "row p-1")}>
                                            <div className={cx(styles.personalDetailsFormInputContainer, "col-12 p-2")}>
                                                <input className={cx(styles.personalDetailsFormInput, "w-100")} value={gstin} onChange={(e) => SetGstin(e.target.value)} placeholder="GSTIN" />
                                            </div>
                                            <div className={cx(styles.personalDetailsFormInputContainer, "col-12 p-2")}>
                                                <input className={cx(styles.personalDetailsFormInput, "w-100")} value={haveGstName} onChange={(e) => setGstName(e.target.value)} placeholder="Name" />
                                            </div>
                                        </div>
                                        : ``}
                            </div>
                        </div>
                        <div>
                            <p className={cx(styles.grandTotalText)}>GRAND TOTAL</p>
                            <p className={cx(styles.grandTotal)}>₹ {booking && booking.total}</p>
                            <div className={cx(styles.bookbynowamount)}>
                                <div>
                                    {
                                        booking.partial_amount_rule !== null ?
                                            <>
                                                <input type="radio" /><p className={cx(styles.booknowbytext)}>Reserve now by paying {(booking.partial_amount_rule) ? booking.partial_amount_rule : "100%"}</p>
                                                <input type="radio" /><p className={cx(styles.booknowbytext)}>Book Now by paying 100%</p>
                                            </>
                                            :
                                            <p className={cx(styles.booknowbytext)}>Book Now by paying {(booking.partial_amount_rule) ? booking.partial_amount_rule : "100%"}</p>
                                    }
                                </div>
                                <div className={cx(styles.amountPaybaleTextDiv)}>
                                    <p className={cx(styles.amountPayableText)}>AMOUNT PAYABLE</p>
                                    <p className={cx(styles.amountPayable)}>₹ {(booking.partial_payable_amount && booking.partial_payable_amount > 0) ? booking.partial_payable_amount : booking.total}</p>
                                </div>
                            </div>
                            <button className={cx(styles.submitButton, "col-12")} onClick={() => paymentPostQuotation()}>Submit</button>
                        </div>
                        <div className={cx(styles.postQuotationPaymentSectionPayment)}>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default PostQuotationPayment