import { useState, useEffect, createElement } from "react"
import axios from "axios"
import { useHistory } from "react-router-dom"
import { useSelector } from "react-redux"
import Header from "../../components/Header/HeaderHome";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container";
import FooterButtonSection from "../../components/UI/FooterButtonSection";
import { getPaymentDetails } from "../../services/payment"

function RoomSelection() {

  const history = useHistory()
  // const booking = useSelector((state) => {
  //   return state.booking ? state.booking : []
  // })

  const [paymentOption, setPaymentOption] = useState("BookBy100")

  const booking = useSelector((state) => {
    return state.booking ? state.roomReducer.bookingsummary : []
  })

  const guestDetails = useSelector((state) => {
    return state.rootReducer ? state.rootReducer.guestDetails : null
  })


  const allGuests = useSelector((state) => state.roomReducer.allGuests)
  const queryParams = new URLSearchParams(window.location.search)

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const paymentDetails = async () => {

    // console.log("Data", allGuests[0].guests)

    const params = {
      "variables": {
        "input": {
          "amount": paymentOption !== "BookBy100" ? booking.partial_payable_amount : null,
          "payment_option_id": paymentOption !== "BookBy100" ? booking.payment_option_id : null,
          "contact": {
            "firstname": allGuests[0].hasOwnProperty("guests") ? allGuests[0].guests[0].name : allGuests[0].name,
            "lastname": allGuests[0].hasOwnProperty("guests") ? allGuests[0].guests[0].lastname : allGuests[0].lastname,
            "email": allGuests[0].hasOwnProperty("guests") ? allGuests[0].guests[0].email : allGuests[0].email,
            "phoneNumber": allGuests[0].hasOwnProperty("guests") ? allGuests[0].guests[0].countryCode + " " + allGuests[0].guests[0].phoneNumber : allGuests[0].countryCode + " " + allGuests[0].phoneNumber,
            "gstin": null,
            "gstin_name": haveGstName != '' ? haveGstName : null,
            "gstin_phone": null,
            "pan": panno != '' ? panno : null,
            "pan_name": panname != '' ? panname : null
          },
          "paymentInfo": {
            "plan": "",
            "voucherNumber": "",
            "promoCode": "",
            "partial": false,
            "name": allGuests[0].hasOwnProperty("guests") ? allGuests[0].guests[0].name : allGuests[0].name,
            "email": allGuests[0].hasOwnProperty("guests") ? allGuests[0].guests[0].email : allGuests[0].email
          }
        }
      },
      "booking_id": booking.booking.id
    }

    const paymentDetails = await getPaymentDetails(params)

    let { pg_data } = paymentDetails.data

    const form = document.createElement("form")
    form.setAttribute("method", "post")
    form.setAttribute("action", pg_data.payu_url)

    for (var key in pg_data.payu_body) {
      if (pg_data.payu_body.hasOwnProperty(key)) {
        const hiddenField = document.createElement("input")
        hiddenField.setAttribute("type", "hidden")
        hiddenField.setAttribute("name", key)
        hiddenField.setAttribute("value", pg_data.payu_body[key])
        form.appendChild(hiddenField)
      }
    }

    document.body.appendChild(form)
    form.submit()
  }

  const [havePan, setHavePan] = useState(1)
  const [panname, setPanname] = useState("")
  const [panno, setPanno] = useState("")

  const [haveGst, SetHaveGst] = useState(1)
  const [gstin, SetGstin] = useState("")
  const [haveGstName, setGstName] = useState("")
  const [haveMobilenumber, setMobilenumber] = useState("")

  const havePanChange = e => setHavePan(e.target.value)
  const changePanName = e => setPanname(e.target.value)
  const changePanNo = e => setPanno(e.target.value)

  const changeHaveGst = e => SetHaveGst(e.target.value)
  const changeGstin = e => SetGstin(e.target.value)
  const changeHaveGstName = e => setGstName(e.target.value)
  const changeHaveMobilenumber = e => setMobilenumber(e.target.value)

  return (
    <div className="Home">
      <Header bgPurple platform={queryParams.get("platform")} />
      <Container strict={true}>
        <BookingFlowCards.ProgressBar current={3} />
        <BookingFlowCards.PaymentSumarry
          havePan={havePan}
          havePanChange={havePanChange}
          panname={panname}
          changePanName={changePanName}
          panno={panno}
          changePanNo={changePanNo}
          haveGst={haveGst}
          gstin={gstin}
          haveGstName={haveGstName}
          haveMobilenumber={haveMobilenumber}
          changeHaveGst={changeHaveGst}
          changeGstin={changeGstin}
          changeHaveGstName={changeHaveGstName}
          changeHaveMobilenumber={changeHaveMobilenumber}
          paymentOption={paymentOption}
          setPaymentOption={setPaymentOption}
        />
        <FooterButtonSection
          onClick={paymentDetails}
          text="Proceed To Pay"
        />
      </Container>
      {/* <Footer /> */}
      {
        (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
      }
    </div>
  );
}

export default RoomSelection;
