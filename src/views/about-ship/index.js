import React from 'react'
import Header from "../../components/Header/HeaderHome"
import Container from "../../components/Container/index"
import Footer from "../../components/Footer/Footer"
import FooterButtonSection from "../../components/UI/FooterButtonSection"
import Banner from "../../assets/img/about-ship/banner.jpg"
import styles from "./aboutship.module.css"
import cx from "classnames";
import * as Section from '../../components/Section'
import { bestSellingCruises } from '../home/feed'
import ReactPlayer from "react-player/youtube"
import Slider from "react-slick"
import ReactHtmlParser from "react-html-parser"
import { Button } from "antd"


import Interior from "../../assets/img/about-ship/shore-excursions/interior.jpg"
import Balcony from "../../assets/img/about-ship/shore-excursions/Balcony.jpg"
import Curl from "../../assets/img/about-ship/shore-excursions/curl.jpg"
import Star from "../../assets/img/about-ship/shore-excursions/star.jpg"
import EmpressOne from "../../assets/img/empress/casino-b.jpg"
import EmpressTwo from "../../assets/img/empress/bars-b.jpg"
import EmpressThree from "../../assets/img/empress/royal-b.jpg"
import EmpressFour from "../../assets/img/empress/acc-b.jpg"
import Spa from "../../assets/img/empress/spam.jpg"
import Shopping from "../../assets/img/empress/shoppingn.jpg"
import Fitness from "../../assets/img/empress/fn.jpg"
import Deckplan from "../../assets/img/empress/deckplan.jpg"
import Hadventure from "../../assets/img/empress/hadventure.jpg"
import Hfamily from "../../assets/img/empress/hfamily.jpg"
import Hcouple from "../../assets/img/empress/hcouple.jpg"
import Helderly from "../../assets/img/empress/helderly.jpg"
import Lakshdweep from "../../assets/img/wedding/lakshdweep-img.jpg"
import Goa from "../../assets/img/wedding/goa-img.jpg"
import Srilanka from "../../assets/img/wedding/srilanka.jpg"
import Diu from "../../assets/img/wedding/diu-img.jpg"
import Wedding from "../../assets/img/lakshdweep-page/weddingo.jpg"
import CorporateEvents from "../../assets/img/lakshdweep-page/corprateevents.jpg"
import Groups from "../../assets/img/lakshdweep-page/groups.jpg"
import Lakait from "../../assets/img/empress/lakit.jpg"
import Goait from "../../assets/img/empress/goait.jpg"
import Diuit from "../../assets/img/empress/diu.jpg"
import Seait from "../../assets/img/empress/seait.jpg"
import Gaout from "../../assets/img/empress/gaout.jpg"


const OccasionsData = {
    title: "CRUISE HOLIDAYS FOR <strong>EVERY OCCASION</strong>",
    description:
        "From weddings at the sea to corporate getaways or just a big fam-jam, there's a cruise holiday for every special moment.",
    list: [
        {
            image: Wedding,
            title: "WEDDINGS",
            description:
                "Embark on a special journey to solemnize your auspicious wedding vows in the middle of the vast ocean. Celebrate the most important beginning of your life with the gorgeous ocean as the backdrop, and the sound of the waves as a testimony to the auspicious moment.",
            link: "",
        },
        {
            image: CorporateEvents,
            title: "CORPORATE EVENTS",
            description:
                "Give your offsite and business events a touch of grandeur by hosting them aboard Cordelia Cruises. From spectacular venues to specialty cuisines and team engagement activities, make your next offsite a grand affair with offbeat experiences.",
            link: "",
        },
        {
            image: Groups,
            title: "GROUPS & FAMILY HOLIDAYS",
            description:
                "Forget your regular family vacations vacations because seacations are here to give you the much-needed power-packed dose of adventure, fun, and entertainment in the middle of the ocean.",
            link: "",
        },
    ],
};

const weddingDestinationList = [
    {
        image: Lakshdweep,
        destination: "Lakshadweep",
        heading: "India’s Tropical Heaven",
        description:
            "With the picture-perfect flawless combination of awe-inspiring land and seascapes, Lakshadweep is a destination that outshines its reputation. Lakshadweep’s magnificence lies underwater with 4200 sq. km of archipelago lagoons and coral reefs, making it the only place in India where you can snorkel in the reefs.",
        link: "/cruise-destinations/cruise-to-lakshadweep",
    },
    {
        image: Goa,
        destination: "Goa",
        heading: "Experience the Charm",
        description:
            "Goa is undoubtedly India’s favorite travel destination. A backpacker’s paradise and a weekend travel hub, Goa is famous for its tropical vibe, young identity, and cultural adaptations. This Konkan state in the country’s southwestern coast is an interesting mix of sun, sand, and spice. Whether you’re traveling with your family and friends or traveling solo, Goa’s versatility never fails to charm.",
        link: "/cruise-destinations/cruise-to-goa",
    },
    {
        image: Srilanka,
        destination: "Sri Lanka",
        heading: "Surf, Sambal & Safari",
        description:
            "The land of surf, spices, and sambal. A fabulous combination of natural beauty and warm hospitality, Sri Lanka is a travel paradise.  Endless beaches, wildlife, and signature flavors make this country irresistible. Sri Lanka is a fascinating mix of culture and modernity.        ",
        link: "/cruise-destinations/cruise-to-colombo",
    },
    {
        image: Diu,
        destination: "Diu",
        heading: "Colors and Culture",
        description:
            "Perched off the Kathiawar peninsula of Gujarat is the small, secluded island of Diu.  What was once a Portuguese colony from the 16th to 20th century is now a picturesque destination that’s an ideal destination for weekend getaways from Mumbai. With its pristine beaches, diverse wildlife, and rich cultural heritage, this little island is perfect for travellers looking for a slice of history along with breathtaking views.",
        link: "/cruise-destinations/cruise-to-diu",
    },
];

const CruiseListData = {
    title: "CRUISE HOLIDAYS <strong>FOR EVERYONE</strong>",
    description:
        "From family holidays, to honeymoons and holidays for the elderly, cruise holidays are for everyone.",
    list: [
        {
            image: Hadventure,
            title: "Adventure Buffs",
            subtitle: "The action awaits onboard and at the shores",
        },
        {
            image: Hfamily,
            title: "Families",
            subtitle: "A complete family holiday experience.",
        },
        {
            image: Hcouple,
            title: "Couples",
            subtitle: "Celebrate love with gorgeous ocean views",
        },
        {
            image: Helderly,
            title: "Elderly Travelers",
            subtitle: "From special rooms to activities and entertainment.",
        },
    ],
};

const DeckPlanData = {
    title: " EMPRESS <strong>DECK PLAN</strong>",
    description:
        "Your detailed guide to deck layouts, stateroom locations, and shipboard facilities.",
    decks: [
        {
            facilities:
                "Lorem ipsum is a simply dummy text used in typing industry since 1500s",
            image: Deckplan,
        },
        {
            facilities:
                "Lorem ipsum is a simply dummy text used in typing industry since 1500s",
            image: Deckplan,
        },
    ],
};

const CruiseExperienceData = {
    description:
        "World-class experiences and services for every cruise traveler.",
    list: [
        {
            title: "SPA",
            description: "Relax in the lap of ultimate luxury",
            image: Spa,
        },
        {
            title: "Shopping",
            description: "Shop till you drop at the finest boutiques onboard.",
            image: Shopping,
        },
        {
            title: "Fitness Centre",
            description: "A fitness centre with a 180-degree view of the ocean! ",
            image: Fitness,
        },
    ],
};

const ShoreExcursionsData = {
    title: "ONBOARD <strong>THE EMPRESS</strong>",
    description:
        "Lorem ipsum is a simply dummy text used in typing industry since 1500s.",
    items: [
        {
            // image: "images/empress/interior.jpg",
            image: Interior,
            mobileimage: "images/laskhdeep-page/featured1-mobile.jpg",
            title: "STATE-OF-THE-ART INTERIORS",
            description:
                "With contemporary and modern-chic interiors, spacious and comfortable staterooms, specialty dining and infinite onboard experiences, The Empress brings the best of Indian and international holidays to the sea",
        },
        {
            // image: "images/empress/Balcony.jpg",
            image: Balcony,
            mobileimage: "images/laskhdeep-page/featured2-mobile.jpg",
            title: "LUXURIOUS ACCOMMODATIONS",
            description:
                "Set sail for a culinary journey, and indulge in the finest dining experience in the middle of the ocean. Our unparalleled combination of flavors and aromatics bring the best of Indian and global cuisines to your plate.",
        },
        {
            // image: "images/empress/curl.jpg",
            image: Curl,
            mobileimage: "images/laskhdeep-page/featured3-mobile.jpg",
            title: "CULINARY BRILLIANCE",
            description:
                "Inspiring vacations deserve inspirational retreats to sit back and relax. Our luxurious and comfortable staterooms have been created to help you recharge after a day full of onboard entertainment.",
        },
        {
            image: Star,
            // image: "images/empress/star.jpg",
            mobileimage: "images/laskhdeep-page/featured4-mobile.jpg",
            title: "ONBOARD EXPERIENCES",
            description:
                "Your vacation will be full of action because when onboard the Empress there’s non-stop fun and entertainment for all.",
        },
    ],
}

const VenuesList = [
    {
        image: EmpressOne,
        imagemobile: EmpressOne,
        headerTitle: <strong>THE CASINO</strong>,
        headerDescription:
            "The stakes maybe high at the high seas, but if luck is on your side there’s plenty of action expected. Play the biggest table and slot games at our plush and sophisticated casino. Play to win at the biggest casino in India, and celebrate your victory with a few drinks at the Casino Bar.",
    },
    {
        image: EmpressTwo,
        imagemobile: EmpressTwo,
        headerTitle: (
            <>
                <strong>BARS & LOUNGES</strong>
            </>
        ),
        headerDescription:
            "Enjoy our premium bar and lounges to relax, enjoy and recharge. Enjoy tropical cocktails by the pool as sun goes down, lounge in style at the Connexions Bar and Chairman’s Club, and get ready for the after party at the crown of the Empress",
    },
    {
        image: EmpressThree,
        imagemobile: EmpressThree,
        headerTitle: (
            <>
                <strong>THE ROYAL THEATRE</strong>
            </>
        ),
        headerDescription:
            "India’s largest theatre in the middle of the ocean! An experience like never before. This 800-seater theatre takes entertainment to a new level. Enjoy India’s most popular musical show at this theatre.",
    },
    {
        image: EmpressFour,
        imagemobile: EmpressFour,
        headerTitle: (
            <>
                <strong>CORDELIA ACADEMY</strong>
            </>
        ),
        headerDescription:
            "When on a vacation, kids need their own special zone. Onboard the Empress, we have an exclusive kids zone with educational and fun experiences for kids of all age groups. Let your teen and toddlers have fun under the guidance of our child care experts while you enjoy your me-time.",
    },
];


const IternariesData = {
    title: "THE EMPRESS <strong>2021 ITINERARIES</strong>",
    itineraries: [
        {
            price: "₹ 67,000",
            image: Lakait,
            title: "5 NIGHTS LAKSHADWEEP",
            subtitle: "Heaven On the Earth",
            description:
                "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat.",
        },
        {
            price: "₹ 21,000",
            image: Goait,
            title: "3 NIGHTS GOA",
            subtitle: "Explore the Madness",
            description:
                "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat.",
        },
        {
            price: "₹ 18,000",
            image: Diu,
            title: "2 NIGHTS DIU",
            subtitle: "City of Colours",
            description:
                "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat.",
        },
        {
            price: "₹ 18,000",
            image: Seait,
            title: "2 NIGHTS HIGH SEAS",
            subtitle: "Place where Peace Emerge",
            description:
                "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat.",
        },
        {
            price: "₹ 18,000",
            image: Gaout,
            title: "2 NIGHTS GOA",
            subtitle: "Explore the Madness",
            description:
                "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat.",
        },
    ],
};

const PageBottom = ({ pagestyles }) => {
    const sectionStyles = pagestyles || styles;

    return (
        <div
            className={cx(
                sectionStyles["social-media-area"],
                sectionStyles["wedding-page-section"],
                sectionStyles["pageSection"],
                sectionStyles["social-custom-align"]
            )}
        >
            <div className={cx(sectionStyles["social-media-area-inner"])}>
                <div className={cx(sectionStyles["social-media-area-heading-group"])}>
                    <h2
                        className={cx(sectionStyles["wedding-page-title"], "text-center")}
                    >
                        FOLLOW US ON <strong>SOCIAL MEDIA</strong>
                    </h2>
                    <h6
                        className={cx(
                            sectionStyles["wedding-page-subtitle"],
                            "text-center"
                        )}
                    >
                        Follow us for the latest updates and offers
                    </h6>
                </div>
                <div className="text-center">
                    <ul className={cx(sectionStyles["social-media-list"])}>
                        <li>
                            <a
                                href="https://www.facebook.com/cordeliacruises"
                                target="_blank"
                                aria-label="Facebook"
                                rel="noreferrer"
                            >
                                <i className="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.instagram.com/cordeliacruises_india/?hl=en"
                                target="_blank"
                                aria-label="Facebook"
                                rel="noreferrer"
                            >
                                <i className="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.linkedin.com/company/cordelia-cruises"
                                target="_blank"
                                aria-label="Facebook"
                                rel="noreferrer"
                            >
                                <i className="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://twitter.com/CordeliaCruises"
                                target="_blank"
                                aria-label="Facebook"
                                rel="noreferrer"
                            >
                                <i className="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

const Aboutship = () => {

    const queryParams = new URLSearchParams(window.location.search)
    const [submitted, setSubmitted] = React.useState(false)

    const settingsInterior = {
        infinite: true,
        slidesToShow: 3,
        // className: "center",
        // centerMode: true,
        // centerPadding: "100px",
        speed: 300,
        cssEase: 'linear',
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                },
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    const explorationSlider = {
        infinite: false,
        slidesToShow: 1,
        speed: 300,
        cssEase: "linear",
        dots: true,
        arrows: false,
    };

    return (
        <>
            <div className="Home">
                <Header bgPurple platform={queryParams.get("platform")} />
                <div className={cx(styles.bannerSection, "text-center")}>
                    <img className={styles.bannerImage} src={Banner} />
                    <div className={cx(styles.bannerContent)}>
                        <p className={cx(styles.bannerTitle)}>EMPRESS</p>
                        <p className={cx(styles.bannerSubHeading)}>BY CORDELIA CRUISES</p>
                    </div>
                </div>
                <Container strict={true}>
                    <div className={cx(styles.cityOnSeaComplete)}>
                        {/* <div className="d-flex flex-wrap mt-5"> */}
                        <div className={cx(styles.cityOnSeaDiv)}>
                            <p className={cx(styles.cityOnSea)}>A <b>CITY ON</b> THE <b>SEA</b></p>
                        </div>
                        <div className={cx(styles.cityOnSeaDescription)}>
                            {/* <div className="col-8"> */}
                            <p className={cx(styles.cityOnSeaDescriptionText)}>Cruising the spectacular coasts of the Indian subcontinent, The Empress by Cordelia Cruises is the majestic queen of the seas. The Empress is a grand holiday destination where vacations are transformed into seacations. The excitement lies between the shores.</p>
                            <p className={cx(styles.brochureLink)}><i className="fal fa-arrow-to-bottom"></i>  View Brochure</p>
                        </div>
                    </div>
                </Container>

                <div className={cx(styles.itiComplete)}>
                    <Container strict={true}>
                        <h1 className={cx(styles.itiTitle)}>{ReactHtmlParser(IternariesData.title)}</h1>
                    </Container>
                    <Slider className="primary-slider" {...settingsInterior} id={Date.now()}>
                        {IternariesData.itineraries.map((item, idx) => {
                            return (
                                <div key={item.title + idx}>
                                    <div className={cx(styles.itiSlider, "cards-carousel__slider__card text-left")}>
                                        {item.image && (
                                            <figure>
                                                <img alt="cordelia cruises" height="245px" width="100%" src={item.image} />
                                            </figure>
                                        )}

                                        <div className="cards-carousel__slider__card__body">
                                            <div className="cards-carousel__slider__card__body__top">
                                                <h4 className="cards-carousel__slider__card__body__title">{item.title}</h4>
                                                {item.subtitle && <p style={{ fontSize: "12pt" }} className="cards-carousel__slider__card__body__subtitle font-weight-bold">{item.subtitle}</p>}
                                                {item.description && (
                                                    <p className={`cards-carousel__slider__card__body__description`}>
                                                        {item.description}
                                                    </p>
                                                )}
                                            </div>
                                            <div className="cards-carousel__slider__card__body__bottom">

                                                {item.price && (
                                                    <div className="cards-carousel__slider__card__body__bottom__price">
                                                        <span>Starting From</span><strong className="">{item.price}</strong> / Twin sharing basis
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </Slider>
                </div>
                <div className={cx(styles.empressSection)}>
                    <Container strict="true">
                        <div className={cx(styles.empressSectionContent)}>
                            <h1 className={styles.empressSectionTitle}>THE <strong>EMPRESS</strong></h1>
                            <p className={cx(styles.theEmpressDescription)}>Ruling the seas with its majesty and awe-inspiring brilliance.</p>
                        </div>
                    </Container>
                    <div className={cx(styles.empressInfoSection)}>
                        <div className="row mt-3">
                            <div className={cx(styles.shipInfoSec, "col-2")}>
                                <p className={cx(styles.shipInfoTitle)}>DECKS</p>
                                <p className={styles.shipInfoDes}>11</p>
                            </div>

                            <div className={cx(styles.shipInfoSec, "col-2")}>
                                <p className={styles.shipInfoTitle}>STATEROOMS</p>
                                <p className={styles.shipInfoDes}>796</p>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className={cx(styles.shipInfoSec, "col-2")}>
                                <p className={styles.shipInfoTitle}>TONNAGE</p>
                                <p className={styles.shipInfoDes}>48,563</p>
                            </div>

                            <div className={cx(styles.shipInfoSec, "col-2")}>
                                <p className={styles.shipInfoTitle}>LENGTH</p>
                                <p className={styles.shipInfoDes}>692 feet</p>
                            </div>
                        </div>
                    </div>
                </div>
                <Container strict={true}>
                    <div className="video-container">
                        <ReactPlayer
                            className="react-player"
                            controls={true}
                            playing={true}
                            light={Banner}
                            url="https://www.youtube.com/embed/2UiuEpn_CZM"
                        />
                    </div>
                </Container>


                <div className={cx(styles.onBoardEmpress)}>
                    <Container strict={true}>
                        <h1 className={cx(styles.onBoardEmpressTitle)}>{ReactHtmlParser(ShoreExcursionsData.title)}</h1>
                        <p className={cx(styles.onBoardEmpressDescription)}>{ReactHtmlParser(ShoreExcursionsData.description)}</p>
                    </Container>

                    <div className={cx(styles.ShoreExcursionsDataDesktop)}>
                        <img src={Interior} />
                        <div className={cx(styles.stateOfTheArtInteriors, "rounded-lg")}>
                            <h5 className={cx(styles.stateOfTheArtInteriorsTitle, "font-weight-bold")}>STATE-OF-THE-ART INTERIORS</h5>
                            <p className={cx("")}>With contemporary and modern-chic interiors, spacious and comfortable staterooms, specialty dining and infinite onboard experiences, The Empress brings the best of Indian and international holidays to the sea</p>
                        </div>
                        <Slider className="primary-slider" {...settingsInterior} id={Date.now()}>
                            {ShoreExcursionsData.items.slice(1, 4).map((item, idx) => {
                                return (
                                    <div key={item.title + idx}>
                                        <div className="cards-carousel__slider__card">
                                            {item.image && (
                                                <figure>
                                                    <img alt="cordelia cruises" src={item.image} />
                                                </figure>
                                            )}

                                            <div className="cards-carousel__slider__card__body text-left">
                                                <div className="cards-carousel__slider__card__body__top" style={{ height: "150px" }}>
                                                    <h4 className="cards-carousel__slider__card__body__title">{item.title}</h4>
                                                    {item.description && (
                                                        <p className={`cards-carousel__slider__card__body__description`}>
                                                            {item.description}
                                                        </p>
                                                    )}
                                                </div>
                                                <div className="cards-carousel__slider__card__body__bottom text-center justify-content-center mt-5">
                                                    <a className="cc-button cc-button--primary text-white">
                                                        KNOW MORE
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </Slider>
                    </div>

                    <div className={cx(styles.ShoreExcursionsDataMobile)}>
                        <Slider className="primary-slider" {...settingsInterior} id={Date.now()}>
                            {ShoreExcursionsData.items.slice(1, 4).map((item, idx) => {
                                return (
                                    <div key={item.title + idx}>
                                        <div className="cards-carousel__slider__card">
                                            {item.image && (
                                                <figure>
                                                    <img alt="cordelia cruises" src={item.image} />
                                                </figure>
                                            )}

                                            <div className="cards-carousel__slider__card__body text-left">
                                                <div className="cards-carousel__slider__card__body__top" style={{ height: "150px" }}>
                                                    <h4 className="cards-carousel__slider__card__body__title">{item.title}</h4>
                                                    {item.description && (
                                                        <p className={`cards-carousel__slider__card__body__description`}>
                                                            {item.description}
                                                        </p>
                                                    )}
                                                </div>
                                                <div className="cards-carousel__slider__card__body__bottom text-center justify-content-center mt-5">
                                                    <a className="cc-button cc-button--primary text-white">
                                                        KNOW MORE
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </Slider>
                    </div>

                </div>
                <div className={styles.explorationOptionDesktop}>
                    <Slider className={cx(styles.slider, "primary-slider")} {...explorationSlider} id={Date.now()}>
                        {VenuesList.map((item, idx) => {
                            return (
                                <div className={cx("position-relative")} key={item.title + idx}>
                                    <img style={{ width: "100%", padding: "0px", margin: "0px" }} src={item.image} />
                                    <div className={cx(styles.explorationImageContent, "text-center")}>
                                        <h3 style={{ color: "white", fontSize: "34pt" }}>{item.headerTitle}</h3>
                                        <p style={{ fontSize: "14pt" }}>{item.headerDescription}</p>
                                    </div>
                                </div>
                            )
                        })}
                    </Slider>
                </div>

                <div className={styles.explorationOptionMobile}>
                    <Slider className={cx(styles.slider, "primary-slider")} {...explorationSlider} id={Date.now()}>
                        {VenuesList.map((item, idx) => {
                            return (
                                <div className={cx(styles.explorationOptionSlider, "position-relative")} key={item.title + idx}>
                                    <img style={{ width: "100%", height: "360px", objectFit: "cover", padding: "0px", margin: "0px" }} src={item.image} />
                                    <div className={cx(styles.explorationOptionMask)}>

                                    </div>
                                    <div className={cx(styles.exploreMoreOption)}>
                                        <i className="fal fa-sign-out"></i>
                                        <span className={cx(styles.glowingBallExploreOption)}></span>
                                    </div>
                                    <p className={cx(styles.exploreMoreText)}>Explore More</p>
                                </div>
                            )
                        })}
                    </Slider>
                </div>

                <Container strict={true}>
                    <div className={cx("row justify-content-center mt-5")}>
                        <h5 className={styles.cruiseExper}>{CruiseExperienceData.description}</h5>
                        <div className={cx(styles.servicesOfEmpress)}>
                            {
                                CruiseExperienceData.list.map((item, index) => {
                                    return <div key={index} className={cx(styles.serviceName)}>
                                        <img src={item.image} height="434px" />
                                        <div className={cx(styles.serviceTitleDescription)}>
                                            <h5>{item.title}</h5>
                                            <p>{item.description}</p>
                                        </div>
                                    </div>
                                })
                            }
                        </div>
                    </div>
                    <div className={cx(styles.empressDeckPlan, "mt-5")}>
                        <h2 className={cx(styles.DeckPlanTitle)}>{ReactHtmlParser(DeckPlanData.title)}</h2>
                        <span className={cx(styles.DeckPlanDescription)}>{DeckPlanData.description}</span>
                        <Slider arrows={false} style={{ marginTop: "50px" }}>
                            {DeckPlanData.decks.map((item, index) => {
                                return <div key={index}>
                                    <img src={item.image} />
                                </div>
                            })}
                        </Slider>
                    </div>
                </Container>
                <div className={cx(styles.cruiseHolidays, "mt-5")}>
                    <Container strict="true">
                        <h1 className={cx(styles.cruiseHolidaysTitle)}>{ReactHtmlParser(CruiseListData.title)}</h1>
                        <p className={cx(styles.cruiseHolidaysDescription)}>{CruiseListData.description}</p>
                        <div className={cx(styles.cruiseHolidaysTypesDesktop)}>
                            {
                                CruiseListData.list.map((item, index) => {
                                    return <div key={index} className={cx(styles.cruiseHolidyContent)}>
                                        <img src={item.image} height="360px" />
                                        <div className={cx(styles.cruiseHolidaysImageContent)}>
                                            <h5>{item.title}</h5>
                                        </div>
                                    </div>
                                })
                            }
                        </div>
                    </Container>
                    <div className={cx(styles.cruiseHolidaysTypesMobile)}>
                        <Slider className={cx(styles.slider, "primary-slider")} {...explorationSlider} id={Date.now()}>
                            {
                                CruiseListData.list.map((item, index) => {
                                    return <div key={index} className={cx(styles.cruiseHolidyContent)}>
                                        <img src={item.image} height="360px" width="100%" />
                                        <div className={cx(styles.cruiseHolidaysImageContent)}>
                                            <h5>{item.title}</h5>
                                        </div>
                                    </div>
                                })
                            }
                        </Slider>
                    </div>
                </div>
                <div className={cx(styles.cruiseHolidayDestination, "mt-5")}>
                    <Container strict="true">
                        <h1 className={cx(styles.cruiseHolidayDestinationHeading)}>CRUISE HOLIDAY <strong>DESTINATIONS</strong></h1>
                    </Container>
                    <div className={cx(styles.cruiseHolidaysDestinationItemsDesktop)}>
                        {
                            weddingDestinationList.map((item, index) => {
                                return <div key={index} className={cx(styles.cruiseHolidyDestinationContent)}>
                                    <img src={item.image} style={{ objectFit: "cover" }} height="520px" width="316px" />
                                    <div className={cx(styles.cruiseHolidayDestinationImageContent, "text-left")}>
                                        <span>{item.destination}</span>
                                        <p>{item.heading}</p>
                                        <p className={cx(styles.cruiseDestinationDescription)}>{item.description}</p>
                                    </div>
                                </div>
                            })
                        }
                    </div>

                    <div className={cx(styles.cruiseHolidaysDestinationItemsMobile)}>
                        <Slider className={cx(styles.slider, "primary-slider")} {...explorationSlider} id={Date.now()}>
                            {
                                weddingDestinationList.map((item, index) => {
                                    return <div key={index} className={cx(styles.cruiseHolidyDestinationContent)}>
                                        <img src={item.image} style={{ objectFit: "cover" }} height="520px" width="100%" />
                                        <div className={cx(styles.cruiseHolidayDestinationImageContent, "text-left")}>
                                            <span>{item.destination}</span>
                                            <p>{item.heading}</p>
                                            <p className={cx(styles.cruiseDestinationDescription)}>{item.description}</p>
                                        </div>
                                    </div>
                                })
                            }
                        </Slider>
                    </div>
                </div>
                <div className={cx(styles.occasions)}>
                    <Container strict="true">
                        <h1 className={styles.occasionsTitle}>{ReactHtmlParser(OccasionsData.title)}</h1>
                        <p className={cx(styles.occasionDescription)}>{OccasionsData.description}</p>
                    </Container>
                    <Slider className="primary-slider mt-5" {...settingsInterior} id={Date.now()}>
                        {OccasionsData.list.map((item, idx) => {
                            return (
                                <div key={item.title + idx}>
                                    <div className="cards-carousel__slider__card">
                                        {item.image && (
                                            <figure>
                                                <img alt="cordelia cruises" src={item.image} />
                                            </figure>
                                        )}

                                        <div className="cards-carousel__slider__card__body text-left">
                                            <div className="cards-carousel__slider__card__body__top" style={{ height: "150px" }}>
                                                <h4 className="cards-carousel__slider__card__body__title">{item.title}</h4>
                                                {item.description && (
                                                    <p className={`cards-carousel__slider__card__body__description`}>
                                                        {item.description}
                                                    </p>
                                                )}
                                            </div>
                                            <div className="cards-carousel__slider__card__body__bottom text-center justify-content-center mt-5">
                                                <a className="cc-button cc-button--primary text-white">
                                                    KNOW MORE
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </Slider>
                </div>
                <div>
                    <PageBottom />
                </div>
                <FooterButtonSection />
                {/* <Footer /> */}
                {
                    (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
                }
            </div >
        </>
    )
}

export default Aboutship
