import React from "react"
import { useHistory } from "react-router-dom"
import classes from "./booking-success.module.css"
import cx from "classnames"
import Header from "../../components/Header/HeaderHome"
import Footer from "../../components/Footer/Footer"
import localStore from "../../utils/localStore"
import axios from "axios"

const BookingSuccessPage = () => {

    const history = useHistory()

    const gotoBookingFlow = async () => {
        const user = localStore.get("user")

        let obj = {
            name: user[0].firstname + " " + user[0].lastname,
            phone_number: user[0].phone,
            country_code: user[0].countrycode
        }
        const { data } = await axios.post(`https://staging.cordeliacruises.com/api/send_otp`, obj, {})
        localStorage.setItem("auth", JSON.stringify(data.data.auth))

        history.push("/room-type?from=booking-success")
    }

    return (
        <>
            <Header />
            <div className={cx(classes.bookingSuccess)}>
                <h1>Thank you!</h1>
                <div className={cx(classes.paymentTextWithIcon)}>
                    <i class="far fa-check-circle" style={{ color: "green", fontSize: "20pt" }}></i>
                    <p className={cx(classes.paymentText)}>We have received your payment.</p>
                </div>
                <div className={cx(classes.mailConfirmationSection)}>
                    <p className={cx(classes.mailConfirmationPara)}>A confirmation has been sent to your email.</p>
                    <div className={cx(classes.mailConfirmationBox)}>
                        <div className={cx(classes.boxDiv)}>
                            <p className={cx(classes.boxLabel)}>ORDER ID</p>
                            <p className={cx(classes.boxDescription)}>C-2110260020</p>
                        </div>
                        <div className={cx(classes.boxDiv)}>
                            <p className={cx(classes.boxLabel)}>INVOICE </p>
                            <div className={cx(classes.boxDescriptionBox)}>
                                <i class="fa fa-download" aria-hidden="true" style={{ color: "#500e4b", fontSize: "14pt" }}></i>
                                <p className={cx(classes.boxDescription, "ml-2")}>Download</p>
                            </div>
                        </div>
                        <div className={cx(classes.boxDiv)}>
                            <p className={cx(classes.boxLabel)}>SHIP</p>
                            <p className={cx(classes.boxDescription)}>Visit Goa from Mumbai</p>
                        </div>
                        <div className={cx(classes.departAndArrivalSection, "col-12 mt-5")}>
                            <div className={cx("col-6")}>
                                <p className={cx(classes.boxLabel)}>DEPARTURE</p>
                                <p className={cx(classes.boxDescription)}>Visit Goa from Mumbai</p>
                                <p className={cx(classes.boxDescription)}>Sat, 6 Nov 2021, 5:00 pm</p>
                            </div>
                            <div className={cx("col-6")}>
                                <p className={cx(classes.boxLabel)}>ARRIVAL</p>
                                <p className={cx(classes.boxDescription)}>Visit Goa from Mumbai</p>
                                <p className={cx(classes.boxDescription)}>Mon, 8 Nov 2021, 12:00 pm</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={cx(classes.completeBookingSection)}>
                    <p className={cx(classes.completeBookingHeading)}>Complete your Booking to view More Details</p>
                    <button className={cx(classes.completeBookingButton)} onClick={gotoBookingFlow}>COMPLETE YOUR BOOKING</button>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default BookingSuccessPage
