import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCabins, removeCabin } from "../../redux/actions/cabinAction";
import cx from "classnames";
import SelectedCabin from "./SelectedCabin";
import classes from "./SelectedCabinList.module.css";
import CabinCard from "./CabinCard";

const SelectedCabinList = (props) => {
    const dispatch = useDispatch();
    const { getSelectedCabin, deckNumbers, onSelect, selectedIndex = 9999, isCabinPage = false, fromSelectionPage = false, focusedCabinId, setFocusCabinId } = props;

    const { cabins } = useSelector((state) => ({
        cabins: state.allcabins,
    }));

    //run on render
    useEffect(() => {
        dispatch(getCabins());
    }, []);

    useEffect(() => {
        if (!fromSelectionPage) {
            if (selectedIndex != 9999)
                setFocusCabinId(cabins[selectedIndex].id);
            else
                setFocusCabinId(cabins[0].id);
            if (getSelectedCabin) {
                if (selectedIndex != 9999)
                    getSelectedCabin(cabins[selectedIndex])
                else
                    getSelectedCabin(cabins[0])
            }
        } else {
            if (cabins[0].name != "Cabin 1" && focusedCabinId == "") {
                if (selectedIndex != 9999)
                    setFocusCabinId(cabins[selectedIndex].id);
                else
                    setFocusCabinId(cabins[0].id);
                if (getSelectedCabin) {
                    if (selectedIndex != 9999)
                        getSelectedCabin(cabins[selectedIndex])
                    else
                        getSelectedCabin(cabins[0])
                }
            }
        }
    }, [cabins]);

    const cabinCardClickHandler = (cabinCard) => {
        getSelectedCabin(cabinCard)
        setFocusCabinId(cabinCard.id);
        if (onSelect) {
            onSelect(cabinCard);
        }
    };

    const cabinRemoveHandler = async (cabin) => {
        if (cabins.length === 1) return;
        let updateStore = !isCabinPage;
        await dispatch(removeCabin(cabin.id, updateStore));
    };

    return (
        <div className={props.className}>
            <div className={classes.heading}>Cabins</div>
            <div className="row d-none d-md-flex">
                {cabins &&
                    cabins.map((cabin, index) => (
                        <div
                            key={cabin.id}
                            className={cx(
                                "col-lg-3 col-md-4 col-sm-5 col-sx-12",
                                classes.cabinCard
                            )}
                        >
                            <SelectedCabin
                                cabin={cabin}
                                deckNo={cabin.deck && cabin.deck.id}
                                roomno={cabin.room !== undefined && cabin.room[0].roomno}
                                onSelect={cabinCardClickHandler}
                                onRemove={cabinRemoveHandler}
                                isDefault={index === 0}
                                isSelected={focusedCabinId === cabin.id}
                            />
                        </div>
                    ))}
            </div>
            <div className="row flex-row flex-nowrap overflow-auto py-3 d-md-none">
                {cabins &&
                    cabins.map((cabin, index) => (
                        <div
                            key={cabin.id}
                            className={cx(
                                "col",
                                classes.cabinCard
                            )}
                        >
                            <SelectedCabin
                                cabin={cabin}
                                deckNo={cabin.deck && cabin.deck.id}
                                onSelect={cabinCardClickHandler}
                                onRemove={cabinRemoveHandler}
                                isDefault={index === 0}
                                isSelected={focusedCabinId === cabin.id}
                            />
                        </div>
                    ))}
            </div>
        </div>
    );
};

export default SelectedCabinList;
