import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { checkAvailability } from "../../redux/actions/bookingAction";
import { setCabinName } from "../../redux/actions/cabinAction";
import Header from "../../components/Header/HeaderHome";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Footer from "../../components/Footer/Footer";
import FooterButtonSection from "../../components/UI/FooterButtonSection";
import SelectedCabinList from "./SelectedCabinList";
import CabinSection from "./CabinSection";
import OfferSection from "./OfferSection";
import Container from "../../components/Container/index";
import CabinDetailModal from "./CabinDetailModal";
import Loader from "../../components/Loading/LoadingIcon";
import localStore from "../../utils/localStore";
import Modal from "../../components/Modal/index";
import cx from "classnames";
import classes from "./SelectedCabinList.module.css";

import {
    ArrowRightOutlined,
    ArrowLeftOutlined,
    CloseOutlined,
    DownOutlined,
} from "@ant-design/icons"
import axios from "axios"


function RoomSelection() {
    const dispatch = useDispatch();
    let history = useHistory();
    const [isLoading, setIsLoading] = useState(true);
    const [showModal, setShowModal] = useState(false);
    const [postQuotationFlag, setPostQuotationFlag] = useState(false);
    const [selectedCabin, setSelectedCabin] = useState({});
    const [selectedIndex, setSelectedIndex] = useState(0);

    const [focusedCabinId, setFocusCabinId] = useState("");

    const { availability } = useSelector((state) => ({
        availability: state.booking.availability,
    }))

    const queryParams = new URLSearchParams(window.location.search)

    const { cabinList } = useSelector((state) => ({
        cabinList: state.allcabins,
    }));

    //run on render (fetching data)
    useEffect(async () => {
        window.scrollTo(0, 0);
        let id = localStore.get("itineraryID")
        let allcabins = localStore.get("allcabins")
        let guestarr = []

        for (let i = 0; i < cabinList.length; i++) {
            guestarr.push({ adults: cabinList[i].guest.adults, infants: cabinList[i].guest.infants, children: cabinList[i].guest.children })
        }

        let param = {
            promoCode: "",
            rooms: guestarr,
        };
        setIsLoading(true);

        if (queryParams.get("from")) {
            if (queryParams.get("from") === "booking-success") {
                const booking = localStore.get("bookingSummary")
                let data = await dispatch(checkAvailability(booking.booking.id, param, "staging"))
            }
        } else {
            let data = await dispatch(checkAvailability(id, param))
        }
        setIsLoading(false);
    }, []);
    const getSelectedCabin = (val) => {
        setSelectedCabin(val);
    }
    const onCabinSelect = async (val) => {
        // cabinList.map(data=>{
        //   if(data == selectedCabin){}
        // })
        // console.log("cabin list", cabinList);
        // console.log("cabin in", selectedCabinIndex());

        if (selectedIndex + 1 < cabinList.length) {
            setSelectedIndex(selectedIndex + 1)
        }
        else if (selectedIndex == cabinList.length - 1) {
            setSelectedIndex(0)
        }

        let payload = {
            selectedCabin: selectedCabin,
            name: val.name,
            detail: val
        }
        await dispatch(setCabinName(payload))
    };
    const selectDeckClickHandler = (e) => {
        // queryParams.get("platform") === "true" ? history.push('/deck-selection?platform=true') : history.push('/deck-selection')

        let rem = ''
        for (let i = 0; i < cabinList.length; i++) {
            rem += cabinList[i].name.split(" ").join("_") + '_'
            rem += 'A_' + cabinList[i].guest.adults + '_'
            rem += 'C_' + cabinList[i].guest.children + '_'
            rem += 'I_' + cabinList[i].guest.infants + '_'
        }

        if (queryParams.get("from")) {
            if (queryParams.get("from") === "booking-success") {
                queryParams.get("platform") === "true" ?
                    history.push(`/deck-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&from=booking-success&platform=true`) :
                    history.push(`/deck-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&from=booking-success`)
            }
        } else {
            queryParams.get("platform") === "true" ?
                history.push(`/deck-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&platform=true`) :
                history.push(`/deck-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}`)
        }
    };

    const onOffersClickHandler = () => {
        setShowModal(true);
    }
    const { cabins } = useSelector((state) => ({
        cabins: state.allcabins,
    }));

    const checkCabins = () => {
        let res = false;
        cabins.map((item) => {
            if (!item.hasOwnProperty('detail')) {
                res = true;
            }
        })
        return res;
    }

    const selectedCabinIndex = () => {
        let result = 0
        cabins.map((item, index) => {
            if (item === selectedCabin) {
                result = index;
            }
        })
        return result;
    }

    return (
        <>
            <div className="Home">
                <Header bgPurple platform={queryParams.get("platform")} />
                <Container strict={true}>
                    <BookingFlowCards.ProgressBar />
                    <SelectedCabinList
                        getSelectedCabin={getSelectedCabin}
                        focusedCabinId={focusedCabinId}
                        setFocusCabinId={setFocusCabinId}
                        selectedIndex={selectedIndex}
                    />
                    <div className="row selectYourCabinPT">
                        {!isLoading ? (
                            <>
                                <CabinSection
                                    selectedCabin={selectedCabin}
                                    selectedCabinIndex={selectedCabinIndex}
                                    availability={availability}
                                    onCabinSelect={onCabinSelect}
                                    className={cx("col-md-7 col-12 mt-5", classes.cabinContainer)}
                                    onOffersClickHandler={onOffersClickHandler}
                                />
                                {availability && availability[selectedCabinIndex()].categories.map((item, i) => {
                                    return (item.is_offer ? (
                                        <>
                                            <OfferSection
                                                selectedCabin={item}
                                                className="col-md-5 col-12 mt-5 pt-4 offers"
                                            />
                                            <Modal
                                                show={showModal}
                                            >
                                                <div className={"header"}>
                                                    <div>INTERIOR</div>
                                                    <CloseOutlined className={"crossIcon"} onClick={() => setShowModal(false)} />
                                                </div>
                                                <OfferSection
                                                    selectedCabin={item}
                                                    className="col-md-12 col-12 mt-5 "
                                                />
                                                <div className="mt-4" ></div>
                                            </Modal>
                                        </>
                                    ) : null)
                                })}
                            </>
                        ) : (
                            <Loader />
                        )}
                    </div>
                </Container>
                <FooterButtonSection
                    disabled={checkCabins()}
                    text="Select Deck"
                    onClick={selectDeckClickHandler}
                />
                {/* <Footer /> */}
                {
                    (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
                }
            </div>
        </>
    );
}

export default RoomSelection;
