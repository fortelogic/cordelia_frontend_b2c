import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCabins, removeCabin } from "../../redux/actions/cabinAction";
import cx from "classnames";
import SelectedCabin from "./SelectedCabin";
import classes from "./SelectedCabinList.module.css";
import CabinCard from "./CabinCard";

const SelectedCabinList = (props) => {
  const dispatch = useDispatch();
  const { getSelectedCabin, deckNumbers, onSelect, isCabinPage = false, fromSelectionPage = false } = props;
  const [focusedCabinId, setFocusCabinId] = useState("");

  const { cabins } = useSelector((state) => ({
    cabins: state.allcabins,
  }));

  //run on render
  useEffect(() => {
    dispatch(getCabins());
  }, []);

  useEffect(() => {
    if (!fromSelectionPage) {
      setFocusCabinId(cabins[0].id);
      if (getSelectedCabin) {
        getSelectedCabin(cabins[0])
      }
    } else {
      if (cabins[0].name != "Cabin 1" && focusedCabinId == "") {
        setFocusCabinId(cabins[0].id);
        if (getSelectedCabin) {
          getSelectedCabin(cabins[0])
        }
      }
    }
  }, [cabins]);


  useEffect(() => {
    const notcabinselected = []
    const notdeckselected = []
    const notroomselected = []

    if (window.location.pathname == "/room-type") {
      for (let i = 0; i < cabins.length; i++) {
        if (cabins[i].name.includes("Cabin")) {
          notcabinselected.push(cabins[i])
        }
      }
    } else if (window.location.pathname == "/deck-selection") {
      for (let i = 0; i < cabins.length; i++) {
        if (!cabins[i].hasOwnProperty("deck")) {
          notdeckselected.push(cabins[i])
        }
      }
    } else if (window.location.pathname == "/room-selection") {
      for (let i = 0; i < cabins.length; i++) {
        if (!cabins[i].hasOwnProperty("room")) {
          notroomselected.push(cabins[i])
        }
      }
    }

    if (notcabinselected.length > 0) {
      cabinCardClickHandler(notcabinselected[0])
    }

    if (notdeckselected.length > 0) {
      cabinCardClickHandler(notdeckselected[0])
    }

    if (notroomselected.length > 0) {
      cabinCardClickHandler(notroomselected[0])
    } else {
      if (window.location.pathname == "/room-selection") {
        cabinCardClickHandler(cabins[0])
      }
    }

  }, [cabins]);


  const cabinCardClickHandler = (cabinCard) => {
    getSelectedCabin(cabinCard)
    setFocusCabinId(cabinCard.id);
    if (onSelect) {
      onSelect(cabinCard);
    }
  };

  const cabinRemoveHandler = async (cabin) => {
    if (cabins.length === 1) return;
    let updateStore = !isCabinPage;
    await dispatch(removeCabin(cabin.id, updateStore));
  };
  return (
    <div className={props.className}>
      <div className={classes.heading}>Cabins</div>
      <div className="row d-none d-md-flex">
        {cabins &&
          cabins.map((cabin, index) => (
            <div
              key={cabin.id}
              className={cx(
                "col-lg-3 col-md-4 col-sm-5 col-sx-12",
                classes.cabinCard
              )}
            >
              <SelectedCabin
                cabin={cabin}
                deckNo={cabin.deck && cabin.deck.id}
                roomno={(cabin.room !== undefined && cabin.room !== []) && cabin.room[0] ? cabin.room[0].roomno : ``}
                onSelect={cabinCardClickHandler}
                onRemove={cabinRemoveHandler}
                isDefault={index === 0}
                isSelected={focusedCabinId === cabin.id}
              />
            </div>
          ))}
      </div>
      <div className="row flex-row flex-nowrap overflow-auto py-3 d-md-none">
        {cabins &&
          cabins.map((cabin, index) => (
            <div
              key={cabin.id}
              className={cx(
                "col",
                classes.cabinCard
              )}
            >
              <SelectedCabin
                cabin={cabin}
                deckNo={cabin.deck && cabin.deck.id}
                roomno={cabin.room !== undefined && cabin.room[0] ? cabin.room[0].roomno : ``}
                onSelect={cabinCardClickHandler}
                onRemove={cabinRemoveHandler}
                isDefault={index === 0}
                isSelected={focusedCabinId === cabin.id}
              />
            </div>
          ))}
      </div>
    </div>
  );
};

export default SelectedCabinList;
