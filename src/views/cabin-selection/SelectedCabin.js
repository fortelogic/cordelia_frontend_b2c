import React from "react";
import Card from "../../components/UI/Card";
import { ReactComponent as DeleteIcon } from "../../assets/img/delete-icon.svg";
import cx from "classnames";
import classes from "./SelectedCabin.module.css";

const selectedCabin = (props) => {
  const { cabin, onSelect, deckNo, onRemove, isDefault, isSelected, roomno } = props;
  return (
    <Card
      className={cx(
        classes.selectedCabin,
        classes.cabinsCards,
        `${isSelected ? classes.focused : ""}`
      )}
      onClick={() => onSelect(cabin)}
    >
      <div className={cx(classes.heading)}>
        <div className={cx(classes.headingText)}>{cabin.name}</div>
        {!isDefault && (
          <DeleteIcon
            className={cx(classes.deleteIcon)}
            onClick={() => onRemove(cabin)}
          />
        )}
      </div>
      <div className={cx(classes.subHeading)}>Guest : {cabin.guest.total}</div>
      {deckNo && (
        <div className={cx(classes.subHeading)}>Deck No. : {deckNo}</div>
      )}
      {roomno && (
        <div className={cx(classes.subHeading)}>Room No. : {roomno}</div>
      )}
    </Card>
  );
};

export default selectedCabin;
