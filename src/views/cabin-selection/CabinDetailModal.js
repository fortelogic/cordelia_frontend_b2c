import React, { useState, useEffect } from "react";
import Modal from "../../components/Modal/index";
import FooterButtonSection from "../../components/UI/FooterButtonSection";
import { Button } from "antd";
import cx from "classnames";
import { useSelector } from "react-redux";
import classes from "./CabinDetailModal.module.scss";
import { ReactComponent as ScaleIcon } from "../../assets/icons/scale.svg";
import { ReactComponent as UserIcon } from "../../assets/icons/user.svg";
import { ReactComponent as DishIcon } from "../../assets/icons/dish.svg";
import { ReactComponent as EntertainIcon } from "../../assets/icons/entertain.svg";
import { ReactComponent as Ellipse } from "../../assets/icons/ellipse.svg";
import { Collapse } from "antd";
import Slider from "react-slick";
import "./index.css"
import localStore from '../../utils/localStore'
import ReactGA from "react-ga"


import {
  ArrowRightOutlined,
  ArrowLeftOutlined,
  CloseOutlined,
  DownOutlined,
} from "@ant-design/icons";

const { Panel } = Collapse;

const CabinDetailModal = (props) => {
  console.log("Props", props.details.imageList)
  const [imageList, setImageList] = useState([]);
  // const [constructed, setConstructed] = useState({})
  const { isShow, onClose, onSelect, catLength, details, onNextCategoryHandler, onPreviousCategoryHandler } = props;
  const { selectedCabin } = props
  const { availability } = props
  const { id, category_id, index, name, onOffersClickHandler, description, feature1, feature2, price, btnText = "Select", onDetail, onCabinSelect, priceKey, images, inclusions, is_offer, offer_description } = props;
  const booking = localStore.get("booking")

  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    centerPadding: "50px",
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
        },
      },
      {
        breakpoint: 767,
        settings: {
          dots: true,
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  //   const [details, setDetails] = useState(category);
  //   const [categoryIndex, setCategoryIndex] = useState(0);
  const { categories } = useSelector((state) => ({
    categories: state.booking.availability[0].categories,
  }))

  //Run on render
  //   useEffect(() => {
  //     debugger;
  //     setDetails(categories[category.id]);
  //   }, [isShow]);

  // const onSelectClickHandler = () => {
  //   onSelect();
  // };

  const onCabinSelectHandler = (category) => {
    // console.log("CategoryId", category)
    // console.log("Availability", availability)
    const constructed = availability.categories.find(item => item.category_id === category.category_id ? item : null)

    // const { id, category_id, name, description, feature1, feature2, price, priceKey, inclusions, imageList, is_offer, offer_description } = category
    // console.log("Category", category_id)
    // console.log("Name", name)
    // console.log("Description", feature1[0] + feature2[1])

    const id = category.index
    const category_id = category.category_id
    const name = category.name
    const description = category.description
    const feature1 = category.feature1
    const feature2 = category.feature2
    const price = constructed.price.total
    const priceKey = constructed.priceKey
    const inclusions = category.inclusions
    const is_offer = constructed.is_offer
    const offer_description = "NULL"

    if (booking) {
      const selectedCabin = { id, category_id, name, description, feature1, feature2, price, priceKey, inclusions, imageList, is_offer, offer_description };

      onCabinSelect(selectedCabin);

      const allports = []

      for (let i = 0; i < booking[0].ports.length; i++) {
        allports.push(booking[0].ports[i].port.name)
      }

      ReactGA.event({
        category: 'Room Type',
        action: 'User has selected the room type',
        value: 'Room-' + booking[0].nightCount + 'N-' + allports.join("-").toLowerCase(),
        label: 'Room-' + booking[0].nightCount + 'N-' + allports.join("-").toLowerCase()
      });
    } else {
      const booking = localStore.get("bookingSummary")

      const selectedCabin = { id, category_id, name, description, feature1, feature2, price, priceKey, inclusions, imageList, is_offer, offer_description };
      onCabinSelect(selectedCabin);

      const allports = []

      for (let i = 0; i < booking.itinerary.ports.length; i++) {
        allports.push(booking.itinerary.ports[i].port.name)
      }

      ReactGA.event({
        category: 'Room Type',
        action: 'User has selected the room type',
        value: 'Room-' + booking.itinerary.nightCount + 'N-' + allports.join("-").toLowerCase(),
        label: 'Room-' + booking.itinerary.nightCount + 'N-' + allports.join("-").toLowerCase()
      });
    }
    onSelect()
  }

  return (
    <Modal
      show={isShow}
      onClose={onClose}
      className={cx(classes.cabinDetailModal)}
    >
      <div className={cx(classes.header)}>
        <div>Cabin 1</div>
        <CloseOutlined className={classes.crossIcon} onClick={onClose} />
      </div>
      <div className={cx(classes.modalBody)}>
        <div className={classes.nameSection}>
          <Button
            type="primary"
            className={classes.arrowBtn}
            disabled={details.index == 0}
            onClick={() => { onPreviousCategoryHandler(); }}
          >
            <ArrowLeftOutlined />
          </Button>
          <div className={cx(classes.cabinName)}>{details.name}</div>
          <Button
            type="primary"
            className={classes.arrowBtn}
            onClick={onNextCategoryHandler}
            disabled={details.index == catLength - 1}
          >
            <ArrowRightOutlined />
          </Button>
        </div>
        <Slider className={classes.carousel} {...settings}>
          {details.imageList &&
            details.imageList.map((img, i) => {
              return (
                <div key={i} className={classes.slide}>
                  {/* <img src={img} /> */}
                  <img src={img.hasOwnProperty("src") ? "https://images.cordeliacruises.com" + img.src : img} />
                </div>
              );
            })}
          {details.imageList &&
            details.imageList.map((img, i) => {
              return (
                <div key={i} className={classes.slide}>
                  <img src={img.hasOwnProperty("src") ? "https://images.cordeliacruises.com" + img.src : img} />
                </div>
              );
            })}
          {details.images &&
            details.images.map((img, i) => {
              return (
                <div key={i} className={classes.slide}>
                  {/* <img src={"https://www.cordeliacruises.com/" + img.src} /> */}
                  <img src={img.hasOwnProperty("src") ? "https://images.cordeliacruises.com" + img.src : img} />
                </div>
              );
            })}
        </Slider>
        <div className={cx(classes.content)}>
          <div className={classes.description}>
            <p>{details.description}</p>
          </div>
          <div className="row no-gutters">
            <div className="col-md-6">
              <div className={classes.subHeading}>Room Feature</div>
              <div className={classes.subContent}>
                {details.feature2 ? (
                  <div>
                    <UserIcon className={classes.icon} width="20" height="20" />
                    <div>{details.feature2}</div>
                  </div>
                ) : null}
                {details.feature1 ? (
                  <div>
                    <ScaleIcon className={classes.icon} />
                    <div>{details.feature1}</div>
                  </div>
                ) : null}
              </div>
            </div>
            <div className="col-md-6">
              <div className={classes.subHeading}>Inclusions</div>
              <div className={classes.subContent}>
                {details.inclusions &&
                  details.inclusions.map((inclusion, i) => {
                    return (
                      <div>
                        <Ellipse
                          className={classes.icon}
                          width="15"
                          height="15"
                        />
                        <div>{inclusion}</div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
          <div className={classes.priceSection}>
            <Collapse
              className={classes.priceHeading}
              defaultActiveKey={[]}
              expandIconPosition="right"
              ghost
              expandIcon={(panelProps) => (
                <div className={cx(classes.subHeading, classes.price)}>
                  <span>
                    &#8377; {details.price ? details.price.total : ""}
                  </span>
                  <DownOutlined className={cx(classes.priceDetailIcon)} />
                </div>
              )}
            >
              <Panel className={classes.priceHeader} header="PRICE DETAILS" key="1">
                <div className={classes.priceDetails}>
                  <div className="row">
                    <div className="col-md-8 col-6">Fare</div>
                    <div className={cx("col-md-4 col-6", classes.amount)}>
                      {details && details.price.individual[0].fare}
                    </div>
                  </div>
                  {/* <div className="row">
                    <div className="col-md-8 col-6">PortCharges</div>
                    <div className={cx("col-md-4 col-6", classes.amount)}>
                      {details && details.price.individual[0].portCharges}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-8 col-6">Gratuity</div>
                    <div className={cx("col-md-4 col-6", classes.amount)}>
                      {details && details.price.individual[0].gratuity}
                    </div>
                  </div> */}
                  <div className="row">
                    <div className="col-md-8 col-6">Other Charges</div>
                    <div className={cx("col-md-4 col-6", classes.amount)}>
                      {details && details.price.individual[0].portCharges + details.price.individual[0].gratuity}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-8 col-6">Insurance</div>
                    <div className={cx("col-md-4 col-6", classes.amount)}>
                      {details && details.price.individual[0].insurance}
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-md-8 col-6" style={{ fontWeight: "600" }}>
                      Total
                    </div>
                    <div
                      className={cx("col-md-4 col-6", classes.amount)}
                      style={{ fontWeight: "600" }}
                    >
                      {details && details.price.individual[0].total}
                    </div>
                  </div>
                </div>
              </Panel>
            </Collapse>
          </div>
        </div>
      </div>
      <FooterButtonSection
        text="Select"
        // onClick={onSelectClickHandler}
        onClick={() => onCabinSelectHandler(details)}
        margin="2.2rem"
      />
    </Modal>
  );
};

export default CabinDetailModal;
