import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux"
import CabinCard from "./CabinCard";
import cx from "classnames";
import "./index.css";
import CabinDetailModal from "./CabinDetailModal";

const CabinSection = (props) => {
  const { onCabinSelect, availability, selectedCabin, onOffersClickHandler, selectedCabinIndex } = props;
  const [cabinDetailShow, setCabinDetailShow] = useState(false);
  const [cabinDetail, setCabinDetals] = useState("");


  const cabins = useSelector(state => state.allcabins)

  const cabinDetailCloseHandler = () => {
    setCabinDetailShow(false);
  };

  const cabinDetailHandler = (selectedCabin) => {
    console.log('details', selectedCabin);
    setCabinDetals(selectedCabin);
    setCabinDetailShow(true);
  };

  const cabinSelectHandler = (selectedCabin) => {
    setCabinDetailShow(false);
  };

  const getIndex = () => {
    let res = -1
    availability[selectedCabinIndex()].categories.map((data, index) => {
      if (data.category_id == cabinDetail.category_id) {
        res = index;
      }
    });
    return res;
  }

  const onPreviousCategoryHandler = (e) => {
    console.log(getIndex())
    if (getIndex() != 0) {
      let selected = {};
      let room = availability[selectedCabinIndex()].categories[getIndex() - 1];
      selected['id'] = room.id
      selected['index'] = getIndex() - 1
      selected['name'] = room.name;
      selected['category_id'] = room.category_id;
      selected['description'] = room.features[0] ? room.features[0] : "" + room.features[1] ? room.features[1] : "";
      selected['feature1'] = room.features[2] ? room.features[2] : "";
      selected['feature2'] = room.features[3] ? room.features[3] : "";
      selected['images'] = room.images;
      selected['inclusions'] = room.inclusions;
      selected['price'] = room.price;
      setCabinDetals(selected)
    }
  };

  const onNextCategoryHandler = (e) => {
    if (getIndex() + 1 < availability[selectedCabinIndex()].categories.length) {
      let selected = {};
      let room = availability[selectedCabinIndex()].categories[getIndex() + 1];
      selected['id'] = room.id;
      selected['index'] = getIndex() + 1
      selected['category_id'] = room.category_id;
      selected['name'] = room.name;
      selected['description'] = room.features[0] ? room.features[0] : "" + room.features[1] ? room.features[1] : "";
      selected['feature1'] = room.features[2] ? room.features[2] : "";
      selected['feature2'] = room.features[3] ? room.features[3] : "";
      // selected['images'] = room.images;
      selected['imageList'] = room.images;
      selected['inclusions'] = room.inclusions;
      selected['price'] = room.price;
      setCabinDetals(selected);
      console.log("Img",room.images)
    }
  };

  return (
    <>
      <CabinDetailModal
        isShow={cabinDetailShow}
        onClose={cabinDetailCloseHandler}
        onSelect={cabinSelectHandler}
        catLength={availability[selectedCabinIndex()].categories.length}
        details={cabinDetail}
        onNextCategoryHandler={onNextCategoryHandler}
        onPreviousCategoryHandler={onPreviousCategoryHandler}
        selectedCabin={selectedCabin}
        onCabinSelect={onCabinSelect}
        availability={availability[selectedCabinIndex()]}
      />
      <div className={cx(props.className)}>
        <div className="heading"><p className="inside-heading"><b>Select Your Cabin</b></p></div>
        {availability &&
          availability[selectedCabinIndex()].categories.map((room, index) => {
            return (
              <CabinCard
                images={room.images}
                key={index}
                id={index}
                index={index}
                category_id={room.category_id}
                name={room.name}
                description={`${room.features[0] ? room.features[0] : ""} ${room.features[1] ? room.features[1] : ""}`}
                feature1={`${room.features[2] ? room.features[2] : ""}`}
                feature2={`${room.features[3] ? room.features[3] : ""}`}
                price={room.price}
                priceKey={room.priceKey}
                inclusions={room.inclusions}
                is_offer={room.is_offer}
                offer_description={room.offer_description}
                onDetail={cabinDetailHandler}
                onCabinSelect={onCabinSelect}
                onOffersClickHandler={onOffersClickHandler}
                selectedCabin={selectedCabin}
                room={room}
              />
            );
          })}
      </div>
    </>
  );
};

export default CabinSection;
