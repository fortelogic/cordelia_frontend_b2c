import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import Card from '../../components/UI/Card';
import Carousel from '../../components/UI/Carousel';
import { ReactComponent as ScaleIcon } from '../../assets/icons/scale.svg';
import { Button } from "antd";
import cabin1 from '../../assets/img/cabin1.jpg';
import cabin2 from '../../assets/img/cabin2.jpg';
import cabin3 from '../../assets/img/cabin3.jpg';
import classes from "./SelectedCabinList.module.css";
import cx from "classnames";
import SpecialOffer from "../../assets/img/room-type/special-offer.png"
import ReactGA from "react-ga"
import localStore from '../../utils/localStore'
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded'
import Modal from "../../components/Modal/index";
import ModalCloseImage from "../../assets/img/room-type/iconmonstr-x-mark-thin-16.png";
import Checkmark from "../../assets/img/room-type/checkmark.png";


const CabinCard = (props) => {
    const [imageList, setImageList] = useState([]);
    const { selectedCabin } = props
    const { id, category_id, index, name, onOffersClickHandler, description, feature1, feature2, price, btnText = "Select", onDetail, onCabinSelect, priceKey, images, inclusions, is_offer, offer_description, room } = props;
    const [subCategoryModel,setSubCategoryModel] = useState(false);
    const cabins = useSelector((state) => state.allcabins)

    const onDetailClickHandler = (e) => {
        const selectedCabin = { id, index, category_id, name, description, feature1, feature2, price, priceKey, imageList, inclusions };
        onDetail(selectedCabin);
    };

    const booking = localStore.get("booking")

    const onCabinSelectHandler = (subcategory = null) => {

        if (booking) {
            if(subcategory != null) {
                console.log("with subcategory")
                const selectedCabin = room.sub_category.filter(data => data.category_id == subcategory)  
                onCabinSelect(selectedCabin[0])

            } else {
                console.log("aa without subcategory")
                const selectedCabin = { id, category_id, name, description, feature1, feature2, price, priceKey, inclusions, imageList, is_offer, offer_description };
                onCabinSelect(selectedCabin)
            }

            const allports = []

            for (let i = 0; i < booking[0].ports.length; i++) {
                allports.push(booking[0].ports[i].port.name)
            }

            ReactGA.event({
                category: 'Room Type',
                action: 'User has selected the room type',
                value: 'Room-' + booking[0].nightCount + 'N-' + allports.join("-").toLowerCase(),
                label: 'Room-' + booking[0].nightCount + 'N-' + allports.join("-").toLowerCase()
            });
        } else {
            const booking = localStore.get("bookingSummary")

            const selectedCabin = { id, category_id, name, description, feature1, feature2, price, priceKey, inclusions, imageList, is_offer, offer_description };
            onCabinSelect(selectedCabin);

            const allports = []

            for (let i = 0; i < booking.itinerary.ports.length; i++) {
                allports.push(booking.itinerary.ports[i].port.name)
            }

            ReactGA.event({
                category: 'Room Type',
                action: 'User has selected the room type',
                value: 'Room-' + booking.itinerary.nightCount + 'N-' + allports.join("-").toLowerCase(),
                label: 'Room-' + booking.itinerary.nightCount + 'N-' + allports.join("-").toLowerCase()
            });
        }

        // for (let i = 0; i < cabins.length; i++) {
        //     if (cabins[i].id === selectedCabin.id) {
        //         getSelectedCabin(cabins[i + 1])
        //         console.log("Cabins should", cabins[i + 1])
        //         setFocusCabinId(cabins[i + 1].id)
        //         break;
        //     }
        // }
        // console.log("Focused", focusedCabinId)
        // cabins.map((item, index) => {
        //     if (item.id === selectedCabin.id) {
        //         console.log("Next Cabin", cabins[index + 1])
        //         getSelectedCabin(cabins[index + 1])
        //         setFocusCabinId(cabins[index + 1].id)
        //     }
        // })
        console.log("Selected Cabins", selectedCabin)
    };

    useEffect(async () => {
        let res = images.map(({ src }) => {
            return `https://images.cordeliacruises.com${src}`
        })
        setImageList(res)
    }, [images]);

    const onClose = () => {
        setSubCategoryModel(!subCategoryModel);
    }

    return (
        <Card className={cx(selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === category_id ? classes.overlayCard : ``, "mt-4")}>
            <div className="row no-gutters position-relative">
                {
                    selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === category_id ? <CheckCircleRoundedIcon className={cx(classes.checkCircleRound)} /> : ``
                }
                <div className="col-md-5 position-relative">
                    {
                        is_offer &&
                        <img className={cx(classes.specialOffer)} src={SpecialOffer} />
                    }
                    <Carousel className={cx(classes.caroselImage)} id={id} imgs={imageList} />
                </div>
                <div className="col-md-7">
                    <div className="card-body">
                        <div className="row">
                            <h5 className={cx(classes.cardName, "card-title text-uppercase font-weight-bold")}>{name}</h5>
                            <a href='#' onClick={onDetailClickHandler} className={cx(classes.detailsMob)}>View Details</a>
                        </div>
                        <p className={cx(classes.description, "card-text text-muted")}>{description}</p>
                        <div className="row">
                            <div className="col-1">
                                <ScaleIcon style={{ marginRight: "0.5rem" }} />
                            </div>
                            <div className="col-10">
                                {/* <p className="text-muted text-capitalize mb-0">Room Size - {size} <span className={"text-uppercase"}>SQ.FT</span></p> */}
                                <p className="text-muted text-capitalize mb-0">{feature1}</p>
                                {feature2 ? <p className="text-muted text-capitalize mb-0">{feature2}</p> : null}
                            </div>
                        </div>
                        <a href='#' onClick={onDetailClickHandler} className={cx(classes.detailsWeb)}>Details</a>
                        {is_offer && <div className="view-offers">
                            <a href='#' onClick={onOffersClickHandler}>Check all 2 offers available on this cabin</a>
                        </div>}
                        <footer className="mt-3 row">
                            <div className="col-6">
                                <span>Starting from</span>
                                <h5 className="font-weight-bold mb-0" style={{ lineHeight: "30px" }}>&#8377;{` ` + price.total}</h5>
                            </div>
                            <div className="col-6">
                                {/* <Button type="primary" className="select-cabin-button" onClick={onCabinSelectHandler} disabled={selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === category_id}> */}
                                <Button type="primary" className="select-cabin-button" onClick={()=>room.hasOwnProperty("sub_category") && room.sub_category.length > 0 ? onClose() : onCabinSelectHandler() }>
                                    {selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === category_id ? `Selected` : btnText}
                                </Button>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
            <Modal
                show={subCategoryModel}
                onClose={onClose}
                className={cx(classes.mainModelSubCategory)}
            >
                <div className={cx(classes.mainModel)}>
                    {
                        room.hasOwnProperty("sub_category") && room.sub_category.length > 0 ? 
                            <div className="d-flex">
                                <div className={cx(selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === category_id ? classes.overlayCard : ``, "")}>
                                    {
                                        selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === category_id ? <CheckCircleRoundedIcon style={{ color: "#0F9D58", float: "right" }} /> : ``
                                    }
                                    <p className={cx(classes.modalCardHeading, "text-center")}> {name} </p>
                                    <div key={index} className={cx(classes.modalCard)}>
                                        <div>
                                            <Carousel className={cx(classes.caroselImage)} id={index} imgs={imageList} />
                                        </div>
                                        {/* {item.name} */}
                                        <div className={cx(classes.descriptionOfTheCategory)}>
                                            <p className={cx(classes.deckonModal)}>Deck no: 3, 4, 5, 6</p>
                                            <p className={cx(classes.benefitTitle)}>Special Benefits</p>
                                            <ul className={cx(classes.categoryBenefits)}>
                                                <li className={cx(classes.categoryBenefit)}><img className={cx(classes.benefitCheckmark)} src={Checkmark} /> Lorem Ipsum facility </li>
                                                <li className={cx(classes.categoryBenefit)}><img className={cx(classes.benefitCheckmark)} src={Checkmark} /> Lorem Ipsum facility </li>
                                                <li className={cx(classes.categoryBenefit)}><img className={cx(classes.benefitCheckmark)} src={Checkmark} /> Lorem Ipsum facility </li>
                                                <li className={cx(classes.categoryBenefit)}><img className={cx(classes.benefitCheckmark)} src={Checkmark} /> Lorem Ipsum facility </li>
                                            </ul>
                                            <p className={cx(classes.modalCabinPrice)}>₹ {` `} {price.total}/Cabin</p>
                                            <Button type="primary" className="select-cabin-button" onClick={() => onCabinSelectHandler()} style={{ width: "-webkit-fill-available" }}>
                                                {selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === category_id ? `Selected` : btnText}
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                                {
                                    room.sub_category.map((item, index) => {
                                        return <>
                                            <div className={cx(selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === item.category_id ? classes.overlayCard : ``, "")}>
                                                {
                                                    selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === item.category_id ? <CheckCircleRoundedIcon style={{ color: "#0F9D58", float: "right" }} /> : ``
                                                }
                                                <p className={cx(classes.modalCardHeading, "text-center")}> {item.name} </p>
                                                <div key={index} className={cx(classes.modalCard)}>
                                                    <div>
                                                        <Carousel className={cx(classes.caroselImage)} id={index} imgs={imageList} />
                                                    </div>
                                                    {/* {item.name} */}
                                                    <div className={cx(classes.descriptionOfTheCategory)}>
                                                        <p className={cx(classes.deckonModal)}>Deck no: 3, 4, 5, 6</p>
                                                        <p className={cx(classes.benefitTitle)}>Special Benefits</p>
                                                        <ul className={cx(classes.categoryBenefits)}>
                                                            <li className={cx(classes.categoryBenefit)}><img className={cx(classes.benefitCheckmark)} src={Checkmark} /> Lorem Ipsum facility </li>
                                                            <li className={cx(classes.categoryBenefit)}><img className={cx(classes.benefitCheckmark)} src={Checkmark} /> Lorem Ipsum facility </li>
                                                            <li className={cx(classes.categoryBenefit)}><img className={cx(classes.benefitCheckmark)} src={Checkmark} /> Lorem Ipsum facility </li>
                                                            <li className={cx(classes.categoryBenefit)}><img className={cx(classes.benefitCheckmark)} src={Checkmark} /> Lorem Ipsum facility </li>
                                                        </ul>
                                                        <p className={cx(classes.modalCabinPrice)}>₹ {` `} {item.price.total}/Cabin</p>
                                                        <Button type="primary" className="select-cabin-button" onClick={() => onCabinSelectHandler(item.category_id)} style={{ width: "-webkit-fill-available" }}>
                                                            {selectedCabin.hasOwnProperty("detail") && selectedCabin.detail.category_id === item.category_id ? `Selected` : btnText}
                                                        </Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </>
                                    })
                                }
                            </div>
                        : ``
                    }
                    <div className={cx(classes.modalCloseX)}>
                        {/* <svg  xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293 10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"/></svg> */}
                        <img onClick={()=>onClose()} src={ModalCloseImage} />
                    </div>
             
                </div>
            </Modal>
            {/* <p>{room.hasOwnProperty("sub_category") && room.sub_category.length > 0 ? room.sub_category[0].name : '' }</p> */}
        </Card>
    )
}

export default CabinCard;
