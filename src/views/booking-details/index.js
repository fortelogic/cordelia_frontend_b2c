import React,{useState, useEffect} from 'react';
import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container/index";
import styles from "./bookingdeatils.module.css"
import bg from "../../assets/img//web-check-in/checkin-bg.png";
import cx from "classnames";
import { useHistory } from 'react-router-dom';
import { CalendarOutlined} from '@ant-design/icons';
import Modal from '../../components/Modal';
import {
    Button,DatePicker
  } from "antd";


function BookingDeatils() {
    const [id, setId] = useState("");

    let history = useHistory();
    const queryParams = new URLSearchParams(window.location.search)


    const redirectToWebCheckIn=(()=>{
        if (queryParams.get("platform") && queryParams.get("platform") == "true") {
            window.location=`/web-check-in/guest-details/${id}?platform=true`
        } else {
            window.location=`/web-check-in/guest-details/${id}`
        }
    })

    useEffect(()=>{
       
    },[])

    return (
        <div>
            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Header className="mb-5" bgPurple  noAuth noMulticurrency webcheckin/>)
            }
            <Container wrapperClass={styles.waveBg}>
                <div className="px-3 px-md-0 mb-5"><h3 className={styles.heading}>Web Check-IN</h3></div>
                <div className={cx("container",styles.bookingDetails)}>
                    <div className="row">
                        <div className="col-12 col-md-3">
                            <p className={styles.label}>Booking Reference ID</p>
                            <input type="text" placeholder="Enter your Booking ID" onChange={(e)=>{setId(e.target.value)}}/>
                        </div>
                        {/* <div className="col-6 col-md-2">
                            <p className={styles.label}>Name</p>
                            <input type="text" placeholder="First Name" />
                        </div>
                        <div className="col-6 col-md-2">
                            <p className={styles.label}>&nbsp;</p>
                            <input type="text" placeholder="Last Name" />
                        </div>
                        <div className="col-12 col-md-3">
                            <p className={styles.label}>Choose Sailing Date</p>
                            <DatePicker bordered={false} className={styles.dateInput} placeholder="Choose Sailing date" suffixIcon={<CalendarOutlined className="inputIcon"/>}/>
                        </div> */}
                        <div className="col-12 col-md-2 text-center text-md-left">
                            <p className={cx(styles.label,"d-none d-md-block")}>&nbsp;</p>
                            <Button disabled={id.length == 0 } type="primary" className="bg-purple-button" onClick={()=>redirectToWebCheckIn()}>Check In</Button>
                        </div>
                    </div>

                </div>
                <h6 className={styles.headingSecond}>
                    Important Notes
                </h6>
                <ul className={cx ("mb-5",styles.list)}>
                    <li>All guests above 18 years need to have a mandatory double vaccination certificate & a negative RT-PCR report, 48 hours prior to sailing and should be ICMR approved with the QR code.</li>
                    <li>All guests below 18 years need to have a mandatory negative RT-PCR report, 48 hours prior to sailing and should be ICMR approved with the QR code.</li>
                    <li>All guests below 5 years will be exempted from the RT-PCR reports. </li>
                    <li>Pregnant women are only allowed to sail if pregnant for 24 completed weeks or less at time of cruise disembarkation.</li>
                    <li>The infant must have an ID with a photo, also authorization of either parent if travelling with only one parent</li>            
                    <li>Please fill in the attached Health Declaration form and keep it ready for verification in the terminal.</li>  
                </ul>
                <div className="py-5 mt-5"></div>
            </Container>
            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
            }
            {/* <Footer /> */}
        </div>
    );
}

export default BookingDeatils;