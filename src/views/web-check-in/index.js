import React,{useState, useEffect} from 'react';
import Details from "../../components/WebCheckIn/Details";
import Form from "../../components/WebCheckIn/Form";
import FormFooter from "../../components/WebCheckIn/Footer";
import Header from "../../components/Header/HeaderHome";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container/index";
import styles from "./webcheckin.module.css"
import bg from "../../assets/img//web-check-in/checkin-bg.png";
import cx from "classnames";
import { useHistory,useParams } from 'react-router-dom';
import Modal from '../../components/Modal';
import {
    Button
  } from "antd";
import {getBooking,saveGuestDetails} from '../../services/webcheckin' 


let bedArray = []

function WebCheckIn(props) {
    const [booking, setBooking] = useState();
    const [roomData, setRoomData] = useState();
    const [itineraryData, setItineraryData] = useState();
    const [pricingData, setPricingData] = useState();
    const [total, setTotal] = useState();
    const [tax, setTax] = useState();
    const [id,setId]=useState();
    const [isInternational,setIsInternational]=useState();
    const [guests, setGuests]= useState()
    const [health, setHealth]= useState()
    const [status, setStatus]= useState("Loading")
    const [error, setError]= useState()
    const [message, setMessage]= useState()
    const [loading, setLoading]= useState(true)
    const [roomBedSelection, setRoomBedSelection] = useState([])
    const [valid, setValid]= useState(false)
    let { refNumber } = useParams();

    let history = useHistory();
    let guestArray=[]
    let healthArray=[]
    const queryParams = new URLSearchParams(window.location.search)


    // const redirectToBoarding=(()=>{
    //     setIsModalShow(false);
    // },()=>window.location="/boarding-pass")

    useEffect(async ()=>{
        setLoading(true)
        console.log(process.env,"nik-log")
        const response = await getBooking(refNumber)
        console.log(response,"nik-log")
        if(response===undefined){
            setError("Invalid Booking ID")
            setStatus(false)
            setLoading(false)
        }else if(response.status===200&&response.data.status!=="failed"){
            if(response.data.booking.rooms[response.data.booking.rooms.length-1].guests[0].web_checkin_done){
                window.location=`/web-check-in/boarding-pass/${response.data.booking.id}`
            }
            setId(response.data.booking.id)
            setBooking(response.data.booking)
            setRoomData(response.data.booking.rooms)
            setPricingData(response.data.booking.rooms)
            setTotal(parseFloat(response.data.booking.total_amnt))
            setTax(response.data.booking.gst)
            var resposnseRequest;
            response.data.booking.rooms.map((data,i)=> {
                resposnseRequest = {
                    is_selected: 0
                }
                bedArray.push(resposnseRequest)
            })

            console.log(bedArray)
            setRoomBedSelection(bedArray)
            checkIfInternational(response.data.booking.itinerary.ports)
            setItineraryData(response.data.booking.itinerary)
            if(response.data.booking.status==="CONFIRMED"){
                setStatus(true)
                setError(false)
            } else{
                setError("Invalid Booking ID")
                setStatus(false)
            }
            response.data.booking.rooms.map(room=>{
                        room.guests.map(guest=>{
                            pushToArray(guestArray,guest);
                            pushToHealthArray(healthArray,guest);})
            })
            setGuests(guestArray)
            setHealth(healthArray)
            setLoading(false)
        }else if(response.status===200&&response.data.status==="failed"){
            setError(response.data.message)
            setStatus(false)
            setLoading(false)
        }else if(response.status===400){
            setError("Invalid Booking ID")
            setStatus(false)
            setLoading(false)
        }
    },[])

    const handleBedPreference = (index, value) => {
        let newArr = [...roomBedSelection];
        newArr[index].is_selected = value
        setRoomBedSelection(newArr)
    }

    const checkIfInternational =(array)=>{
        let obj = array.find(o => o.country !== "India");
        console.log(obj,"nik-log international chekc")
        if(obj===undefined){
            setIsInternational(false)
        }else setIsInternational(true)
    }

    const pushToArray =(array,guest) =>{
        let obj = array.find(o => o.id === guest.id);
        if(obj===undefined){
            array.push({
                "id":guest.id,
                "country": guest.country,
                "state": guest.state,
                "meal":guest.meal,
                "comments": guest.comments,
                "web_checkin_doc_type": guest.web_checkin_doc_type,
                "web_checkin_doc_number": guest.web_checkin_doc_number,
                "web_checkin_doc_place": guest.web_checkin_doc_place,
                "web_checkin_doc_date": guest.web_checkin_doc_date,
                // "is_vaccinated":false
            })
        }
    }
    const pushToHealthArray =(array,guest)=>{
        let obj = array.find(o => o.id === guest.id);
        if(obj===undefined){
            array.push({
                "id":guest.id,
                "declaration":guest.declaration_accepted?guest.declaration_accepted:false,
                "checkbox":false
            })
        }
    }
    const checkForm=(guestArray,healthArray)=>{
        let valid1,valid2,valid3 =null
        if(healthArray){
        for (let i = 0; i < healthArray.length; i++) {
            if (healthArray[i]["checkbox"] ===false) {
                // setMessage("Please accept that you have not been tested COVID positive")
                valid1 = false;
                break;
            }
        }}else valid1=false
        console.log(valid1,"nik-log valid 1")
        if(healthArray){
            for (let i = 0; i < healthArray.length; i++) {
                // console.log(healthArray[i]["declaration"],"nik-log")
            if (healthArray[i]["declaration"]===false) {
                valid2 = false;
                break;
            }
        }}else valid2=false
        console.log(valid2,"nik-log valid 2")
        if(guestArray){
            for (let i = 0; i < guestArray.length; i++) {
            if (
                guestArray[i]["state"]===null||guestArray[i]["state"]==="Choose a State"||
                guestArray[i]["web_checkin_doc_number"]===null||guestArray[i]["web_checkin_doc_number"]===""||
                guestArray[i]["web_checkin_doc_type"]===null||guestArray[i]["web_checkin_doc_type"]==="") {
                    console.log(guestArray[i],"nik-log")
                    valid3 = false;
                    break;
            }
        }}else valid3=false
        console.log(valid3,"nik-log valid 3")
        if(
            valid1!==false&&
            valid2!==false
            &&
            valid3!==false
            ){
            setMessage("")
            setValid(true)
            console.log(valid,"nik-log valid final")
            return true
        }
        else if(valid3===false){
            setMessage("Please enter all guest details")
            setValid(false)
            return false
        }
        else if(valid2===false){
            setMessage("Please complete health declaration for each guest")
            setValid(false)
            return false
        }
        else if(valid1===false){
            setMessage("Please accept that you have not been tested COVID positive")
            setValid(false)
            return false
        }
    }

    const updateGuestDetails =async()=>{
        // setGuests(guestArray)
        // setHealth(healthArray)
        if(valid||checkForm(guests,health))
        {
            setLoading(true)
            var formData = new FormData();
            formData.append("booking_id",id)
            guests.map((obj)=>{
                Object.keys(obj).forEach((key)=>{
                    if(obj[key]!==null){
                        formData.append(`guests[]${key}`, obj[key]);
                    }
                });
            })
            roomData.map((obj,i) => {
                Object.keys(obj).forEach((key)=>{
                    if(key == 'id') {
                        formData.append(`rooms[]${key}`, obj.id);
                    }
                })   
                formData.append(`rooms[]bed_selection`, roomBedSelection[i].is_selected == 0 ? "twin_bed" : "double");
            })
            const response = await saveGuestDetails(formData)
            if(response.status===200){
                if(response.data[0].status!=="failure"){
                    window.location=`/web-check-in/boarding-pass/${id}`
                }
                else{
                    setMessage(response.data[0].message)
                }
            }
            setLoading(false)
        }
    }
    return (
        <div>
            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Header className="mb-5" bgPurple noAuth noMulticurrency webcheckin />)
            }
            <Container wrapperClass={styles.waveBg}>
                <div className="px-3 px-md-0">
                    <h3 className={styles.heading}>Web Check-IN</h3>
                </div>
                {!loading&&!error ? <> 
                        <Details 
                            roomData={roomData} 
                            itineraryData={itineraryData} 
                            pricingData={pricingData} 
                            total={total} tax={tax} 
                            booking={booking} 
                            handleBedPreference={handleBedPreference} 
                            roomBedSelection={roomBedSelection}
                        />
                    <div className="px-3 px-md-0">
                        <h6 className={styles.headingSecond}>Please Provide Following Details for web check in </h6>
                    </div>
                    {roomData ? 
                        <Form roomData={roomData} bookingId={id} setGuestState={setGuests} setHealth={setHealth} guestArray={guests} healthArray={health} isInternational={isInternational}/>
                    :null 
                    }
                    <div className="px-3 px-md-0">
                        <h6 className={styles.headingSecond}>
                            Important Notes
                        </h6>
                        <ul className={cx ("mb-5",styles.list)}>
                            <li><strong>Kindly carry QR Code Negative RT-PCR test report not earlier than 48 hours of Departure</strong></li>
                            <li> Pregnant women are only allowed to sail if pregnant for 24 completed weeks or less at timeof cruise disembarkation.</li>
                            <li>Infant must have an ID with photo, also authorization of either parent if travelling with only one parent</li>
                            <li>RT-PCR test with negative result 48 Hrs. Prior to Sailing is mandatory for all guests irrespective of vaccination checks.</li>
                            <li>QR CODE RT-PCR test is not mandatory for children below 14 years of Age.</li>            
                            <li>Please fill in the attached Health Declaration form and keep it ready for verification in the terminal.</li>   
                        </ul>
                    </div>
                </>:
                    error? <h6 className={cx(styles.headingSecond,"text-center py-4 my-5")}>
                    {error}
                </h6>:<div className="my-5 py-5">
                    <div className="text-center mx-auto mb-3">Please Wait...</div>
                    <div class="spinner-border d-flex justify-content-center mx-auto" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>}
                {/* <h6 className={cx(styles.headingSecond,"text-center py-4 my-5")}>
                    {error}
                </h6> */}
                {/* <FormFooter openModal={()=>setIsModalShow(true)}/> */}
                <div className="text-center">
                    <h6 className={cx(styles.headingSecond,"text-center text-danger")}>
                    {message}
                    </h6>
                </div>
                    <div className="text-center mb-5">
                        <Button 
                            type="primary" 
                            className="bg-purple-button" 
                            // onClick={()=>{ history.push("/boarding-pass")}}
                            onClick={()=>updateGuestDetails()}
                            disabled={status!==true}
                        >
                            {loading ? <div class="spinner-border spinner-border-sm text-light" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>:"Proceed"}
                        </Button>
                </div>
            </Container>
            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
            }
        </div>
    );
}

export default WebCheckIn;