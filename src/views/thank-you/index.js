import React from "react"
import Header from "../../components/Header/HeaderNoBanner";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Card from "../../components/UI/Card";
import Footer from "../../components/Footer/Footer";
import "./thank-you.scss";
import CheckIcon from "../../assets/img/check-icon.svg";
import InteriarImg from "../../assets/img/interiar-img.jpg";
import ShipIcon from "../../assets/img/ship-icon.svg";
import LocalStore from '../../utils/localStore'
import moment from 'moment'
import bookingdetails from "../../services/booking-details"
import Carousel from "../../components/UI/Carousel";
import { Link } from "react-router-dom"

// function RoomSelection() {
function Thankyou() {

  const queryParams = new URLSearchParams(window.location.search)
  const [bookingDetails, setBookingDetails] = React.useState([])
  const summary = LocalStore.get("bookingSummary")
  const cabins = LocalStore.get("cabins")
  const allguests = LocalStore.get("allguests")
  const itineraryID = LocalStore.get("itineraryID")

  React.useEffect(async () => {
    const { data } = await bookingdetails(summary.booking.id);
    setBookingDetails(data)
  }, [])


  return bookingDetails.hasOwnProperty("booking") ? (
    <div className="Home">
      <Header className="mb-5" platform={queryParams.get("platform")} />
      <div className="thank-you-section">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="thankyou--pg text-center">
                <h1 className="title-fst">Thank you!</h1>
                <h3 className="sub-title-scnd"><img src={CheckIcon} /> Your Booking is confirmed</h3>
                <p className="booking-id">Booking ID: <span>{summary.booking.number}</span></p>
                <p className="inform--ticket">E-Ticket has been sent to your registered mobile nubmer and Email ID</p>
              </div>
              <div className="download-buttons-for-invoices-voucher">
                {/* {/* <a href={bookingDetails.booking.invoice_url} target="_blank"><button className="download-invoice">Download Invoice </button></a> */}
                {/* <a href={bookingDetails.booking.evoucher_url} role="link" tabIndex="0" download={true}><button className="download-e-voucher">Download E-Voucher </button></a> */}
                <Link to={bookingDetails.booking.invoice_url} target="_blank" download>
                  <button className="download-invoice">Download Invoice </button>
                </Link>
                <Link to={bookingDetails.booking.evoucher_url} target="_blank" download>
                  <button className="download-e-voucher">Download E-Voucher </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="full-card--section">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <Card className="cardtnk-bg">
                <div className="card-header card-title-main">
                  <h3 className="title-card-heading">BOOKING SUMMARY</h3>
                </div>
                <div className="card-body p-0">
                  <div className="row rowbody--card">
                    <div className="col-12 col-lg-3">
                      <div className="summary-loaction-col">
                        <div className="txt--local--sum">
                          <h5 className="statehead--local">{bookingDetails.booking.itinerary.ports[0].city}</h5>
                          <span className="city--localhead">{bookingDetails.booking.itinerary.ports[0].country}</span>
                        </div>
                        <div className="bottom-timeorder">
                          <h4><span>IST {moment(bookingDetails.booking.itinerary.start_time).format("hh:mm A")}</span><span>{moment(bookingDetails.booking.itinerary.end_time).format("ddd, D-MMM-YYYY")}</span></h4>
                          <p className="txt--col--viewmain">{bookingDetails.booking.itinerary.ports[0].city}</p>
                          <span className="mapLink--sumry"><a href="#" className=""><i>Google Map Link</i></a></span>
                        </div>
                      </div>
                    </div>
                    <div className="col-12 col-lg-6">
                      <div className="order--locatin-ship">
                        <div className="ship--icon-middle">
                          <div className="Img--iconShip"><img src={ShipIcon} /></div>
                          <span className="in-out--ship">2 halting ports</span>
                        </div>
                        <div className="halting--port-last">
                          <p><strong>Halting Ports</strong>: <span>Cochin (30 July 2021)</span><span>Chennai (04 August 2021)</span></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-12 col-lg-3">
                      <div className="summary-loaction-col">
                        <div className="txt--local--sum">
                          <h5 className="statehead--local">{bookingDetails.booking.itinerary.ports[bookingDetails.booking.itinerary.ports.length - 1].city}</h5>
                          <span className="city--localhead">{bookingDetails.booking.itinerary.ports[bookingDetails.booking.itinerary.ports.length - 1].country}</span>
                        </div>
                        <div className="bottom-timeorder">
                          <h4><span>IST {moment(bookingDetails.booking.itinerary.end_time).format("hh:mm A")}</span><span>{moment(bookingDetails.booking.itinerary.end_time).format("ddd, D-MMM-YYYY")}</span></h4>
                          <p className="txt--col--viewmain">{bookingDetails.booking.itinerary.ports[bookingDetails.booking.itinerary.ports.length - 1].city}</p>
                          <span className="mapLink--sumry"><a href="#" className=""><i>Google Map Link</i></a></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Card>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <Card className="cardtnk-bg">
                <div className="card-header card-title-main">
                  <h3 className="title-card-heading">CABIN DETAILS</h3>
                </div>
                <div className="card-body p-0">
                  <div className="row rowbody--card">
                    {
                      bookingDetails.booking.rooms.map((item, index) => {
                        return <div className="col-12 col-lg-6">
                          <div className="card-detail-grid-box">
                            <h4 className="detial--title-activity">Cabin {index + 1}</h4>
                            <div className="interior--box-card">
                              <div className="thankyou--cabin-img">
                                {/* <img className="Img--room" src={InteriarImg} /> */}
                                {/* <img className="Img--room" src={item.images[0]} /> */}
                                <Carousel id={"random" + index} imgs={item.images} className="carousel" />
                              </div>
                              <div className="thnk--dtl-info">
                                <div className="dtl--rightflex--wig">
                                  <h5 className="interior--title">{item.code}</h5>
                                  <div className="room-viewtxt">
                                    <span className="cmn--txt--room">Deck No: {item.deck_no}</span>
                                    <span className="cmn--txt--room">Room No: {item.number}</span>
                                  </div>
                                  <div className="room-tottal-gst">
                                    <span className="spn--totl-gst">Total Guests: <span>{item.guests.length} Guests</span></span>
                                  </div>
                                  <ul className="listcard--nameview">
                                    {
                                      item.guests.map((guest) => {
                                        return <li className="tt-list--itemsmain">{guest.first_name + ` ` + guest.last_name}</li>
                                      })
                                    }
                                    {/* <li className="tt-list--itemsmain">Samuel John</li>
                                    <li className="tt-list--itemsmain">Esther Howard</li>
                                    <li className="tt-list--itemsmain">Jenny Wilson</li> */}
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      })
                    }


                    {/* <div className="col-12 col-lg-6">
                      <div className="card-detail-grid-box">
                        <h4 className="detial--title-activity">Cabin 1</h4>
                        <div className="interior--box-card">
                          <div className="thankyou--cabin-img">
                            <img className="Img--room" src={InteriarImg} />
                          </div>
                          <div className="thnk--dtl-info">
                            <div className="dtl--rightflex--wig">
                              <h5 className="interior--title">INTERIOR</h5>
                              <div className="room-viewtxt">
                                <span className="cmn--txt--room">Deck No: 05</span>
                                <span className="cmn--txt--room">Room No: 2996</span>
                              </div>
                              <div className="room-tottal-gst">
                                <span className="spn--totl-gst">Total Guests: <span>03 Guests</span></span>
                              </div>
                              <ul className="listcard--nameview">
                                <li className="tt-list--itemsmain">Samuel John</li>
                                <li className="tt-list--itemsmain">Esther Howard</li>
                                <li className="tt-list--itemsmain">Jenny Wilson</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-12 col-lg-6">
                      <div className="card-detail-grid-box">
                        <h4 className="detial--title-activity">Cabin 2</h4>
                        <div className="interior--box-card">
                          <div className="thankyou--cabin-img">
                            <img className="Img--room" src={InteriarImg} />
                          </div>
                          <div className="thnk--dtl-info">
                            <div className="dtl--rightflex--wig">
                              <h5 className="interior--title">OCEAN VIEW</h5>
                              <div className="room-viewtxt">
                                <span className="cmn--txt--room">Deck No: 05</span>
                                <span className="cmn--txt--room">Room No: 2996</span>
                              </div>
                              <div className="room-tottal-gst">
                                <span className="spn--totl-gst">Total Guests: <span>03 Guests</span></span>
                              </div>
                              <ul className="listcard--nameview">
                                <li className="tt-list--itemsmain">Samuel John</li>
                                <li className="tt-list--itemsmain">Esther Howard</li>
                                <li className="tt-list--itemsmain">Jenny Wilson</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> */}

                  </div>
                </div>
              </Card>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <Card className="cardtnk-bg">
                <div className="card-header card-title-main">
                  <h3 className="title-card-heading">SHORE EXCURSION DETAILS</h3>
                </div>
                <div className="card-body p-0">
                  <div className="row rowbody--card">
                    <div className="col-12 col-lg-6">
                      <div className="card-detail-grid-box">
                        <h4 className="detial--title-activity">Activity 1</h4>
                        <div className="interior--box-card">
                          <div className="thankyou--cabin-img">
                            <img className="Img--room" src={InteriarImg} />
                          </div>
                          <div className="thnk--dtl-info">
                            <div className="dtl--rightflex--wig">
                              <h5 className="interior--title">CHENNAI CITY TOUR</h5>
                              <div className="room-tottal-gst">
                                <span className="spn--totl-gst">Date: <span>04 August 2021</span></span>
                              </div>
                              <div className="room-tottal-gst">
                                <span className="spn--totl-gst">Port: <span>Chennai</span></span>
                              </div>
                              <div className="short--disc-thanky">
                                <p>Lorem ipsum is a simply dummy text used in typing industry since 1500’s</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-12 col-lg-6">
                      <div className="card-detail-grid-box">
                        <h4 className="detial--title-activity">Activity 2</h4>
                        <div className="interior--box-card">
                          <div className="thankyou--cabin-img">
                            <img className="Img--room" src={InteriarImg} />
                          </div>
                          <div className="thnk--dtl-info">
                            <div className="dtl--rightflex--wig">
                              <h5 className="interior--title">CHENNAI CITY TOUR</h5>
                              <div className="room-tottal-gst">
                                <span className="spn--totl-gst">Date: <span>04 August 2021</span></span>
                              </div>
                              <div className="room-tottal-gst">
                                <span className="spn--totl-gst">Port: <span>Chennai</span></span>
                              </div>
                              <div className="short--disc-thanky">
                                <p>Lorem ipsum is a simply dummy text used in typing industry since 1500’s</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Card>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <Card className="cardtnk-bg">
                <div className="card-header card-title-main">
                  <h3 className="title-card-heading">PAYMENT DETAILS</h3>
                </div>
                <div className="card-body p-0">
                  <div className="row rowbody--card">
                    <div className="col-12 col-md-7 col-xl-8">
                      <div className="cancell-txt-content">
                        <h4 className="title-txt-cancwl">Cancellation & Reschedule Policy</h4>
                        <ul className="items--cancel--list">
                          <li>Cancellation fee will be applicable only on cabin fare</li>
                          <li>Port Charges & gratuity will be refunded completely.</li>
                          <li>Free rescheduling is limited to one time</li>
                          <li>Rescheduled itinerary must commence on or before 31st March 2022</li>
                          <li>Fare difference is applicable for higher cabin category or increase in sailing nights</li>
                        </ul>
                        <p>For more Detailed information <a className="btn-more-dtl" href="#">click here</a></p>
                      </div>
                    </div>
                    <div className="col-12 col-md-5 col-xl-4">
                      <div className="payment--priceTable-sumry">
                        <h4 className="title--sumry-paymnt">Payment Summary</h4>
                        {/* <ul className="payment-sumary-list">
                          <li className="payment-sumary-td"><span className="payment-title-sum payment-title-sum-1">Cabin 1</span><span className="payment-title-sum payment-title-sum-2">₹12,256</span></li>
                          <li className="payment-sumary-td"><span className="payment-title-sum payment-title-sum-1">Cabin 2</span><span className="payment-title-sum payment-title-sum-2">₹22,256</span></li>
                          <li className="payment-sumary-td"><span className="payment-title-sum payment-title-sum-1">Port Charges</span><span className="payment-title-sum payment-title-sum-2">₹2,256</span></li>
                          <li className="payment-sumary-td sumry-discount"><span className="payment-title-sum payment-title-sum-1">Discount</span><span className="payment-title-sum payment-title-sum-2">₹2,256</span></li>
                          <li className="payment-sumary-td"><span className="payment-title-sum payment-title-sum-1">GST</span><span className="payment-title-sum payment-title-sum-2">₹3,256</span></li>
                          <li className="payment-sumary-td total-amt-sumry mt-3"><span className="payment-title-sum payment-title-sum-1">Grand Total</span><span className="payment-title-sum payment-title-sum-2">₹92,000</span></li>
                          <li className="payment-sumary-td total-amt-sumry"><span className="payment-title-sum payment-title-sum-1">Amount Paid</span><span className="payment-title-sum payment-title-sum-2">₹92,000</span></li>
                        </ul> */}

                        <ul className="payment-sumary-list">
                          {
                            bookingDetails.booking.rooms.map((item, index) => {
                              return <><li className="payment-sumary-td"><span className="payment-title-sum payment-title-sum-1">{item.code}</span><span className="payment-title-sum payment-title-sum-2">₹ {item.cabin_fare}</span></li>
                                <li className="payment-sumary-td"><span className="payment-title-sum payment-title-sum-1">Port Charges</span><span className="payment-title-sum payment-title-sum-2">₹ {item.port_charges}</span></li>
                                <li className="payment-sumary-td sumry-discount"><span className="payment-title-sum payment-title-sum-1">Discount</span><span classNam e="payment-title-sum payment-title-sum-2">₹ {item.discount}</span></li>
                                <li className="payment-sumary-td"><span className="payment-title-sum payment-title-sum-1">GST</span><span className="payment-title-sum payment-title-sum-2">₹ {item.gst}</span></li>
                              </>
                            })
                          }
                          <li className="payment-sumary-td total-amt-sumry mt-3"><span className="payment-title-sum payment-title-sum-1">Grand Total</span><span className="payment-title-sum payment-title-sum-2">₹ {bookingDetails.booking.price_details.grand_total.total}</span></li>
                          {/* <li className="payment-sumary-td total-amt-sumry"><span className="payment-title-sum payment-title-sum-1">Amount Paid</span><span className="payment-title-sum payment-title-sum-2">₹ 92,000</span></li> */}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </Card>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col-12">
              <div className="text-center thank-you-btn">
                <span className="mb-3 mb-sm-0">
                  <Link to={`/destination?bookingid=${bookingDetails.booking.id}`}>
                    <button type="button" className="btn btn--choose mr-0 mr-sm-2">Choose add-on</button>
                  </Link>
                </span>
                <span className="mb-3 mb-sm-0">
                  <Link to={`/my-bookings`}>
                    <button type="button" className="btn btn--choose mr-0 mr-sm-2">My Bookings</button>
                  </Link>
                </span>
                {/* <span>
                  <button type="button" className="btn btn--webcheck ml-0 ml-sm-2">Web Check In</button>
                </span> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <Footer /> */}
      {
        (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
      }
    </div>
  ) : ``;
}

// export default RoomSelection;
export default Thankyou
