import React, { useEffect, useState } from "react";
import cx from "classnames";
import "./index.css";
import Card from "../../components/UI/Card";
import { Button } from "antd";
import { useDispatch, useSelector } from "react-redux";
// import Deck from "../../assets/img/deck-selection/deck-selection.svg"
import D0 from "../../assets/img/deck-selection/0.svg"
// import D3 from "../../assets/img/deck-selection/1.svg"
// import D4 from "../../assets/img/deck-selection/2.svg"
import D3 from "../../assets/img/deck-selection/3.svg"
import D4 from "../../assets/img/deck-selection/4.svg"
import D5 from "../../assets/img/deck-selection/5.svg"
import D6 from "../../assets/img/deck-selection/6.svg"
import D7 from "../../assets/img/deck-selection/7.svg"
import D8 from "../../assets/img/deck-selection/8.svg"
import D9 from "../../assets/img/deck-selection/9.svg"
import D10 from "../../assets/img/deck-selection/10.svg"
import D11 from "../../assets/img/deck-selection/11.svg"


let selectedStyle = {
  color: "white", background: '#EA725B'
}

const ActionButton = (props) => {

  const { selectedCabin: { deck } } = props

  return (
    !props.deck.selectable ? (
      <div className="occupied">Occupied</div>
    ) : (
      <Button style={deck !== undefined && deck.id === props.deck.id ? selectedStyle : null} onClick={() => { props.addDeck(props.deck) }}
        disabled={deck !== undefined && deck.id === props.deck.id}>
        {deck !== undefined && deck.id === props.deck.id ? `Selected` : `Select`}
      </Button>
    )
  )
}

const DeckSection = (props) => {
  const { deckCollection, addDeck, cabins, pointer, setPointer } = props;
  const { selectedCabin } = props
  const [selectedDeck, setSelectedDeck] = useState(0);
  const [hoverDeck, setHoverDeck] = useState(0);

  const onSelectDeck = (deck) => {
    setSelectedDeck(11)
    addDeck(deck)
  }

  return (
    <div className={cx(props.className)}>
      <div className="heading">Choose a Deck</div>
      <h4 className="subHeading mt-3">
        Please Select A Deck From Below To See The Available Room Options
      </h4>

      <div className="deck-graph">
        <img height="250px" className={hoverDeck == 3 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 3 ? (hoverDeck != 3 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D3} />
        <img height="250px" className={hoverDeck == 4 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 4 ? (hoverDeck != 4 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D4} />
        <img height="250px" className={hoverDeck == 5 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 5 ? (hoverDeck != 5 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D5} />
        <img height="250px" className={hoverDeck == 6 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 6 ? (hoverDeck != 6 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D6} />
        <img height="250px" className={hoverDeck == 7 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 7 ? (hoverDeck != 7 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D7} />
        <img height="250px" className={hoverDeck == 8 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 8 ? (hoverDeck != 8 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D8} />
        <img height="250px" className={hoverDeck == 9 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 9 ? (hoverDeck != 9 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D9} />
        <img height="250px" className={hoverDeck == 10 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 10 ? (hoverDeck != 10 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D10} />
        <img height="250px" className={hoverDeck == 11 ? "d-block" : (selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 11 ? (hoverDeck != 11 && hoverDeck != 0) ? "d-none" : "d-block" : "d-none")} src={D11} />
        <img height="250px" className={!selectedCabin.hasOwnProperty("deck") ? hoverDeck != 0 ? "d-none putin-sticky" : "d-block putin-sticky" : "d-none"} src={D0} />

        {/* <img className={selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 3 ? "d-block deck-three" : "deck-three"} src={D3} />
        <img className={!selectedCabin.hasOwnProperty("deck") ? "d-block" : "d-none"} src={D0} /> */}
        {/* {
          selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 3 ?
            <img className="deck-image" src={D3} /> :
            selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 4 ?
              <img className="deck-image" src={D4} /> :
              selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 5 ?
                <img className="deck-image" src={D5} /> :
                selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 6 ?
                  <img className="deck-image" src={D6} /> :
                  selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 7 ?
                    <img className="deck-image" src={D7} /> :
                    selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 8 ?
                      <img className="deck-image" src={D8} /> :
                      selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id == 9 ?
                        <img className="deck-image" src={D9} /> :
                        <img className="deck-image" src={D0} />

        } */}
      </div>

      <Card className="mt-4 overflow-hidden">
        <table className="table">
          <thead className="tHeader">
            <tr>
              <th className="align-middle">Deck NO.</th>
              <th className="align-middle">Details</th>
            </tr>
          </thead>
          <tbody className="tbody" style={{ direction: 'revert' }}>
            {deckCollection &&
              deckCollection.slice().reverse().map((deck) => (
                // className = { deck !== undefined && cabins !== undefined && cabins.deck !== undefined && deck.id === cabins.deck.id ? `deckback` : ``}
                <tr className={selectedCabin.hasOwnProperty("deck") && selectedCabin.deck.id === deck.id ? `deckback decksclass` : `decksclass`} key={deck.id} onMouseEnter={() => { setHoverDeck(deck.id) }} onMouseLeave={() => setHoverDeck(0)}>
                  <td>{deck.id}</td>
                  <td className={`decksclass`}>
                    <span>{deck.description}</span>
                    <ActionButton selectedDeck={selectedDeck} setSelectedDeck={setSelectedDeck} addDeck={onSelectDeck} deck={deck} selectedCabin={selectedCabin} />
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </Card>
    </div>
  );
};

export default DeckSection;
