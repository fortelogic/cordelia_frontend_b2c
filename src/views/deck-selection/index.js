import Header from "../../components/Header/HeaderHome";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container/index";
import SelectedCabinList from "../cabin-selection/SelectedCabinList";
import DeckSection from "./DeckSection";
import FooterButtonSection from "../../components/UI/FooterButtonSection";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getDecks } from "../../redux/actions/deckAction";
import { addDeck } from "../../redux/actions/cabinAction";
import localStore from "../../utils/localStore";
import Loader from "../../components/Loading/LoadingIcon";
import { useHistory } from "react-router-dom";

function RoomSelection() {
  const dispatch = useDispatch();
  let history = useHistory();
  const [isLoading, setIsLoading] = useState(true);
  const [selectedCabin, setSelectedCabin] = useState('');
  const [pointer, setPointer] = useState(0)
  const { deckCollection } = useSelector((state) => ({
    deckCollection: state.deckCollection,
  }));
  const [focusedCabinId, setFocusCabinId] = useState("");


  const { cabinList } = useSelector((state) => ({
    cabinList: state.allcabins,
  }));

  const queryParams = new URLSearchParams(window.location.search)

  useEffect(async () => {
    window.scrollTo(0, 0);
    setIsLoading(true);
    const itineraryID = localStore.get("itineraryID");
    const booking = localStore.get("bookingSummary")
    const cabin = localStore.get("cabins")

    if (queryParams.get("from")) {
      if (queryParams.get("from") === "booking-success") {
        await dispatch(getDecks(booking.itinerary.id, cabin[0].detail.category_id, "staging"));
      }
    } else {
      await dispatch(getDecks(itineraryID, cabin[0].detail.category_id));
    }
    setIsLoading(false);
  }, []);

  const onSelectRoomClickHandler = (e) => {

    let rem = ''
    for (let i = 0; i < cabinList.length; i++) {
      rem += cabinList[i].name.split(" ").join("_") + '_'
      rem += 'A_' + cabinList[i].guest.adults + '_'
      rem += 'C_' + cabinList[i].guest.children + '_'
      rem += 'I_' + cabinList[i].guest.infants + '_'
      rem += 'deck_' + cabinList[i].deck.id
    }


    if (queryParams.get("from")) {
      if (queryParams.get("from") === "booking-success") {
        queryParams.get("platform") === "true" ?
          history.push(`/room-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&from=booking-success&platform=true`) :
          history.push(`/room-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&from=booking-success`)
      }
    } else {
      queryParams.get("platform") === "true" ?
        history.push(`/room-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}&platform=true`) :
        history.push(`/room-selection?sailingFrom=${queryParams.get("sailingFrom") ? queryParams.get("sailingFrom") : ''}&sailingTo=${queryParams.get("sailingTo") ? queryParams.get("sailingTo") : ''}&month=${queryParams.get("month")}&info=${rem}`)
    }
  };
  const getDefaultSelectedCabin = (cabin) => {
    setSelectedCabin(cabin)
  }
  const onSelectCabinHandler = (cabin) => {
    setSelectedCabin(cabin)
  };

  const onAddDeck = async (deck) => {
    setIsLoading(true);
    let payload = {
      selectedCabin: selectedCabin,
      deck: deck
    }
    await dispatch(addDeck(payload));
    setIsLoading(false);
  }

  const { cabins } = useSelector((state) => ({
    cabins: state.allcabins,
  }));

  const checkCabins = () => {
    let res = false;
    cabins.map((item) => {
      if (!item.hasOwnProperty('deck')) {
        res = true;
      }
    })
    return res;
  }

  return (
    <div className="Home">
      <Header bgPurple platform={queryParams.get("platform")} />
      <Container strict={true}>
        <BookingFlowCards.ProgressBar />
        <SelectedCabinList
          getSelectedCabin={getDefaultSelectedCabin}
          isCabinPage={false}
          deckNumbers={["03", "07"]}
          className="selectedCabinMB"
          onSelect={onSelectCabinHandler}
          setFocusCabinId={setFocusCabinId}
        />
        {isLoading ? (
          <Loader />
        ) : (
          <DeckSection addDeck={onAddDeck} pointer={pointer} setPointer={setPointer} cabins={cabinList} deckCollection={deckCollection} selectedCabin={selectedCabin} />
        )}
        <FooterButtonSection
          disabled={checkCabins()}
          text="Select Room"
          onClick={onSelectRoomClickHandler}
        />
      </Container>
      {/* <Footer /> */}
      {
        (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
      }
    </div>
  );
}

export default RoomSelection;
