import React from 'react'
import cx from "classnames"
import styles from "./contact-us.module.css"
import Header from "../../components/Header/HeaderHome"
import Container from "../../components/Container/index"
import Footer from "../../components/Footer/Footer"
import FooterButtonSection from "../../components/UI/FooterButtonSection"
import ReactHtmlParser from "react-html-parser"
import Slider from "react-slick"
import { Button } from "antd"
import ContactUsMobileBanner from "../../assets/img/contact-us/contact-us-mobile.webp"
import ContactUsWebBanner from "../../assets/img/contact-us/contact-us-web.webp"

const Contactus = () => {

    const queryParams = new URLSearchParams(window.location.search)
    let mapFrame = `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.3083763541827!2d72.81632481490009!3d19.00612838712808!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ce972240b6af%3A0xa3400548f629f168!2sContinental%20Building%2C%20B%20Wing%2C%20BDD%20Chawls%20Worli%2C%20Worli%2C%20Mumbai%2C%20Maharashtra%20400018!5e0!3m2!1sen!2sin!4v1612385624781!5m2!1sen!2sin" width="250" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>`;

    return (
        <div className={cx(styles.contactus)}>
            <Header bgPurple platform={queryParams.get("platform")} />
            <div className={cx(styles.contactBanner)}>
                <img className={cx(styles.contactBannerImage)} src={ContactUsWebBanner} />
            </div>

            <div className={cx(styles.contactMediumSection)}>
                {/* <div className=""> */}
                <div className={cx(styles.contactMedium)}>
                    <a
                        className={cx(styles.contactMediumRef)}
                        href={`tel:${process.env.NEXT_PUBLIC_PHONE_NUMBER}`}
                    >
                        <div
                            className={cx(styles.contactMediumRefDiv)}
                        >
                            <i className="fal fa-phone-alt"></i>
                        </div>
                        <div>
                            <h4 className={cx(styles.contactMediumRefTitle)}>Call</h4>
                            <p className="">
                                +912875592385
                                {process.env.NEXT_PUBLIC_PHONE_NUMBER}
                            </p>
                        </div>
                    </a>
                </div>
                <div className={cx(styles.contactMedium)}>
                    <a
                        className={cx(styles.contactMediumRef)}
                        href={`https://wa.me/${process.env.NEXT_PUBLIC_WHATSAPP_NUMBER}`}
                        target="_blank"
                        aria-label="Whatsapp"
                        rel="noreferrer"
                    >
                        <div
                            className={cx(styles.contactMediumRefDiv)}
                        >
                            <i className="fab fa-whatsapp"></i>
                        </div>
                        <div>
                            <h4 className={cx(styles.contactMediumRefTitle)}>WhatsApp</h4>
                            <p className="">
                                +912875592385
                                {process.env.NEXT_PUBLIC_WHATSAPP_NUMBER}
                            </p>
                        </div>
                    </a>
                </div>
                <div className={cx(styles.contactMedium)}>
                    <a
                        className={cx(styles.contactMediumRef)}
                        href={`mailto:${process.env.NEXT_PUBLIC_EMAIL_ADDRESS}`}
                        aria-label="Email"
                        rel="noreferrer"
                    >
                        <div
                            className={cx(styles.contactMediumRefDiv)}
                        >
                            <i className="fal fa-envelope"></i>
                        </div>
                        <div>
                            <h4 className={cx(styles.contactMediumRefTitle)}>Email</h4>
                            <p className="">
                                {process.env.NEXT_PUBLIC_EMAIL_ADDRESS}
                                info@cordeliacruises.com
                            </p>
                        </div>
                    </a>
                </div>
                <div className={cx(styles.contactMedium)}>
                    <a
                        className={cx(styles.contactMediumRef)}
                        href="#"
                        aria-label="Form"
                        rel="noreferrer"
                    >
                        <div
                            className={cx(styles.contactMediumRefDiv)}
                        >
                            <i className="fal fa-pen-nib"></i>
                        </div>
                        <div>
                            <h4 className={cx(styles.contactMediumRefTitle)}>Write to us</h4>
                            <p className="">
                                Use the web form below
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            {/* </div> */}

            <div className={cx(styles.writeAndHereSection)}>
                <div className={cx(styles.HereSection)}>
                    <h1 className={cx(styles.HereSectionTitle)}>We are Here</h1>
                    <div className={cx(styles.HereSectionMap)}>
                        {ReactHtmlParser(mapFrame)}
                    </div>
                    <p className={cx(styles.HereSectionAddress)}>
                        M/s. Waterways Leisure Tourism Pvt. Ltd. <br /> <br />
                        1st Floor, Continental Building,
                        <br /> <br />
                        135, Dr. Annie Besant Road,
                        <br /> <br />
                        Next to Doordarshan Building,
                        <br /> <br />
                        Worli, Mumbai 400018
                    </p>
                    <p className={cx(styles.HereSectionConnectText, "text-center")}>Connect with us and stay in the loop</p>
                    <div className={cx(styles.HereSectionConnect)}>
                        <a
                            className="cursor-pointer text-3xl"
                            href="https://www.linkedin.com/company/cordelia-cruises/mycompany/?viewAsMember=true"
                            target="_blank"
                        >
                            <i className="fab fa-linkedin"></i>
                        </a>
                        <a
                            href="https://www.facebook.com/cordeliacruises"
                            target="_blank"
                            className="cursor-pointer text-3xl"
                        >
                            <i className="fab fa-facebook-square"></i>
                        </a>
                        <a
                            href="https://www.instagram.com/cordeliacruises_india/?hl=en"
                            target="_blank"
                            className="cursor-pointer text-3xl"
                        >
                            <i className="fab fa-instagram-square"></i>
                        </a>
                        <a
                            href="https://twitter.com/CordeliaCruises"
                            target="_blank"
                            className="cursor-pointer text-3xl"
                        >
                            <i className="fab fa-twitter-square"></i>
                        </a>
                        <a
                            href="https://blog.cordeliacruises.com/"
                            target="_blank"
                            className="cursor-pointer text-3xl"
                            rel="noreferrer"
                        >
                            <i className="fab fa-blogger"></i>
                        </a>
                        <a
                            href="https://www.youtube.com/channel/UCIGZzyqWsbCH1-VNFsXrY9g"
                            target="_blank"
                            className="cursor-pointer text-3xl"
                        >
                            <i className="fab fa-youtube-square"></i>
                        </a>
                    </div>
                </div>
                <div className={cx(styles.WriteUsSection)}>
                    <h1 className={cx(styles.WriteUsSectionTitle)}>Write to us</h1>
                    <div className={cx(styles.contactUsForm)}>
                        <div>
                            <i className="fal fa-user"></i>
                            <input className={cx(styles.WriteUsSectionInput)} style={{ height: "42px", width: "100%" }} placeholder="Enter your name" />
                        </div>
                        <div className="mt-5">
                            <i className="fal fa-user"></i>
                            <input className={cx(styles.WriteUsSectionInput)} style={{ height: "42px", width: "100%" }} placeholder="Enter your name" />
                        </div>
                        <div className="mt-5">
                            <i className="fal fa-user"></i>
                            <input className={cx(styles.WriteUsSectionInput)} style={{ height: "42px", width: "100%" }} placeholder="Enter your name" />
                        </div>
                        <div className="mt-5">
                            <textarea className={cx(styles.WriteUsSectionInput)} style={{ height: "150px", width: "100%", borderRadius: "5px" }} placeholder="Write your message here">
                            </textarea>
                        </div>
                        <button className={cx(styles.contactUsFormButton)}>SEND</button>
                    </div>
                </div>
            </div>
            <FooterButtonSection />
            {/* <Footer /> */}
            {
                (queryParams.get("platform") && queryParams.get("platform") == "true" ? "" : <Footer />)
            }
        </div >
    )
}

export default Contactus
